<?php

return array(
		'default' => array(
				'model' => 'user',
				//Login providers
				'login' => array(
						'password' => array(
								'login_field' => 'Login',
								//Make sure that the corresponding field in the database
								//is at least 50 characters long
								'password_field' => 'Password'
						),
				),
				//Role driver configuration
				'roles' => array(
						'driver' => 'relation',
						'type' => 'has_many',
						//Field in the roles table
						//that holds the models name
						'name_field' => 'name',
						'relation' => 'roles'
				)
		)
);
<?php

return array(
    'su/partners' => array('/<controller>/<action>(/page-<page>)', array(
                    'controller' => 'Su',
                    'action' => 'partners',
                    'page' => 1
                    )
                ),
    'su/distribs' => array('/<controller>/<action>(/page-<page>)', array(
                    'controller' => 'su',
                    'action' => 'distribs',
                    'page' => 1
                    )
                ),
    'partner/distribs' => array('/<controller>/<action>(/page-<page>)', array(
                    'controller' => 'partner',
                    'action' => 'distribs',
                    'page' => 1
                    )
                ),
    'admin/distribs' => array('/<controller>/<action>(/page-<page>)', array(
                    'controller' => 'admin',
                    'action' => 'distribs',
                    'page' => 1
                    )
                ),
    'su/events' => array('/<controller>/<action>(/page-<page>)', array(
                    'controller' => 'su',
                    'action' => 'events',
                    'page' => 1
                    )
                ),
    'admin/distribshow' => array('/<controller>/<action>/<id>(/page-<page>)', array(
                    'controller' => 'admin',
                    'action' => 'distribshow',
                    'page' => 1
                    )
                ),
	'default' => array(
		'(/<controller>(/<action>(/<id>)))',
		array(
			'controller' => 'admin',
			'action' => 'distribs'
		),
	),
);

<div class="row ">
    <div class="col-xs-12 col-md-2 text-center"></div>
    <div class="col-xs-12 col-md-8">
        <div class="box-form-reg-all">
            <div class="step">
            <div class="row ">
                <div class="col-xs-12 col-md-12 text-center">
                    <div class="title-reg-l">
                        АНАЛИТИКА
                    </div>
                </div>
                <div class="col-xs-12 col-md-12 text-left">
                    <form method="POST" id="analyticsform" enctype="multipart/form-data">
                        <div class="vidrass">
                             <input id="day" type="radio" name="period" value="day" <?php echo($periodname=="day" ? "checked" : "");?>>
                             <label type="checkbox" for="day">День</label>
                             <input id="week" type="radio" name="period" value="week" <?php echo($periodname=="week" ? "checked" : "");?>>
                             <label type="checkbox" for="week">Неделя</label>
                             <input id="month" type="radio" name="period" value="month" <?php echo($periodname=="month" ? "checked" : "");?>>
                             <label type="checkbox" for="month">Месяц</label>
                        </div>
                        <input type="number" style="width:20%" class="field-form" name="percnt" placeholder="Кол-во периодов..." value="<?php $_($percnt);?>">
                        <br>
                        <br>
                        <button type="submit" class="btn-red" name="submitanbtn">Обновить</button>
                    </form>
                </div>
            </div> 
            </div>
        </div>
    </div>
</div>

<br>
<label>Кол-во регистраций</label>
<label><span style="color: red">Кол-во оплат</span></label>
<label><span style="color: green">Сумма оплат</span></label>
<label><span style="color: blue">Кол-во рассылок</span></label>
<table class="table">
    <tr><th>Источник</th>
    <?php for($i = 0; $i < $percnt; $i++):?>
    <th><?php $_($period[$i]);?></th>
    <?php endfor;?>
    </tr>
    <?php foreach($sources as $src):?>
        <tr>
            <td>
                <?php echo( '<a href="' . (explode("/", $_SERVER['REQUEST_URI'])[1]=="partner" ? '/partner/clients?src=' . $src : '/su/users?src=' . $src) . '">' .  ($src=="-" ? "Нет источника" : $src)  . '</a>');?>
            </td>
            <?php for($i = 0; $i < $percnt; $i++):?>
            <td>
                <table>
                <tr>
                <?php echo($regs[$i][$src] . ' <span style="color: red;"> ' . $paycnt[$i][$src] . ' </span> <span style="color: green";> ' . $paysum[$i][$src]/100 . ' </span> <span style="color: blue";> ' . $distribcnt[$i][$src] . ' </span> ');?>
                </tr>
                </table>
            </td>
            <?php endfor;?>
        </tr>
    <?php endforeach;?>
</table>
<legend>API голосовых обзвонов</legend>

<div class="entry-content">
<p>Звонки можно заказать с помощью нашего API. Запросы отправляются простыми post-запросами http.</p>
<h3>Обрабатываемые запросы</h3>
<p>1. Запрос на звонок</p>
<p><b>url:</b>http://<?= $_($linktolk); ?>/api/call</p>
<p><b>параметры:</b></p>
<table class="table table-bordered">
<tbody>
<tr>
<td style="text-align: center;" ><strong>Имя</strong></td>
<td style="text-align: center;" ><strong>Значение</strong></td>
<td style="text-align: center;" ><strong>Примечание</strong></td>
</tr>
<tr>
<td>uid</td>
<td>ваш API-KEY</td>
<td>API ключ нужно запросить у менеджера</td>
</tr>
<tr>
<td>record</td>
<td>id аудиозаписи</td>
<td>можно посмотреть в личном кабинете в разделе аудиозаписи</td>
</tr>
<tr>
<td>outnumber</td>
<td>номер, с которого поступает вызов</td>
<td>номер должен быть предварительно добавлен и подтвержден в личном кабинете</td>
</tr>
<tr>
<td>number</td>
<td>телефон, на который поступает вызов</td>
<td></td>
</tr>

<tr>
<td>ivr</td>
<td>1</td>
<td>включение голосового меню</td>
</tr>

<tr>
<td>ivr1</td>
<td>id аудиозаписи</td>
<td>аудиоролик, который проговаривается при нажатии цифры 1</td>
</tr>
<tr>
<td>ivr2</td>
<td>id аудиозаписи</td>
<td>аудиоролик, который проговаривается при нажатии цифры 2</td>
</tr>
<tr>
<td>ivr3</td>
<td>id аудиозаписи</td>
<td>аудиоролик, который проговаривается при нажатии цифры 3</td>
</tr>
<tr>
<td>blst</td>
<td>1</td>
<td>добавлять в черный список при нажатии цифры 3</td>
</tr>
<tr>
<td>tonumber</td>
<td>телефон, на который поступает вызов при нажатии 1</td>
<td></td>
</tr>
</tbody>
</table>
<p>Пример запроса на jquery:</p>
<code>
$.ajax({<Br>
    type: "POST",<Br>
    url: "<?= $_($linktolk); ?>/api/call",<Br>
    data: "uid=1dc81e21010171e712eXXXXXXX&record=1&ivr1=2&ivr2=3&ivr3=4&outnumber=79999999999&number=79888888880&tonumber=79999999999&ivr=1&blst=1",<Br>
    success: function(data){<Br>
        alert( data );<Br>
    }<Br>
});
</code>
<p></p>
<p>2. Запрос статуса звонка</p>
<p><b>url:</b>http://<?= $_($linktolk); ?>/api/status</p>
<p><b>параметры:</b></p>
<table  class="table table-bordered">
<tbody>
<tr>
<td style="text-align: center;" ><strong>Имя</strong></td>
<td style="text-align: center;" ><strong>Значение</strong></td>
<td style="text-align: center;" ><strong>Примечание</strong></td>
</tr>
<tr>
<td>uid</td>
<td>ваш API-KEY</td>
<td>API ключ нужно запросить у менеджера</td>
</tr>
<tr>
<td>call</td>
<td>id звонка</td>
<td></td>
</tr>
</tbody>
</table>
<p>Пример запроса на jquery:</p>
<code>
$.ajax({<Br>
    type: "POST",<Br>
    url: "/api/status",<Br>
    data: "uid=1dc81e21010171eXXXXXXXXX&call=1",<Br>
    success: function(data){<Br>
        alert( data );<Br>
    }<Br>
});
</code>

<p>3. Запрос баланса</p>
<p><b>url:</b>http://<?= $_($linktolk); ?>/api/balance</p>
<p><b>параметры:</b></p>
<table  class="table table-bordered">
<tbody>
<tr>
<td style="text-align: center;" ><strong>Имя</strong></td>
<td style="text-align: center;" ><strong>Значение</strong></td>
<td style="text-align: center;" ><strong>Примечание</strong></td>
</tr>
<tr>
<td>uid</td>
<td>ваш API-KEY</td>
<td>API ключ нужно запросить у менеджера</td>
</tr>
</tbody>
</table>
<p>Пример запроса на jquery:</p>
<code>
$.ajax({<Br>
    type: "POST",<Br>
    url: "/api/balance",<Br>
    data: "uid=1dc81e21010171eXXXXXXXXX",<Br>
    success: function(data){<Br>
        alert( data );<Br>
    }<Br>
});
</code>

<h3>Ответы</h3>
<p>1. Ошибки</p>
<p>Неправильный API-ключ: <code>"{'error' : 'wrong api key'}"</code></p>
<p>Пользователь с таким ключем не существует: <code>"{'error' : 'user is not exists'}"</code></p>
<p>Неправильный номер для звонка: <code>"{'error' : 'wrong number to call'}"</code></p>
<p>Неправильный номер звонящего:  <code>"{'error' : 'wrong outbound number'}"</code></p>
<p>Неправильный номер менеджера для переадресации:  <code>"{'error' : 'wrong manager number to call'}"</code></p>
<p>Неверный id аудиоролика приветствия:  <code>"{'error' : 'welcome record is not exists or not available'}"</code></p>
<p>Неверный id аудиоролика при нажатии цифры 1:  <code>"{'error' : 'ivr1 record is not exists or not available'}"</code></p>
<p>Неверный id аудиоролика при нажатии цифры 2:  <code>"{'error' : 'ivr2 record is not exists or not available'}"</code></p>
<p>Неверный id аудиоролика при нажатии цифры 3:  <code>"{'error' : 'ivr3 record is not exists or not available'}"</code></p>
<p>Неверный id звонка:  <code>"{'error' : 'call is not exists'}"</code></p>
<p>Достигнут лимит звонков, попробуйте позже:  <code>"{'error' : 'call limit reached for that number'}"</code></p>
<p>2. Ответ на запрос звонка</p>
<p><code>"{'id' : '<id звонка>', 'status' : 'accepted'}"</code></p>
<p>3. Ответ на запрос баланса</p>
<p><code>"{'amount' : '<сумма>' , 'cur' : '<валюта>'}"</code></p>
<p>4. Ответ на статусный запрос</p>
<p>Звонок обрабатывается: <code>"{'id' : '<id звонка>' , 'status' : 'Processing'}"</code></p>
<p>Звонок в процессе/закончен<code>"{'id' : '<id звонка>' , 'status' : '<текущий статус звонка>' , 'duration' : <длительность звонка>, 'cost' : <стоимость звонка в копейках>, 'starttime' : '<время начала звонка>', 'ivr':<какую цифру нажали>, 'manstatus':<статус звонка при нажатии 1>, 'manstarttime':<время начала звонка при нажатии 1>, 'durationman':<длительность звонка при нажатии 1>}"</code></p>

<h3>Статусы</h3>
<p>ABORT - звонок был отменен со стороны системы или оператора связи</p>
<p>ANSWER - ответил на звонок</p>
<p>BUSY - занято</p>
<p>CANCEL - звонок был отменен со стороны набираемого абонента</p>
<p>CHANUNAVAIL - оператор не пропустил данный звонок</p>
<p>CONGESTION - занят или недоступен</p>
<p>NOANSWER - не ответил на звонок</p>
<p>FINISHED - звонок закончен нормально</p>
<p>BEGIN - звонок начался</p>
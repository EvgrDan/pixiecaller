<script>
    function basecheck(baseid){;
        $('td[name="loading"]').each(function(){
            var $id = $(this).attr('id');
            $.ajax({
              type: "POST",
                  url: "/admin/basecheck",
                  data: "id="+$id,
                  success: function(data){
                    if($.trim(data)!="fail"){
                        $("#"+$id).text($.trim(data));
                        $("#"+$id).attr('name', '');;
                    }
              }
            });
        });
        
    }
</script>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="title-reg">
            НОВАЯ БАЗА
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-2 text-center"></div>
    <div class="col-xs-12 col-md-8 text-center">
        <div class="box-form-reg-all">
            <form method="POST" class="form-horizontal" id="newbase" enctype="multipart/form-data">
                <fieldset>
                    <div class="step-1 step">
                         <div class="row ">
                             <div class="col-xs-12 col-md-12 text-left">
                                 <div class="title-step">
                                     Название базы (используйте только латинские буквы без пробелов)
                                 </div>
                                 <input class="field-form" name="basename" type="text" placeholder="Введите название..." required/>
                             </div>
                         </div>
                     </div>
                    <div class="line"></div>
                    <div class="step-2 step">
                        <div class="row ">
                             <div class="col-xs-12 col-md-12 text-left">
                                <!--<div style="white-space: nowrap;">-->
                                <div class="title-step">
                                     Загрузить базу
                                 <a href="#" class="btnpodskazka" data-toggle="tooltip" data-placement="right otleft" title="" data-original-title='допустимый формат - xls, номера должны находиться в столбце "A" первого листа и начинаться с "7", в столбце должны содержаться только номера, больше в документе ничего не должно быть'>
                                 <i class="fa fa-question-circle" data-original-title="" title=""></i>
                                 </a>
                                 </div>
                                <input class="field-form" type="file" name="basefile"  accept="application/vnd.ms-excel" required>
                            </div>
                        </div>
                    </div>
                    <div class="line"></div>
                    <div class="step-3 step">
                        <div class="row ">
                            <div class="col-xs-12 col-md-12 text-left">
                                <div class="title-step">
                                     Комментарий (не более 300 символов)
                                 </div>
                                <textarea name="basecomment" class="field-form" rows="3"></textarea>
                                <button class="btn-form-small" type="submit" >Загрузить базу</button>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="simple-title">Загруженные базы</div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
    <div class="table-responsive">
        <table class="mailingtable table" cellpadding="5">
            <tr><th>Название</th><th>Кол-во номеров</th><th>Время создания(МСК)</th><th>Управление</th></tr>
            <?php foreach($bases as $base):?>
                <tr>
                    <td>
                        <?php $_($base->BaseName);?>
                    </td>
                        <?php if($base->Size!=""):?>
                        <td>
                        <?php $_($base->Size);?>
                        </td>   
                        <?php else:?>
                        <td name="loading" id="<?php $_($base->id);?>">
                        <i style="color:#4D4D4D" class="fa fa-1x fa-spinner fa-spin" aria-hidden="true"></i>
                        </td>
                        <?php endif;?>
                                <td>
                        <?php $_($base->CreateTime);?>
                    </td>
                    <td>
                        <a class="btn" href=<?php $_( "/admin/baseshow/" . (string)$base->id);?> title="Показать"><i style="color:#4D4D4D" class="fa fa-2x fa-info-circle"></i></a>
                        <a class="btn" title="Удалить" href=<?php $_( "/admin/baseremove/" . (string)$base->id);?>><i style="color:#4D4D4D" class="fa fa-2x fa-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach;?>
        </table>
    </div>
    </div>
</div>

<script>
    $(document).ready(function(){ 
            basecheck();  
            setInterval('basecheck()',1000);
    });
</script>

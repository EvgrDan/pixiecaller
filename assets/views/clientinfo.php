<div class="row ">
    <div class="col-xs-12 col-md-2 text-center"></div>
    <div class="col-xs-12 col-md-8">
        <div class="box-form-reg-all">
            <div class="step">
            <div class="row ">
                <div class="col-xs-12 col-md-12 text-center">
                    <div class="title-reg-l">
                        ИНФОРМАЦИЯ О КЛИЕНТЕ <?php $_($user->Login);?> <i style="color:<?php $_($colors[$user->ClientStatus]);?>" class="fa fa-1x  fa-circle"></i>
                    </div>
                </div>
            </div> 
            </div>
            <div class="step-1 step">
                <div class="row ">
                    <div class="col-xs-12 col-md-12 text-left">
                        <div class="title-step">
                            Общая информация
                        </div>
                        <div>Имя:</div><input class="field-form" id="username" name="username" type="text" value = "<?php $_($user->Login);?>"/>
                        <div>Телефон:</div><input class="field-form" type="text" value = "<?php $_($user->UserPhone);?>" readonly/>
                        <div>Информация:</div>
                        <textarea class="field-form" name="userinfo" rows="3" value = "<?php $_($userinfo);?>"><?php $_($userinfo);?></textarea>
                        <div>Партнер: <?php $_($partner->Login);?></div>
                        <div>Баланс: <?php $_($user->balance/100);?> руб.</div>
                        <div>Рассылок: <?php $_($distrs);?></div>
                        <?php if($refmail!=""): ?>
                        <div>Реферал: <a href="/<?php $_($ty);?>/clientinfo/<?php $_($user->RefID);?>"><?php $_($refmail);?></a></div>
                        <?php endif; ?>
                        <div>Дата регистрации: <?php $_($regdate);?></div>
                    </div>
                </div>
            </div>
            <div class="step-2 step">
                <div class="row ">
                    <div class="col-xs-12 col-md-12 text-left">
                        <div class="title-step">Тариф</div>
                        <div>Перезвон: <?php $_($cost->recall/100);?> руб/мин.</div>
                        <div>Обычная: <?php $_($cost->ordinary/100);?> руб/мин.</div>
                        <div>Дозвон: <?php $_($cost->dial/100);?> руб/мин.</div>
                    </div>
                </div>
            </div>
            <div class="step-3 step">
                <div class="row ">
                    <div class="col-xs-12 col-md-12 text-left">
                        <div class="title-step">UTM-метки</div>
                        <div>source:</div><input class="field-form" id="utmsource"  name="utmsource" type="text" value = "<?php $_($usource);?>" <?php echo($currole=="Manager" ? "readonly" : "")?>/>
                        <div>campaign:</div><input class="field-form" id="utmcampaign" name="utmcampaign" type="text" value = "<?php $_($ucampaign);?>" <?php echo($currole=="Manager" ? "readonly" : "")?>/>
                        <div>term:</div><input class="field-form" id="utmterm" name="utmterm" type="text" value = "<?php $_($uterm);?>" <?php echo($currole=="Manager" ? "readonly" : "")?>/>
                    </div>
                </div>
            </div>
            <div class="step-4 step">
                <div class="row ">
                    <div class="col-xs-12 col-md-12 text-left">
                        <div class="title-step">Дополнительно</div>
                        <input type="checkbox" name="chkemail" id="chkemail" <?php $_($checkedemail);?>>
                        <label type="checkbox" for="chkemail" style="margin-top: 20px;" class="checkbox" name="chkemaillbl">
                            Получать уведомления по электронной почте
                        </label>
                        <input type="checkbox" name="chkcall" id="chkcall" <?php $_($checkedcall);?>> 
                        <label type="checkbox" for="chkcall" style="margin-top: 20px;" class="checkbox" name="chkcalllbl">
                           Получать уведомления по телефону
                        </label>
                        <?php if($ty=="su"):?>
                        <div>Максимум рассылок:</div><input class="field-form" id="distribcount"  name="distribcount" type="number" value = "<?php $_($user->DistrCount);?>"/>
                        <div>Максимальная скорость:</div>
                        <div class="field-form-sel-box">
                        <select  class="field-form-sel" id="maxspeed" name="maxspeed">
                            <?php foreach($speeds as $speed):?>
                                <?php if($speed->id == $user->MaxSpeed):?>
                                    <option value="<?php $_($speed->id);?>" selected>
                                        <?php $_($speed->Value);?> в час
                                    </option>
                                <?php else: ?>
                                    <option value="<?php $_($speed->id);?>">
                                        <?php $_($speed->Value);?> в час
                                    </option>
                                <?php endif;?>
                            <?php endforeach;?>
                        </select>
                        </div>
                        <?php endif;?>
                        <label class="btn-form-big-red" name="refreshbtn" onclick="refresh(<?php $_($user->id);?>)">Обновить</label>
                        <label class="btn-form-big-grey" name="respass" onclick="show('block')">Восстановить пароль</label>
                        <?php if($user->UserType=="User" and $this->pixie->auth->user()->UserType!="Manager"):?>
                        <label class="btn-form-big-grey" name="transferbtn" onclick="<?php echo(($refmail!="" and $refmail2!="")?"":"show1('block')"); ?>" >Передать другому партнеру <?php echo(($refmail!="" and $refmail2!="")?"(есть реферал!)":""); ?></label>
                        <?php endif;?>
                        <?php if($user->UserType=="User"):?>
                        <label class="btn-form-big-grey" name="transfemanbtn" onclick="show2('block')">Передать другому менеджеру</label>
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <?php if($ty=="su"):?>
            <div class="step-5 step">
                <div class="row">
                    <div class="col-xs-12 col-md-12 text-left">
                        <div class="title-step">API ключ:</div>
                        <input class="field-form" id="apikey" name="apikey" type="text" value = "<?php $_($apikey);?>"/>
                        <label class="btn-form-big-grey" name="apibutton" onclick="refreshapi(<?php $_($user->id);?>)">Сгенерировать</label>
                        
                    </div>
                </div>
            </div>
            <?php endif;?>
            <?php if($user->UserType=="Partner"):?>
            <div class="line"></div>
            <div class="step-6 step">
                <div class="row ">
                    <div class="col-xs-12 col-md-12 text-left">
                        <div class="title-step">
                             Настройки партнерской программы
                        </div>
                                <!--<label><b>Имя</b></label>
                                <input name="name" type="text" placeholder="Иван Иваныч..." value = "<?php echo($contact!="None" ? $contact->UserName : ""); ?>" required/>-->
                                <div class="num-golos num-golos-1">
                                    <div class="title-step">Контакты</div>
                                    <div>Телефон</div>
                                    <input class="field-form" name="phone" type="text" placeholder="7XXXXXXXXXX" value = "<?php echo($contact!="None" ? $contact->UserPhone : ""); ?>" required/>
                                    <div>Email</div>
                                    <input class="field-form" name="email" type="text" placeholder="sample@example.org" value = "<?php echo($contact!="None" ? $contact->UserEmail : "");?>" required/>
                                </div>
                                <div style="margin-top:20px" class="line"></div>
                                <div class="num-golos num-golos-2">
                                    <div class="title-step">Аудиоролик для приветствия</div>
                                    <input type="checkbox" id="welcomecall" name="welcomecall" <?php echo($check ? "checked" : "");?>> 
                                    <label type="checkbox" for="welcomecall" class="checkbox" name="lblwelcomecall">
                                       Не совершать звонок с приветствием
                                    </label>
                                    <input type="file" name="welcome1" accept="audio/wav,.mp3">
                                    <audio style="margin-top: 20px;" controls="controls" id="welcome1audioplayer">
                                      Your browser does not support the <code>audio</code> element.
                                      <source id="welcomeaudio" src="<?php echo($wAudio); ?>" type="audio/wav">
                                    </audio>
                                </div>
                                <div style="margin-top:20px" class="line"></div>
                                <div class="num-golos num-golos-3">
                                    <div class="title-step">Аудиоролик для тестовой рассылки</div>
                                    <input type="file" name="test" accept="audio/wav,.mp3">
                                    <audio style="margin-top: 20px;" controls="controls" id="test1audioplayer">
                                      Your browser does not support the <code>audio</code> element.
                                      <source id="testaudio" src="<?php echo($tAudio); ?>" type="audio/wav">
                                    </audio>
                                </div>
                                <div  style="margin-top:20px" class="line"></div>
                                <div class="num-golos num-golos-4">
                                    <div class="title-step">Логотип партнерки</div>
                                    <?php if($this->pixie->orm->get('domain')->where('name', 'like', '%' . $_SERVER['SERVER_NAME'])->find()->loaded()): ?>
                                        <img src="/assets/img/<?php echo("partnerlogo" . $this->pixie->orm->get('domain')->where('name', 'like', '%' . $_SERVER['SERVER_NAME'])->find()->UserID); ?>" alt="" width="42" height="42">
                                    <?php endif ?>
                                    <input type="file" name="piclogo" accept="image/*">
                                </div>
                                <div  style="margin-top:20px" class="line"></div>
                                <div class="num-golos num-golos-4">
                                    <div class="title-step">Фон партнерки</div>
                                    <?php if($this->pixie->orm->get('domain')->where('name', 'like', '%' . $_SERVER['SERVER_NAME'])->find()->loaded()): ?>
                                        <img src="/assets/img/<?php echo("partnerback" . $this->pixie->orm->get('domain')->where('name', 'like', '%' . $_SERVER['SERVER_NAME'])->find()->UserID); ?>" alt="" width="100" height="100">
                                    <?php endif ?>
                                    <input type="file" name="picback" accept="image/*">
                                </div>
                                <div  style="margin-top:20px" class="line"></div>
                                <div class="num-golos num-golos-4">
                                    <div class="title-step">Основной цвет</div>
                                    <input type="color" id="html5colorpicker" onchange="clickColor(0, -1, -1, 5)" name="maincolor" value="<?php echo($contact->maincolor);?>" style="width:85%;">
                                </div>
                                <div  style="margin-top:20px" class="line"></div>
                                <div class="num-golos num-golos-5">
                                    <div class="title-step">Реферальная программа</div>
                                    <input type="checkbox" name="ref" id="ref" <?php echo($contact->refon==1 ? "checked" : "");?>>
                                    <label type="checkbox"  for="ref" class="checkbox" name="lblref"> Включена
                                    </label>
                                    <div style="margin-top:10px; margin-bottom:10px">Процент от первого уровня</div>
                                    <input style="width:20%" class="field-form" name="percent1" type="text" value = "<?php echo($contact!="None" ? $contact->percent1 : "");?>" required/>
                                    <div style="margin-top:10px; margin-bottom:10px">Процент от второго уровня</div>
                                    <input style="width:20%" class="field-form" name="percent2" type="text" value = "<?php echo($contact!="None" ? $contact->percent2 : "");?>" required/>
                                </div>
                                <div  style="margin-top:20px" class="line"></div>
                                <div class="num-golos num-golos-6">
                                    <div class="title-step">Дополнительный функционал</div>
                                    <input type="checkbox" id="hlr" name="hlr" <?php echo($contact->hlron==1 ? "checked" : "");?>>
                                    <label type="checkbox" for="hlr" class="checkbox" name="lblhlr">
                                        Лайт-HLR
                                    </label>
                                    <!--<div style="white-space: nowrap;">-->
                                    <input type="checkbox" id="promomail" name="promomail" <?php echo($contact->promomailon==1 ? "checked" : "");?>>
                                    <label type="checkbox" for="promomail" class="checkbox" name="lblpromomail">
                                        Письма и смс с промокодами
                                    </label>
                                    <!--<i name="promomailquest" style="display: inline-block;" class="icon-question-sign" data-toggle="tooltip" data-placement="right" title="Если 25 дней Ваш клиент не заходил в админку, ему приходит письмо и смс с промокодом. Если клиент введет промокод, ему зачислится промо-платеж 20 рублей, если клиент решит их потратить - часть этой суммы будет списана с Вашего счета."></i>
                                    </div>-->
                                    <!--<div style="white-space: nowrap;">-->
                                    <input type="checkbox" name="casemail" id="casemail" <?php echo($contact->casemailon==1 ? "checked" : "");?>>
                                    <label type="checkbox" for="casemail" class="checkbox" name="lblcasemail">
                                        Письма с вариантами использования
                                    </label>
                                    <!--<i name="casemailquest" style="display: inline-block;" class="icon-question-sign" data-toggle="tooltip" data-placement="right" title="На четвертый день после регистрации Вашему клиенту придет письмо с кейсами (реальными примерами применения 'Звонка-сброса' и 'Голосовой рассылки'). Внимание! Расчеты в кейсе основаны на том, что звонок-сброс стоит 10 копеек, а минута обычного звонка 2,4 рубля."></i>
                                    </div>-->
                                    <input type="checkbox" name="newson" id="newson" <?php echo($contact->newson==1 ? "checked" : "");?>>
                                    <label type="checkbox" for="newson" class="checkbox" name="lblnews">
                                        Показывать новости
                                    </label>
                                    <input type="checkbox" name="notifyon" id="notifyon" <?php echo($contact->notifyon==1 ? "checked" : "");?>>
                                    <label type="checkbox" for="notifyon" class="checkbox" name="lblnotify">
                                        Показывать подсказки
                                    </label>
                                    <div><b>Реквизиты</b>(будут отображаться по нажатию на кнопку оплатить)</div>
                                    <textarea class="field-form" id="reqtext" name="requisites" class="form-control" rows="5"><?php echo($contact->requisites);?></textarea>
                                </div>
                    </div>
                </div>
            </div>
            <?php endif;?>
                        <div class="title-step">События</div>
                        <input class="field-form" id="comment" name="comment" type="text" placeholder="Коммент..."/>
                        <label class="btn-grey" name="commentbtn" onclick="addcomment(<?php $_($user->id);?>)">Добавить коммент</label>
                        <form method="POST" class="form-horizontal" id="eventfilter1" enctype="multipart/form-data">
                            <fieldset>
                                <input id="eventtype" class="field-form" name="eventtype" type="text" placeholder="Тип..."/>
                                <button type="submit" class="btn-red" name="cfilterbtn">Фильтр</button>
                            </fieldset>
                        </form>
                        <table class="table">
                            <tr><th>Время</th><th>Тип</th><th>Субьект</th><th>Обьект</th><th>Данные</th></tr>
                            <?php foreach($events as $event):?>
                                <tr>
                                    <td>
                                        <?php $_($event[0]);?>
                                    </td>
                                    <td>
                                        <?php $_($event[1]);?>
                                    </td>
                                    <td>
                                        <?php $_($event[2]);?>
                                    </td>
                                    <td>
                                        <?php $_($event[3]);?>
                                    </td>
                                    <td>
                                        <?php $_($event[4]);?>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </table>
        </div>  
    </div>
</div>
            
<div onclick="show1('none')" id="wrap1" style="display: none;"></div>
<div id="window1" style="display: none;">
    <center>
        <legend>Передача клиента</legend>
        <form method="POST" class="form-horizontal" id="transferto" enctype="multipart/form-data">
            <fieldset>
                <input class="field-form" id="newpartner" name="newpartner" type="text"/>
                <div id="transfer" onclick="transferto('<?php $_($user->id);?>')" style="cursor: pointer;"  class="btn-form-big-red ">Передать</div>
            </fieldset>
        </form>
    </center>
</div>

<div onclick="show2('none')" id="wrap2" style="display: none;"></div>
<div id="window2" style="display: none;">
    <center>
        <legend>Передача клиента</legend>
        <form method="POST" class="form-horizontal" id="transfertoman" enctype="multipart/form-data">
            <fieldset>
                <div class="field-form-sel-box">
                <select  class="field-form-sel" id="newmanager" name="newmanager">
                    <?php foreach($managers as $manager1):?>
                            <option value="<?php $_($manager1->id);?>">
                                <?php $_($manager1->Login);?> 
                            </option>
                    <?php endforeach;?>
                </select>
                </div>
                <div id="transferman" onclick="transfertomanager('<?php $_($user->id);?>')" style="cursor: pointer;"  class="btn-form-big-red ">Передать</div>
            </fieldset>
        </form>
    </center>
</div>

<div onclick="show('none')" id="wrap" style="display: none;"></div>
<div id="window" style="display: none;">
    <center>
        <legend>Смена пароля пользователя</legend>
        <form method="POST" class="form-horizontal" id="changepass" enctype="multipart/form-data">
            <fieldset>
                <input class="field-form" id="newpass" name="newpass" type="password" placeholder="Новый пароль..."/>
                <div id="changeandrestore" onclick="newpass('<?php $_($user->Login);?>')"  class="btn-form-big-red ">Сменить и отправить на почту</div>
                <div id="restore" onclick="restorepass('<?php $_($user->Login);?>')" class="btn-form-big-grey ">Сгенерировать и отправить на почту</div>
            </fieldset>
        </form>
    </center>
</div>

<style type="text/css">
	#wrap{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
	
	#window{
		width: 400px;
		height: 300px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
	
    #wrap1{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
    
    #window1{
		width: 400px;
		height: 300px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
    
    #wrap2{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
    
    #window2{
		width: 400px;
		height: 300px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
    
    .close{
		margin-left: 364px;
		margin-top: 4px;
		cursor: pointer;
	}
	
</style>

<script>
    
    function addcomment(userid){;
        $.ajax({
          type: "POST",
          url: "/<?php $_($ty);?>/addcomment",
          data: "userid="+userid+"&text="+$('input[name="comment"]').val(),
          success: function(data){
            swal( data );
            window.location.replace("/<?php $_($ty);?>/clientinfo/"+userid);
          }
        });
    }
    
    function refresh(userid){;
        $.ajax({
          type: "POST",
          url: "/<?php $_($ty);?>/editinfo",
          <?php if($ty=="su"):?>
          data: "userid="+userid+"&username="+$('input[name="username"]').val()+"&utmsource="+$('input[name="utmsource"]').val()+"&utmcampaign="+$('input[name="utmcampaign"]').val()+"&utmterm="+$('input[name="utmterm"]').val()+"&userinfo="+$('textarea[name="userinfo"]').val()+"&chkemail="+$('input[name="chkemail"]').is(':checked')+"&chkcall="+$('input[name="chkcall"]').is(':checked')+"&maxspeed="+$('#maxspeed option:selected').val()+"&distribcount="+$('input[name="distribcount"]').val(),
          <?php else:?>
          data: "userid="+userid+"&username="+$('input[name="username"]').val()+"&utmsource="+$('input[name="utmsource"]').val()+"&utmcampaign="+$('input[name="utmcampaign"]').val()+"&utmterm="+$('input[name="utmterm"]').val()+"&userinfo="+$('textarea[name="userinfo"]').val()+"&chkemail="+$('input[name="chkemail"]').is(':checked')+"&chkcall="+$('input[name="chkcall"]').is(':checked'),
          <?php endif;?>
          success: function(data){
            swal( data );
            window.location.replace("/<?php $_($ty);?>/clientinfo/"+userid);
          }
        });
    }
    
    function refreshapi(userid){;
        $.ajax({
          type: "POST",
          url: "/<?php $_($ty);?>/editkey",
          data: "userid="+userid,
          success: function(data){
            $('input[name="apikey"]').val(data);
          }
        });
    }
    
    function show(state){;
        document.getElementById('window').style.display = state;            
        document.getElementById('wrap').style.display = state;
    }
    
    function show1(state){;
        document.getElementById('window1').style.display = state;            
        document.getElementById('wrap1').style.display = state;
    }
    
    function show2(state){;
        document.getElementById('window2').style.display = state;            
        document.getElementById('wrap2').style.display = state;
    }
    
    function restorepass(login){;
        $.ajax({
          type: "POST",
          url: "/admin/forgetpassword",
          data: "Login="+login,
          success: function(data){
            swal( data );
            show('none');
          }
        });
    }
    
    function newpass(login){;
        $.ajax({
          type: "POST",
          url: "/admin/restorepassword",
          data: "Login="+login+"&Password="+$('#newpass').val(),
          success: function(data){
            swal( data );
            show('none');
          }
        });
    }
    
    function transferto(id){;
        $.ajax({
          type: "POST",
          url: "/partner/transferto",
          data: "User="+id+"&Partner="+$('#newpartner').val(),
          success: function(data){
            swal( data );
            show1('none');
          }
        });
    }
    
    function transfertomanager(id){;
        $.ajax({
          type: "POST",
          url: "/partner/transfertomanager",
          data: "User="+id+"&Manager="+$('#newmanager').val(),
          success: function(data){
            swal( data );
            show1('none');
          }
        });
    }
     
</script>
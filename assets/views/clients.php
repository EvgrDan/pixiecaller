<?php if($currole=="Partner"): ?>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="title-reg">
            ТАРИФ ПО УМОЛЧАНИЮ
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-2 text-center"></div>
    <div class="col-xs-12 col-md-8 text-center">
        <div class="box-form-reg-all">
            <form name="changedefault" id="changedefault" method="POST">
                <fieldset>
                    <div class="row ">
                    <div class="col-xs-12 col-md-12 text-center">
                    <div class="table-responsive">
                        <table class="mailingtable table">
                            <tr><th>Перезвон(коп.)</th><th>Обычная(коп.)</th><th>Дозвон(коп.)</th></tr>
                            <td><input type="number" class="input-small" name="recall" placeholder="Перезвон..." value="<?php $_((string)$defaultcosts->recall);?>"></td>
                            <td><input type="number" class="input-small" name="ordinary" placeholder="Обычная..." value="<?php $_((string)$defaultcosts->ordinary);?>"></td>
                            <td><input type="number" class="input-small" name="dial" placeholder="Дозвон..." value="<?php $_((string)$defaultcosts->dial);?>"></td>
                        </table>
                    </div>
                    </div>
                    </div>
                    <div class="btn-form-big-red" name="subdefault" style="cursor: pointer;">Сохранить</div>
                </fieldset>
            </form>
            
        </div>
    </div>
</div>
<div class="line"></div>
<?php endif; ?>
<div class="row ">
<div class="col-xs-12 col-md-2 text-center"></div>
<div class="col-xs-12 col-md-8 text-center" >
    <div class="box-form-reg-all">
        
            <div class="col-xs-12 col-md-12 text-center">
                <div class="title-reg">
                    КЛИЕНТЫ
                </div>
            </div>
            <div class="col-xs-12 col-md-12">
                <form method="POST" id="clientfilter" enctype="multipart/form-data">
                    <fieldset>
                        <div style="float:left" class="title-step">Активность</div>
                        <div class="field-form-sel-box" id="seldiv">
                            <select class="field-form-sel" id="clienttype" form="clientfilter" name="clienttype">
                            <option value="-1" <?php echo ($cltype==-1 ? "selected" : "");?>>
                                Все
                            </option>
                            <option value="4" <?php echo ($cltype==4 ? "selected" : "");?>>
                                Свежие
                            </option>
                            <option value="1" <?php echo ($cltype==1 ? "selected" : "");?>>
                                Активные
                            </option>
                            <option value="2" <?php echo ($cltype==2 ? "selected" : "");?>>
                                Неактивные
                            </option>
                            <option value="3" <?php echo ($cltype==3 ? "selected" : "");?>>
                                Мертвые
                            </option>
                            </select>
                        </div>
                        <div style="float:left" class="title-step">Менеджер</div>
                        <div class="field-form-sel-box" id="seldiv1">
                            <select class="field-form-sel" id="managerfilter" form="clientfilter" name="managerfilter">
                            <option value="-1" <?php echo ($managerf==-1 ? "selected" : "");?>>
                                Все
                            </option>
                            <?php foreach($managers as $manager):?>
                            <option value="<?php echo ($manager->id);?>" <?php echo ($managerf==$manager->id ? "selected" : "");?>>
                                <?php echo ($manager->Login);?>
                            </option>
                            <?php endforeach;?>
                            </select>
                        </div>
                        <div class="btn-red" name="clfilterbtn" style="cursor: pointer;">Фильтр</div>
                    </fieldset>
                </form>
            </div>
        
    </div>
</div>
</div>

<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <a href="/partner/loadinfo" id="loadinfo"><div class="btn-form-big-grey" style="cursor: pointer;">Выгрузить в excel</div></a>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="table-responsive">
            <table class="mailingtable table" cellpadding="5">
                <tr><th>Email</th><th>Телефон</th><th>Тариф</th><th>Баланс</th><th>Рассылок</th><th>Управление</th></tr>
                <?php foreach($users as $user):?>
                    <tr>
                        <td>
                            <i style="color:<?php $_($colors[$user->ClientStatus]);?>" class="fa fa-1x  fa-circle"></i> 
                            <?php echo ($user->Manager==$this->pixie->auth->user()->id ? "<b>" : "");?>
                            <?php $_($user->Login);?>
                            <?php echo ($user->Manager==$this->pixie->auth->user()->id ? "</b>" : "");?>
                        </td>
                        <td>
                            <?php echo ($user->Manager==$this->pixie->auth->user()->id ? "<b>" : "");?>
                            <?php $_($user->UserPhone);?>
                            <?php echo ($user->Manager==$this->pixie->auth->user()->id ? "</b>" : "");?>
                        </td>
                        <td>
                            <?php echo ($user->Manager==$this->pixie->auth->user()->id ? "<b>" : "");?>
                            <?php $_($costs[$user->id]);?>
                            <?php echo ($user->Manager==$this->pixie->auth->user()->id ? "</b>" : "");?>
                        </td>
                        <td>
                            <?php echo ($user->Manager==$this->pixie->auth->user()->id ? "<b>" : "");?>
                            <?php $_($user->balance/100);?> руб.
                            <?php echo ($user->Manager==$this->pixie->auth->user()->id ? "</b>" : "");?>
                        </td>
                        <td>
                            <?php echo ($user->Manager==$this->pixie->auth->user()->id ? "<b>" : "");?>
                            <?php $_($distrs[$user->id]);?>
                            <?php echo ($user->Manager==$this->pixie->auth->user()->id ? "</b>" : "");?>
                        </td>
                        <td>
                            <?php if($currole=="Partner"): ?>
                            <a class="btn" onclick="show('block', <?php $_($user->id);?>, '<?php $_($user->Login);?>')" title="Пополнить"><i style="color:#4D4D4D" class="fa fa-2x  fa-plus-circle"></i></a>
                            <a class="btn" onclick="show1('block', <?php $_($user->id);?>, '<?php $_($user->Login);?>', '<?php $_($costs[$user->id]);?>')" title="Изменить тариф"><i style="color:#4D4D4D" class="fa fa-2x  fa-wrench"></i></a>
                            <!--<a class="btn" onclick="setdefault('<?php $_($user->id);?>')" title="Задать по-умолчанию"><i style="color:#4D4D4D" class="fa fa-2x  fa-undo"></i></a>-->
                            <?php endif; ?>
                            <a class="btn" href=<?php $_( "/partner/clientinfo/" . (string)$user->id);?> title="Подробно"><i style="color:#4D4D4D" class="fa fa-2x  fa-info-circle"></i></a>
                        </td>
                    </tr>
                <?php endforeach;?>
            </table>
        </div>
    </div>
</div>
<div onclick="show('none')" id="wrap" style="display: none;"></div>
<div id="window" style="display: none;">
    <center>
        <form method="POST" class="form-horizontal" id="changebal" enctype="multipart/form-data">
            <fieldset>
                <legend id="nazv"></legend>
                <div>
                    <div style="width:30%; display:inline-block;">
                    <input class="field-form" name="summ" type="number" value = "0" required/>
                    </div> руб.
                    <div style="width:40%; display:inline-block;">
                        <select class="field-form" id="seltype" name="seltype">
                        <?php foreach($types as $type):?>
                            <option value="<?php $_($type->name);?>">
                                <?php $_($type->description);?>
                            </option>
                        <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <div id="subpay"  class="btn-red ">Зачислить</div>
            </fieldset>
        </form>
        <legend id="histleg">История платежей (последние 4)</legend>
        <div class="row ">
            <div class="col-xs-12 col-md-12 text-center">
            <div class="table-responsive">
                <table class="mailingtable table" id = "histtable">
                <tr><th>Дата</th><th>Тип платежа</th><th>Сумма</th></tr>
                
                </table>
            </div>
            </div>
        </div>
    </center>
</div>

<div onclick="show1('none')" id="wrap1" style="display: none;"></div>
<div id="window1" style="display: none;">
    <center>
        <form class="form-inline" id="changetar" method="POST">
            <fieldset>
                <legend id="nazv1"></legend>
                <div class="row ">
                    <div class="col-xs-12 col-md-12 text-center">
                    <div class="table-responsive">
                        <table class="mailingtable table">
                            <tr><th>Перезвон(руб.)</th><th>Обычная(руб.)</th><th>Дозвон(руб.)</th></tr>
                            <td><input type="number" class="input-small" name="recall1" id="recall1" placeholder="Перезвон..."></td>
                            <td><input type="number" class="input-small" name="ordinary1" id="ordinary1" placeholder="Обычная..."></td>
                            <td><input type="number" class="input-small" name="dial1" id="dial1" placeholder="Дозвон..."></td>
                        </table>
                    </div>
                    </div>
                </div>
                <label id="subtariff" class="btn-red ">Изменить тариф</label>
            </fieldset>
        </form>
    </center>
</div>
<style type="text/css">
	#wrap{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
	
	#window{
		width: 400px;
		height: 700px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
	
    #wrap1{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
	
	#window1{
		width: 800px;
		height: 300px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
    
	.close{
		margin-left: 364px;
		margin-top: 4px;
		cursor: pointer;
	}
	
</style>
<script type="text/javascript">
 
            //Функция показа
    function show(state, userid, useremail){
        var table = document.getElementById("histtable");
        for (i=table.tBodies[0].rows.length-1; i>0; i--) {table.tBodies[0].deleteRow(i);}
        $.ajax({
          type: "POST",
          url: "/partner/gethistory",
          data: "userid="+userid,
          success: function(data){
            if(data.trim()!=''){
                data.split(";").forEach(function(item, i, arr) {
                  arr = item.split(",");
                  var newRow=table.insertRow(i+1);
                  var newCell = newRow.insertCell(0);
                  newCell.innerHTML=arr[0];
                  var newCell = newRow.insertCell(1);
                  newCell.innerHTML=arr[1];
                  var newCell = newRow.insertCell(2);
                  newCell.innerHTML=arr[2]/100 + " руб.";
                });
            }
          }
        });
        document.getElementById('window').style.display = state;            
        document.getElementById('wrap').style.display = state;
        document.getElementById('nazv').innerHTML = "Изменение баланса пользователя "+useremail;  
        document.getElementById('subpay').setAttribute("onclick", "changebalance('"+userid+"')");
    }
    
    function show1(state, userid, useremail, costs){
        if(costs!=null){
            var elem = document.getElementById("recall1");
            elem.value = costs.split("::")[0];
            var elem = document.getElementById("dial1");
            elem.value = costs.split("::")[2];
            var elem = document.getElementById("ordinary1");
            elem.value = costs.split("::")[1];
        }
        document.getElementById('window1').style.display = state;            
        document.getElementById('wrap1').style.display = state;
        document.getElementById('nazv1').innerHTML = "Изменение тарифа пользователя "+useremail;  
        document.getElementById('subtariff').setAttribute("onclick", "changetariff('"+userid+"')");
    }
    
    function changebalance(userid){;
        $.ajax({
          type: "POST",
          url: "/partner/changebalance",
          data: "userid="+userid+"&sum="+$('input[name="summ"]').val()+"&type="+$('select[name="seltype"]').val(),
          success: function(data){
            swal( data );
            show('none');
            window.location.replace("/partner/clients");
          }
        });
    }
    
    function changetariff(userid){;
        $.ajax({
          type: "POST",
          url: "/partner/changetariff",
          data: "userid="+userid+"&recall="+$('input[name="recall1"]').val() * 100 +"&ordinary="+$('input[name="ordinary1"]').val() * 100 +"&dial="+$('input[name="dial1"]').val() * 100,
          success: function(data){
            swal( data );
            show1('none');
            window.location.replace("/partner/clients");
          }
        });
    }
    
    function setdefault(userid){;
        $.ajax({
          type: "POST",
          url: "/partner/setdefault",
          data: "userid="+userid,
          success: function(data){
            swal( data );
            window.location.replace("/partner/clients");
          }
        });
    }
     
</script>
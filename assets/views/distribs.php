<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="title-reg">
            РАССЫЛКИ <?php if($control=="su"):?> (<?php echo $speedsum;?>) <?php endif?>
        </div>
        <?php if($control=="partner"):?>
        <div class="col-xs-12 col-md-12">
            <form method="POST" id="distribfilter" enctype="multipart/form-data">
                <fieldset>
                    <div style="float:left" class="title-step">Менеджер</div>
                    <div class="field-form-sel-box" id="seldiv1">
                        <select class="field-form-sel" id="managerfilter" form="distribfilter" name="managerfilter">
                        <option value="-1" <?php echo ($managerf==-1 ? "selected" : "");?>>
                            Все
                        </option>
                        <?php foreach($managers as $manager):?>
                        <option value="<?php echo ($manager->id);?>" <?php echo ($managerf==$manager->id ? "selected" : "");?>>
                            <?php echo ($manager->Login);?>
                        </option>
                        <?php endforeach;?>
                        </select>
                    </div>
                    <div class="btn-red" name="distrfilterbtn" style="cursor: pointer;">Фильтр</div>
                </fieldset>
            </form>
        </div>
        <?php endif; ?>
    </div>
</div>
<br></br>
<?php if($empt==0):?>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <ul class="pagination floatl">
                <?php if($current_page>4):?>
                    <li>
                        <a href="<?php echo $pager->url(1);?>">1</a>
                    </li>
                    <li>
                        <a href="#">...</a>
                    </li>
                <?php endif?>
                <?php for($i=1; $i<=$pager->num_pages; $i++): ?>
                    
                    <?php if($i>=$current_page-3 and $i<=$current_page+3):?>
                        <?php if($i==$current_page): ?>
                        <li class="active">
                        <?php else:?>
                        <li>
                        <?php endif?>
                            <a href="<?php echo $pager->url($i);?>"><?php echo $i;?></a>
                        </li>   
                    <?php endif?>
                <?php endfor;?>
                <?php if($current_page<$pager->num_pages-4):?>
                    <li>
                        <a href="#">...</a>
                    </li>   
                    <li>
                        <a href="<?php echo $pager->url($pager->num_pages);?>"><?php echo $pager->num_pages;?></a>
                    </li>
                <?php endif?>
        </ul>
        <ul class="pagination itemperpage">
            <li <?php echo( $item_per_page==20 ? "class='active'" : "");?>><a href="/<?php echo $control;?>/distribs?items=20">20</a></li>
            <li <?php echo( $item_per_page==50 ? "class='active'" : "");?>><a href="/<?php echo $control;?>/distribs?items=50">50</a></li>
            <li <?php echo( $item_per_page==100 ? "class='active'" : "");?>><a href="/<?php echo $control;?>/distribs?items=100">100</a></li>
        </ul>
    </div>
    <div class="col-xs-12 col-md-12 text-center" style="margin-top:20px">
        
        <div class="table-responsive">
            <table id="distribtable" class="mailingtable table tablesorter" cellpadding="5">
                <thead>
                <tr style="cursor: pointer">
                <th>Название</th>
                <th>Статус</th>
                <?php if($control=="su" or $control=="partner"):?>
                <th>Пользователь</th>
                <?php endif ?>
                <?php if($control!="su"):?>
                <th>Кол-во номеров</th><th>Стоимость</th>
                <?php endif ?>
                <th>Время (МСК)</th>
                <?php if($control=="admin" or $control=="su" or $control=="partner"):?>
                <th>Управление</th>
                <?php endif ?>
                </thead>
                <tbody>
                <?php foreach($pager->current_items() as $distrib):?>
                    <tr>
                        <?php if($control!="su"):?>
                        <td>
                            <?php $_($distrib->DistribName);?>
                        </td>
                        <?php else:?>
                        <td>
                            <?php $_($distrib->DistribName);?>; Размер: <?php $_($distrib->Size);?>; Стоимость: <?php $_($distrib->cost/100 . " (" . $distrib->partnercost/100 . ", " . $distrib->admincost/100 . ")");?> руб.; Скорость: <?php $_($speeds[$distrib->SpeedID]);?>
                        </td>
                        <?php endif ?>
                        <td>
                            <?php if($distrib->Status=="Ready" and $distrib->StartTime!="0000-00-00 00:00:00" and $distrib->StartTime>$time_now):?>
                            <?php $_("Запланирована на " . $distrib->StartTime);?>
                            <?php else: ?>
                            <?php $_($statuses[$distrib->Status]);?>
                            <?php endif ?>
                        </td>
                        <?php if($control=="partner"):?>
                        <td>
                            <?php $_($this->pixie->orm->get('user')->where('id', $distrib->UserID)->find()->Login);?>
                            <?php if($this->pixie->orm->get('user')->where('id', $distrib->UserID)->find()->Manager != null):?>
                            <?php $_($this->pixie->orm->get('user')->where('id', $this->pixie->orm->get('user')->where('id', $distrib->UserID)->find()->Manager)->find()->Login);?>
                            <?php endif ?>
                        </td>
                        <?php endif ?>
                        <?php if($control=="su"):?>
                        <td>
                            <?php $_($this->pixie->orm->get('user')->where('id', $distrib->UserID)->find()->Login);?> 
                            <?php $_($this->pixie->orm->get('user')->where('id', $this->pixie->orm->get('user')->where('id', $distrib->UserID)->find()->Partner)->find()->Login);?>
                        </td>
                        <?php endif ?>
                        
                        <?php if($control!="su"):?>
                        <td>
                            <?php $_($distrib->Size);?>
                        </td>
                        <td>
                            <?php if($control=="admin"):?>
                            <?php $_($distrib->cost/100);?> руб.
                            <?php endif ?>
                            <?php if($control=="partner"):?>
                            <?php $_($distrib->cost/100 . " (" . $distrib->partnercost/100 . ", " . ($distrib->cost - $distrib->partnercost)/100 . ")");?> руб.
                            <?php endif ?>
                            <?php if($control=="su"):?>
                            <?php $_($distrib->cost/100 . " (" . $distrib->partnercost/100 . ", " . $distrib->admincost/100 . ")");?> руб.
                            <?php endif ?>
                        </td>
                        <?php endif ?>
                        <td>
                            <?php $_(date_format(date_create($distrib->CreateTime), 'Y-m-d H:i'));?>
                        </td>
                        <?php if($control=="admin" or $control=="su" or $control=="partner"):?>
                        <td>
                            <a class="btn" href=<?php $_( "/admin/distribshow/" . (string)$distrib->id);?> title="Показать"><i style="color:#4D4D4D" class="fa fa-2x  fa-info-circle"></i></a>
                            <a class="btn" href=<?php $_( "/admin/distribfile/" . (string)$distrib->id);?> title="Скачать"><i style="color:#4D4D4D" class="fa fa-2x  fa-arrow-circle-o-down"></i></a>
                        
                            <?php if($distrib->Status=="Started"):?>
                                <a class="btn" href=<?php $_( "/admin/pausedistr/" . (string)$distrib->id);?> title="Пауза"><i style="color:#4D4D4D" class="fa fa-2x  fa-pause-circle-o"></i></a>
                            <?php else:?>
                                <?php if($distrib->Status=="Paused"):?>
                                    <a class="btn" href=<?php $_( "/admin/startdistr/" . (string)$distrib->id);?> title="Старт"><i style="color:#4D4D4D" class="fa fa-2x  fa-play-circle-o"></i></a>
                                <?php endif ?>
                            <?php endif ?>
                            <?php if($distrib->Status=="Paused" or $distrib->Status=="Started"):?>
                                <a class="btn" href=<?php $_( "/admin/stopdistr/" . (string)$distrib->id);?> title="Стоп"><i style="color:#4D4D4D" class="fa fa-2x  fa-stop-circle-o"></i></a>
                            <?php endif ?>
                            <?php if($distrib->Status=="Ready" and $distrib->StartTime!="0000-00-00 00:00:00" and $distrib->StartTime>$time_now):?>
                                 <a class="btn" href=<?php $_( "/admin/stopdistr/" . (string)$distrib->id);?> title="Стоп"><i style="color:#4D4D4D" class="fa fa-2x  fa-stop-circle-o"></i></a>
                            <?php endif ?>
                            <?php if($control=="su"):?>
                                <a class="btn" onclick="show('block', <?php $_($distrib->id);?>)" title="Прослушать записи"><i style="color:#4D4D4D" class="fa fa-2x  fa-file-audio-o"></i></a>
                                <?php if($distrib->Status=="Moderating"):?>
                                    <br>
                                    <a class="btn" href=<?php $_( "/su/acceptdistr/" . (string)$distrib->id);?> title="Принять"><i style="color:#00FF00" class="fa fa-3x  fa-check-circle-o"></i></a>
                                    <a class="btn" href=<?php $_( "/su/declinedistr/" . (string)$distrib->id);?> title="Отклонить"><i style="color:#FF0000" class="fa fa-3x  fa-times-circle-o"></i></a>
                                <?php endif ?>
                            <?php endif ?>
                            <?php if($distrib->admincomment != "" or $control=="su"):?>
                                <div onclick="show1('block', '<?php $_($distrib->id); ?>', '<?php $_($distrib->admincomment);?>')" title="Комментарий" style="cursor: pointer;"><i style="color:#4D4D4D" class="fa fa-2x  fa-commenting"></i></div>
                            <?php endif ?>                            
                            <a class="btn" href=<?php $_( "/admin/distribinfo/" . (string)$distrib->id);?> title="Выгрузить не ответивших и не перезвонивших"><i style="color:#4D4D4D" class="fa fa-2x  fa-bell-slash"></i></a>
                        </td>
                        <?php endif ?>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-xs-12 col-md-12 text-center">
        <ul class="pagination floatl">
                <?php if($current_page>4):?>
                    <li>
                        <a href="<?php echo $pager->url(1);?>">1</a>
                    </li>
                    <li>
                        <a href="#">...</a>
                    </li>
                <?php endif?>
                <?php for($i=1; $i<=$pager->num_pages; $i++): ?>
                    
                    <?php if($i>=$current_page-3 and $i<=$current_page+3):?>
                        <?php if($i==$current_page): ?>
                        <li class="active">
                        <?php else:?>
                        <li>
                        <?php endif?>
                            <a href="<?php echo $pager->url($i);?>"><?php echo $i;?></a>
                        </li>   
                    <?php endif?>
                <?php endfor;?>
                <?php if($current_page<$pager->num_pages-4):?>
                    <li>
                        <a href="#">...</a>
                    </li>   
                    <li>
                        <a href="<?php echo $pager->url($pager->num_pages);?>"><?php echo $pager->num_pages;?></a>
                    </li>
                <?php endif?>
        </ul>
        <ul class="pagination itemperpage">
            <li <?php echo( $item_per_page==20 ? "class='active'" : "");?>><a href="/<?php echo $control;?>/distribs?items=20">20</a></li>
            <li <?php echo( $item_per_page==50 ? "class='active'" : "");?>><a href="/<?php echo $control;?>/distribs?items=50">50</a></li>
            <li <?php echo( $item_per_page==100 ? "class='active'" : "");?>><a href="/<?php echo $control;?>/distribs?items=100">100</a></li>
        </ul>
    </div>
</div>
<?php else:?>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
    <div>Рассылок пока нет</div>
    </div>
</div>
<?php endif?>

<div onclick="show('none')" id="wrap" style="display: none;"></div>
<div id="window" style="display: none;">
    <center>
        <fieldset>
            <legend id="nazv"></legend>
            <label style="margin-top: 20px;" id="wlbl">Приветствие</label>
            <audio controls="controls" id="welcomeaudioplayer" class="hide">
              Your browser does not support the <code>audio</code> element.
              <source id="welcomeaudio" src="" type="audio/wav">
            </audio>
            <label style="margin-top: 20px;" id="1lbl">IVR 1</label>
            <audio controls="controls" id="ivr1audioplayer" class="hide">
              Your browser does not support the <code>audio</code> element.
              <source id="ivr1audio" src="" type="audio/wav">
            </audio>
            <label style="margin-top: 20px;" id="2lbl">IVR 2</label>
            <audio controls="controls" id="ivr2audioplayer" class="hide">
              Your browser does not support the <code>audio</code> element.
              <source id="ivr2audio" src="" type="audio/wav">
            </audio>
            <label style="margin-top: 20px;" id="3lbl">IVR 3</label>
            <audio controls="controls" id="ivr3audioplayer" class="hide">
              Your browser does not support the <code>audio</code> element.
              <source id="ivr3audio" src="" type="audio/wav">
            </audio>
            <label style="margin-top: 20px;" id="smslbl" class="hide">SMS</label>
            <textarea name="smsmoderate" id="smsmoderate" class="form-control hide" rows="3" maxlength="300"></textarea>
        </fieldset>
    </center>
</div>

<div onclick="show1('none')" id="wrap2" style="display: none;"></div>
<div id="window2" style="display: none;">
    <center>
        <legend>Комментарий</legend>
        <form method="POST" class="form-horizontal" id="commentform" enctype="multipart/form-data">
            <fieldset>
                <?php if($control=="su" or $control=="partner"):?>
                    <textarea id="commenttext" name="commenttext" type="text" rows="3" maxlength="300" cols="35"></textarea>
                <?php else:?>
                    <textarea id="commenttext" name="commenttext" type="text" rows="5" maxlength="300" cols="35" readonly></textarea>    
                <?php endif ?>
                <?php if($control=="su" or $control=="partner"):?>
                    <div id="commentbtn" style="cursor: pointer;" class="btn-form-big-grey ">Отправить</div>
                <?php endif ?>
            </fieldset>
        </form>
    </center>
</div>

<style type="text/css">
	#wrap{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
	
	#window{
		width: 400px;
		height: 600px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
	
    #wrap1{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
	
	#window1{
		width: 400px;
		height: 400px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
    #wrap2{
        display: none;
        opacity: 0.8;
        position: fixed;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        padding: 16px;
        background-color: rgba(1, 1, 1, 0.725);
        z-index: 100;
        overflow: auto;
    }
    
    #window2{
        width: 400px;
        height: 300px;
        margin: 50px auto;
        display: none;
        background: #fff;
        z-index: 200;
        position: fixed;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        padding: 16px;
    }
	.close{
		margin-left: 364px;
		margin-top: 4px;
		cursor: pointer;
	}
	
</style>

<script type="text/javascript">
    
    $(document).ready(function(){
      $.tablesorter.addParser({ 
        // set a unique id 
        id: 'status', 
        is: function(s) { 
            // return false so this parser is not auto detected 
            return false; 
        }, 
        format: function(s) { 
            // format your data for normalization 
            return s.replace(/На модерации/,6).replace(/Обработка/,5).replace(/В процессе/,4).replace(/На паузе/,3).replace(/Отменена/,0).replace(/Остановлена/,0).replace(/Закончена/,0); 
        }, 
        // set type, either numeric or text 
        type: 'numeric' 
    });
      $("#distribtable").tablesorter({ 
            headers: { 
                1: { 
                    sorter:'status' 
                } 
            },
            sortList: [[1,1], [4, 1]]
        });
    });
 
            //Функция показа
    function show(state, distrid){
        $.ajax({
          type: "POST",
          url: "/su/getsounds",
          data: "did="+distrid,
          success: function(data){
            if(data.trim()!=''){
                $("#welcomeaudioplayer").addClass('hide');
                $("#welcomeaudio").attr("src", "").detach().appendTo("#welcomeaudioplayer");
                $("#wlbl").addClass('hide');
                $("#ivr1audioplayer").addClass('hide');
                $("#ivr1audio").attr("src", "").detach().appendTo("#ivr1audioplayer");
                $("#1lbl").addClass('hide');
                $("#ivr2audioplayer").addClass('hide');
                $("#ivr2audio").attr("src", "").detach().appendTo("#ivr2audioplayer");
                $("#2lbl").addClass('hide');
                $("#ivr3audioplayer").addClass('hide');
                $("#ivr3audio").attr("src", "").detach().appendTo("#ivr3audioplayer");
                $("#3lbl").addClass('hide');
                var obj = jQuery.parseJSON( data );
                if(obj.WelcomeRecord!=""){
                    $("#welcomeaudioplayer").removeClass('hide');
                    $("#welcomeaudio").attr("src", obj.WelcomeRecord).detach().appendTo("#welcomeaudioplayer");
                    $("#wlbl").removeClass('hide');
                }
                else{
                    $("#welcomeaudioplayer").addClass('hide');
                }
                if(obj.IVR1!=""){
                    $("#ivr1audioplayer").removeClass('hide');
                    $("#ivr1audio").attr("src", obj.IVR1).detach().appendTo("#ivr1audioplayer");
                    $("#1lbl").removeClass('hide');
                }
                else{
                    $("#ivr1audioplayer").addClass('hide');
                }
                if(obj.IVR2!=""){
                    $("#ivr2audioplayer").removeClass('hide');
                    $("#ivr2audio").attr("src", obj.IVR1).detach().appendTo("#ivr2audioplayer");
                    $("#2lbl").removeClass('hide');
                }
                else{
                    $("#ivr2audioplayer").addClass('hide');
                }
                if(obj.IVR3!=""){
                    $("#ivr3audioplayer").removeClass('hide');
                    $("#ivr3audio").attr("src", obj.IVR1).detach().appendTo("#ivr3audioplayer");
                    $("#3lbl").removeClass('hide');
                }
                else{
                    $("#ivr3audioplayer").addClass('hide');
                }
                if(obj.SmsText!=""){
                    $("#smsmoderate").removeClass('hide');
                    $("#smslbl").removeClass('hide');
                    $("#smsmoderate").val(obj.SmsText);
                }
                else{
                    ("#smsmoderate").addClass('hide');
                    $("#smslbl").addClass('hide');
                    $("#smsmoderate").val("");
                }
            }
          }
        });
        document.getElementById('window').style.display = state;            
        document.getElementById('wrap').style.display = state;
        document.getElementById('nazv').innerHTML = "Записи рассылки "+distrid;
        
    }

    function show1(state, distrid, admincomment){;
        document.getElementById('window2').style.display = state;            
        document.getElementById('wrap2').style.display = state;
        document.getElementById('commenttext').value = admincomment;
        document.getElementById('commentbtn').setAttribute("onclick", "addcomment('"+distrid+"')");
    }
    
    function addcomment(distrid){;
        $.ajax({
          type: "POST",
          url: "/su/commentdistrib",
          data: "distrid="+distrid+"&text="+$('textarea[name="commenttext"]').val(),
          success: function(data){
            show1('none');
            window.location.replace("/su/distribs");
          }
        });
    }
</script>
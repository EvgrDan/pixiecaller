<h2>Рассылка <?php $_($distrib->DistribName);?></h2>
<ul class="nav nav-tabs">
<li class="active" id="callss" onclick="showcalls()"><a href="#">Звонки</a></li>
<?php if($distrib->DistribType == "ordinary"): ?>
<li class="" id="recallss" onclick="showrecalls()"><a href="#">Перезвонившие</a></li>
<?php endif?>
<li class="" id="infoss" onclick="showinfo()"><a href="#">Информация</a></li>
</ul>
<div id="distribstable" class="row " style="margin-top:20px">
    <div class="col-xs-12 col-md-12 text-center" >
        <ul class="pagination floatl" >
            <?php if($current_page>4):?>
                <li>
                    <a href="<?php echo $url . "/page-1";?>">1</a>
                </li>
                <li>
                    <a href="#">...</a>
                </li>
            <?php endif?>
            <?php for($i=1; $i<=$num_pages; $i++): ?>
                
                <?php if($i>=$current_page-3 and $i<=$current_page+3):?>
                    <?php if($i==$current_page): ?>
                    <li class="active">
                    <?php else:?>
                    <li>
                    <?php endif?>
                        <a href="<?php echo $url . "/page-" . $i;?>"><?php echo $i;?></a>
                    </li>   
                <?php endif?>
            <?php endfor;?>
            <?php if($current_page<$num_pages-4):?>
                <li>
                    <a href="#">...</a>
                </li>   
                <li>
                    <a href="<?php echo $url . "/page-" . $num_pages;?>"><?php echo $num_pages;?></a>
                </li>
            <?php endif?>
        </ul>
        <ul class="pagination itemperpage">
            <li <?php echo( $item_per_page==20 ? "class='active'" : "");?>><a href="<?php echo $url;?>?items=20">20</a></li>
            <li <?php echo( $item_per_page==50 ? "class='active'" : "");?>><a href="<?php echo $url;?>?items=50">50</a></li>
            <li <?php echo( $item_per_page==100 ? "class='active'" : "");?>><a href="<?php echo $url;?>?items=100">100</a></li>
        </ul>
    </div>
    <div class="col-xs-12 col-md-12 text-center" style="margin-top:20px">
        <div class="table-responsive">
            <table class="mailingtable table">
                <tr><th>Номер</th>
                <?php if($distrib->DistribType == "recall" or ($distrib->IVR1!="none" or $distrib->IVR2!="none" or $distrib->IVR3!="none")): ?>
                <?php echo("<th>Цифра</th>"); ?>
                <?php endif ?>
                <th>Длительность</th>
                <?php if($distrib->DistribType == "recall" or ($distrib->IVR1!="none" or $distrib->IVR2!="none" or $distrib->IVR3!="none")): ?>
                <?php echo("<th>Разговор</th>"); ?>
                <?php endif ?>
                <th>Время звонка</th><th>Стоимость</th>
                 <?php if($distrib->DistribType == "recall"): ?>
                <?php echo("<th>Время перезвона</th>"); ?>
                <?php endif ?>
                </tr>
                <?php foreach($stats as $stat):?>
                    <tr>
                        <td>
                            <?php $_($stat[0]);?>
                        </td>
                        <?php if($distrib->DistribType == "recall" or ($distrib->IVR1!="none" or $distrib->IVR2!="none" or $distrib->IVR3!="none")): ?>
                        <?php echo("<td>" . $stat[1] . "</td>"); ?>
                        <?php endif ?>
                        <td>
                            <!--дикий костыль тут в пхп -->
                            <?php if($distrib->DistribType == "ordinary"): ?>
                            <?php $_((($stat[2]<5 and $stat[2]>0) ? $stat[2]+1:$stat[2]));?>
                            <?php else: ?>
                            <?php $_($stat[2]);?>
                            <?php endif ?>
                        </td>
                        <?php if($distrib->DistribType == "recall"  or ($distrib->IVR1!="none" or $distrib->IVR2!="none" or $distrib->IVR3!="none")): ?>
                        <?php echo("<td>" . $stat[5] . "</td>"); ?>
                        <?php endif ?>
                        <td>
                            <?php $_($stat[3]);?>
                            <?php if($stat[1] == 1): ?>
                            <audio style="margin-top: 20px;" controls="controls" id="audioplayer<?php $_($stat[0]);?>">
                              Your browser does not support the <code>audio</code> element.
                              <source id="audio<?php $_($stat[0]);?>" src="/assets/audio/records/<?php $_($distrib->id . "/" . $stat[0] . ".wav");?>" type="audio/wav">
                            </audio>
                            <?php endif ?>
                        </td>
                        <td>
                        <?php if($distrib->DistribType == "recall"): ?>
                        <?php if(trim(strval($stat[1])) == "1"): ?>
                        <?php $_($cost->dial/100+$cost->recall*$stat[5]/60/100); ?>
                        <?php else: ?>
                        <?php $_($cost->dial/100); ?>
                        <?php endif ?>
                        <?php else: ?>
                        <?php if($distrib->IVR1!="none" or $distrib->IVR2!="none" or $distrib->IVR3!="none"):?>
                        <?php $_($cost->ordinary*(($stat[2]<5 and $stat[2]>0) ? $stat[2]+1:$stat[2])/60/100 + $cost->recall*$stat[5]/60/100); ?>
                        <?php else: ?>
                        <?php $_($cost->ordinary*(($stat[2]<5 and $stat[2]>0) ? $stat[2]+1:$stat[2])/60/100); ?>
                        <?php endif ?>
                        <?php endif ?>
                        </td>
                        <?php if($distrib->DistribType == "recall"): ?>
                        <td>
                        <?php $_($stat[6]); ?>
                        </td>
                        <?php endif ?>
                    </tr>
                <?php endforeach;?>
            </table>
        </div>
    </div>
    <div class="col-xs-12 col-md-12 text-center" >
        <ul class="pagination floatl" >
            <?php if($current_page>4):?>
                <li>
                    <a href="<?php echo $url . "/page-1";?>">1</a>
                </li>
                <li>
                    <a href="#">...</a>
                </li>
            <?php endif?>
            <?php for($i=1; $i<=$num_pages; $i++): ?>
                
                <?php if($i>=$current_page-3 and $i<=$current_page+3):?>
                    <?php if($i==$current_page): ?>
                    <li class="active">
                    <?php else:?>
                    <li>
                    <?php endif?>
                        <a href="<?php echo $url . "/page-" . $i;?>"><?php echo $i;?></a>
                    </li>   
                <?php endif?>
            <?php endfor;?>
            <?php if($current_page<$num_pages-4):?>
                <li>
                    <a href="#">...</a>
                </li>   
                <li>
                    <a href="<?php echo $url . "/page-" . $num_pages;?>"><?php echo $num_pages;?></a>
                </li>
            <?php endif?>
        </ul>
        <ul class="pagination itemperpage">
            <li <?php echo( $item_per_page==20 ? "class='active'" : "");?>><a href="<?php echo $url;?>?items=20">20</a></li>
            <li <?php echo( $item_per_page==50 ? "class='active'" : "");?>><a href="<?php echo $url;?>?items=50">50</a></li>
            <li <?php echo( $item_per_page==100 ? "class='active'" : "");?>><a href="<?php echo $url;?>?items=100">100</a></li>
        </ul>
    </div>
</div>
<div id="distrinfo" class="hide">
<?php echo("<label>Тип рассылки: <b>" . $types[$distrib->DistribType] . "</b></label>");?>
<?php echo("<label>Стоимость рассылки: <b>" . $distrib->cost/100 . " руб. </b></label>");?>
<label>Сделано звонков: <b><?php $_((int)$cnt);?> из <?php $_($distrib->Size);?></b></label>
<?php if($distrib->DistribType == "ordinary"): ?>
<?php echo("<label>Взяли трубку: <b>" . $cntP . "</b></label>");?>
<?php echo("<label>Перезвонили: <b>" . $cntrecall . "</b></label>");?>
<?php endif ?>
<?php if($distrib->DistribType == "recall"): ?>
<?php echo("<label>Людей перезвонило: <b>" . $cntP . "</b></label>");?>
<?php endif ?>
<?php if($distrib->DistribType == "recall" or ($distrib->IVR1!="none" or $distrib->IVR2!="none" or $distrib->IVR3!="none")): ?>
<?php echo("<label>Нажали 1: <b>" . $cnt1 . "</b></label>");?>
<?php echo("<label>Нажали 2: <b>" . $cnt2 . "</b></label>");?>
<?php echo("<label>Нажали 3: <b>" . $cnt3 . "</b></label>");?>
<?php endif ?>
<?php echo("<label>Номер обзвона: <b>" . $distrib->SenderNumber . "</b></label>");?>
<?php echo("<label>Время создания: <b>" . $distrib->CreateTime . "</b></label>");?>
<?php if($distrib->StartTime == "-infinity"): ?>
<?php echo("<label>Время старта: <b>" . $distrib->CreateTime . "</b></label>");?>
<?php else: ?>
<?php echo("<label>Запланирована на: <b>" . $distrib->StartTime . "</b></label>");?>
<?php endif ?>
<?php if($distrib->ToNumber != ""): ?>
<?php echo("<label>Номер менеджера: <b>" . $distrib->ToNumber . "</b></label>");?>
<?php endif ?>
<?php if($distrib->StopTime != "-infinity"): ?>
<?php echo("<label>Время окончания: <b>" . $distrib->StopTime . "</b></label>");?>
<?php endif ?>
<?php echo("<label>Скорость рассылки: <b>" . $speed . " звонков в час </b></label>");?>
<?php if($distrib->recording == 1): ?>
<?php echo("<a href='/admin/loadrecords/$distrib->id'>Скачать аудиозаписи</a>");?>
<?php endif ?>
<?php if($distrib->SmsText != ""): ?>
<?php echo("<label>Текст СМС: " . $distrib->SmsText . "</label>");?>
<?php endif ?>
</div>
<div id="ordrecall" class="hide">
    <div class="col-xs-12 col-md-12 text-center" style="margin-top:20px">
        <div class="table-responsive">
            <table class="mailingtable table">
                <tr><th>Номер</th>
                <?php if($distrib->DistribType == "recall" or ($distrib->IVR1!="none" or $distrib->IVR2!="none" or $distrib->IVR3!="none")): ?>
                <?php echo("<th>Цифра</th>"); ?>
                <?php endif ?>
                <th>Длительность</th>
                <?php if($distrib->DistribType == "recall" or ($distrib->IVR1!="none" or $distrib->IVR2!="none" or $distrib->IVR3!="none")): ?>
                <?php echo("<th>Разговор</th>"); ?>
                <?php endif ?>
                <th>Время звонка</th><th>Стоимость</th>
                </tr>
                <?php foreach($statsrecall as $stat):?>
                    <tr>
                        <td>
                            <?php $_($stat[0]);?>
                        </td>
                        <?php if($distrib->DistribType == "recall" or ($distrib->IVR1!="none" or $distrib->IVR2!="none" or $distrib->IVR3!="none")): ?>
                        <?php echo("<td>" . $stat[1] . "</td>"); ?>
                        <?php endif ?>
                        <td>
                            <?php $_($stat[2]);?>
                        </td>
                        <td>
                        <?php if($distrib->DistribType == "recall" or ($distrib->IVR1!="none" or $distrib->IVR2!="none" or $distrib->IVR3!="none")): ?>    
                            <?php $_($stat[3]);?>
                            <?php if($stat[1] == 1): ?>
                            <audio style="margin-top: 20px;" controls="controls" id="audioplayer<?php $_($stat[0]);?>">
                              Your browser does not support the <code>audio</code> element.
                              <source id="audio<?php $_($stat[0]);?>" src="/assets/audio/records/<?php $_($distrib->id . "/" . $stat[0] . ".wav");?>" type="audio/wav">
                            </audio>
                            <?php endif ?>
                        <?php endif ?>
                        </td>
                        <!--<?php if($distrib->DistribType == "recall"  or ($distrib->IVR1!="none" or $distrib->IVR2!="none" or $distrib->IVR3!="none")): ?>
                        <?php echo("<td>" . $stat[3] . "</td>"); ?>
                        <?php endif ?>-->
                        <td>
                            <?php $_($stat[4]);?>
                        </td>
                        <td>
                            <?php $_($cost->recall*$stat[3]/60/100); ?>
                        </td>
                    </tr>
                <?php endforeach;?>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">

    function showcalls(){
        $("#distribstable").removeClass('hide');
        $("#callss").addClass('active');
        $("#infoss").removeClass('active');
        $("#recallss").removeClass('active');
        $("#distrinfo").addClass('hide');
        $("#ordrecall").addClass('hide');
    }
    
    function showinfo(){
        $("#distribstable").addClass('hide');
        $("#callss").removeClass('active');
        $("#infoss").addClass('active');
        $("#recallss").removeClass('active');
        $("#distrinfo").removeClass('hide');
        $("#ordrecall").addClass('hide');
    }
    
    function showrecalls(){
        $("#distribstable").addClass('hide');
        $("#callss").removeClass('active');
        $("#infoss").removeClass('active');
        $("#recallss").addClass('active');
        $("#distrinfo").addClass('hide');
        $("#ordrecall").removeClass('hide');
    }
    
</script>
<table class="table">
    <tr><th>Название</th><th>Тип</th><th>Статус</th><th>Кол-во номеров</th><th>Пользователь</th><th>Партнер</th><th>Время создания(МСК)</th><th>Время начала(МСК)</th><th>Стоимость</th><th>Модерация</th><th></th><th></th></tr>
    <?php foreach($distribs as $distrib):?>
        <tr>
            <td>
                <?php $_($distrib->DistribName);?>
            </td>
            <td>
                <?php $_($types[$distrib->DistribType]);?>
            </td>
            <td>
                <?php $_($statuses[$distrib->Status]);?>
            </td>
            <td>
                <?php $_($distrib->Size);?>
            </td>
			<td>
                <?php $_($this->pixie->orm->get('user')->where('id', $distrib->UserID)->find()->Login);?>
            </td>
            <td>
                <?php $_($this->pixie->orm->get('user')->where('id', $this->pixie->orm->get('user')->where('id', $distrib->UserID)->find()->Partner)->find()->Login);?>
            </td>
			<td>
                <?php $_($distrib->CreateTime);?>
            </td>
            <td>
                <?php $_($distrib->StartTime);?>
            </td>
            <td>
                <?php $_($distrib->cost/100 . " (" . $distrib->partnercost/100 . ", " . $distrib->admincost/100 . ")");?> руб.
            </td>
            <td>
                <button class="btn" onclick="show('block', <?php $_($distrib->id);?>)">Прослушать записи</button>
            </td>
            <?php if($distrib->Status=="Moderating"):?>
            <td>
                <a class="btn" href=<?php $_( "/su/acceptdistr/" . (string)$distrib->id);?>>Принять</a>
            </td>
            <td>
                <a class="btn" href=<?php $_( "/su/declinedistr/" . (string)$distrib->id);?>>Отклонить</a>
            </td>
            <?php endif ?>
        </tr>
    <?php endforeach;?>
</table>

<div onclick="show('none')" id="wrap" style="display: none;"></div>
<div id="window" style="display: none;">
    <center>
        <fieldset>
            <legend id="nazv"></legend>
            <label style="margin-top: 20px;" id="wlbl">Приветствие</label>
            <audio controls="controls" id="welcomeaudioplayer" class="hide">
              Your browser does not support the <code>audio</code> element.
              <source id="welcomeaudio" src="" type="audio/wav">
            </audio>
            <label style="margin-top: 20px;" id="1lbl">IVR 1</label>
            <audio controls="controls" id="ivr1audioplayer" class="hide">
              Your browser does not support the <code>audio</code> element.
              <source id="ivr1audio" src="" type="audio/wav">
            </audio>
            <label style="margin-top: 20px;" id="2lbl">IVR 2</label>
            <audio controls="controls" id="ivr2audioplayer" class="hide">
              Your browser does not support the <code>audio</code> element.
              <source id="ivr2audio" src="" type="audio/wav">
            </audio>
            <label style="margin-top: 20px;" id="3lbl">IVR 3</label>
            <audio controls="controls" id="ivr3audioplayer" class="hide">
              Your browser does not support the <code>audio</code> element.
              <source id="ivr3audio" src="" type="audio/wav">
            </audio>
        </fieldset>
    </center>
</div>

<style type="text/css">
	#wrap{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
	
	#window{
		width: 400px;
		height: 400px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
	
    #wrap1{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
	
	#window1{
		width: 400px;
		height: 400px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
    
	.close{
		margin-left: 364px;
		margin-top: 4px;
		cursor: pointer;
	}
	
</style>
<script type="text/javascript">
 
            //Функция показа
    function show(state, distrid){
        $.ajax({
          type: "POST",
          url: "/su/getsounds",
          data: "did="+distrid,
          success: function(data){
            if(data.trim()!=''){
                $("#welcomeaudioplayer").addClass('hide');
                $("#welcomeaudio").attr("src", "").detach().appendTo("#welcomeaudioplayer");
                $("#wlbl").addClass('hide');
                $("#ivr1audioplayer").addClass('hide');
                $("#ivr1audio").attr("src", "").detach().appendTo("#ivr1audioplayer");
                $("#1lbl").addClass('hide');
                $("#ivr2audioplayer").addClass('hide');
                $("#ivr2audio").attr("src", "").detach().appendTo("#ivr2audioplayer");
                $("#2lbl").addClass('hide');
                $("#ivr3audioplayer").addClass('hide');
                $("#ivr3audio").attr("src", "").detach().appendTo("#ivr3audioplayer");
                $("#3lbl").addClass('hide');
                var obj = jQuery.parseJSON( data );
                if(obj.WelcomeRecord!=""){
                    $("#welcomeaudioplayer").removeClass('hide');
                    $("#welcomeaudio").attr("src", obj.WelcomeRecord).detach().appendTo("#welcomeaudioplayer");
                    $("#wlbl").removeClass('hide');
                }
                else{
                    $("#welcomeaudioplayer").addClass('hide');
                }
                if(obj.IVR1!=""){
                    $("#ivr1audioplayer").removeClass('hide');
                    $("#ivr1audio").attr("src", obj.IVR1).detach().appendTo("#ivr1audioplayer");
                    $("#1lbl").removeClass('hide');
                }
                else{
                    $("#ivr1audioplayer").addClass('hide');
                }
                if(obj.IVR2!=""){
                    $("#ivr2audioplayer").removeClass('hide');
                    $("#ivr2audio").attr("src", obj.IVR1).detach().appendTo("#ivr2audioplayer");
                    $("#2lbl").removeClass('hide');
                }
                else{
                    $("#ivr2audioplayer").addClass('hide');
                }
                if(obj.IVR3!=""){
                    $("#ivr3audioplayer").removeClass('hide');
                    $("#ivr3audio").attr("src", obj.IVR1).detach().appendTo("#ivr3audioplayer");
                    $("#3lbl").removeClass('hide');
                }
                else{
                    $("#ivr3audioplayer").addClass('hide');
                }
            }
          }
        });
        document.getElementById('window').style.display = state;            
        document.getElementById('wrap').style.display = state;
        document.getElementById('nazv').innerHTML = "Записи рассылки "+distrid;
        
    }
</script>
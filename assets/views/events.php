<div class="row ">
    <div class="col-xs-12 col-md-2 text-center"></div>
    <div class="col-xs-12 col-md-8">
        <div class="box-form-reg-all">
            <div class="step">
            <div class="row ">
                <div class="col-xs-12 col-md-12 text-center">
                    <div class="title-reg-l">
                        СОБЫТИЯ
                    </div>
                </div>
                <div class="col-xs-12 col-md-12 text-left">
                    <form method="POST" id="eventfilter" enctype="multipart/form-data">
                        <fieldset>
                            <div class="title-step">Тип</div>
                            <input class="field-form" id="eventtype" name="eventtype" type="text" placeholder="Тип..." value="<?php echo $eventtype;?>"/>
                            <div class="title-step">Субьект</div>
                            <input class="field-form" id="eventsubj" name="eventsubj" type="text" placeholder="Субьект..." value="<?php echo $eventsubj;?>"/>
                            <button type="submit" class="btn-red" name="filterbtn">Фильтр</button>
                        </fieldset>
                    </form>
                </div>
            </div> 
            </div>
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
    <div class="row">
        <ul class="pagination floatl">
            <?php if($current_page>4):?>
                <li>
                    <a href="<?php echo $url . "/page-1";?>">1</a>
                </li>
                <li>
                    <a href="#">...</a>
                </li>
            <?php endif?>
            <?php for($i=1; $i<=$num_pages; $i++): ?>
                
                <?php if($i>=$current_page-3 and $i<=$current_page+3):?>
                    <?php if($i==$current_page): ?>
                    <li class="active">
                    <?php else:?>
                    <li>
                    <?php endif?>
                        <a href="<?php echo $url . "/page-" . $i;?>"><?php echo $i;?></a>
                    </li>   
                <?php endif?>
            <?php endfor;?>
            <?php if($current_page<$num_pages-4):?>
                <li>
                    <a href="#">...</a>
                </li>   
                <li>
                    <a href="<?php echo $url . "/page-" . $num_pages;?>"><?php echo $num_pages;?></a>
                </li>
            <?php endif?>
        </ul>
        <ul class="pagination itemperpage">
            <li <?php echo( $item_per_page==20 ? "class='active'" : "");?>><a href="<?php echo $url;?>?items=20">20</a></li>
            <li <?php echo( $item_per_page==50 ? "class='active'" : "");?>><a href="<?php echo $url;?>?items=50">50</a></li>
            <li <?php echo( $item_per_page==100 ? "class='active'" : "");?>><a href="<?php echo $url;?>?items=100">100</a></li>
        </ul>
    </div>
    <div class="table-responsive">
        <table class="mailingtable table" cellpadding="5" style="width:1000px; table-layout:fixed; word-wrap:break-word;">
            <tr><th>Время</th><th>Тип</th><th>Субьект</th><th>Обьект</th><th style="width:300px; table-layout:fixed; overflow:hidden; word-wrap:break-word;">Данные</th></tr>
            <?php foreach($events as $event):?>
                <tr>
                    <td>
                        <?php $_($event[0]);?>
                    </td>
                    <td>
                        <?php $_($event[1]);?>
                    </td>
                    <td>
                        <a href="<?php $_($event[5]);?>"><?php $_($event[2]);?></a>
                    </td>
                    <td>
                        <a href="<?php $_($event[6]);?>"><?php $_($event[3]);?></a>
                    </td>
                    <td style="width:200px; table-layout:fixed; overflow:hidden; word-wrap:break-word;">
                        <?php $_($event[4]);?>
                    </td>
                </tr>
            <?php endforeach;?>
        </table>
    </div>
    <div class="row">
        <ul class="pagination floatl">
            <?php if($current_page>4):?>
                <li>
                    <a href="<?php echo $url . "/page-1";?>">1</a>
                </li>
                <li>
                    <a href="#">...</a>
                </li>
            <?php endif?>
            <?php for($i=1; $i<=$num_pages; $i++): ?>
                
                <?php if($i>=$current_page-3 and $i<=$current_page+3):?>
                    <?php if($i==$current_page): ?>
                    <li class="active">
                    <?php else:?>
                    <li>
                    <?php endif?>
                        <a href="<?php echo $url . "/page-" . $i;?>"><?php echo $i;?></a>
                    </li>   
                <?php endif?>
            <?php endfor;?>
            <?php if($current_page<$num_pages-4):?>
                <li>
                    <a href="#">...</a>
                </li>   
                <li>
                    <a href="<?php echo $url . "/page-" . $num_pages;?>"><?php echo $num_pages;?></a>
                </li>
            <?php endif?>
        </ul>
        <ul class="pagination itemperpage">
            <li <?php echo( $item_per_page==20 ? "class='active'" : "");?>><a href="<?php echo $url;?>?items=20">20</a></li>
            <li <?php echo( $item_per_page==50 ? "class='active'" : "");?>><a href="<?php echo $url;?>?items=50">50</a></li>
            <li <?php echo( $item_per_page==100 ? "class='active'" : "");?>><a href="<?php echo $url;?>?items=100">100</a></li>
        </ul>
    </div>
    </div>
</div>
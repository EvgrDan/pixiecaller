<legend>Инструкция по работе в системе</legend>
		
	</header><!-- .entry-header -->

		<div class="entry-content">
		<h3>Создание первой голосовой рассылки</h3>
<p>В верхнем меню нажмите на “Создать рассылку”</p>
<p>1. <strong>Название рассылки:</strong> по умолчанию в название рассылки записывается время и дата создания рассылки, так же вы можете указать свое название, например “Тестовая рассылка”</p>
<p>2. <strong>Источник номеров:</strong> здесь Вы можете ввести номера вручную или загрузить номера из базы. Ручной ввод позволяет ввести не больше 10 номеров через запятую.</p>
<p>3. <strong>Номер отправителя:</strong> выберите номер из выпадающего списка или добавьте свой во вкладке меню “Добавить номер”. После ввода номера в поле Вам поступит звонок, необходимо ответить и прослушать сообщение. В этом сообщении робот проговорит четырехзначный код.</p>
<p><strong>Блокировать номер на:</strong> при обычной рассылке с системного номера, он не блокируются, а это значит, что всем пользователям доступен этот номер. Если Вы хотите, чтобы ваши клиенты, перезванивая попадали на Вашу аудиозапись, то заблокируйте номер до окончания рассылки.</p>
<p><a href="/assets/images/instruction/addnumber.png"><img class="alignnone wp-image-106 size-full" src="/assets/images/instruction/addnumber.png" alt="Добавление номера" /></a></p>
<p>4. <strong>Приветствие:</strong> введите в поле “Текст приветствия” текст, который будет проговариваться роботом, когда клиент возьмет трубку. Либо загрузите файл в формате mp3, нажав на кнопку “Загрузить файл”.</p>
<p><a href="/assets/images/instruction/newdistrib1.png"><img class="alignnone wp-image-106 size-full" src="/assets/images/instruction/newdistrib1.png" alt="Создание рассылки 1" /></a></p>
<p>5. <strong>Голосовое меню (обработка нажатия цифр):</strong> при выборе этого пункта появится меню, в котором Вы можете задать действие при нажатии на клавишу телефона. Внимание! Обязательно надо выбрать все цифры, но не обязательно загружать аудиоролик.</p>
<p>При нажатии на цифру 1 можно осуществить соединение между клиентом, которому была адресована рассылка и менеджером, который принимает входящие звонки. Для этого необходимо выбрать переадресацию при нажатии на цифру 1 и указать номер телефона, начинающийся с “7”.</p>
<p>Также можно добавить номер в черный список при нажатии на “3”, для этого так же необходимо выбрать дополнительное действие.</p>
<p>6. <strong>Скорость рассылки (в час):</strong> здесь необходимо задать скорость обзвона, которую хотите осуществить в течении часа. Со временем Вам будет доступна более быстрая скорость до 10 000 звонков в час.</p>
<p>7. <strong>Кнопка “Показать дополнительные поля”</strong> открывает нам доп. функционал сервиса.</p>
<p><a href="/assets/images/instruction/newdistrib2.png"><img class="alignnone wp-image-106 size-full" src="/assets/images/instruction/newdistrib2.png" alt="Создание рассылки 2" /></a></p>
<p>8. <strong>Тип рассылки:</strong> можно выбрать 2 типа рассылки</p>
<p>8.1. Обычная</p>
<p>8.2. Позвонил - сбросил</p>
<p>9. <strong>Запись звонков:</strong> вы можете записать звонки, но это приведет к удорожанию рассылки на 20%, зато вы сможете прослушать то, как слышал Клиент рассылку и как менеджер общался с этим клиентом. Записи хранятся 14 дней с момента запуска рассылки.</p>
<p>10. <strong>Запланировать рассылку:</strong> выберите время запуска рассылки с помощью этого меню.</p>
<p>11. <strong>Сохранить как шаблон:</strong> нажмите эту кнопку и рассылка сохраниться как шаблон. Внимание! Название шаблона будет таким же как и название рассылки.</p>
<p>12. <strong>Запустить:</strong> кнопка для запуска Вашей рассылки. После этого статус рассылки станет “На модерации”. Модерация займет от 2 до 8 минут, после этого статус будет “В процессе”.</p>
<p><a href="/assets/images/instruction/newdistrib3.png"><img class="alignnone wp-image-106 size-full" src="/assets/images/instruction/newdistrib3.png" alt="Создание рассылки 3" /></a></p>
<h3>Тип рассылки</h3>
<p>В сервисе существует 2 типа рассылок: обычная и позвонил-сбросил, у последнего типа есть два подвида: позвонил сбросил (на большое количество номеров) и тестовая рассылка (позволяет добавить не больше десяти номеров и создается с помощью соответствующей галочки в создании рассылки)</p>
<table>
<tbody>
<tr>
<td></td>
<td style="text-align: center;" ><strong>Блокировки номера для перезвона (чтобы им никто не пользовался)</strong></td>
</tr>
<tr>
<td><strong>обычная</strong></td>
<td>номер не блокируется</td>
</tr>
<tr>
<td><strong>позвонил сбросил тестовая</strong></td>
<td>номер блокируется на 10 минут<br />
не более 10 номеров в рассылке</td>
</tr>
<tr>
<td><strong>позвонил сбросил</strong></td>
<td>номер блокируется на 24 часа<br />
не менее 1000 номеров в рассылке</td>
</tr>
<tr>
<td><strong>обычная с галочкой “блокировать номер”</strong></td>
<td>Номер блокируется на выбранное время</td>
</tr>
</tbody>
</table>
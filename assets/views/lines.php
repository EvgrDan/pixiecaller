<ul class="nav nav-tabs">
<li class="active" id="liness" onclick="showlines()"><a href="#">Линии</a></li>
<li class="" id="numberss" onclick="shownumbers()"><a href="#">Номера</a></li>
</ul>
<div id="lines">
<div>Количество свободных линий: <?php $_($countsfree);?></div>
<div>Количество доступных линий: <?php $_($counts);?></div>
<br>
<table class="table">
    <tr><th>Номер</th><th>Оператор</th><th>Доступна</th><th>Занята</th><th>Рассылка</th><th></th></tr>
    <?php foreach($lines as $line):?>
        <tr>
            <td>
                <?php $_($line->callorder);?>
            </td>
            <td>
                <?php $_($line->operator);?>
            </td>
            <td>
                <?php if($line->isenabled==1):?>
                <?php $_("Да");?>
                <?php else:?>
                <?php $_("Нет");?>
                <?php endif;?>
            </td>
            <td>
                <?php if($line->isbusy==1):?>
                <?php $_("Да");?>
                <?php else:?>
                <?php $_("Нет");?>
                <?php endif;?>
            </td>
			<td>
                <?php $_($line->distrid);?>
            </td>
            <td>
                <a class="btn" href=<?php $_( "/su/freeline/" . (string)$line->id);?>>Освободить</a>
            </td>
        </tr>
    <?php endforeach;?>
</table>
</div>
<div id="numbers" class="hide">
<table class="table">
    <tr><th>Номер</th><th>Статус</th><th>Доступен для</th><th>Рассылка</th><th>Чья рассылка</th><th>Код подтверждения</th><th></th></tr>
    <?php foreach($numbers as $num):?>
        <tr>
            <td>
                <?php $_($num->Number);?>
            </td>
            <td>
                <?php if($num->Status=="ForTest"):?>
                <?php $_("На тесте");?>
                <?php else:?>
                    <?php if($num->Status=="Available"):?>
                        <?php $_("Доступен");?>
                    <?php else:?>
                        <?php if($num->Status=="UserNum"):?>
                            <?php $_("Пользовательский");?>
                        <?php else:?>
                            <?php if($num->Status=="Busy"):?>
                                <?php $_("Занят");?>
                            <?php else:?>
                                <?php if($num->Status=="Accepted"):?>
                                    <?php $_("Не подтвержден");?>
                                <?php else:?>
                                    <?php $_($num->Status);?>
                                <?php endif;?>
                            <?php endif;?>
                        <?php endif;?>
                    <?php endif;?>
                <?php endif;?>
            </td>
            <td>
                <?php if($num->AvailableFor==-1):?>
                <?php $_("Всех");?>
                <?php else:?>
                <?php $_($this->pixie->orm->get('user')->where('id', $num->AvailableFor)->find()->Login);?>
                <?php endif;?>
            </td>
			<td>
                <?php $_($num->DistribID);?>
            </td>
            <td>
                <?php if($num->DistribID!=-1):?>
                <?php $_($this->pixie->orm->get('user')->where('id', $this->pixie->orm->get('distrib')->where('id', $num->DistribID)->find()->UserID)->find()->Login);?>
                <?php endif;?>
            </td>
            <td>
                <?php $_($num->VerCode);?>
            </td>
            <td>
                <a class="btn" href=<?php $_( "/su/freenumber/" . (string)$num->id);?>>Освободить</a>
            </td>
        </tr>
    <?php endforeach;?>
</table>
</div>
<script type="text/javascript">

    function showlines(){
        $("#lines").removeClass('hide');
        $("#liness").addClass('active');
        $("#numberss").removeClass('active');
        $("#numbers").addClass('hide');
    }
    
    function shownumbers(){
        $("#lines").addClass('hide');
        $("#liness").removeClass('active');
        $("#numberss").addClass('active');
        $("#numbers").removeClass('hide');
    }
    
</script>
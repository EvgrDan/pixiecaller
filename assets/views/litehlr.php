<div class="row ">
    <div class="col-xs-12 col-md-12 text-left">
        <div class="step">
        <div class="title-reg padbot0">
            ПРОВЕРКА БАЗЫ
        </div>
        <p>Стоимость проверки одного номера: <?php $_($hlrcost); ?> коп.</p>
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <form method="POST" id="newhlr" enctype="multipart/form-data">
            <fieldset>
                <div class="step">
                    <div class="row ">
                        <div class="col-xs-12 col-md-12 text-left">
                            <!--<div style="white-space: nowrap;">-->
                            <div class="title-step">
                                 Название
                            </div>
                            <!--<i name="basenamequest" style="display: inline-block;" class="icon-question-sign" data-toggle="tooltip" data-placement="right" title='используйте только латинские буквы или цифры без пробелов'></i>
                            </div>-->
                            <input class="field-form" name="hlrname"  type="text" placeholder="Введите название..." value="<?php $_($hlrname); ?>" required/>
                            <!--<div style="white-space: nowrap; margin-top: 20px;">-->
                            <div class="title-step">
                                 Загрузить базу
                            </div>
                            <!--<i name="basenamequest" style="display: inline-block;" class="icon-question-sign" data-toggle="tooltip" data-placement="right" title='допустимый формат - xls, номера должны находиться в столбце "A" первого листа и начинаться с "7", в столбце должны содержаться только номера, больше в документе ничего не должно быть'></i>
                            </div>-->
                            <input class="field-form" type="file" name="hlrfile"  accept="application/vnd.ms-excel" required>
                             <div>Внимание: вы не можете совершать более трех проверок в день!</div>
                             <div>Внимание 2: для номеров мегафона данный функционал дает погрешность для абонентов у которых "занято" (около 2-3% абонентов мегафона)</div>
                            <button type="submit" class="btn-form-small">Проверить</button>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="simple-title">Базы</div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="step">
            <div class="table-responsive">
                <table class="mailingtable table" cellpadding="5">
                    <tr><th>Название</th><th>Кол-во номеров</th><th>Статус</th><th>Время создания(МСК)</th><th>Управление</th></tr>
                    <?php foreach($hlrbases as $hlr):?>
                        <tr>
                            <td>
                                <?php $_($hlr->Name);?>
                            </td>
                            <td>
                                <?php $_($hlr->Size);?>
                            </td>
                            <td>
                                <?php $_($statuses[$hlr->Status]);?>
                            </td>
                            <td>
                                <?php $_($hlr->CreateTime);?>
                            </td>
                            <td>
                                <a href=<?php $_( "/admin/hlrbase/" . (string)$hlr->id . "?show=all");?> title="Вся база"><i style="color:#4D4D4D" class="fa fa-2x fa-circle-o"></i></a>
                                <a href=<?php $_( "/admin/hlrbase/" . (string)$hlr->id . "?show=reached");?> title="Доступные"><i style="color:#4D4D4D" class="fa fa-2x fa-check-circle-o"></i></a>
                                <a href=<?php $_( "/admin/hlrbase/" . (string)$hlr->id . "?show=notreached");?> title="Недоступные"><i style="color:#4D4D4D" class="fa fa-2x fa-times-circle-o"></i></a>
                            </td>
                        </tr>
                    <?php endforeach;?>
                </table>
            </div>
        </div>
    </div>
</div>
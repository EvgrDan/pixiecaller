<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="title-reg">
            ВХОД В ЛИЧНЫЙ КАБИНЕТ
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-3 text-center"></div>
    <div class="col-xs-12 col-md-6 text-center">
        <div class="box-form-reg">
            <form method="POST" id="newuser">
                <fieldset>
                    <input type="text" class="field-form field-email" name="Login" placeholder="Email...">
                    <input type="password" class="field-form field-pass" name="Password" placeholder="Пароль...">
                    <div  class="box-btn-form-reg">
                        <button type="submit" class="btn-form-reg">Войти</button>
                    </div>
                    <a class="btn" href="/admin/register">Зарегистрироваться</a>
                </fieldset>
                <a class="btn" id="fpass">Забыли пароль?</a>
            </form>
        </div>
    </div>
</div>
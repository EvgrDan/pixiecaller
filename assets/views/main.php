<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
        <title>Личный кабинет</title>
        <!--<?php if(explode("/", $_SERVER['REQUEST_URI'])[1]=="admin"):?>
        <?php echo('<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">');?>
        <?php endif ?> -->
        <meta name="robots" content="noindex, nofollow"/>
		<meta name="viewport" content="width=device-width">
        <link type="text/css"    rel="stylesheet" href="/assets/css/bootstrap.min.css"  />
        <link type="text/css"    rel="stylesheet" href="/assets/css/style.css"  />
        <link rel="stylesheet" type="text/css" href="/assets/css/sweetalert.css">
        <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js'></script>
        <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
        <!--<link href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
        <script type='text/javascript' src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>		-->
        <script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=cB1rVoKGBNQ7iec0sUMxw*0HOzLPm1e58QvceacQ1vllIQDDhxy/AVozjO*5KBFchvBVbTCOicdw5D3jTgwui*XcYmZXZmszazXpoL7XbIMcS3tJL0yUF/KvSYsyb/4EtG5q/R9iwvwmI9uHSe0xwr2MAqBzm3AWtCtYl2ziICI-';</script>
        <script src="/assets/js/jquery.validate.min.js"></script>
        <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">-->
        <link type="text/css" rel="stylesheet" href="/assets/css/font-awesome.css"> 
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
        <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-5MXF3K"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5MXF3K');</script>
        <!-- End Google Tag Manager -->
    </head>

    <!--<style>
        .error {
           color: red;
           font-size: 12px;
        }
    </style>-->
    <!--<body style="background-image: url(/assets/images/backgroundmain.jpg">-->
        <body>
        <header>
        <div class="container ">
			<div class="row ">
				<div class="col-xs-12 col-md-3">
                <?php if($this->pixie->orm->get('domain')->where('name', 'like', '%' . $_SERVER['SERVER_NAME'])->find()->loaded() and file_exists("/var/www/html/pixiecaller/web/assets/img/partnerlogo" . $this->pixie->orm->get('domain')->where('name', 'like', '%' . $_SERVER['SERVER_NAME'])->find()->UserID)): ?>
                    <img alt="" src="/assets/img/<?php echo("partnerlogo" . $this->pixie->orm->get('domain')->where('name', 'like', '%' . $_SERVER['SERVER_NAME'])->find()->UserID); ?>" width="42" height="42">
                <?php endif ?>
				</div>
                <?php if($this->pixie->auth->user() != null): ?>
				<div class="col-xs-12 col-md-6 text-right">
					<div>
						<div class="box-hello">
							Здравствуйте, <?php echo($this->pixie->auth->user()->Login);?>
						</div>
						<div class="box-balance">
							Ваш баланс: <?php $_($this->pixie->auth->user()->balance/100);?> руб.
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-3">
					<div>
						<a href= "/admin/logout">
							<!--<div class="exit floatr">
							</div>-->
                            <div class="floatr" style="margin-left:10px; margin-top:12px;">
                            <i style="color:white" class="fa fa-2x fa-sign-out"></i>
                            </div>
						</a>
                        <?php if($this->pixie->auth->user()->UserType == "User" or $this->pixie->auth->user()->UserType == "Partner"): ?>
                        <?php echo("<div style=\"cursor: pointer;\" class=\"btn-plus floatr\" onclick=\"showpay('block')\">ПОПОЛНИТЬ</div>");?>
                        <?php endif ?>
						<!--<div class="btn-plus floatr">
							ПОПОЛНИТЬ
						</div>-->
					</div>
				</div>
                <?php endif ?>
			</div>
		</div>
	</header>
    <?php if($this->pixie->auth->user() != null): ?>
    <?php if($this->pixie->auth->user()->UserType == "User" or $this->pixie->auth->user()->UserType == "Manager"): ?>
        <section class="main-menu" style="background:<?php echo($this->pixie->orm->get("contact")->where('id', $this->pixie->orm->get("user")->where("id", $this->pixie->auth->user()->Partner)->find()->Contacts)->find()->maincolor);?>">
    <?php else: ?>
        <?php if($this->pixie->auth->user()->UserType == "Partner"): ?>
            <section class="main-menu" style="background:<?php echo($this->pixie->orm->get("contact")->where('id', $this->pixie->auth->user()->Contacts)->find()->maincolor);?>">
        <?php else: ?>
            <section class="main-menu">
        <?php endif ?>
    <?php endif ?>
        <div class="container ">
			<div class="row ">
				<div class="col-xs-12 col-md-12">
                
					<nav role="navigation" class="navbar navbar-default">
					  <div class="navbar-header">
						<button type="button" data-target="#mainMenu" data-toggle="collapse" class="navbar-toggle">
						  <span class="sr-only">Меню</span>
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						</button>
					  </div>
					  <div id="mainMenu" class="collapse navbar-collapse">
						<ul class="nav navbar-nav">
                        <?php if($this->pixie->auth->user()->UserType == "User"): ?>
                            <?php if(explode("/", $_SERVER['REQUEST_URI'])[1]==""): ?>
                                <?php echo("<script type='text/javascript'> window.location.href = '/admin/distribs' </script>"); ?>
                            <?php endif ?>
                            <?php if(count(explode("/", $_SERVER['REQUEST_URI']))<3): ?>
                                <?php echo("<script type='text/javascript'> window.location.href = '/admin/distribs' </script>"); ?>
                            <?php endif ?>
                            <?php if(explode("/", $_SERVER['REQUEST_URI'])[1]=="admin"): ?>
                                <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="new_distrib" ? ' class="active"' : '') . '><a href="/admin/new_distrib">Создать рассылку</a></li>');?>
                                <?php echo('<li class="dropdowns "' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="distribs" ? ' class="active"' : '') . '><a href="/admin/distribs">Рассылки<span class="caret"></span></a>');?>
                                <?php echo('<ul class="dropdown-menus"><li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="record" ? ' class="active"' : '') . '><a href="/admin/record">Аудиоролики</a></li></ul></li>');?>
                                <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="base" ? ' class="active"' : '') . '><a href="/admin/base">База</a></li>');?>
                                <?php echo('<li class="dropdowns "' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="profile" ? ' class="active"' : '') . '><a href="/admin/profile">Профиль<span class="caret"></span></a>');?>
                                <?php echo('<ul class="dropdown-menus"><li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="phone" ? ' class="active"' : '') . '><a href="/admin/phone">Добавить номер</a></li></ul></li>');?>
                                <?php if($this->pixie->orm->get("contact")->where('id', $this->pixie->orm->get("user")->where("id", $this->pixie->auth->user()->Partner)->find()->Contacts)->find()->refon==1): ?>
                                    <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="refferal" ? ' class="active"' : '') . '><a href="/admin/refferal">Рефералы</a></li>');?>
                                <?php endif ?>
                                <?php if($this->pixie->orm->get("contact")->where('id', $this->pixie->orm->get("user")->where("id", $this->pixie->auth->user()->Partner)->find()->Contacts)->find()->hlron==1): ?>
                                    <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="litehlr" ? ' class="active"' : '') . '><a href="/admin/litehlr">Лайт HLR</a></li>');?>
                                <?php endif ?>
                                <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="smsreports" ? ' class="active"' : '') . '><a href="/admin/smsreports">СМС</a></li>');?>
                            <?php else: ?>
                                <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="newrobot" ? ' class="active"' : '') . '><a href="/coldcalls/newrobot">Создать робота</a></li>');?>
                                <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="statistics" ? ' class="active"' : '') . '><a href="/coldcalls/statistics">Статистика</a></li>');?>
                            <?php endif ?>
                        <?php endif ?>
                        <?php if($this->pixie->auth->user()->UserType == "Partner"): ?>
                            <?php if(explode("/", $_SERVER['REQUEST_URI'])[1]==""): ?>
                                <?php echo("<script type='text/javascript'> window.location.href = '/partner/clients' </script>"); ?>
                            <?php endif ?>
                            <?php if(count(explode("/", $_SERVER['REQUEST_URI']))<3): ?>
                                <?php echo("<script type='text/javascript'> window.location.href = '/partner/clients' </script>"); ?>
                            <?php endif ?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="clients" ? ' class="active"' : '') . '><a href="/partner/clients">Клиенты</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="managers" ? ' class="active"' : '') . '><a href="/partner/managers">Менеджеры</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="distribs" ? ' class="active"' : '') . '><a href="/partner/distribs">Рассылки</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="profile" ? ' class="active"' : '') . '><a href="/partner/profile">Профиль</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="events" ? ' class="active"' : '') . '><a href="/partner/events">События</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="analytics" ? ' class="active"' : '') . '><a href="/partner/analytics">Аналитика</a></li>');?>
                        <?php endif ?>
                        <?php if($this->pixie->auth->user()->UserType == "Manager"): ?>
                            <?php if(explode("/", $_SERVER['REQUEST_URI'])[1]==""): ?>
                                <?php echo("<script type='text/javascript'> window.location.href = '/partner/clients' </script>"); ?>
                            <?php endif ?>
                            <?php if(count(explode("/", $_SERVER['REQUEST_URI']))<3): ?>
                                <?php echo("<script type='text/javascript'> window.location.href = '/partner/clients' </script>"); ?>
                            <?php endif ?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="clients" ? ' class="active"' : '') . '><a href="/partner/clients">Клиенты</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="distribs" ? ' class="active"' : '') . '><a href="/partner/distribs">Рассылки</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="profile" ? ' class="active"' : '') . '><a href="/manager/profile">Профиль</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="events" ? ' class="active"' : '') . '><a href="/partner/events">События</a></li>');?>
                        <?php endif ?>
                        <?php if($this->pixie->auth->user()->UserType == "Admin"): ?>
                            <?php if(explode("/", $_SERVER['REQUEST_URI'])[1]==""): ?>
                                <?php echo("<script type='text/javascript'> window.location.href = '/su/distribs' </script>"); ?>
                            <?php endif ?>
                            <?php if(count(explode("/", $_SERVER['REQUEST_URI']))<3): ?>
                                <?php echo("<script type='text/javascript'> window.location.href = '/su/distribs' </script>"); ?>
                            <?php endif ?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="distribs" ? ' class="active"' : '') . '><a href="/su/distribs">Рассылки</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="partners" ? ' class="active"' : '') . '><a href="/su/partners">Партнеры</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="users" ? ' class="active"' : '') . '><a href="/su/users">Пользователи</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="profile" ? ' class="active"' : '') . '><a href="/su/profile">Профиль</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="events" ? ' class="active"' : '') . '><a href="/su/events">События</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="lines" ? ' class="active"' : '') . '><a href="/su/lines">Линии</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="analytics" ? ' class="active"' : '') . '><a href="/su/analytics">Аналитика</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="smsreports" ? ' class="active"' : '') . '><a href="/su/smsreports">Смс</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="news" ? ' class="active"' : '') . '><a href="/su/news">Новости</a></li>');?>
                            <?php echo('<li' . (explode("/", $_SERVER['REQUEST_URI'])[2]=="record" ? ' class="active"' : '') . '><a href="/su/record">Аудиозаписи</a></li>');?>
                        <?php endif ?>
						</ul>
					  </div>
					</nav>


				</div>
			</div>
		</div>
    </section>
    <?php endif ?>
        
        
    
                <!--
                    Here we will include the file
                    specified in the $subview
                 -->
        
        <?php if($this->pixie->orm->get('domain')->where('name', 'like', '%' . $_SERVER['SERVER_NAME'])->find()->loaded() and file_exists("/var/www/html/pixiecaller/web/assets/img/partnerback" . $this->pixie->orm->get('domain')->where('name', 'like', '%' . $_SERVER['SERVER_NAME'])->find()->UserID)): ?>
            <section class="big-container " style="background: #E6E6E6 url(../img/<?php echo("partnerback" . $this->pixie->orm->get('domain')->where('name', 'like', '%' . $_SERVER['SERVER_NAME'])->find()->UserID); ?>) top center repeat;">
        <?php else: ?>
            <section class="big-container">
        <?php endif ?>
            <div class="container ">
                <div class="row ">
                    <?php if($this->pixie->auth->user() != null): ?>
                        <?php if($this->pixie->auth->user()->UserType == "User"): ?>
                            <?php if($this->pixie->orm->get('distrib')->where("UserID", $this->pixie->auth->user()->id)->count_all()<3): ?>
                                <div class="row ">
                                    <div class="col-xs-12 col-md-2 text-center"></div>
                                    <div class="col-xs-12 col-md-8 text-center">
                                        <a href= "/admin/test"><div class="btn-form-big-red w100" name="testrass" style="cursor: pointer;">Тестовая рассылка</div></a>
                                    </div>
                                    </div>
                                </div>
                            <?php endif ?>
                            <?php if($this->pixie->auth->user()->NewsRead==0 and $this->pixie->auth->user()->NewsTo==1 and $this->pixie->orm->get("news")->count_all()>0 and $this->pixie->orm->get("contact")->where('id', $this->pixie->orm->get("user")->where("id", $this->pixie->auth->user()->Partner)->find()->Contacts)->find()->newson==1): ?>
                            <div id="newswindow" class="green-window window">
                              <a href="#" class="delete-window" data-dismiss="alert" style="margin:0" aria-label="close" onclick="newsdisable()"><div class="udalit"></div></a>
                              <div class="new-title"><?php echo($this->pixie->orm->get("news")->where('AvailableFor', -1)->where('or', array('AvailableFor', $this->pixie->auth->user()->Partner))->order_by("id", "desc")->find()->Name);?></div>
                              <div class="new-text"><?php echo($this->pixie->orm->get("news")->where('AvailableFor', -1)->where('or', array('AvailableFor', $this->pixie->auth->user()->Partner))->order_by("id", "desc")->find()->Text);?></div>
                            </div>
                            <?php endif ?>
                            <?php if($this->pixie->auth->user()->NotifyRead==0 and $this->pixie->auth->user()->NotifyTo==1 and $this->pixie->orm->get("notification")->where("AvailableFor", -1)->where('or', array("AvailableFor", $this->pixie->auth->user()->id))->count_all()>0  and $this->pixie->orm->get("contact")->where('id', $this->pixie->orm->get("user")->where("id", $this->pixie->auth->user()->Partner)->find()->Contacts)->find()->notifyon==1): ?>
                            <div id="notifywindow" class="red-window window">
                              <a href="#" class="delete-window" data-dismiss="alert" style="margin:0" aria-label="close" onclick="notifydisable()"><div class="udalit"></div></a>
                              <div class="new-title"><?php echo($this->pixie->orm->get("notification")->where('AvailableFor', -1)->where('or', array('AvailableFor', $this->pixie->auth->user()->id))->order_by("id", "desc")->find()->Name);?></div>
                              <div class="new-text"><?php echo($this->pixie->orm->get("notification")->where('AvailableFor', -1)->where('or', array('AvailableFor', $this->pixie->auth->user()->id))->order_by("id", "desc")->find()->Text);?></div>
                            </div>
                            <?php endif ?>
                        <?php endif ?>
                    <?php endif ?>
                    <?php $colors = array(0 => "#C0C0C0", 1 => "#008000", 2 => "#FF0000", 3 => "#000000", 4 => "#FF8C00"); ?>
                    <?php include($subview.'.php');?>
                </div>
            </div>
        </section>
        <footer>
            <div class="container ">
                <div class="row ">
                    <div class="col-xs-12 col-md-3">
                    </div>
                    <div class="col-xs-12 col-md-6 text-center">
                        <div class="text-foot">СЕРВИС ГОЛОСОВЫХ РАССЫЛОК</div>
                        <?php $dom = $this->pixie->orm->get('domain')->where('name', 'like', '%' . $_SERVER['SERVER_NAME'])->find(); ?>
                        <?php if($dom->loaded()): ?>
                        <?php $part = $this->pixie->orm->get('user')->where('id', $dom->UserID)->find();?>
                        <?php if($part->Contacts!=NULL):?>
                             <?php $contact1 = $this->pixie->orm->get("contact")->where("id", $part->Contacts)->find();?>
                        <?php endif ?>
                        <?php if($part->Contacts!=NULL and $contact1->loaded()):?>
                        <div  class="box-footer-contakt">
                            <div class="mail-footer"><i style="color:white; margin-right:4px;" class="fa fa-1x fa-envelope"></i><a href="mailto:<?php echo($contact1->UserEmail);?>"><?php echo($contact1->UserEmail);?></a></div>
                            <div  class="phone-footer"><i style="color:white; margin-right:4px;" class="fa fa-1x fa-phone-square"></i><a href="tel:<?php echo($contact1->UserPhone);?>"><?php echo($contact1->UserPhone);?></a></div>
                        </div>
                        <?php endif ?>
                        <div class="text-foot text-foot2">
                            <a href="/admin/terms">Пользовательское соглашение </a>/<a href="/admin/apiinfo">API для рассылок</a>
                        </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </footer>

        
        <div onclick="showpay('none')" id="wrappay" style="display: none;"></div>
        <div id="windowpay" style="display: none;">
            <center>
                <form class="form-inline" id="changetar" method="POST">
                    <fieldset>
                        <legend>Пополнение баланса</legend>
                        <?php if($this->pixie->auth->user() != null and $this->pixie->auth->user()->UserType == "User"): ?>
                        <div><?php echo($this->pixie->orm->get("contact")->where('id', $this->pixie->orm->get("user")->where("id", $this->pixie->auth->user()->Partner)->find()->Contacts)->find()->requisites); ?></div>
                        <?php endif ?>
                        <?php if($this->pixie->auth->user() != null and $this->pixie->auth->user()->UserType == "Partner"): ?>
                        <div><?php echo($this->pixie->orm->get("contact")->where('id', $this->pixie->orm->get("user")->where("UserType", "Admin")->find()->Contacts)->find()->requisites); ?></div>
                        <?php endif ?>
                        <?php if($this->pixie->auth->user() != null and $this->pixie->auth->user()->UserType == "User" and $this->pixie->orm->get("contact")->where('id', $this->pixie->orm->get("user")->where("id", $this->pixie->auth->user()->Partner)->find()->Contacts)->find()->w1on==1): ?>
                        <legend>Оплата электронными деньгами</legend>
                        <i id="amountquest" style="display: inline-block;" class="icon-question-sign" data-toggle="tooltip" data-placement="left" title="До 1000 р. - 7% комиссии, более 1000р. - 2% комиссии"></i>
                        <input class="field-form" type="number" style="height:auto" min="0" name="amount" value="1000" id="amount" placeholder="Сумма...">
                        <div style="cursor: pointer;" id="paybtn" class="btn-red" onclick="w1pay()">Оплатить</div>
                        <br>
                        <div>Сумма к оплате:</div>
                        <div id="amountpercent"></div>
                        <?php endif ?>
                    </fieldset>
                </form>
            </center>
        </div>
<style type="text/css">
	#wrappay{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
	
	#windowpay{
		width: 400px;
		height: 400px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
    
	.close{
		margin-left: 364px;
		margin-top: 4px;
		cursor: pointer;
	}
	
</style>

<script type="text/javascript">
 
    $('i').on('click',function(){$(this).tooltip('show');});
            //Функция показа
    function showpay(state){
        document.getElementById('windowpay').style.display = state;            
        document.getElementById('wrappay').style.display = state;
        if($("#amount").val()>0 && $("#amount").val()<1000){
           $("#amountpercent").text(($("#amount").val()*1.07).toFixed(2));
       }
       else if($("#amount").val()>=1000){
           $("#amountpercent").text(($("#amount").val()*1.02).toFixed(2));
       }
       else{
           $("#amountpercent").text((0).toFixed(2));
       }
    }
    
    function newsdisable(){
        $.ajax({
          type: "POST",
          url: "/admin/disablenews",
          success: function(data){
            $("#newswindow").addClass("hide");
          }
        });
    }
    
    function notifydisable(){
        $.ajax({
          type: "POST",
          url: "/admin/disablenotify",
          success: function(data){
            $("#notifywindow").addClass("hide");
          }
        });
    }
    
    function w1pay(){
        $.ajax({
          type: "POST",
          url: "/admin/w1pay",
          data: "amount="+$('input[name="amount"]').val(),
          success: function(data){
            // alert( data );
            form = $.parseHTML( data );
            $(form).submit();
            showpay('none');
          }
        });
    }
    
    $( "#amount" ).on('input', function() {
       if($("#amount").val()>0 && $("#amount").val()<1000){
           $("#amountpercent").text(($("#amount").val()*1.07).toFixed(2));
       }
       else if($("#amount").val()>=1000){
           $("#amountpercent").text(($("#amount").val()*1.02).toFixed(2));
       }
       else{
           $("#amountpercent").text((0).toFixed(2));
       }
    });
     
</script>

		<script src="/assets/js/newdistr-form.js"></script>
        <script src="/assets/js/newbase-form.js"></script>
        <script src="/assets/js/newphone-form.js"></script>
        <script src="/assets/js/changeuser-form.js"></script>
        <script src="/assets/js/cookie_saver.js"></script>
        <script src="/assets/js/newuser-form.js"></script>
        <script src="/assets/js/newrecord-form.js"></script>
        <script src="/assets/js/promo-form.js"></script>
        <script src="/assets/js/changedefault-form.js"></script>
        <script src="/assets/js/jquery.tablesorter.js"></script>
        <script src="/assets/js/sweetalert.min.js"></script>
        <script src="/assets/js/distribs-form.js"></script>
        <script src="/assets/js/pace.min.js"></script> 
        <script> 
        $('a.btnpodskazka').tooltip({ 
        trigger: 'hover focus click', 
        }) 

        $('a.btnpodskazka').click(function(){ 
        return false; 
        }); 
        </script>
    </body>
</html>

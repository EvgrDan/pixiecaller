<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="title-reg">
            МЕНЕДЖЕРЫ
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="btn-red" style="cursor: pointer;" onclick="show('block')"><i style="color:#FFF" class="fa fa-1x  fa-plus-circle"></i> ДОБАВИТЬ</div>
    </div>
</div>
<br></br>
<?php if(count($managers)==0):?>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div>У вас еще нет ни одного менеджера. Как только вы добавите менеджера, все ваши клиенты будут привязаны к нему.</div>
    </div>
</div>
<?php else:?>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="table-responsive">
            <table class="mailingtable table" cellpadding="5">
                <tr><th>Email</th><th>Кол-во клиентов</th></tr>
                <?php foreach($managers as $manager):?>
                    <tr>
                        <td>
                            <?php $_($manager->Login);?>
                        </td>
                        <td>
                            <?php $_($uscnt[$manager->id]);?>
                        </td>
                    </tr>
                <?php endforeach;?>
            </table>
        </div>
    </div>
</div>
<?php endif;?>

<div onclick="show('none')" id="wrap" style="display: none;"></div>
<div id="window" style="display: none;">
    <center>
        <legend>НОВЫЙ МЕНЕДЖЕР</legend>
        <form method="POST" class="form-horizontal" id="newmanager" enctype="multipart/form-data" autocomplete="off">
            <fieldset>
                <!-- fake fields are a workaround for chrome autofill getting the wrong fields -->
                <input style="display:none" type="text" name="fakeusernameremembered"/>
                <input style="display:none" type="password" name="fakepasswordremembered"/>
                <input autocomplete="off" name="Login" id = "Login" type="text" class="field-form field-email" placeholder="Email..." required/>
                <input autocomplete="off" name="Phone" id = "Phone" type="text" class="field-form field-phone" placeholder="Телефон..." required/>
                <input autocomplete="off" name="Password" type="password" class="field-form field-pass" placeholder="Пароль..." required/>
                <input class="btn-form-big" type="submit" name="submitbtn" value="ДОБАВИТЬ">
            </fieldset>
        </form>
    </center>
</div>

<style type="text/css">
	#wrap{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
	
	#window{
		width: 400px;
		height: 370px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
    
</style>

<script>
    function show(state){;
        document.getElementById('window').style.display = state;            
        document.getElementById('wrap').style.display = state;
    }
</script>
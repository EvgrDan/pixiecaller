
<div class="col-xs-12 col-md-2 text-center"></div>
<div class="col-xs-12 col-md-8 text-center">
<div class="box-form-reg-all">
    <div class="step">
    <div class="row ">
        <div class="col-xs-12 col-md-12 text-left">
            <div class="title-reg-l">
                СОЗДАНИЕ НОВОЙ РАССЫЛКИ
            </div>
        </div>
    </div>
    <div class="row ">
        <div class="col-xs-12 col-md-12 text-left">
            <div class="title-reg-small ">
                <a href="/admin/instruction">Инструкция по работе в системе</a>
            </div>
        </div>
    </div>
    </div>

    <form method="POST" class="form-horizontal" id="newdistr" enctype="multipart/form-data">
        <fieldset>

            <div class="step-1 step">
                <div class="row ">
                    <div class="col-xs-12 col-md-12 text-left">
                        <div class="title-step">
                           Название рассылки
                        </div>
                        <input class="field-form" id="distribname" name="distribname" type="text" placeholder="Введите название..." value="<?php $_($distrname) ?>" required>
                        <input type="checkbox" name="chkPat" id="pattern">
                        <label type="checkbox" for="pattern" class="checkbox" name="patLbl">
                            Загрузить из шаблона
                        </label>
                        <div class="field-form-sel-box" id="sel5div">
                        <select class="field-form-sel" id="sel5" form="newdistr" name="patternsel">
                        <option value="0">
                                Нет
                        </option>
                        <?php foreach($patterns as $pat):?>
                            <option value="<?php $_($pat->id);?>">
                                <?php $_($pat->Name);?>
                            </option>
                        <?php endforeach;?>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="line"></div>

             <div class="step-2 step">
                 <div class="row ">
                     <div class="col-xs-12 col-md-12 text-left">
                        <div class="title-step">
                             Источник номеров
                        </div>
                        <div class="btn-group for-btn" data-toggle="buttons">
                            <label class="btn-grey floatl" name="baseclick" required>
                                <input type="radio" id="q158" name="uploadtype" value="base" /> Из базы
                            </label>
                            <label class="btn-red floatl active" name="numbersclick">
                                <input type="radio" id="q159" name="uploadtype" value="numbers" />Ввести номера
                            </label>
                        </div>
                         <textarea name="testNum" class="field-form" rows="1" placeholder="7XXXXXXXXXX, 7XXXXXXXXXX, ..." required></textarea>
                         <div class="field-form-sel-box" id="distrbasedev">   
                             <select class="field-form-sel" id="sel2" form="newdistr" name="distribbase" required>
                             <option disabled selected>База номеров</option>
                             <?php foreach($bases as $base):?>
                                <option value="<?php $_($base->id);?>" tag="<?php $_($base->Size);?>">
                                    <?php $_($base->BaseName . " (номеров: " . $base->Size . ")");?>
                                </option>
                             <?php endforeach;?>
                             </select>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="line"></div>

             <div class="step-3 step">
                 <div class="row ">
                     <div class="col-xs-12 col-md-12 text-left">
                         <div class="title-step">
                             Номер отправителя
                         </div>
                        <div class="field-form-sel-box">
                            <select class="field-form-sel fa" id="sel1" form="newdistr" name="distribnumber" required>
                            <option disabled selected="selected">Номер отправителя</option>
                            <?php foreach($numbers as $number):?>
                                <option value="<?php $_($number->Number);?>" <?php echo($number->Status=="Available" ? "id='setblock'" : "");?> class="fa">
                                    <?php if($number->Status=="Available"):?>
                                    &#xf0c0;
                                    <?php else: ?>
                                    &#xf007;
                                    <?php endif; ?>
                                    <?php $_($number->Number);?>
                                </option>
                            <?php endforeach;?>
                            <?php foreach($numbersall as $number):?>
                                <option value="<?php $_($number->Number);?>" <?php echo($number->Status=="Available" ? "id='setblock'" : "");?> class="fa">
                                    <?php if($number->Status=="Available"):?>
                                    &#xf0c0; 
                                    <?php else: ?>
                                    &#xf007; 
                                    <?php endif; ?>
                                    <?php $_($number->Number);?>
                                </option>
                            <?php endforeach;?>
                            <?php foreach($numbersuser as $number):?>
                                <option value="<?php $_($number->Number);?>" <?php echo($number->Status=="Available" ? "id='setblock'" : "");?> class="fa">
                                    <?php if($number->Status=="Available"):?>
                                    &#xf0c0;
                                    <?php else: ?>
                                    &#xf007;
                                    <?php endif; ?>
                                    <?php $_($number->Number);?>
                                </option>
                            <?php endforeach;?>
                            <option id="href" value="/admin/phone" class="fa">&#xf067; Добавить номер</option>
                            </select>
                            <select class="field-form-sel fa" id="sel3" form="newdistr" name="distribordnumber" required>
                            <option disabled selected="selected">Номер отправителя</option>
                            <?php foreach($numbersord as $number):?>
                                <option value="<?php $_($number->Number);?>" <?php echo($number->Status=="Available" ? "id='setblock'" : "");?> class="fa">
                                    <?php if($number->Status=="Available"):?>
                                    &#xf0c0;
                                    <?php else: ?>
                                    &#xf007;
                                    <?php endif; ?>
                                    <?php $_($number->Number);?>
                                </option>
                            <?php endforeach;?>
                            <?php foreach($numbersall as $number):?>
                                <option value="<?php $_($number->Number);?>" <?php echo($number->Status=="Available" ? "id='setblock'" : "");?> class="fa">
                                    <?php if($number->Status=="Available"):?>
                                    &#xf0c0;
                                    <?php else: ?>
                                    &#xf007;
                                    <?php endif; ?>
                                    <?php $_($number->Number);?>
                                </option>
                            <?php endforeach;?>
                            <?php foreach($numbersuser as $number):?>
                                <option value="<?php $_($number->Number);?>" <?php echo($number->Status=="Available" ? "id='setblock'" : "");?> class="fa">
                                    <?php if($number->Status=="Available"):?>
                                    &#xf0c0;
                                    <?php else: ?>
                                    &#xf007;
                                    <?php endif; ?>
                                    <?php $_($number->Number);?>
                                </option>
                            <?php endforeach;?>
                            <option id="href" value="/admin/phone" class="fa">&#xf067; Добавить номер</option>
                            </select>
                        </div>
                        <!--<div style="white-space: nowrap;">-->
                        <input type="checkbox" id="chkblocknum" name="chkblocknum">
                        <label type="checkbox" for="chkblocknum" class="checkbox" name="blocknumLbl">
                             Блокировать номер на:
                        </label>
                        <!--<i name="blocknumquest" style="display: inline-block;" class="icon-question-sign" data-toggle="tooltip" data-placement="right" title="При тестовой рассылке всегда блокируется на 10 минут"></i>
                        </div>-->
                        <div class="field-form-sel-box">
                        <select class="field-form-sel" id="selblock" form="newdistr" name="blockon">
                            <option value="1">1 час</option>
                            <option value="2">2 часа</option>
                            <option value="6">6 часов</option>
                            <option value="12">12 часов</option>
                            <option value="18">18 часов</option>
                            <option value="24" selected="selected">24 часа</option>
                            <option value="36">36 часов</option>
                        </select>
                        </div>
                     </div>
                 </div>
             </div>
             <div class="line"></div>
            
            <div class="step-4 step">
                 <div class="row ">
                     <div class="col-xs-12 col-md-12 text-left">
                         <div class="title-step">
                             Аудиоролик или текст
                         </div>
                         <div class="btn-group for-btn" data-toggle="buttons">
                            <label class="btn-grey floatl" name="textclick" required>
                                <input type="radio" id="q156" name="recordtype" value="text" />Ввести текст
                            </label>
                            <label class="btn-red floatl active" name="fileclick">
                                <input type="radio" id="q157" name="recordtype" value="record" />Загрузить
                            </label>
                            <label class="btn-grey floatl" name="libclick">
                                <input type="radio" id="q155" name="recordtype" value="fromlib" />Из базы
                            </label>
                         </div>
                         
                        <textarea name="welcomeText" class="field-form" rows="1" placeholder="Текст приветствия..." required></textarea>
                        <input class="field-form" type="file" name="welcome"  accept="audio/wav,.mp3">
                        <audio controls="controls" id="welcomeaudioplayer" class="hide">
                          Your browser does not support the <code>audio</code> element.
                          <source id="welcomeaudio" src="" type="audio/wav">
                        </audio>
                        
                        <div class="field-form-sel-box" id="seldevrec">
                        <select class="field-form-sel" id="selrec" form="newdistr" name="reclib" required>
                        <option disabled selected>Загруженные ролики</option>
                        <?php foreach($records as $record):?>
                            <option value="<?php $_($record->id);?>">
                                <?php $_($record->name);?>
                            </option>
                        <?php endforeach;?>
                        </select>
                        </div>
                     </div>
                 </div>
            </div>
            <div class="line"></div>
            
             <div class="step-5 step">
                 <div class="row ">
                     <div class="col-xs-12 col-md-12 text-left">
                        <input id="golos" type="checkbox" name="ivrcheck" value="Голосовое меню">
                        <label type="checkbox" for="golos" class="checkbox" name="voicelbl">Голосовое меню (обработка нажатия цифр)</label>
                        <div id="ivrmenu">
                            
                            <input id="ivr1" type="checkbox" name="chk1">
                            <label style="margin-left: 20px;" type="checkbox" class="checkbox" name="lblivr1" for="ivr1"> Цифра 1, аудиоролик при нажатии
                            </label>
                                <input class="field-form" style="margin-left: 40px;" type="file" name="ivr1" accept="audio/wav,.mp3">
                                <audio style="margin-top: 20px;" controls="controls" id="ivr1audioplayer" class="hide">
                                  Your browser does not support the <code>audio</code> element.
                                  <source id="ivr1audio" src="" type="audio/wav">
                                </audio>
                            <input id="tonum" type="checkbox" name="tn"> 
                            <label class="checkbox" style="margin-left: 40px;" type="checkbox" for="tonum" name="lblivrtonumber">
                                При нажатии на цифру 1 перезвонить на номер (начинается с "7", без пробелов)
                            </label>
                                <input class="field-form" style="margin-left: 40px;" onkeyup="this.value = this.value.replace (/\D/, '')" id = "tonumber" name="tonumber" type="text" placeholder="Введите номер..."/>
                            <input id="smsto" type="checkbox" name="smsto">
                            <label class="checkbox" style="margin-left: 40px;" type="checkbox" for="smsto" name="lblsmstonumber">
                                 При нажатии на цифру 1 отправить СМС
                            </label>
                                <!--<div style="white-space: nowrap;">-->
                                <label style="margin-left: 20px; display: inline-block;" name="smstextlbl">Текст смс (не более 70 символов)</label>
                                <!--<a href="#" style="display: inline-block;" class="btnpodskazka" data-toggle="tooltip" data-placement="right" title="" data-original-title="При отправке номеров телефонов и ссылок на номера абонентов Билайн смс не пройдет"><i class="fa fa-question"></i></a>
                                <i name="smstoquest" style="display: inline-block;" class="icon-question-sign" data-toggle="tooltip" data-placement="right" title="При отправке номеров телефонов и ссылок на номера абонентов Билайн смс не пройдет"></i>
                                </div>-->
                                <textarea style="margin-left: 40px;" name="smstext" class="field-form" rows="3" maxlength="70"></textarea>
                            <input id="ivr2" type="checkbox" name="chk2">
                            <label style="margin-left: 20px;" type="checkbox" for="ivr2" class="checkbox" name="lblivr2">
                                Цифра 2
                            </label>
                            <input class="field-form" style="margin-left: 40px;" type="file" name="ivr2" accept="audio/wav,.mp3">
                            <audio style="margin-top: 20px;" controls="controls" id="ivr2audioplayer" class="hide">
                              Your browser does not support the <code>audio</code> element.
                              <source id="ivr2audio" src="" type="audio/wav">
                            </audio>
                            <input type="checkbox" name="chk3" id="ivr3">
                            <label style="margin-left: 20px;" type="checkbox" for="ivr3" class="checkbox" name="lblivr3">
                                Цифра 3
                            </label>
                            <input  class="field-form" style="margin-left: 40px;" type="file" name="ivr3" accept="audio/wav,.mp3">
                            <audio style="margin-top: 20px;" controls="controls" id="ivr3audioplayer" class="hide">
                              Your browser does not support the <code>audio</code> element.
                              <source id="ivr3audio" src="" type="audio/wav">
                            </audio>
                            <!--<div style="white-space: nowrap;">-->
                            <input id="block3" type="checkbox" name="chkblock">
                            <label style="margin-left: 40px;" type="checkbox" for="block3" class="checkbox" name="chkblocklbl">
                                Добавлять номер в черный список при нажатии цифры 3
                            </label>
                            <!--<i name="blacklistquest" style="display: inline-block;" class="icon-question-sign" data-toggle="tooltip" data-placement="right" title="Добавление в черный список производится один раз в сутки"></i>
                            </div>-->
                        </div>

                     </div>
                 </div>
             </div>
             <div class="line"></div>
             
             <div class="step-6 step">
                 <div class="row ">
                     <div class="col-xs-12 col-md-12 text-left">
                         <div class="title-step">
                             Скорость рассылки
                         </div>
                         <div class="field-form-sel-box">
                            <select class="field-form-sel" id="sel4" form="newdistr" name="distrspeed" required>
                            <?php foreach($speeds as $speed):?>
                                <option value="<?php $_($speed->id);?>">
                                    <?php $_($speed->Value);?> в час
                                </option>
                            <?php endforeach;?>
                            </select>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="line"></div>
             <div class="step-7 step">
                 <div class="row ">
                     <div class="col-xs-12 col-md-12 text-left">
                         <div class="title-dop">
                             Дополнительные настройки
                         </div>
                         <a name = "showfields" style="cursor: pointer;">Показать</a>
                         
                        <!--<div style="white-space: nowrap;">-->
                        <label style="" name="typelabel">Тип рассылки
                                <a href="#" class="btnpodskazka" data-toggle="tooltip" data-placement="right otleft" title="При обычной рассылке скорость зависит от длины ролика"><i class="fa fa-question-circle"></i></a></label>
                        <!--<i name="distribtypequest" style="display: inline-block;" class="icon-question-sign" data-toggle="tooltip" data-placement="right" title="При обычной рассылке скорость зависит от длины ролика"></i>
                        </div>-->
                        <div class="btn-group for-btn" data-toggle="buttons" required>
                            <label class="btn-grey floatl" name="ordclick">
                                <input type="radio" id="q160" name="distribtype" value="ordinary"/> Обычная
                            </label>
                            <label class="btn-red floatl active" name="recallclick">
                                <input type="radio" id="q161" name="distribtype" value="recall" />Позвонил-сбросил
                            </label>
                        </div>
                        <input type="checkbox" name="chkTest" id="chkTest">
                        <label type="checkbox" class="checkbox" name="tstLbl" for="chkTest">
                            Тестовая рассылка (не более 10 номеров)
                        </label>
                        <input type="checkbox" name="chkplan" id="chkplan">
                        <label type="checkbox" class="checkbox" for="chkplan" name="lblplan">
                            Запланировать рассылку
                        </label>
                        <label style="margin-top: 20px;" name="lblbegin">Время начала (МСК)</label>
                        <div class='input-group date field-form' id='datetimepicker1'>
                            <input type='text' class="form-control" name="datetime" id="datetime" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        <label style="margin-top: 20px;" name="commentlbl">Комментарий (не более 300 символов)</label>
                        <textarea name="distribcomment" class="field-form" rows="3" maxlength="300" placeholder="Текст комментария..."></textarea>
                        
                        <label class="btn-form-big-grey" style="margin-top: 20px;" name="patternbtn">Сохранить как шаблон</label>
                     </div>
                 </div>
             </div>
             <div class="step-8 step">
                <input class="btn-form-big" type="submit" name="submitbtn" value="ЗАПУСТИТЬ РАССЫЛКУ">
             </div>


            <!--<div style="white-space: nowrap;">
            <label style="margin-top: 20px; display: inline-block;">Название</label>
            <i name="distribnamequest" style="display: inline-block;" class="icon-question-sign" data-toggle="tooltip" data-placement="right" title="Используйте только латинские буквы без пробелов"></i>
            </div>
            <input id="distribname" name="distribname" type="text" placeholder="Введите название..." value="<?php $_($distrname) ?>" required/>-->
            
            <!--<label style="margin-top: 20px;" name="numsource">Источник номеров (не более 10 для тестовой и не менее 1000 для не тестовой)</label>
            <label style="margin-top: 20px;" name="numsource1">Источник номеров</label>
            <div class="btn-group" data-toggle="buttons">
                <label name="baseclick" class="btn btn-default" required>
                    <input type="radio" id="q158" name="uploadtype" value="base" /> Из базы
                </label>
                <label name="numbersclick" class="btn btn-default active">
                    <input type="radio" id="q159" name="uploadtype" value="numbers"  />Ввести номера
                </label>
            </div>-->
            
            
            <!--<label style="margin-top: 20px;" name="welcomelbl">Приветствие</label>
            <div class="btn-group" data-toggle="buttons">
                <label name="textclick" class="btn btn-default" required>
                    <input type="radio" id="q156" name="recordtype" value="text" /> Ввести текст
                </label>
                <label name="fileclick" class="btn btn-default active">
                    <input type="radio" id="q157" name="recordtype" value="file"  /> Загрузить файл
                </label>
            </div>-->
            
            
            <!-- НЕОБЯЗАТЕЛЬНЫЕ ПОЛЯ -->
            
            <!--
            <br></br>
            <button type="submit" class="btn btn-primary" name="submitbtn">Запустить</button>
            -->
        </fieldset>
    </form>
</div>
</div>
<form method="POST" class="form-horizontal" id="newrobot" enctype="multipart/form-data">
    <fieldset>
        <legend>Новый робот</legend>
        <label style="margin-top: 20px;">Название</label>
        <i name="robotnamequest" class="icon-question-sign" data-toggle="tooltip" data-placement="right" title="Используйте только латинские буквы без пробелов"></i>
        <input id="robotname" name="robotname" type="text" placeholder="Введите название..." required/>
        </select>
        <label style="margin-top: 20px;" name="numsourcerobot1">Источник номеров</label>
        <div class="btn-group" data-toggle="buttons">
            <label name="baseclickrobot" class="btn btn-default" required>
                <input type="radio" id="q160" name="basetype" value="base" /> Из базы
            </label>
            <label name="numbersclickrobot" class="btn btn-default active">
                <input type="radio" id="q161" name="basetype" value="numbers"  />Ввести номера
            </label>
        </div>
        <textarea name="numberscall" style="margin-top: 5px;" class="form-control" rows="1" placeholder="7XXXXXXXXXX, 7XXXXXXXXXX, ..." required></textarea>
        
        <select style="margin-top: 5px;" class="form-control" id="selbase" form="newrobot" name="robotbase" required>
        <option disabled selected="selected">База номеров</option>
        <?php foreach($bases as $base):?>
            <option value="<?php $_($base->id);?>" tag="<?php $_($base->Size);?>">
                <?php $_($base->BaseName . " (номеров: " . $base->Size . ")");?>
            </option>
        <?php endforeach;?>
        </select>
        <select style="margin-top: 20px;" class="form-control" id="sel3" form="newrobot" name="robotordnumber" required>
        <option disabled selected="selected">Номер отправителя</option>
        <?php foreach($numbersord as $number):?>
            <option value="<?php $_($number->Number);?>">
                <?php $_($number->Number);?>
            </option>
        <?php endforeach;?>
        <?php foreach($numbersall as $number):?>
            <option value="<?php $_($number->Number);?>">
                <?php $_($number->Number);?>
            </option>
        <?php endforeach;?>
        <?php foreach($numbersuser as $number):?>
            <option value="<?php $_($number->Number);?>">
                <?php $_($number->Number);?>
            </option>
        <?php endforeach;?>
        </select>
        <label style="margin-top: 20px;" name="audioslbl">Аудиоролики</label>
        <label name="lblaudio1"> Аудиоролик 1 </label>
        <input type="file" name="audio1" accept="audio/wav,.mp3">
        <label name="lblaudio2"> Аудиоролик 2 </label>
        <input type="file" name="audio2" accept="audio/wav,.mp3">
        <label name="lblaudio3"> Аудиоролик 3 </label>
        <input type="file" name="audio3" accept="audio/wav,.mp3">
        <label name="lblaudio4"> Аудиоролик 4 </label>
        <input type="file" name="audio4" accept="audio/wav,.mp3">
        <label name="lblaudio5"> Аудиоролик 5 </label>
        <input type="file" name="audio5" accept="audio/wav,.mp3">
        <label name="lblaudio6"> Аудиоролик 6 </label>
        <input type="file" name="audio6" accept="audio/wav,.mp3">
        <label name="lblaudio7"> Аудиоролик 7 </label>
        <input type="file" name="audio7" accept="audio/wav,.mp3">
        <label name="lblaudio8"> Аудиоролик 8 </label>
        <input type="file" name="audio8" accept="audio/wav,.mp3">
        
        <label name="options3lbl">Варианты 1</label>
        <textarea name="options3" class="form-control" rows="1"></textarea>
        <label name="options4lbl">Варианты 2</label>
        <textarea name="options4" class="form-control" rows="1"></textarea>
        <label name="options5lbl">Варианты 3</label>
        <textarea name="options5" class="form-control" rows="1"></textarea>
        <label name="options6lbl">Варианты 4</label>
        <textarea name="options6" class="form-control" rows="1"></textarea>
        <label name="options7lbl">Варианты 5</label>
        <textarea name="options7" class="form-control" rows="1"></textarea>
        <label name="options8lbl">Варианты 6</label>
        <textarea name="options8" class="form-control" rows="1"></textarea>
        
        <label name="speedrobotlbl" style="margin-top: 20px;">Скорость рассылки (в час)</label>
        <select class="form-control" id="sel4" form="newrobot" name="robotspeed" required>
        <?php foreach($speeds as $speed):?>
            <option value="<?php $_($speed->id);?>">
                <?php $_($speed->Value);?>
            </option>
        <?php endforeach;?>
        </select>
        <br></br>
        
        <label style="margin-top: 20px;" name="commentrobotlbl">Комментарий (не более 300 символов)</label>
        <textarea name="robotcomment" class="form-control" rows="3"></textarea>
        
        <label style="margin-top: 20px;" class="btn" name="patternrobotbtn">Сохранить как шаблон</label>
        <br></br>
        <button type="submit" class="btn btn-primary" name="submitrobotbtn">Запустить</button>
        
    </fieldset>
</form>
<legend>Новости</legend>
<form method="POST" class="form-horizontal" id="eventfilter" enctype="multipart/form-data">
    <fieldset>
        <label>Название</label>
        <input id="newsname" name="newsname" type="text" maxlength="30"/>
        <label>Текст</label>
        <textarea id="newstext" name="newstext" type="text" rows="3" maxlength="300"></textarea>
        <button type="submit" class="btn btn-primary" name="addnewsbutton">Добавить</button>
    </fieldset>
</form>
<table class="table">
    <tr><th>Название</th><th>Текст</th><th>Доступна</th></tr>
    <?php foreach($news as $new):?>
        <tr>
            <td>
                <?php $_($new->Name);?>
            </td>
            <td>
                <?php $_($new->Text);?>
            </td>
            <td>
                <?php $_($new->AvailableFor);?>
            </td>
        </tr>
    <?php endforeach;?>
</table>
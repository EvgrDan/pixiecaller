<legend>Партнеры</legend>
<ul class="pagination">
        <?php if($current_page>4):?>
            <li>
                <a href="<?php echo $pager->url(1);?>">1</a>
            </li>
            <li>
                <a href="#">...</a>
            </li>
        <?php endif?>
        <?php for($i=1; $i<=$pager->num_pages; $i++): ?>
            
            <?php if($i>=$current_page-3 and $i<=$current_page+3):?>
                <?php if($i==$current_page): ?>
                <li class="active">
                <?php else:?>
                <li>
                <?php endif?>
                    <a href="<?php echo $pager->url($i);?>"><?php echo $i;?></a>
                </li>   
            <?php endif?>
        <?php endfor;?>
        <?php if($current_page<$pager->num_pages-4):?>
            <li>
                <a href="#">...</a>
            </li>   
            <li>
                <a href="<?php echo $pager->url($pager->num_pages);?>"><?php echo $pager->num_pages;?></a>
            </li>
        <?php endif?> 
</ul>
<ul class="pagination itemperpage">
    <li <?php echo( $item_per_page==20 ? "class='active'" : "");?>><a href="/su/partners?items=20">20</a></li>
    <li <?php echo( $item_per_page==50 ? "class='active'" : "");?>><a href="/su/partners?items=50">50</a></li>
    <li <?php echo( $item_per_page==100 ? "class='active'" : "");?>><a href="/su/partners?items=100">100</a></li>
</ul>
<table class="table">
    <tr><th>Email</th><th>Телефон</th><th>Тариф</th><th>Баланс</th><th>Кол-во рассылок</th><th>Изменение баланса</th><th>Изменение тарифа</th><th>Подробности</th></tr>
    <?php foreach($pager->current_items() as $user):?>
        <tr>
            <td>
                <?php $_($user->Login);?>
            </td>
            <td>
                <?php $_($user->UserPhone);?>
            </td>
            <td>
                <?php $_($costs[$user->id]);?>
            </td>
            <td>
                <?php $_($user->balance/100);?> руб.
            </td>
			<td>
                <?php $_($distrs[$user->id]);?>
            </td>
            <td>
                <button class="btn" onclick="show('block', <?php $_($user->id);?>, '<?php $_($user->Login);?>')">Пополнить</button>
            </td>
            <td>
                <button class="btn" onclick="show1('block', <?php $_($user->id);?>, '<?php $_($user->Login);?>', '<?php $_($costs[$user->id]);?>')">Изменить</button>
            </td>
            <td>
                <a class="btn" href=<?php $_( "/su/clientinfo/" . (string)$user->id);?>>Подробно</a>
            </td>
        </tr>
    <?php endforeach;?>
</table>
<ul class="pagination floatl">
        <?php if($current_page>4):?>
            <li>
                <a href="<?php echo $pager->url(1);?>">1</a>
            </li>
            <li>
                <a href="#">...</a>
            </li>
        <?php endif?>
        <?php for($i=1; $i<=$pager->num_pages; $i++): ?>
            
            <?php if($i>=$current_page-3 and $i<=$current_page+3):?>
                <?php if($i==$current_page): ?>
                <li class="active">
                <?php else:?>
                <li>
                <?php endif?>
                    <a href="<?php echo $pager->url($i);?>"><?php echo $i;?></a>
                </li>   
            <?php endif?>
        <?php endfor;?>
        <?php if($current_page<$pager->num_pages-4):?>
            <li>
                <a href="#">...</a>
            </li>   
            <li>
                <a href="<?php echo $pager->url($pager->num_pages);?>"><?php echo $pager->num_pages;?></a>
            </li>
        <?php endif?>
</ul>
<ul class="pagination itemperpage">
    <li <?php echo( $item_per_page==20 ? "class='active'" : "");?>><a href="/su/partners?items=20">20</a></li>
    <li <?php echo( $item_per_page==50 ? "class='active'" : "");?>><a href="/su/partners?items=50">50</a></li>
    <li <?php echo( $item_per_page==100 ? "class='active'" : "");?>><a href="/su/partners?items=100">100</a></li>
</ul>
<div onclick="show('none')" id="wrap" style="display: none;"></div>
<div id="window" style="display: none;">
    <center>
        <form method="POST" class="form-horizontal" id="changebal" enctype="multipart/form-data">
            <fieldset>
                <legend id="nazv"></legend>
                <input name="summ" type="number" value = "0" required/> руб.
                <select id="seltype" name="seltype">
                <?php foreach($types as $type):?>
                    <option value="<?php $_($type->name);?>">
                        <?php $_($type->description);?>
                    </option>
                <?php endforeach;?>
                </select>
                <label id="subpay" class="btn btn-primary">Зачислить платеж</label>
            </fieldset>
        </form>
        <legend id="histleg">История платежей (последние 4)</legend>
        <table class="table" id = "histtable">
            <tr><th>Дата</th><th>Тип платежа</th><th>Сумма</th></tr>
        </table>
    </center>
</div>

<div onclick="show1('none')" id="wrap1" style="display: none;"></div>
<div id="window1" style="display: none;">
    <center>
        <form class="form-inline" id="changetar" method="POST">
            <fieldset>
                <legend id="nazv1"></legend>
                <table class="table">
                    <tr><th>Перезвон(коп.)</th><th>Обычная(коп.)</th><th>Дозвон(коп.)</th></tr>
                    <td><input type="number" class="input-small" name="recall1" id="recall1" placeholder="Перезвон..."></td>
                    <td><input type="number" class="input-small" name="ordinary1" id="ordinary1" placeholder="Обычная..."></td>
                    <td><input type="number" class="input-small" name="dial1" id="dial1" placeholder="Дозвон..."></td>
                </table>
                <label id="subtariff" class="btn ">Изменить тариф</label>
            </fieldset>
        </form>
    </center>
</div>
<style type="text/css">
	#wrap{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
	
	#window{
		width: 400px;
		height: 400px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
	
    #wrap1{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
	
	#window1{
		width: 400px;
		height: 400px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
    
	.close{
		margin-left: 364px;
		margin-top: 4px;
		cursor: pointer;
	}
	
</style>
<script type="text/javascript">
 
            //Функция показа
    function show(state, userid, useremail){
        var table = document.getElementById("histtable");
        for (i=table.tBodies[0].rows.length-1; i>0; i--) {table.tBodies[0].deleteRow(i);}
        $.ajax({
          type: "POST",
          url: "/su/gethistory",
          data: "userid="+userid,
          success: function(data){
            if(data.trim()!=''){
                data.split(";").forEach(function(item, i, arr) {
                  arr = item.split(",");
                  var newRow=table.insertRow(i+1);
                  var newCell = newRow.insertCell(0);
                  newCell.innerHTML=arr[0];
                  var newCell = newRow.insertCell(1);
                  newCell.innerHTML=arr[1];
                  var newCell = newRow.insertCell(2);
                  newCell.innerHTML=arr[2]/100 + " руб.";
                });
            }
          }
        });
        document.getElementById('window').style.display = state;            
        document.getElementById('wrap').style.display = state;
        document.getElementById('nazv').innerHTML = "Изменение баланса партнера "+useremail;  
        document.getElementById('subpay').setAttribute("onclick", "changebalance('"+userid+"')");
    }
    
    function show1(state, userid, useremail, costs){
        if(costs!=null){
            var elem = document.getElementById("recall1");
            elem.value = costs.split("/")[0];
            var elem = document.getElementById("dial1");
            elem.value = costs.split("/")[2];
            var elem = document.getElementById("ordinary1");
            elem.value = costs.split("/")[1];
        }
        document.getElementById('window1').style.display = state;            
        document.getElementById('wrap1').style.display = state;
        document.getElementById('nazv1').innerHTML = "Изменение тарифа партнера "+useremail;  
        document.getElementById('subtariff').setAttribute("onclick", "changetariff('"+userid+"')");
    }
    
    function changebalance(userid){;
        $.ajax({
          type: "POST",
          url: "/su/changebalance",
          data: "userid="+userid+"&sum="+$('input[name="summ"]').val()+"&type="+$('select[name="seltype"]').val(),
          success: function(data){
            swal( data );
            show('none');
            window.location.replace("/su/partners");
          }
        });
    }
    
    function changetariff(userid){;
        $.ajax({
          type: "POST",
          url: "/su/changetariff",
          data: "userid="+userid+"&recall="+$('input[name="recall1"]').val()+"&ordinary="+$('input[name="ordinary1"]').val()+"&dial="+$('input[name="dial1"]').val(),
          success: function(data){
            swal( data );
            show1('none');
            window.location.replace("/su/partners");
          }
        });
    }
     
</script>
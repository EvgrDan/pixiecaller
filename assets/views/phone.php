<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="title-reg">
            ДОБАВИТЬ НОМЕР
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-2 text-center"> </div>
    <div class="col-xs-12 col-md-8 text-center">
        <div class="stepwinum">
            <div class="row ">
                <div class="col-xs-12 col-md-12 text-left">
                    <form method="POST" id="newphone" enctype="multipart/form-data">
                        <fieldset>
                            <div class="title-step">
                                 Добавить номер телефона
                            </div>
                            <input maxlength="11" class="field-form" id = "num" name="num" type="text" placeholder="7XXXXXXXXXX"/>
                            <div class="btn-form-big-grey w100" name="subphonebtn" style="cursor: pointer;">Выслать код подтверждения</div>
                            <div name="codelbl" class="title-step">
                                 Введите код подтверждения
                            </div>
                            <input maxlength="4" class="field-form" id = "phonecode" name="phonecode" type="text" placeholder="0000"/>
                            <div class="btn-form-big-red w100" name="submitphonebtn" style="cursor: pointer;">Создать</div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
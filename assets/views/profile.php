<div class="row ">
    <div class="col-xs-12 col-md-2 text-center"></div>
    <div class="col-xs-12 col-md-8">
        <div class="box-form-reg-all">
            <div class="step">
            <div class="row ">
                <div class="col-xs-12 col-md-12 text-center">
                    <div class="title-reg-l">
                        МОЙ ПРОФИЛЬ
                    </div>
                </div>
            </div> 
            </div>
            <div class="step-1 step">
                <div class="row ">
                    <div class="col-xs-12 col-md-12 text-left">
                        <div class="title-step">
                             Текущий тариф
                        </div>
                        <div class="title-dop"> <?php $_("Дозвон-сброс: " . $cost->dial/100 . " руб.");?></div>
                        <div class="title-dop"> <?php $_("Обычный звонок: " . $cost->ordinary/100 . " руб./мин.");?></div>
                        <div class="title-dop"> <?php $_("Перезвон: " . $cost->recall/100 . " руб./мин.");?></div>
                    </div>
                </div>
            </div>
             <div class="step-2 step">
                 <div class="row ">
                     <div class="col-xs-12 col-md-12 text-left">
                        <form method="POST" class="form-horizontal" id="changeuser" enctype="multipart/form-data">
                            <fieldset>
                                <div class="title-step">Телефон</div>
                                <input class="field-form" name="userphone" type="text" placeholder="7XXXXXXXXXX" value = "<?php $_($user->UserPhone);?>" required/>
                                <div class="title-step">Email</div>
                                <input class="field-form" name="usermail" type="text" placeholder="sample@example.org" value = "<?php $_($user->Login);?>" required/>
                                <input type="checkbox" id="chkemail" name="chkemail" <?php $_($checkedemail);?>>
                                <label type="checkbox" for="chkemail" style="margin-top: 20px;" class="checkbox" name="chkemaillbl">
                                    Получать уведомления по электронной почте
                                </label>
                                <input type="checkbox" name="chkcall" id="chkcall" <?php $_($checkedcall);?>>
                                <label type="checkbox" for="chkcall" class="checkbox" name="chkcalllbl">
                                    Получать уведомления по телефону
                                </label>
                                <input type="checkbox" name="chksms" id="chksms" <?php $_($checkedsms);?>>
                                <label type="checkbox" for="chksms" class="checkbox" name="chksmslbl">
                                    Получать уведомления через смс
                                </label>
                                <input type="checkbox" name="chknotify" id="chknotify" <?php $_($checkednotify);?>>
                                <label type="checkbox" for="chknotify" class="checkbox" name="chksmslbl">
                                    Выводить подсказки в админке
                                </label>
                                <input type="checkbox" name="chknews" id="chknews" <?php $_($checkednews);?>>
                                <label type="checkbox" for="chknews" class="checkbox" name="chksmslbl">
                                    Выводить новости в админке
                                </label>
                                <button type="submit" class="btn-form-big-grey">Сохранить</button>
                            </fieldset>
                        </form>
                     </div>
                 </div>
             </div>
            <div class="step-3 step">
                 <div class="row ">
                    <div class="col-xs-12 col-md-12 text-left">
                        <form id="promoform">
                            <fieldset>
                                <div class="title-step">
                                    Ввести промокод
                                </div>
                                <input class="field-form" id = "code" name="code" type="text" placeholder="Введите промокод..."/>
                                <div class="btn-form-big-red" name="subpromobtn" style="cursor: pointer;">Получить приз!</div>
                            </fieldset>
                        </form>
                    </div>
                 </div>
            </div>
        </div>
    </div>
</div>
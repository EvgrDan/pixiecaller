<form method="POST" class="form-horizontal" id="changeuser" enctype="multipart/form-data">
    <fieldset>
        <legend>Мой профиль</legend>
        <!--<label><b>Имя</b></label>
        <input name="username" type="text" placeholder="Иван Иваныч..." value = "<?php $_($user->UserName);?>" required/>-->
        <label><b>Телефон</b></label>
        <input name="userphone" type="text" placeholder="7XXXXXXXXXX" value = "<?php $_($user->UserPhone);?>" required/>
        <label><b>Email</b></label>
        <input name="usermail" type="text" placeholder="sample@example.org" value = "<?php $_($user->Login);?>" required/>
        <p></p>
		<button type="submit" class="btn btn-primary">Сохранить</button>
    </fieldset>
</form>
<form class="form-horizontal" id="contactform" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>Контакты для отображения клиентам</legend>
        <!--<label><b>Имя</b></label>
        <input name="name" type="text" placeholder="Иван Иваныч..." value = "<?php echo($contact!="None" ? $contact->UserName : ""); ?>" required/>-->
        <label><b>Телефон</b></label>
        <input name="phone" type="text" placeholder="7XXXXXXXXXX" value = "<?php echo($contact!="None" ? $contact->UserPhone : ""); ?>" required/>
        <label><b>Email</b></label>
        <input name="email" type="text" placeholder="sample@example.org" value = "<?php echo($contact!="None" ? $contact->UserEmail : "");?>" required/>
        <label><b>Аудиоролик для приветствия</b></label>
        <input type="file" name="welcome1" accept="audio/wav,.mp3">
        <audio style="margin-top: 20px;" controls="controls" id="welcome1audioplayer">
          Your browser does not support the <code>audio</code> element.
          <source id="welcomeaudio" src="<?php echo($wAudio); ?>" type="audio/wav">
        </audio>
        <label><b>Аудиоролик для тестовой рассылки</b></label>
        <input type="file" name="test" accept="audio/wav,.mp3">
        <audio style="margin-top: 20px;" controls="controls" id="test1audioplayer">
          Your browser does not support the <code>audio</code> element.
          <source id="testaudio" src="<?php echo($tAudio); ?>" type="audio/wav">
        </audio>
    </fieldset>
</form>
<label style="margin-top: 20px;" ><b>Реквизиты</b>(будут отображаться по нажатию на кнопку оплатить)</label>
<textarea id="reqtext" name="requisites" class="form-control" rows="5"><?php echo($contact->requisites);?></textarea>
<p></p>
<div class="btn-red" name="subcontactbtnsu">Сохранить</div>
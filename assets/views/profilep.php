<div class="row ">
    <div class="col-xs-12 col-md-2 text-center"></div>
    <div class="col-xs-12 col-md-8">
        <div class="box-form-reg-all">
            <div class="step">
            <div class="row ">
                <div class="col-xs-12 col-md-12 text-center">
                    <div class="title-reg-l">
                        МОЙ ПРОФИЛЬ
                    </div>
                </div>
            </div> 
            </div>
            
            <div class="step-1 step">
                <div class="row ">
                    <div class="col-xs-12 col-md-12 text-left">
                        <div class="title-step">
                             Текущий тариф
                        </div>
                        <div class="title-dop"> <?php $_("Дозвон-сброс: " . $cost->dial/100 . " руб.");?></div>
                        <div class="title-dop"> <?php $_("Обычный звонок: " . $cost->ordinary/100 . " руб./мин.");?></div>
                        <div class="title-dop"> <?php $_("Перезвон: " . $cost->recall/100 . " руб./мин.");?></div>
                    </div>
                </div>
            </div>
            <div class="line"></div>
            <div class="step-2 step">
                <div class="row ">
                    <div class="col-xs-12 col-md-12 text-left">
                        <div class="title-step">
                             Личные данные
                        </div>
                        <form method="POST" id="changeuser" enctype="multipart/form-data">
                            <fieldset>
                                <!--<label><b>Имя</b></label>
                                <input name="username" type="text" placeholder="Иван Иваныч..." value = "<?php $_($user->UserName);?>" required/>-->
                                <div>Телефон</div>
                                <input class="field-form" name="userphone" type="text" placeholder="7XXXXXXXXXX" value = "<?php $_($user->UserPhone);?>" required/>
                                <div>Email</div>
                                <input class="field-form" name="usermail" type="text" placeholder="sample@example.org" value = "<?php $_($user->Login);?>" required/>
                                <button type="submit" class="btn-red">Сохранить</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="line"></div>
            <div class="step-3 step">
                <div class="row ">
                    <div class="col-xs-12 col-md-12 text-left">
                        <div class="title-step">
                             Настройки партнерской программы
                        </div>
                        <form id="contactform" method="post" enctype="multipart/form-data">
                            <fieldset>
                                <!--<label><b>Имя</b></label>
                                <input name="name" type="text" placeholder="Иван Иваныч..." value = "<?php echo($contact!="None" ? $contact->UserName : ""); ?>" required/>-->
                                <div class="num-golos num-golos-1">
                                    <div class="title-step">Контакты</div>
                                    <div>Телефон</div>
                                    <input class="field-form" name="phone" type="text" placeholder="7XXXXXXXXXX" value = "<?php echo($contact!="None" ? $contact->UserPhone : ""); ?>" required/>
                                    <div>Email</div>
                                    <input class="field-form" name="email" type="text" placeholder="sample@example.org" value = "<?php echo($contact!="None" ? $contact->UserEmail : "");?>" required/>
                                </div>
                                <div style="margin-top:20px" class="line"></div>
                                <div class="num-golos num-golos-2">
                                    <div class="title-step">Аудиоролик для приветствия</div>
                                    <input type="checkbox" id="welcomecall" name="welcomecall" <?php echo($check ? "checked" : "");?>> 
                                    <label type="checkbox" for="welcomecall" class="checkbox" name="lblwelcomecall">
                                       Не совершать звонок с приветствием
                                    </label>
                                    <input type="file" name="welcome1" accept="audio/wav,.mp3">
                                    <audio style="margin-top: 20px;" controls="controls" id="welcome1audioplayer">
                                      Your browser does not support the <code>audio</code> element.
                                      <source id="welcomeaudio" src="<?php echo($wAudio); ?>" type="audio/wav">
                                    </audio>
                                </div>
                                <div style="margin-top:20px" class="line"></div>
                                <div class="num-golos num-golos-3">
                                    <div class="title-step">Аудиоролик для тестовой рассылки</div>
                                    <input type="file" name="test" accept="audio/wav,.mp3">
                                    <audio style="margin-top: 20px;" controls="controls" id="test1audioplayer">
                                      Your browser does not support the <code>audio</code> element.
                                      <source id="testaudio" src="<?php echo($tAudio); ?>" type="audio/wav">
                                    </audio>
                                </div>
                                <div  style="margin-top:20px" class="line"></div>
                                <div class="num-golos num-golos-4">
                                    <div class="title-step">Логотип партнерки</div>
                                    <?php if($this->pixie->orm->get('domain')->where('name', 'like', '%' . $_SERVER['SERVER_NAME'])->find()->loaded()): ?>
                                        <img src="/assets/img/<?php echo("partnerlogo" . $this->pixie->orm->get('domain')->where('name', 'like', '%' . $_SERVER['SERVER_NAME'])->find()->UserID); ?>" alt="" width="42" height="42">
                                    <?php endif ?>
                                    <input type="file" name="piclogo" accept="image/*">
                                </div>
                                <div  style="margin-top:20px" class="line"></div>
                                <div class="num-golos num-golos-4">
                                    <div class="title-step">Фон партнерки</div>
                                    <?php if($this->pixie->orm->get('domain')->where('name', 'like', '%' . $_SERVER['SERVER_NAME'])->find()->loaded()): ?>
                                        <img src="/assets/img/<?php echo("partnerback" . $this->pixie->orm->get('domain')->where('name', 'like', '%' . $_SERVER['SERVER_NAME'])->find()->UserID); ?>" alt="" width="100" height="100">
                                    <?php endif ?>
                                    <input type="file" name="picback" accept="image/*">
                                </div>
                                <div  style="margin-top:20px" class="line"></div>
                                <div class="num-golos num-golos-4">
                                    <div class="title-step">Основной цвет</div>
                                    <input type="color" id="html5colorpicker" onchange="clickColor(0, -1, -1, 5)" name="maincolor" value="<?php echo($contact->maincolor);?>" style="width:85%;">
                                </div>
                                <div  style="margin-top:20px" class="line"></div>
                                <div class="num-golos num-golos-5">
                                    <div class="title-step">Реферальная программа</div>
                                    <input type="checkbox" name="ref" id="ref" <?php echo($contact->refon==1 ? "checked" : "");?>>
                                    <label type="checkbox"  for="ref" class="checkbox" name="lblref"> Включена
                                    </label>
                                    <div style="margin-top:10px; margin-bottom:10px">Процент от первого уровня</div>
                                    <input style="width:20%" class="field-form" name="percent1" type="text" value = "<?php echo($contact!="None" ? $contact->percent1 : "");?>" required/>
                                    <div style="margin-top:10px; margin-bottom:10px">Процент от второго уровня</div>
                                    <input style="width:20%" class="field-form" name="percent2" type="text" value = "<?php echo($contact!="None" ? $contact->percent2 : "");?>" required/>
                                </div>
                                <div  style="margin-top:20px" class="line"></div>
                                <div class="num-golos num-golos-6">
                                    <div class="title-step">Дополнительный функционал</div>
                                    <input type="checkbox" id="hlr" name="hlr" <?php echo($contact->hlron==1 ? "checked" : "");?>>
                                    <label type="checkbox" for="hlr" class="checkbox" name="lblhlr">
                                        Лайт-HLR
                                    </label>
                                    <!--<div style="white-space: nowrap;">-->
                                    <input type="checkbox" id="promomail" name="promomail" <?php echo($contact->promomailon==1 ? "checked" : "");?>>
                                    <label type="checkbox" for="promomail" class="checkbox" name="lblpromomail">
                                        Письма и смс с промокодами
                                    </label>
                                    <!--<i name="promomailquest" style="display: inline-block;" class="icon-question-sign" data-toggle="tooltip" data-placement="right" title="Если 25 дней Ваш клиент не заходил в админку, ему приходит письмо и смс с промокодом. Если клиент введет промокод, ему зачислится промо-платеж 20 рублей, если клиент решит их потратить - часть этой суммы будет списана с Вашего счета."></i>
                                    </div>-->
                                    <!--<div style="white-space: nowrap;">-->
                                    <input type="checkbox" name="newclientsms" id="newclientsms" <?php echo($contact->newclientsmson==1 ? "checked" : "");?>>
                                    <label type="checkbox" for="newclientsms" class="checkbox" name="lblnewclientsms">
                                        Отправлять смс при регистрации клиента
                                    </label>
                                    <input type="checkbox" name="casemail" id="casemail" <?php echo($contact->casemailon==1 ? "checked" : "");?>>
                                    <label type="checkbox" for="casemail" class="checkbox" name="lblcasemail">
                                        Письма с вариантами использования
                                    </label>
                                    <!--<i name="casemailquest" style="display: inline-block;" class="icon-question-sign" data-toggle="tooltip" data-placement="right" title="На четвертый день после регистрации Вашему клиенту придет письмо с кейсами (реальными примерами применения 'Звонка-сброса' и 'Голосовой рассылки'). Внимание! Расчеты в кейсе основаны на том, что звонок-сброс стоит 10 копеек, а минута обычного звонка 2,4 рубля."></i>
                                    </div>-->
                                    <input type="checkbox" name="newson" id="newson" <?php echo($contact->newson==1 ? "checked" : "");?>>
                                    <label type="checkbox" for="newson" class="checkbox" name="lblnews">
                                        Показывать новости
                                    </label>
                                    <input type="checkbox" name="notifyon" id="notifyon" <?php echo($contact->notifyon==1 ? "checked" : "");?>>
                                    <label type="checkbox" for="notifyon" class="checkbox" name="lblnotify">
                                        Показывать подсказки
                                    </label>
                                    <div><b>Реквизиты</b>(будут отображаться по нажатию на кнопку оплатить)</div>
                                    <textarea class="field-form" id="reqtext" name="requisites" class="form-control" rows="5"><?php echo($contact->requisites);?></textarea>
                                    <div class="btn-red" name="subcontactbtn">Сохранить</div>
                                </div>
                                
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>

            
        <p></p>
        </div>
    </div>
</div>
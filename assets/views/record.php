<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="title-reg">
            НОВЫЙ АУДИОРОЛИК
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-2 text-center"> </div>
    <div class="col-xs-12 col-md-8 text-center">
        <div class="stepwinum">
            <div class="row ">
                <div class="col-xs-12 col-md-12 text-left">
                    <form method="POST" id="newrecord" enctype="multipart/form-data">
                        <fieldset>
                            <div class="title-step">
                                 Название
                                 <a href="#" class="btnpodskazka" data-toggle="tooltip" data-placement="right otleft" title="Вы можете создать не более <?php $_($this->pixie->auth->user()->RecordCount) ?> аудиороликов в день"><i class="fa fa-question-circle"></i></a>
                            </div>
                            <input class="field-form" id="recordname" name="recordname" type="text" placeholder="Введите название..." value="<?php $_($recordname) ?>" required>
                            <br></br>
                            <div class="btn-group for-btn" data-toggle="buttons">
                                <label name="textclick1" class="btn-grey floatl">
                                    <input type="radio" id="q156" name="recordtype1" value="text" /> Ввести текст
                                </label>
                                <label name="fileclick1" class="btn-red floatl active">
                                    <input type="radio" id="q157" name="recordtype1" value="file"  /> Загрузить файл
                                </label>
                                <label name="dictclick1" class="btn-grey floatl">
                                    <input type="radio" id="q158" name="recordtype1" value="text" /> Продиктовать
                                </label>
                            </div>
                            <textarea name="recordText" class="field-form" rows="1" placeholder="Текст записи..." ></textarea>
                            <input type="file" class="field-form" name="recordFile"  accept="audio/wav,.mp3">
                            <input class="field-form" onkeyup="this.value = this.value.replace (/\D/, '')" id="callNumber" name="callNumber" type="text" placeholder="Введите номер..." required/>
                            <button type="submit" class="btn-red" name="createbtn">Создать</button>
                            <button type="submit" class="btn-red" name="callbtn">Позвонить</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
        <legend>Доступные ролики</legend>
        <table class="table">
            <tr><th>id</th>
            <?php if($this->pixie->auth->user()->UserType=="Admin"):?>
            <th>Пользователь</th>
            <?php endif ?>
            <th>Название</th><th>Статус</th><th>Запись</th><th>Действия</th>
            <?php if($this->pixie->auth->user()->UserType=="Admin"):?>
                <th>Модерация</th>
            <?php endif ?>
            <th>Комментарий модератора</th></tr>
            <?php foreach($records as $record):?>
                <tr>
                    <td>
                        <?php $_($record->id);?>
                    </td>
                    <?php if($this->pixie->auth->user()->UserType=="Admin"):?>
                    <td>
                        <a href="/su/clientinfo/<?php $_($record->userid);?>"><?php $_($users[$record->userid]);?></a>
                    </td>
                    <?php endif ?>
                    <td>
                        <?php $_($record->name);?>
                    </td>
                    <td>
                        <?php $_(array_key_exists($record->status, $statuses)?$statuses[$record->status]:$record->status);?>
                    </td>
                    <td>
                        <audio controls="controls" id="recordaudioplayer">
                          Your browser does not support the <code>audio</code> element.
                          <source id="recordaudio" src="<?php $_($paths[$record->id]);?>" type="audio/wav">
                        </audio>
                    </td>
                    <td>
                    <?php if($this->pixie->auth->user()->UserType=="Admin"):?>
                        <a class="btn" title="Удалить" href=<?php $_( "/su/delrecord/" . (string)$record->id);?>><i style="color:#4D4D4D" class="fa fa-2x fa-trash"></i></a>
                    <?php else:?>
                        <a class="btn" title="Удалить" href=<?php $_( "/admin/delrecord/" . (string)$record->id);?>><i style="color:#4D4D4D" class="fa fa-2x fa-trash"></i></a>
                    <?php endif ?>
                    </td>
                    <?php if($this->pixie->auth->user()->UserType=="Admin"):?>
                    <td>
                    <?php if($record->status=="Moderating"):?>
                        <a class="btn" href=<?php $_( "/su/acceptrecord/" . (string)$record->id);?> title="Принять"><i style="color:#00FF00" class="fa fa-3x  fa-check-circle-o"></i></a>
                        <a class="btn" href=<?php $_( "/su/declinerecord/" . (string)$record->id);?> title="Отклонить"><i style="color:#FF0000" class="fa fa-3x  fa-times-circle-o"></i></a>
                    <?php endif ?>
                    <?php if($record->moderate == 1):?>
                        <a class="btn" href=<?php $_( "/su/moderaterecord/" . (string)$record->id);?> onclick="return confirm('Вы уверены?')" title="Отключить модерацию"><i style="color:#4D4D4D" class="fa fa-2x  fa-eye-slash"></i></a>
                    <?php else: ?>
                        <a class="btn" href=<?php $_( "/su/moderaterecord/" . (string)$record->id);?> title="Включить модерацию"><i style="color:#4D4D4D" class="fa fa-2x  fa-eye"></i></a>
                    <?php endif ?>
                    <div onclick="show('block', '<?php $_($record->id); ?>')" title="Откомментировать" style="cursor: pointer;"><i style="color:#4D4D4D" class="fa fa-3x  fa-commenting"></i></div>
                    </td>
                    <?php endif ?>
                    <td>
                        <?php $_($record->admincomment);?>
                    </td>
                </tr>
            <?php endforeach;?>
        </table>
        
<div onclick="show('none')" id="wrap" style="display: none;"></div>
<div id="window" style="display: none;">
    <center>
        <legend>Добавление комментария</legend>
        <form method="POST" class="form-horizontal" id="commentform" enctype="multipart/form-data">
            <fieldset>
                <input class="field-form" id="commenttext" name="commenttext" type="text"/>
                <div id="commentbtn" style="cursor: pointer;" class="btn-form-big-grey ">Отправить</div>
            </fieldset>
        </form>
    </center>
</div>
<style type="text/css">
	#wrap{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
	
	#window{
		width: 400px;
		height: 300px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
    
    .close{
		margin-left: 364px;
		margin-top: 4px;
		cursor: pointer;
	}
	
</style>
<script>

    function show(state, recordid){;
        document.getElementById('window').style.display = state;            
        document.getElementById('wrap').style.display = state;
        document.getElementById('commentbtn').setAttribute("onclick", "addcomment('"+recordid+"')");
    }
    
    function addcomment(recordid){;
        $.ajax({
          type: "POST",
          url: "/su/commentrecord",
          data: "recordid="+recordid+"&text="+$('input[name="commenttext"]').val(),
          success: function(data){
            show('none');
            window.location.replace("/su/record");
          }
        });
    }

</script>
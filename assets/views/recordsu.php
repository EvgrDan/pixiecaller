<form method="POST" class="form-horizontal" id="newrecord" enctype="multipart/form-data">
    <fieldset>
        <legend>Новый звуковой ролик</legend>
        <label><b>Загрузить</b></label>
        <div class="btn-group" data-toggle="buttons">
            <label name="textclick1" class="btn btn-default" required>
                <input type="radio" id="q156" name="recordtype1" value="text" /> Ввести текст
            </label>
            <label name="fileclick1" class="btn btn-default active">
                <input type="radio" id="q157" name="recordtype1" value="file"  /> Загрузить файл
            </label>
        </div>
        <br></br>
        <textarea name="recordText" style="margin-top: 5px;" class="form-control" rows="1" placeholder="Текст записи..." required></textarea>
        <input type="file" name="recordFile"  accept="audio/wav,.mp3">
        <br></br>
        <button type="submit" class="btn btn-primary">Создать</button>
    </fieldset>
</form>

<legend>Доступные ролики</legend>
<table class="table">
    <tr>
    <th>id</th><th>Название</th><th>Статус</th><th>Запись</th>
    <?php if($record->status=="Moderating"):?>
        <th>Модерация</th>
    <?php endif ?>
    <th>Комментарий модератора</th>
    </tr>
    <?php foreach($records as $record):?>
        <tr>
            <td>
                <?php $_($record->id);?>
            </td>
            <td>
                <?php $_($record->name);?>
            </td>
            <td>
                <?php $_($statuses[$record->status]);?>
            </td>
            <td>
                <audio controls="controls" id="recordaudioplayer">
                  Your browser does not support the <code>audio</code> element.
                  <source id="recordaudio" src="<?php $_($paths[$record->id]);?>" type="audio/wav">
                </audio>
            </td>
            <?php if($record->status=="Moderating"):?>
            <td>
                <a class="btn" href=<?php $_( "/su/acceptrecord/" . (string)$record->id);?>>Принять</a>
            </td>
            <td>
                <a class="btn" href=<?php $_( "/su/declinerecord/" . (string)$record->id);?>>Отклонить</a>
            </td>
            <?php endif ?>
            <td>
                <?php $_($record->admincomment);?>
            </td>
        </tr>
    <?php endforeach;?>
</table>

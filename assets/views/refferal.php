<div class="row ">
    <div class="col-xs-12 col-md-12 text-left">
        <div class="step">
        <div class="title-reg padbot0">
            УСЛОВИЯ РЕФЕРАЛЬНОЙ ПРОГРАММЫ
        </div>
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-12 ">
        <div class="step">
        <p class="boldtext">
            Процент от первого уровня: <?php $_((string)$partnercontact->percent1); ?>%<br>
            Процент от второго уровня: <?php $_((string)$partnercontact->percent2); ?>%<br>
            Реферальная ссылка
        <p>
            <input class="field-form-sq" type="text " placeholder="Ссылка" value="<?php $_((string)$ref); ?>">
            <div class="line"></div>
        </div>

    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-left">
        <div class="step">
        <div class="title-reg padbtop0">
            РЕФЕРАЛЫ
        </div>
        </div>
        <div class="step-1 step marleft-ref">
            <div class="title-step-ref">
                Первый уровень
            </div>
            <div class="text-ref">
                <?php foreach($refusers as $user):?>
                <?php $_($user->Login);?><br>
                <?php endforeach;?>
            </div>
        </div>
        <div class="step-2 step marleft-ref">
            <div class="title-step-ref">
                Второй уровень
            </div>
            <div class="text-ref">
                <?php foreach($refusers2 as $user):?>
                <?php $_($user->Login);?><br>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>

<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="simple-title">Оплаты 1-го уровня</div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
         <div class="step">
            <div class="table-responsive">
                <table class="referralstable table" cellpadding="5">
                <tr><th>Дата</th><th>Email</th><th>Сумма</th><th>Процент</th></tr>
                <?php foreach($pays1 as $pay):?>
                        <tr>
                            <td>
                                <?php $_($pay[0]);?>
                            </td>
                            <td>
                                <?php $_($pay[1]);?>
                            </td>
                            <td>
                                <?php $_($pay[2]);?>
                            </td>
                            <td>
                                <?php $_($pay[3]);?>
                            </td>
                        </tr>
                <?php endforeach;?>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="simple-title">Оплаты 2-го уровня</div>
    </div>
</div>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
         <div class="step">
            <div class="table-responsive">
                <table class="referralstable table" cellpadding="5">
                    <tr><th>Дата</th><th>Email</th><th>Сумма</th><th>Процент</th></tr>
                    <?php foreach($pays2 as $pay):?>
                            <tr>
                                <td>
                                    <?php $_($pay[0]);?>
                                </td>
                                <td>
                                    <?php $_($pay[1]);?>
                                </td>
                                <td>
                                    <?php $_($pay[2]);?>
                                </td>
                                <td>
                                    <?php $_($pay[3]);?>
                                </td>
                            </tr>
                    <?php endforeach;?>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row ">
    <div class="col-xs-12 col-md-2 text-center"></div>
    <div class="col-xs-12 col-md-8">
        <div class="box-form-reg-all">
            <div class="step">
            <div class="row ">
                <div class="col-xs-12 col-md-12 text-center">
                    <div class="title-reg-l">
                        СМС-сообщения
                    </div>
                </div>
            </div> 
            </div>
            <div class="row" style="margin-bottom: 20px">
                <div class="col-xs-12 col-md-12">
                <div>В этом разделе будут появляться SMS сообщения, отправленные тем, кто нажал цифру "1" при прослушивании Ваших сообщений (если Вы поставили соответствующую галочку при создании рассылки). Все SMS бесплатны.</div>
                <div>Обратите внимание на статусы сообщений, некоторые могут быть недоставлены (FAIL), это зависит от оператора связи.</div>
                </div>
            </div>
            <?php if(count($smses)>0):?>
                <table class="table">
                    <tr><th>Время</th><th>Статус</th><th>Номер</th>
                    <?php if($this->pixie->auth->user()->UserType=="Admin"):?>
                    <th>Пользователь</th>
                    <?php endif?>
                    <th>Рассылка</th><th>Сообщение</th></tr>

                    <?php foreach($smses as $sms):?>
                        <tr>
                            <td>
                                <?php $_($sms[3]);?>
                            </td>
                            <td>
                                <?php if(array_key_exists ($sms[1], $statuses)):?>
                                <?php $_($statuses[$sms[1]]);?>
                                <?php else:?>
                                <?php $_($sms[1]);?>
                                <?php endif?>
                            </td>
                            <td>
                                <?php $_($sms[0]);?>
                            </td>
                            <?php if($this->pixie->auth->user()->UserType=="Admin"):?>
                            <td>
                                <?php if($sms[7]!="Нет"):?>
                                <a href="<?php $_($sms[6]);?>"><?php $_($sms[7]);?></a>
                                <?php else:?>
                                <?php $_($sms[7]);?>
                                <?php endif?>
                            </td>
                            <?php endif?>
                            <td>
                                <?php if($sms[5]!="Нет"):?>
                                <a href="<?php $_($sms[4]);?>"><?php $_($sms[5]);?></a>
                                <?php else:?>
                                <?php $_($sms[5]);?>
                                <?php endif?>
                            </td>
                            <td>
                                <?php $_($sms[2]);?>
                            </td>
                        </tr>
                    <?php endforeach;?>
               
            </table>
            <?php else:?>
                
                <div class="row ">
                    <div class="col-xs-12 col-md-12 text-center">
                        <div class="title-step">У Вас пока нет отправленных SMS!</div>
                    </div>
                </div>
            <?php endif?>
            
        </div>
    </div>
</div>
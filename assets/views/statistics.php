<legend>Роботы</legend>
<table class="table">
    <tr><th>Название</th><th>Номер отправителя</th><th>Статус</th><th>Время создания</th><th></th></tr>
    <?php foreach($robots as $robot):?>
        <tr>
            <td>
                <?php $_($robot->RobotName);?>
            </td>
            <td>
                <?php $_($robot->SenderNumber);?>
            </td>
            <td>
                <?php $_($robot->Status);?>
            </td>
            <td>
                <?php $_($robot->CreateTime);?> руб.
            </td>
            <td>
                <a class="btn" href=<?php $_( "/coldcalls/robotinfo/" . (string)$robot->id);?>>Подробно</a>
            </td>
        </tr>
    <?php endforeach;?>
</table>
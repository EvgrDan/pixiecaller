<div class="row ">
    <div class="col-xs-12 col-md-2 text-center"></div>
    <div class="col-xs-12 col-md-8">
        <div class="box-form-reg-all">
            <div class="step">
            <div class="row ">
                <div class="col-xs-12 col-md-12 text-center">
                    <div class="title-reg">
                        Ваш платеж прошел успешно! Баланс будет пополнен в ближайшее время
                    </div>
                </div>
                <div class="col-xs-12 col-md-12 text-center">
                    <form method="POST" class="form-horizontal" id="startform" enctype="multipart/form-data">
                        <fieldset>
                        <a href= "/admin/new_distrib"><div  class="btn-form-big-red w100" style="cursor: pointer;" name="new_distrib">Создать рассылку</div></a>
                        <a href= "/admin/profile"><div class="btn-form-big-grey w100" style="cursor: pointer;" name="profile">Настроить профиль</div></a>
                        </fieldset>
                    </form>
                </div>
            </div> 
            </div>
        </div>
    </div>
</div>
<meta http-equiv="refresh" content="10;url=/admin/new_distrib" />
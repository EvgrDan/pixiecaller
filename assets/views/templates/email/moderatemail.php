<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="width:100%;margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;border-collapse:separate;border:1px solid #dddddd;border-radius:5px;box-shadow: 0px 2px 0px 0px #b4b4b4;">
	<tbody border="0" style="margin:0;padding:0;">
		<tr border="0" style="margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;border-collapse:separate">
			<td border="0" height="60" bgcolor="#f26523" align="center" align=center style="-webkit-text-size-adjust:none;color:white;border-radius:5px 5px 0 0;background-image: url({{adminPanelUrl}}/static/img/email/bg_orange.jpg);background-size:cover;font-size: 24px; margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;line-height:1.6;border-collapse:separate">
				<b>ОТМОДЕРИРУЙ!</b>
			</td>
		</tr>
		<tr border="0" style="margin:0;padding:0;"><td border="0" height="15" style="margin:0;padding:0;"><br></td></tr>
		<tr border="0" style="margin:0;padding:0;font-family: Georgia, Times, 'Times New Roman', serif;font-size:100%;line-height:1.6;border-collapse:separate">
			<td border="0" height="25"  align="center" style="-webkit-text-size-adjust:none;color:black;margin:0;padding:0;font-family: Georgia, Times, 'Times New Roman', serif;font-size:19px;line-height:25px;border-collapse:separate">
			   Вам пришла рассылка на модерацию
			</td>
		</tr>
		
		<tr border="0" style="margin:0;padding:0;font-family: Georgia, Times, 'Times New Roman', serif;font-size:100%;line-height:1.6;border-collapse:separate">
			<td border="0"  align="center" style="-webkit-text-size-adjust:none;color:black;margin:0;padding:0;font-family: Georgia, Times, 'Times New Roman', serif;font-size:19px;line-height:25px;border-collapse:separate">
			   <?=$text;?>
			</td>
		</tr>
        <tr border="0">
            <td border="0">
                <table width="100%">
                    <tr border="0" align="center" width="100%" style="margin:0;padding:0;">
                        <td border="0" style="margin:0;padding:0;">
                            <br>
                        </td>
                        <td bgcolor=#ff9933" border="0" width="235" style="margin:0;padding:0;height:41px;width:235px;border-radius:5px;margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:13px;line-height:1.6;border-collapse:separate">
                           <a href="<?=$linktoaccept;?>" style="-webkit-text-size-adjust:none;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;line-height:41px;border-radius:5px;height:41px;width:270px;display:block;background-image: url({{adminPanelUrl}}/static/img/email/button_orange.jpg);background-size:contain;color:white;text-decoration:none;font-size:17px;" >
                                <b>ОДОБРИТЬ</b>
                           </a>
                        </td>
                        <td bgcolor=#ff9933" border="0" width="235" style="margin:0;padding:0;height:41px;width:235px;border-radius:5px;margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:13px;line-height:1.6;border-collapse:separate">
                           <a href="<?=$linktodecline;?>" style="-webkit-text-size-adjust:none;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;line-height:41px;border-radius:5px;height:41px;width:270px;display:block;background-image: url({{adminPanelUrl}}/static/img/email/button_orange.jpg);background-size:contain;color:white;text-decoration:none;font-size:17px;" >
                                <b>ОТКЛОНИТЬ</b>
                           </a>
                        </td>
                        <td border="0" style="margin:0;padding:0;">
                            <br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
	</tbody>
</table>
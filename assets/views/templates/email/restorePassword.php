<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
    </head>
    <body>
        <table bgcolor="#cfd0d4" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin:0;padding:0;width:100%!important;background:#cfdod4;">
            <tr>
                <td height="100%" width="100%" style="width:100%;margin:0 auto!important;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;border-collapse:separate;border:0;max-width:600px!important;clear:both!important">
                    <table bgcolor="#cfd0d4" border="0" cellpadding="0" cellspacing="0" style="min-width:255px;margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;width:100%!important;">
                        
                        <tr border="0" bgcolor="#e6e7eb" style="margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;width:100%!important;min-height:100%;background:#e6e7eb">
                            <td border="0" align="center" style="margin:0; padding:0;">
                                <table border="0" cellpadding="0" cellspacing="0" style="max-width:600px;min-width:255px;width:100%;margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;border-collapse:separate;">
                                    <tbody border="0" style="margin:0; padding:0;">
                                        <tr border="0" style="margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;border-collapse:separate">
                                            <td border="0" style="margin:0;padding:0;">
                                                <br>
                                            </td>
                                            <td border="0" style="max-width:600px;min-width:255px;width:100%;margin:0 auto!important;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;border-collapse:separate;border:0;display:block!important;clear:both!important">
                                                <table border="0" cellpadding="0" cellspacing="0" style="margin:0; padding:0;height:100%;color:#ffffff;width:100%;">
                                                    <tr><td><br></td></tr>
                                                    <tr border="0" height="20" style="margin:0;padding:0;">
                                                        <td border="0" align="center" style="-webkit-text-size-adjust:none;line-height:20px;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:15px;margin:0;padding:0;color:#000000;">
                                                            Сервис голосовых рассылок <span href="<?=$linktolandos;?>" style="-webkit-text-size-adjust:none;line-height:20px;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:15px;margin:0;padding:0;color:#e6e7eb;display:none;"><?=$projectname;?></span>
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr border="0" style="height:100%;margin:0;padding:0;">
                                                        <td border="0" align="center" style="margin:0;padding:0;">
                                                            <!-- Unique content here-->
 <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="width:100%;margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;border-collapse:separate;border:1px solid #dddddd;border-radius:5px;box-shadow: 0px 2px 0px 0px #b4b4b4;">
	<tbody border="0" style="margin:0;padding:0;">
		<tr border="0" style="margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;border-collapse:separate">
			<td border="0" height="60" bgcolor="#db2a3b" align="center" align="center" style="-webkit-text-size-adjust:none;color:white;border-radius:5px 5px 0 0;background-image: url(http://<?=$linktoadmin;?>/static/img/email/bg_red.jpg);background-size:cover;font-size: 24px; margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;line-height:1.6;border-collapse:separate">
				<b>ЗАБЫЛИ ПАРОЛЬ?</b>
			</td>
		</tr>
		<tr border="0" style="margin:0;padding:0;"><td border="0" height="15" style="margin:0;padding:0;"><br></td></tr>
		<tr border="0" style="margin:0;padding:0;font-family: Georgia, Times, 'Times New Roman', serif;font-size:100%;line-height:1.6;border-collapse:separate">
			<td border="0" height="25"  align="center" style="-webkit-text-size-adjust:none;color:black;margin:0;padding:0;font-family: Georgia, Times, 'Times New Roman', serif;font-size:19px;line-height:25px;border-collapse:separate">
			   Ты получил это письмо, потому что кто-то захотел восстановить твой пароль на нашем сервисе F1call
			</td>
		</tr>
		<tr border="0" height="25"style="margin:0;padding:0;font-family: Georgia, Times, 'Times New Roman', serif;font-size:100%;line-height:1.6;border-collapse:separate;">
			<td border="0"  align="center" style="-webkit-text-size-adjust:none;margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:19px;line-height:25px;border-collapse:separate;width:100%;">
			   <img border="0" alt="Сердечко" style="display:block;" height="14" width="17" src="http://admin.f1call.com/static/img/email/heart.png">
			</td>
		</tr>
		<tr border="0" style="margin:0;padding:0;font-family: Georgia, Times, 'Times New Roman', serif;font-size:100%;line-height:1.6;border-collapse:separate">
			<td border="0"  align="center" style="-webkit-text-size-adjust:none;color:black;margin:0;padding:0;font-family: Georgia, Times, 'Times New Roman', serif;font-size:19px;line-height:25px;border-collapse:separate">
			    Если это был не ты, пожалуйста,<br>
                проигнорируй это письмо
			</td>
		</tr>
		<tr border="0" style="margin:0;padding:0;"><td border="0"   height="20" style="margin:0;padding:0;"><br></td></tr>
        <tr border="0" height="75" style="margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;border-collapse:separate">
			<td border="0" bgcolor="#db2a3b"  align="center"  style="background-image: url(http://admin.f1call.com/static/img/email/bg_red.jpg);background-size:cover;color:white;margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:19px;line-height:1.3;border-collapse:separate">
				<b style="color:white;-webkit-text-size-adjust:none;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:19px;line-height:1.3;">
                    Ссылка для восстановления пароля
                </b>
			</td>
		</tr>
		<tr border="0" height="40" style="margin:0;padding:0;font-family: Georgia, Times, 'Times New Roman', serif;font-size:20px;line-height:1.6;border-collapse:separate">
			<td border="0" align="center" valign="center" width="100%" style="color:black;margin:0;padding:0;font-family: Georgia, Times, 'Times New Roman', serif;font-size:20px;line-height:1.6;border-collapse:separate">
                (действительна в течение часа)
			</td>
		</tr>
        <tr border="0">
            <td border="0">
                <table width="100%">
                    <tr border="0" align="center" width="100%" style="margin:0;padding:0;">
                        <td border="0" style="margin:0;padding:0;">
                            <br>
                        </td>
                        <td bgcolor="#db2a3b" border="0" width="235" style="margin:0;padding:0;height:41px;width:235px;border-radius:5px;margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:13px;line-height:1.6;border-collapse:separate">
                           <a href="{{ adminPanelUrl }}/restore.html?hash={{ restoreHash }}" style="-webkit-text-size-adjust:none;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;line-height:41px;border-radius:5px;height:41px;width:235px;display:block;background-image: url({{ adminPanelUrl }}/static/img/email/button_red.jpg);background-size:contain;color:white;text-decoration:none;font-size:17px;" >
                                <b>ВОССТАНОВИТЬ ПАРОЛЬ</b>
                           </a>
						   тут надо тебе самому писать ссылку
                        </td>
                        <td border="0" style="margin:0;padding:0;">
                            <br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

		<tr border="0" height="40" style="margin:0;padding:0;font-family: Georgia, Times, 'Times New Roman', serif;font-size:100%;line-height:1.6;border-collapse:separate">
			<td border="0" align="center" style="-webkit-text-size-adjust:none;color:black;margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:12px;line-height:1.6;border-collapse:separate">
				Если у тебя есть вопросы - позвони мне
			</td>
		</tr>
	</tbody>
</table>
 <tr border="0" style="margin:0;padding:0;"><td border="0" height="15" style="margin:0;padding:0;"><br></td></tr>                                                            <!-- Content end -->
 <tr border="0" style="margin:0;padding:0;"><td border="0" height="45" style="margin:0;padding:0;"><br></td></tr>
 
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td border="0" style="margin:0;padding:0;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;border-collapse:separate">
                                                <br>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>

<legend>Клиенты</legend>
<div class="col-xs-12 col-md-12">
    <form method="POST" id="clientfilter1" enctype="multipart/form-data">
        <fieldset>
            <div class="field-form-sel-box" id="seldiv">
                <select class="field-form-sel" id="clienttype" form="clientfilter" name="clienttype">
                <option value="-1" <?php echo ($cltype==-1 ? "selected" : "");?>>
                    Все
                </option>
                <option value="4" <?php echo ($cltype==4 ? "selected" : "");?>>
                    Свежие
                </option>
                <option value="1" <?php echo ($cltype==1 ? "selected" : "");?>>
                    Активные
                </option>
                <option value="2" <?php echo ($cltype==2 ? "selected" : "");?>>
                    Неактивные
                </option>
                <option value="3" <?php echo ($cltype==3 ? "selected" : "");?>>
                    Мертвые
                </option>
                </select>
            </div>
            <div class="btn-red" name="usfilterbtn" onclick="filtercl()" style="cursor: pointer;">Фильтр</div>
        </fieldset>
    </form>
</div>
<a href="/su/loademails" id="loademails" class="btn ">Выгрузить емейлы</a><a href="/su/loadphones" id="loadphones" class="btn ">Выгрузить телефоны</a>
<div class="row ">
    <div class="col-xs-12 col-md-12 text-center">
        <div class="table-responsive">
            <table class="mailingtable table" cellpadding="5">
                <tr><th>Email</th><th>Телефон</th><th>Тариф</th><th>Баланс</th><th>Кол-во рассылок</th><th>Управление</th></tr>
                <?php foreach($users as $user):?>
                    <tr>
                        <td>
                            <i style="color:<?php $_($colors[$user->ClientStatus]);?>" class="fa fa-1x  fa-circle"></i> <?php $_($user->Login);?>
                        </td>
                        <td>
                            <?php $_($user->UserPhone);?>
                        </td>
                        <td>
                            <?php $_($costs[$user->id]);?>
                        </td>
                        <td>
                            <?php $_($user->balance/100);?> руб.
                        </td>
                        <td>
                            <?php $_($distrs[$user->id]);?>
                        </td>
                        <td>
                            <a class="btn" onclick="show('block', <?php $_($user->id);?>, '<?php $_($user->Login);?>')" title="Пополнить"><i style="color:#4D4D4D" class="fa fa-2x  fa-plus-circle"></i></a>
                            <a class="btn" onclick="show1('block', <?php $_($user->id);?>, '<?php $_($user->Login);?>', '<?php $_($costs[$user->id]);?>')" title="Изменить тариф"><i style="color:#4D4D4D" class="fa fa-2x  fa-wrench"></i></a>
                            <a class="btn" onclick="show2('block', <?php $_($user->id);?>, '<?php $_($user->Login);?>')" title="Сделать партнером"><i style="color:#4D4D4D" class="fa fa-2x  fa-user-plus"></i></a>
                            <a class="btn" href=<?php $_( "/su/clientinfo/" . (string)$user->id);?> title="Подробно"><i style="color:#4D4D4D" class="fa fa-2x  fa-info-circle"></i></a>
                            <?php if($user->Moderate == 1):?>
                                <a class="btn" href=<?php $_( "/su/moderate/" . (string)$user->id);?> onclick="return confirm('Вы уверены?')" title="Отключить модерацию"><i style="color:#4D4D4D" class="fa fa-2x  fa-eye-slash"></i></a>
                            <?php else: ?>
                                <a class="btn" href=<?php $_( "/su/moderate/" . (string)$user->id);?> title="Включить модерацию"><i style="color:#4D4D4D" class="fa fa-2x  fa-eye"></i></a>
                            <?php endif ?>
                            <a class="btn" href=<?php $_( "/su/record/" . (string)$user->id);?> title="Посмотреть аудиозаписи"><i style="color:#4D4D4D" class="fa fa-2x  fa-file-audio-o"></i></a>
                        </td>
                    </tr>
                <?php endforeach;?>
            </table>
        </div>
    </div>
</div>
<div onclick="show('none')" id="wrap" style="display: none;"></div>
<div id="window" style="display: none;">
    <center>
        <form method="POST" class="form-horizontal" id="changebal" enctype="multipart/form-data">
            <fieldset>
                <legend id="nazv"></legend>
                <input name="summ" type="number" value = "0" required/> руб.
                <select id="seltype" name="seltype">
                <?php foreach($types as $type):?>
                    <option value="<?php $_($type->name);?>">
                        <?php $_($type->description);?>
                    </option>
                <?php endforeach;?>
                </select>
                <label id="subpay" class="btn btn-primary">Зачислить платеж</label>
            </fieldset>
        </form>
        <legend id="histleg">История платежей (последние 4)</legend>
        <table class="table" id = "histtable">
            <tr><th>Дата</th><th>Тип платежа</th><th>Сумма</th></tr>
        </table>
    </center>
</div>

<div onclick="show1('none')" id="wrap1" style="display: none;"></div>
<div id="window1" style="display: none;">
    <center>
        <form class="form-inline" id="changetar" method="POST">
            <fieldset>
                <legend id="nazv1"></legend>
                <table class="table">
                    <tr><th>Перезвон(руб.)</th><th>Обычная(руб.)</th><th>Дозвон(руб.)</th></tr>
                    <td><input type="number" class="input-small" name="recall1" id="recall1" placeholder="Перезвон..."></td>
                    <td><input type="number" class="input-small" name="ordinary1" id="ordinary1" placeholder="Обычная..."></td>
                    <td><input type="number" class="input-small" name="dial1" id="dial1" placeholder="Дозвон..."></td>
                </table>
                <label id="subtariff" class="btn ">Изменить тариф</label>
            </fieldset>
        </form>
    </center>
</div>

<div onclick="show2('none')" id="wrap2" style="display: none;"></div>
<div id="window2" style="display: none;">
    <center>
        <form class="form-inline" id="createpartner" method="POST">
            <fieldset>
                <legend id="nazvuser"></legend>
                <input name="domainname" type="text" placeholder="Имя домена" required/>
                <p></p>
                <input name="landos" type="text" placeholder="Адрес сайта" required/>
                <p></p>
                <label id="subpartner" class="btn ">Создать</label>
            </fieldset>
        </form>
    </center>
</div>
<style type="text/css">
	#wrap{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
	
	#window{
		width: 400px;
		height: 400px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
	
    #wrap1{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
	
	#window1{
		width: 400px;
		height: 400px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
    
    #wrap2{
		display: none;
		opacity: 0.8;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
		background-color: rgba(1, 1, 1, 0.725);
		z-index: 100;
		overflow: auto;
	}
	
	#window2{
		width: 400px;
		height: 200px;
		margin: 50px auto;
		display: none;
		background: #fff;
		z-index: 200;
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		padding: 16px;
	}
    
	.close{
		margin-left: 364px;
		margin-top: 4px;
		cursor: pointer;
	}
	
</style>
<script type="text/javascript">

    //Функция показа
    function show(state, userid, useremail){
        var table = document.getElementById("histtable");
        for (i=table.tBodies[0].rows.length-1; i>0; i--) {table.tBodies[0].deleteRow(i);}
        $.ajax({
          type: "POST",
          url: "/su/gethistory",
          data: "userid="+userid,
          success: function(data){
            if(data.trim()!=''){
                data.split(";").forEach(function(item, i, arr) {
                  arr = item.split(",");
                  var newRow=table.insertRow(i+1);
                  var newCell = newRow.insertCell(0);
                  newCell.innerHTML=arr[0];
                  var newCell = newRow.insertCell(1);
                  newCell.innerHTML=arr[1];
                  var newCell = newRow.insertCell(2);
                  newCell.innerHTML=arr[2]/100 + " руб.";
                });
            }
          }
        });
        document.getElementById('window').style.display = state;            
        document.getElementById('wrap').style.display = state;
        document.getElementById('nazv').innerHTML = "Изменение баланса пользователя "+useremail;  
        document.getElementById('subpay').setAttribute("onclick", "changebalance('"+userid+"')");
    }
    
    function show1(state, userid, useremail, costs){
        if(costs!=null){
            var elem = document.getElementById("recall1");
            elem.value = costs.split("/")[0];
            var elem = document.getElementById("dial1");
            elem.value = costs.split("/")[2];
            var elem = document.getElementById("ordinary1");
            elem.value = costs.split("/")[1];
        }
        document.getElementById('window1').style.display = state;            
        document.getElementById('wrap1').style.display = state;
        document.getElementById('nazv1').innerHTML = "Изменение тарифа пользователя "+useremail;  
        document.getElementById('subtariff').setAttribute("onclick", "changetariff('"+userid+"')");
    }
    
    function show2(state, userid, useremail){
        document.getElementById('window2').style.display = state;            
        document.getElementById('wrap2').style.display = state;
        document.getElementById('nazvuser').innerHTML = "Создание партнера "+useremail;  
        document.getElementById('subpartner').setAttribute("onclick", "createpartner('"+userid+"')");
    }
    
    function changebalance(userid){;
        $.ajax({
          type: "POST",
          url: "/su/changebalance",
          data: "userid="+userid+"&sum="+$('input[name="summ"]').val()+"&type="+$('select[name="seltype"]').val(),
          success: function(data){
            swal( data );
            show('none');
            window.location.replace("/su/users");
          }
        });
    }
    
    function changetariff(userid){;
        $.ajax({
          type: "POST",
          url: "/su/changetariff",
          data: "userid="+userid+"&recall="+$('input[name="recall1"]').val() * 100 +"&ordinary="+$('input[name="ordinary1"]').val() * 100 +"&dial="+$('input[name="dial1"]').val() * 100,
          success: function(data){
            swal( data );
            show1('none');
            window.location.replace("/su/users");
          }
        });
    }
    
    function createpartner(userid){;
        $.ajax({
          type: "POST",
          url: "/su/createpartner",
          data: "userid="+userid+"&domainname="+$('input[name="domainname"]').val()+"&landos="+$('input[name="landos"]').val(),
          success: function(data){
            swal( data );
            show1('none');
            window.location.replace("/su/users");
          }
        });
    }
    
    function filtercl() {
        var filter = $('select[name="clienttype"]').val();
        cookiePath = "/";
        var cookieName = "clienttype";
        var cookieValue = filter;
        var cookieHours = 3; // defaultCookieLifetime
        var d = new Date();
        d.setTime(d.getTime() + (cookieHours * 3600 * 1000));
        var expires = "expires=" + d.toUTCString() + ";";
        var path = "path=" + cookiePath + ";";
        document.cookie = cookieName + "=" + cookieValue + "; " + expires + path;
        window.location.replace("/su/users");
    }
    
    function swalconfirm(){
        return swal({   
        title: "Вы уверены?",   
        text: "Данное действие отключит модерацию пользователю!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Да!",   
        cancelButtonText: "Не-не-не!",   
        closeOnConfirm: false,   
        closeOnCancel: false }, 
        function(isConfirm){   
        if (isConfirm) {     
            swal("Выполнено", "Пользователь без модерации", "success");   
        } else {     
        swal("Отмена", "И не надо!", "error");  
        } });
    }
     
</script>
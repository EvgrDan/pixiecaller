<?php
namespace App\Controller;

class Admin extends \App\Page {
        
    public function action_login() {
        if($this->request->method == 'POST'){
            $login = $this->request->post('Login');
            $password = $this->request->post('Password');
            //$hash = $this->pixie->auth->provider('password')->hash_password($password . $this->salt);
            //Attempt to login the user using his
            //username and password
            $logged = $this->pixie->auth
                ->provider('password')
                ->login($login, $password . $this->salt);
            //On successful login redirect the user to
            //our protected page
            if ($logged and $this->pixie->auth->user()->UserType=="User"){
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "adminenter", $_SERVER['SERVER_NAME']);
                return $this->redirect('/admin/distribs');
            }
            if ($logged and $this->pixie->auth->user()->UserType=="Admin"){
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "adminenter", $_SERVER['SERVER_NAME']);
                return $this->redirect('/su/distribs');
            }
            if ($logged and $this->pixie->auth->user()->UserType=="Partner"){
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "adminenter", $_SERVER['SERVER_NAME']);
                return $this->redirect('/partner/clients');
            }
            if ($logged and $this->pixie->auth->user()->UserType=="Manager"){
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "adminenter", $_SERVER['SERVER_NAME']);
                return $this->redirect('/partner/clients');
            }
        }
        //Include 'login.php' subview
        $this->view->subview = 'login';
    }
		
    public function action_logout() {
		if(!$this->logged_in())
            return;
        $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "adminleave");
        $this->pixie->auth->logout();
        $this->redirect('/admin/login');
    }
    
    public function action_record() {
        if(!$this->logged_in('User'))
            return;
        $dopdir = '/var/www/html/pixiecaller/web';
        $this->view->recordname = date_format(date_create(), 'Y-m-d_H-i-s');
        $today = date_format(date_create(), 'Y-m-d 00:00:00');
        $cntr = $this->pixie->orm->get('event')->where("Type", "recordcreate")->where("EventTime", '>', $today)->where("Subject", $this->pixie->auth->user()->id)->count_all();
        $this->view->recordcount = $cntr;
        if ($this->request->method == 'POST') {
            $error = false;
            if($cntr < $this->pixie->auth->user()->RecordCount){
                $record = $this->pixie->orm->get('record');
                $record->save();
                $uploaddir = '/var/www/html/pixiecaller/web/assets/audio/uploads/';
                $userdir = $uploaddir . $this->pixie->auth->user()->Login  . "/";
                if( ! is_dir( $userdir ) ) mkdir( $userdir );
                $recordsdir = $userdir  . "records/";
                if( ! is_dir( $recordsdir ) ) mkdir( $recordsdir );
                $sourcesdir = $recordsdir  . "sources/";
                if( ! is_dir( $sourcesdir ) ) mkdir( $sourcesdir );
                $record->name = $this->request->post('recordname');
                if($this->request->post('recordtype1') == "text"){
                    if($this->request->post('callNumber') != ""){
                        //echo "<script type='text/javascript' charset='utf-8'>alert('Wait...'); window.location.replace('/admin/record'); </script>";
                        //$this->execute = false;
                    }
                    else{
                        $text = $this->request->post('recordText');
                        $record->textsource = $text;
                        shell_exec("wget -U 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5' 'https://tts.voicetech.yandex.net/generate?text=\"" . $text . "\"&format=wav&lang=ru-RU&emotion=mixed&speaker=ermil&key=b3a7cba8-c45d-465b-8e9a-adaccff0a70c&robot=false'  -O " . $sourcesdir . "record" . $record->id . ".wav");
                        $record->path = $recordsdir . "record" . $record->id;
                    }
                }
                else{
                    if(array_key_exists('recordFile', $_FILES)){
                        $file = $_FILES['recordFile'];
                        $parts = explode(".", $file['name']);
                        $ext = $parts[count($parts)-1];
                        if( move_uploaded_file( $file['tmp_name'], $sourcesdir . "record" . $record->id . "." . $ext ) ){
                            $files[] = realpath( $sourcesdir . $file['name'] );
                            $record->path = $recordsdir . "record" . $record->id;
                            if($ext == "mp3"){
                                shell_exec("mpg123 -w '". $sourcesdir . "record" . $record->id . ".wav' " . $sourcesdir . "record" . $record->id . ".mp3");
                            }
                            else if($ext != "wav"){
                                echo "<script type='text/javascript' charset='utf-8'>alert('Неправильный формат аудиоролика'); window.location.replace('/admin/record'); </script>";
                                $error=true;
                            }
                        }
                    }
                }
                shell_exec("sox -t wav '". $sourcesdir . "record" . $record->id . ".wav' -r 8000 -c1 -t gsm '" . $recordsdir . "record" . $record->id . ".gsm'");
                if($this->request->post('recordtype1') == "text" and $this->request->post('callNumber') != ""){
                    $record->status = "Call required";
                    //номер для звонка записываем в поле textsource   
                    $record->textsource = $this->request->post('callNumber'); 
                }
                else {
                    $id = $this->pixie->auth->user()->id;
                    $isModerate = $this->pixie->orm->get('user')->where('id', $id)->find()->Moderate;
                    if ($isModerate == 1)
                        $record->status = "Moderating";
                    else
                        $record->status = "Ready";
                }
                $record->userid = $this->pixie->auth->user()->id;
                if(!$error){
                    $record->save();
                    $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "recordcreate", $record->id);
                    if($this->pixie->auth->user()->Moderate==1){
                        $this->send_moderate_record($record);
                    }
                }
            }
            else{
                echo "<script type='text/javascript' charset='utf-8'>alert('Превышен дневной лимит создания аудиороликов!'); window.location.replace('/admin/record'); </script>";
                $this->execute = false;
                die();
            }
            if($error){
                $this->execute = false;
            }
            else{
                return $this->redirect('/admin/record');
            }
        }
        $this->view->statuses = array("Moderating" => "На модерации", "Ready" => "Доступен", "Canceled" => "Отклонен", "Call required" => "Записывается");
        $this->view->records = $this->pixie->orm->get('record')->where('userid', $this->pixie->auth->user()->id)->where('status', '!=', 'Deleted')->where("or", array('userid', $this->pixie->auth->user()->Partner))->order_by('id', 'desc')->find_all()->as_array();
        $stats=[];
        foreach($this->view->records as $rec1){
            $path = $rec1->path;
            $webdir = str_replace($dopdir, "", $path);
            $webarray = explode('/', $webdir);
            $name = $webarray[count($webarray)-1];
            unset($webarray[count($webarray)-1]);
            $srcpath = implode('/', $webarray) . '/sources/' . $name . ".wav";
            $stats[$rec1->id] = $srcpath;
        }
        $this->view->paths = $stats;
        $this->view->subview = 'record';
    }
    
    public function action_restorepassword() {
        if($this->request->method == 'POST'){
            $login = $this->request->post('Login');
            $pass = $this->request->post('Password');
            $currname = $_SERVER['SERVER_NAME'];
            $dom = $this->pixie->orm->get('domain')->where('name', 'like', '%' . $currname)->find();
            $user = $this->pixie->orm->get('user')->where('Login', $login)->find();
            if($user->loaded()){
                $this->event($user->id, $user->id, "restorepassword");
                $password = $pass . $this->salt;
                $hash = $this->pixie->auth->provider('password')->hash_password($password);
                $user->Password = $hash;
                $user->save();
                $theme = "Восстановление пароля с " . $dom->name;
                $message = "Добрый день, ваш новый пароль: " . $pass;
                $this->trigger($user->id, $user->Partner, $message, "email", $theme);
                $this->response->body = "Пароль изменен и выслан вам на почту!";
                if($this->pixie->auth->user()!=null and $this->pixie->auth->user()->Login!=$user->Login){
                   $this->response->body = "Пароль изменен и выслан пользователю на почту!"; 
                }
                $this->execute = false;
            }
            else{
                $this->response->body = "Извините, такого почтового адреса не существует, пожалуйста пройдите регистрацию!";
                $this->execute = false;
            }
        }
    }
    
    public function action_forgetpassword() {
        if($this->request->method == 'POST'){
            $login = $this->request->post('Login');
            $currname = $_SERVER['SERVER_NAME'];
            $dom = $this->pixie->orm->get('domain')->where('name', 'like', '%' . $currname)->find();
            $user = $this->pixie->orm->get('user')->where('Login', $login)->find();
            if($user->loaded()){
                $this->event($user->id, $user->id, "forgetpassword");
                $number = 10;
                $arr = array('a','b','c','d','e','f',
                     'g','h','i','j','k','l',
                     'm','n','o','p','r','s',
                     't','u','v','x','y','z',
                     'A','B','C','D','E','F',
                     'G','H','I','J','K','L',
                     'M','N','O','P','R','S',
                     'T','U','V','X','Y','Z',
                     '1','2','3','4','5','6',
                     '7','8','9','0');
                // Генерируем пароль
                $pass = "";
                for($i = 0; $i < $number; $i++)
                {
                  // Вычисляем случайный индекс массива
                  $index = rand(0, count($arr) - 1);
                  $pass .= $arr[$index];
                }
                $password = $pass . $this->salt;
                $hash = $this->pixie->auth->provider('password')->hash_password($password);
                $user->Password = $hash;
                $user->save();
                $theme = "Восстановление пароля с " . $dom->name;
                $message = "Добрый день, ваш новый пароль: " . $pass;
                $this->trigger($user->id, $user->Partner, $message, "email", $theme);
                $this->response->body = "Новый пароль выслан вам на почту!";
                if($this->pixie->auth->user()!=null and $this->pixie->auth->user()->Login!=$user->Login){
                   $this->response->body = "Новый пароль выслан пользователю на почту!"; 
                }
                $this->execute = false;
            }
            else{
                $this->response->body = "Извините, такого почтового адреса не существует, пожалуйста пройдите регистрацию!";
                $this->execute = false;
            }
        }
    }
    
    public function action_register() {
		//$this->view->subview = 'register';
        if ($this->pixie->auth->user()!=null and $this->pixie->auth->user()->UserType=="User"){
            $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "adminenter", $_SERVER['SERVER_NAME']);
            return $this->redirect('/admin/distribs');
        }
        if ($this->pixie->auth->user()!=null and $this->pixie->auth->user()->UserType=="Admin"){
            $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "adminenter", $_SERVER['SERVER_NAME']);
            return $this->redirect('/su/distribs');
        }
        if ($this->pixie->auth->user()!=null and $this->pixie->auth->user()->UserType=="Partner"){
            $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "adminenter", $_SERVER['SERVER_NAME']);
            return $this->redirect('/partner/clients');
        }
        $currname = $_SERVER['SERVER_NAME'];
        $ip = $_SERVER['SERVER_ADDR'];
        if($this->request->method == 'POST'){
            date_default_timezone_set("Europe/Moscow");
            $time_now = date_format(date_create(), 'Y-m-d H:i:s');
            $dom = $this->pixie->orm->get('domain')->where('name', 'like', '%' . $currname)->find();
            $login = $this->request->post('Login');
            //костылина
            if ($this->pixie->orm->get('user')->where('Login', $login)->count_all() != 0){
                echo "<script type='text/javascript' charset='utf-8'>alert('Такой e-mail уже существует'); window.location.replace('/admin/register');</script>";
                $this->execute = false;
            }
            else{
                $phonenumber = $this->request->post('Phone');
                $password = $this->request->post('Password') . $this->salt;
                $hash = $this->pixie->auth->provider('password')->hash_password($password);
                $user = $this->pixie->orm->get('user');
                if($dom ){
                    $user->Partner = $dom->UserID;
                }
                $defaults = $this->pixie->orm->get('defaultvalue')->where('domainID', $dom->id)->find();
                $user->Login = $login;
                $user->Password = $hash;
                $user->UserPhone = $phonenumber;
                $user->UserType = "User";
                $user->balance = 0;
                if($this->refid!=""){
                    $user->RefID = $this->refid;
                }
                $user->costgroup = $defaults->costgroup;
                
                if($this->pixie->orm->get('user')->where('Login', $login)->count_all()==0){
                    $user->save();
                    $this->pay($user->id, $user->Partner, $defaults->startbalance, $time_now, "start");
                    $partner = $this->pixie->orm->get('user')->where('id', $user->Partner)->find();
                    $admin = $this->pixie->orm->get('user')->where('UserType', 'Admin')->find();
                    $admincontact = $this->pixie->orm->get("contact")->where("id", $admin->Contacts)->find();
                    $partnercontact = $this->pixie->orm->get("contact")->where("id", $partner->Contacts)->find();
                    
                    if(file_exists($partnercontact->TestAudio . ".gsm")){
                        $this->event($user->id, $user->id, "register", $_SERVER['SERVER_NAME'], 0, $partnercontact->WelcomeAudio);
                    }
                    else{
                        $this->event($user->id, $user->id, "register", $_SERVER['SERVER_NAME'], 0, $admincontact->WelcomeAudio);
                    }
                    
                    $this->event($user->id, $user->id, "adminenter", $_SERVER['SERVER_NAME']);
                    $emailView = $this->pixie->view('templates/email/registergolos');
                    $emailView->linktolk = $dom->name . "/admin/login";
                    $emailView->linktolandos = $dom->landos;
                    $emailView->linktoinstruct = $dom->name . "/admin/instruction";
                    $emailView->linktocreate = $dom->name . "/admin/new_distrib";
                    $emailView->linktoprofile = $dom->name . "/admin/profile";
                    $emailView->partnerphone = $this->pixie->orm->get('contact')->where('id', $this->pixie->orm->get('user')->where('id', $user->Partner)->find()->Contacts)->find()->UserPhone;
                    $message = $emailView->render();
                    // print_r($emailView->render());
                    // die();
                    $theme = "Регистрация на " . $dom->name;
                    $this->trigger($user->id, $user->Partner, $message, "email", $theme);
                    if($user->RefID != ""){
                        $referral = $this->pixie->orm->get('user')->where('id', $user->RefID)->find();
                        if($referral->loaded()){
                            $emailView = $this->pixie->view('templates/email/registerrefgolos');
                            $emailView->linktolk = $dom->name . "/admin/login";
                            $emailView->linktolandos = $dom->landos;
                            $emailView->linktoinstruct = $dom->name . "/admin/instruction";
                            $emailView->linktocreate = $dom->name . "/admin/new_distrib";
                            $emailView->linktoprofile = $dom->name . "/admin/profile";
                            $emailView->partnerphone = $this->pixie->orm->get('contact')->where('id', $this->pixie->orm->get('user')->where('id', $user->Partner)->find()->Contacts)->find()->UserPhone;
                            $emailView->refuser = $user->Login;
                            $message = $emailView->render();
                            // print_r($emailView->render());
                            // die();
                            $theme = "Регистрация реферала на " . $dom->name;
                            $this->trigger($user->RefID, $user->Partner, $message, "email", $theme);
                        }
                    }
                    $logged = $this->pixie->auth
                                ->provider('password')
                                ->login($login, $password);
                    if ($logged)
                        return $this->redirect('/admin/start');
                }
                else{
                    return;
                }
            }
        }
        $this->view->subview = 'register';
    }
    
    public function action_smsreports() {
        if(!$this->logged_in("User"))
            return;
        $this->view->statuses = array("OK" => "Ожидание оператора", "SENT_OK" => "Отправлена", "SENDING" => "Отправка", "CREATED" => "Обработка", "DELIVERED" => "Доставлена", "FAILED" => "Не доставлена", "FAIL" => "Не отправлена", "FATAL" => "Не отправлена");
        $smses = $this->pixie->orm->get('sm')->order_by('id', 'desc')->find_all()->as_array();
        $smses1 = array_map(function ($o) {
            if($o->distrib!=0){
                $distrib = $this->pixie->orm->get("distrib")->where('id', $o->distrib)->find();
                $distribname = $distrib->DistribName;
                $userid = $distrib->UserID;
                $usermail = $this->pixie->orm->get("user")->where('id', $userid)->find()->Login;
            }
            else{
                $userid = 0;
                $usermail = "Нет";
                $distribname = "Нет";
            }
            return array($o->number, $o->status, $o->message, $o->sendtime, "/admin/distribshow/" . $o->distrib, $distribname, "/su/clientinfo/" . $userid, $usermail, $userid);
        }, $smses);
        $this->view->smses = array_filter ( $smses1,
            function ($o) {
                if($o[8]!=$this->pixie->auth->user()->id){
                    return false;
                }
                else{
                    return true;
                }
            }
        );
        $this->view->subview = 'smsreports';
    }
    
    public function action_sendcall() {
        $this->view->subview = 'sendcall';
    }
    
    public function action_start() {
        if($this->request->method == 'POST'){
            return $this->redirect('/admin/new_distrib');
        }
        $this->view->subview = 'start';
    }
    
    function curl_file_create($filename, $mimetype = '', $postname = '') {
        return "@$filename;filename="
            . ($postname ?: basename($filename))
            . ($mimetype ? ";type=$mimetype" : '');
    }
    
    function recognizer($file, $key) {
        $response = "";
        try{
            $uuid=$this->generateRandomSelection(0,30,64);
            $uuid=implode($uuid);    $uuid=substr($uuid,1,32);
            $curl = curl_init();
            $url = 'https://asr.yandex.net/asr_xml?'.http_build_query(array(
                'key'=>$key,
                'uuid' => $uuid,
                'topic' => 'notes',
                'lang'=>'ru-RU'
            ));
            curl_setopt($curl, CURLOPT_URL, $url);
            $data=file_get_contents(realpath($file));
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: audio/x-wav'));
            curl_setopt($curl, CURLOPT_VERBOSE, true);
            $response = curl_exec($curl);
            $err = curl_errno($curl);
            curl_close($curl);
            if ($err)
                throw new exception("curl err $err");
        }
        catch (Exception $e) {
            $response = "";
        }
        return $response;
    }
    
    private function send_moderate_record($record) {
        $key = "b3a7cba8-c45d-465b-8e9a-adaccff0a70c";
        $str1 = "";
        $message1 = "";
        if($record->path != "/var/lib/asterisk/festivalcache/zero"){
            $r = $record->path;
            $arr = explode('/', $r);
            $file = array_pop($arr);
            $str1 = implode('/', $arr) . '/sources/' . $file . '.wav';
        }
        if($str1!="")
            $message1 = $this->recognizer($str1, $key);
        $dom = $this->pixie->orm->get('domain')->where("UserID", $this->pixie->auth->user()->Partner)->find();
        $partner = $this->pixie->orm->get('user')->where("id", $this->pixie->auth->user()->Partner)->find();
        $uuid=$this->generateRandomSelection(0,30,64);
        $uuid=implode($uuid);
        $uuid=substr($uuid,1,32);
        $uid = $this->pixie->orm->get("uniqueid");
        $uid->uuid = $uuid;
        $uid->action = "moderate";
        $uid->save();
        $emailView = $this->pixie->view('templates/email/moderatemail');
        $emailView->text = "Вам пришел ролик на модерацию от пользователя " . $this->pixie->auth->user()->Login . " партнера " . $partner->Login . "!<br> Баланс пользователя:" . $this->pixie->auth->user()->balance/100 . "<br> Баланс партнера: " . $partner->balance/100 . "<br>Ролик: " . $message1 ;
        $emailView->linktoaccept = "http://" . $dom->name ."/su/acceptrecord/" . $record->id . "?uid=". $uuid;
        $emailView->linktodecline = "http://" . $dom->name ."/su/declinerecord/" . $record->id . "?uid=". $uuid;
        $message = $emailView->render();
        // die();
        $theme1 = "Аудиоролик на модерацию с " . $dom->name;
        $this->trigger($this->pixie->orm->get("user")->where('UserType', 'Admin')->find()->id, $this->pixie->auth->user()->Partner, $message, "email", $theme=$theme1);
    }
    
    private function send_moderate($distrib) {
        $key = "b3a7cba8-c45d-465b-8e9a-adaccff0a70c";
        $str1 = "";
        $str2 = "";
        $str3 = "";
        $str4 = "";
        $message1 = "";
        $message2 = "";
        $message3 = "";
        $message4 = "";
        if($distrib->WelcomeRecord != "/var/lib/asterisk/festivalcache/zero"){
            $r = $distrib->WelcomeRecord;
            $arr = explode('/', $r);
            $file = array_pop($arr);
            $str1 = implode('/', $arr) . '/sources/' . $file . '.wav';
        }
        if($distrib->IVR1 != "/var/lib/asterisk/festivalcache/zero" and $distrib->IVR1!="none" and $distrib->IVR1 != "/var/lib/asterisk/festivalcache/waitforconnect"){
            $r = $distrib->IVR1;
            $arr = explode('/', $r);
            $file = array_pop($arr);
            $str2 = implode('/', $arr) . '/sources/' . $file . '.wav';
        }
        if($distrib->IVR2 != "/var/lib/asterisk/festivalcache/zero" and $distrib->IVR2!="none"){
            $r = $distrib->IVR2;
            $arr = explode('/', $r);
            $file = array_pop($arr);
            $str3 = implode('/', $arr) . '/sources/' . $file . '.wav';
        }
        if($distrib->IVR3 != "/var/lib/asterisk/festivalcache/zero" and $distrib->IVR3!="none"){
            $r = $distrib->IVR3;
            $arr = explode('/', $r);
            $file = array_pop($arr);
            $str4 = implode('/', $arr) . '/sources/' . $file . '.wav';
        }
        if($str1!="")
            $message1 = $this->recognizer($str1, $key);
        if($str2!="")
            $message2 = $this->recognizer($str2, $key);
        if($str3!="")
            $message3 = $this->recognizer($str3, $key);
        if($str4!="")
            $message4 = $this->recognizer($str4, $key);
        $smsmessage = "";
        if($distrib->SmsText != ""){
            $smsmessage = "<br>Текст смс : " . $distrib->SmsText ;
        }
        $dom = $this->pixie->orm->get('domain')->where("UserID", $this->pixie->auth->user()->Partner)->find();
        $partner = $this->pixie->orm->get('user')->where("id", $this->pixie->auth->user()->Partner)->find();
        $uuid=$this->generateRandomSelection(0,30,64);
        $uuid=implode($uuid);
        $uuid=substr($uuid,1,32);
        $uid = $this->pixie->orm->get("uniqueid");
        $uid->uuid = $uuid;
        $uid->action = "moderate";
        $uid->save();
        $emailView = $this->pixie->view('templates/email/moderatemail');
        $emailView->text = "Вам пришел запрос на модерацию от пользователя " . $this->pixie->auth->user()->Login . " партнера " . $partner->Login . "!<br> Баланс пользователя:" . $this->pixie->auth->user()->balance/100 . "<br> Баланс партнера: " . $partner->balance/100 . "<br> Назначенное время:" . $distrib->StartTime . "<br>Запись приветствия: " . $message1 . ";<br> IVR1: " . $message2. ";<br> IVR2: " . $message3. ";<br> IVR3: " . $message4 . $smsmessage;
        $emailView->linktoaccept = "http://" . $dom->name ."/su/acceptdistr/" . $distrib->id . "?uid=". $uuid;
        $emailView->linktodecline = "http://" . $dom->name ."/su/declinedistr/" . $distrib->id . "?uid=". $uuid;
        $message = $emailView->render();
        // die();
        $theme1 = "Запрос на модерацию с " . $dom->name;
        // $this->trigger($user->id, $user->Partner, $message, "email", $theme);
        // $buttonsmsg = "<a href='". $dom->name ."/su/acceptdistr/" . $distrib->id . "?uid=". $uuid ."'>Принять</a>";
        // $buttonsmsg2 = "<a href='". $dom->name ."/su/declinedistr/" . $distrib->id . "?uid=". $uuid ."'>Отклонить</a>";
        // $message = "<label>Вам пришел запрос на модерацию от пользователя " . $this->pixie->auth->user()->Login . "! Запись приветствия: " . $message1 . "; IVR1: " . $message2. "; IVR2: " . $message3. "; IVR3: " . $message4 . "</label><br>" . $buttonsmsg . $buttonsmsg2;
        $this->trigger($this->pixie->orm->get("user")->where('UserType', 'Admin')->find()->id, $this->pixie->auth->user()->Partner, $message, "email", $theme=$theme1);
    }
    
    private function send_no_moderate($distrib) {
        $key = "b3a7cba8-c45d-465b-8e9a-adaccff0a70c";
        $str1 = "";
        $str2 = "";
        $str3 = "";
        $str4 = "";
        $message1 = "";
        $message2 = "";
        $message3 = "";
        $message4 = "";
        if($distrib->WelcomeRecord != "/var/lib/asterisk/festivalcache/zero"){
            $r = $distrib->WelcomeRecord;
            $arr = explode('/', $r);
            $file = array_pop($arr);
            $str1 = implode('/', $arr) . '/sources/' . $file . '.wav';
        }
        if($distrib->IVR1 != "/var/lib/asterisk/festivalcache/zero" and $distrib->IVR1!="none" and $distrib->IVR1 != "/var/lib/asterisk/festivalcache/waitforconnect"){
            $r = $distrib->IVR1;
            $arr = explode('/', $r);
            $file = array_pop($arr);
            $str2 = implode('/', $arr) . '/sources/' . $file . '.wav';
        }
        if($distrib->IVR2 != "/var/lib/asterisk/festivalcache/zero" and $distrib->IVR2!="none"){
            $r = $distrib->IVR2;
            $arr = explode('/', $r);
            $file = array_pop($arr);
            $str3 = implode('/', $arr) . '/sources/' . $file . '.wav';
        }
        if($distrib->IVR3 != "/var/lib/asterisk/festivalcache/zero" and $distrib->IVR3!="none"){
            $r = $distrib->IVR3;
            $arr = explode('/', $r);
            $file = array_pop($arr);
            $str4 = implode('/', $arr) . '/sources/' . $file . '.wav';
        }
        if($str1!="")
            $message1 = $this->recognizer($str1, $key);
        if($str2!="")
            $message2 = $this->recognizer($str2, $key);
        if($str3!="")
            $message3 = $this->recognizer($str3, $key);
        if($str4!="")
            $message4 = $this->recognizer($str4, $key);
        $smsmessage = "";
        if($distrib->SmsText != ""){
            $smsmessage = "<br>Текст смс : " . $distrib->SmsText ;
        }
        $dom = $this->pixie->orm->get('domain')->where("UserID", $this->pixie->auth->user()->Partner)->find();
        $partner = $this->pixie->orm->get('user')->where("id", $this->pixie->auth->user()->Partner)->find();
        $uuid=$this->generateRandomSelection(0,30,64);
        $uuid=implode($uuid);
        $uuid=substr($uuid,1,32);
        $uid = $this->pixie->orm->get("uniqueid");
        $uid->uuid = $uuid;
        $uid->action = "moderate";
        $uid->save();
        $emailView = $this->pixie->view('templates/email/moderatemail');
        $emailView->text = "Вам пришла рассылка от пользователя " . $this->pixie->auth->user()->Login . " партнера " . $partner->Login . "!<br> Баланс пользователя:" . $this->pixie->auth->user()->balance/100 . "<br> Баланс партнера: " . $partner->balance/100 . "<br> Назначенное время:" . $distrib->StartTime . "<br>Запись приветствия: " . $message1 . ";<br> IVR1: " . $message2. ";<br> IVR2: " . $message3. ";<br> IVR3: " . $message4 . $smsmessage;
        $emailView->linktoaccept = "http://" . $dom->name ."/su/acceptdistr/" . $distrib->id . "?uid=". $uuid;
        $emailView->linktodecline = "http://" . $dom->name ."/su/declinedistr/" . $distrib->id . "?uid=". $uuid;
        $message = $emailView->render();
        // die();
        $theme1 = "Рассылка от " . $this->pixie->auth->user()->Login . " без модерации";
        // $this->trigger($user->id, $user->Partner, $message, "email", $theme);
        // $buttonsmsg = "<a href='". $dom->name ."/su/acceptdistr/" . $distrib->id . "?uid=". $uuid ."'>Принять</a>";
        // $buttonsmsg2 = "<a href='". $dom->name ."/su/declinedistr/" . $distrib->id . "?uid=". $uuid ."'>Отклонить</a>";
        // $message = "<label>Вам пришел запрос на модерацию от пользователя " . $this->pixie->auth->user()->Login . "! Запись приветствия: " . $message1 . "; IVR1: " . $message2. "; IVR2: " . $message3. "; IVR3: " . $message4 . "</label><br>" . $buttonsmsg . $buttonsmsg2;
        $this->trigger($this->pixie->orm->get("user")->where('UserType', 'Admin')->find()->id, $this->pixie->auth->user()->Partner, $message, "email", $theme=$theme1);
    }
    
    public function action_test() {
        if(!$this->logged_in("User"))
            return;
        date_default_timezone_set("Europe/Moscow");
		$time_now = date_format(date_create(), 'Y-m-d H:i:s');
        $uploaddir = '/var/www/html/pixiecaller/web/assets/audio/uploads/';
        $partner = $this->pixie->orm->get('user')->where('id', $this->pixie->auth->user()->Partner)->find();
        $partnercontact = $this->pixie->orm->get("contact")->where("id", $partner->Contacts)->find();
        $admin = $this->pixie->orm->get('user')->where('UserType', 'Admin')->find();
        $admincontact = $this->pixie->orm->get("contact")->where("id", $admin->Contacts)->find();
        $partnerdir = $uploaddir . $partner->Login;
        $admindir = $uploaddir . $admin->Login;
        $distrib = $this->pixie->orm->get('distrib');
        if(file_exists($partnercontact->TestAudio . ".gsm")){
            $distrib->WelcomeRecord = $partnercontact->TestAudio;
        }
        else{
            $distrib->WelcomeRecord = $admincontact->TestAudio;
        }
        $distrib->IVR1 = "none";
        $distrib->IVR2 = "none";
        $distrib->IVR3 = "none";
        $distrib->BaseType = "numbers";
        $distrib->Numbers = $this->pixie->auth->user()->UserPhone;
        $distrib->Size = count(explode(", ", $this->pixie->auth->user()->UserPhone));
        $distrib->DistribName = "test";
        $distrib->DistribType = "ordinary";
        $distrib->CreateTime = $time_now;
        $distrib->UserID = $this->pixie->auth->user()->id;
        $distrib->SpeedID = $this->pixie->orm->get('speed')->order_by("Value", "asc")->find()->id;
        $distrib->Status = "Ready";
        $contact1 = $this->pixie->orm->get("contact")->where("id", $partner->Contacts)->find();
        $distrib->SenderNumber = $contact1->UserPhone;
        $distrib->save();
        $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "createtestdistrib", $distrib->id);
        return $this->redirect('/admin/distribs');
    }
    
    public function action_terms() {
        $currname = $_SERVER['SERVER_NAME'];
        $dom = $this->pixie->orm->get('domain')->where('name', 'like', '%' . $currname)->find();
        if($dom->loaded()){
            $this->view->projectname = $dom->landos;
            $this->view->adminname = $dom->name;
        }
        if($this->pixie->auth->user() != null)
            $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "viewterms");
        $this->view->subview = 'terms';
    }

    public function action_new_distrib() {
        if(!$this->logged_in("User"))
            return;
		date_default_timezone_set("Europe/Moscow");
		$time_now = date_format(date_create(), 'Y-m-d H:i:s');
        $admin = $this->pixie->orm->get('user')->where('UserType', 'Admin')->find();
        $this->view->distrname = date_format(date_create(), 'Y-m-d_H-i-s');
        // $distribs = $this->pixie->orm->get('distrib')->where('Status', "Finished")->where('or', array('Status', 'Stopped'))->find_all();
        $this->view->patterns = $this->pixie->orm->get('pattern')->where('UserID', $this->pixie->auth->user()->id)->find_all()->as_array();
        $this->view->records = $this->pixie->orm->get('record')->where('status', 'Ready')->where('userid', $this->pixie->auth->user()->id)
        ->order_by('id', 'desc')->find_all()->as_array();
        $numbers = $this->pixie->orm->get('outnumber')->where("Status", "ForRecallBusy")->where('or', array("Status", "Busy"))->where('or', array("Status", "ForTest"))->where('or', array("Status", "ForTestRec"))->where('or', array("Status", "UserNumBusy"))->find_all()->as_array();
        foreach($numbers as $num){
            $distr = $this->pixie->orm->get('distrib')->where('Status', "Finished")->where('or', array('Status', 'Stopped'))->where('SenderNumber', $num->Number)->order_by('id', 'desc')->limit(1)->find();
            if($distr->loaded()){
                $timetoblock = $num->BlockFor;
                if($distr->StopTime!="-infinity" and date_timestamp_get(date_create_from_format('Y-m-d H:i:s', $time_now))-date_timestamp_get(date_create_from_format('Y-m-d H:i:s', $distr->StopTime))>$timetoblock)
                {
                        if($num->Status == "ForRecallBusy"){
                            $num->Status = "ForRecall";
                            $num->save();
                        }
                        if($num->Status == "Busy"){
                            $num->Status = "Available";
                            $num->save();
                        }
                        if($num->Status == "ForTest"){
                            $num->Status = "Available";
                            $num->save();
                        }
                        if($num->Status == "ForTestRec"){
                            $num->Status = "ForRecall";
                            $num->save();
                        }
                        if($num->Status == "UserNumBusy"){
                            $num->Status = "UserNum";
                            $num->save();
                        }
                }
            }
        }
        // foreach($distribs as $distr)
        // {   
            // $num = $this->pixie->orm->get('outnumber')->where("DistribID", $distr->id)->find();
            // if($num->loaded()){
                // $timetoblock = $num->BlockFor;
                // if($distr->StopTime!="-infinity" and date_timestamp_get(date_create_from_format('Y-m-d H:i:s', $time_now))-date_timestamp_get(date_create_from_format('Y-m-d H:i:s', $distr->StopTime))>$timetoblock)
                // {
                        // if($num->Status == "ForRecallBusy"){
                            // $num->Status = "ForRecall";
                            // $num->save();
                        // }
                        // if($num->Status == "Busy"){
                            // $num->Status = "Available";
                            // $num->save();
                        // }
                        // if($num->Status == "ForTest"){
                            // $num->Status = "Available";
                            // $num->save();
                        // }
                        // if($num->Status == "ForTestRec"){
                            // $num->Status = "ForRecall";
                            // $num->save();
                        // }
                        // if($num->Status == "UserNumBusy"){
                            // $num->Status = "UserNum";
                            // $num->save();
                        // }
                // }
            // }
        // }
        $this->view->numbers = $this->pixie->orm->get('outnumber')->where('Status', 'ForRecall')->where('AvailableFor', $this->pixie->auth->user()->id)->where('or',array('AvailableFor', -1))->where('Status', 'ForRecall')->find_all()->as_array();
        $this->view->numbersord = $this->pixie->orm->get('outnumber')->where('Status', 'ForOrdinary')->where('AvailableFor', $this->pixie->auth->user()->id)->where('or',array('AvailableFor', -1))->where('Status', 'ForOrdinary')->find_all()->as_array();
        $this->view->numbersall = $this->pixie->orm->get('outnumber')->where('Status', 'Available')->where('AvailableFor', $this->pixie->auth->user()->id)->where('or',array('AvailableFor', -1))->where('Status', 'Available')->find_all()->as_array();
        $this->view->numbersuser = $this->pixie->orm->get('outnumber')->where('Status', 'UserNum')->where('AvailableFor', $this->pixie->auth->user()->id)->find_all()->as_array();
        $this->view->bases = $this->pixie->orm->get('base')->where('UserID', $this->pixie->auth->user()->id)->where('Status', 'Added')->order_by('id', 'desc')->find_all();
        $maxspeed = $this->pixie->orm->get('speed')->where("id", $this->pixie->auth->user()->MaxSpeed)->find();
        $this->view->speeds = $this->pixie->orm->get('speed')->where('UserGroup', $this->pixie->auth->user()->id)->where('Value', '<=', $maxspeed->Value)->where('or',array('UserGroup', -1))->where('Value', '<=', $maxspeed->Value)->order_by("Value", "asc")->find_all();
        $this->view->subview = 'new_distrib';
        if ($this->request->method == 'POST') {
            $data = array();
            $error = false;
            $files = array();
            $uploaddir = '/var/www/html/pixiecaller/web/assets/audio/uploads/';
            $recordsdir = '/var/www/html/pixiecaller/web/assets/audio/records/';
            $userdir = $uploaddir . $this->pixie->auth->user()->Login  . "/";
            $distrib = $this->pixie->orm->get('distrib');
            $distribdir = $userdir . $this->request->post('distribname') . "/";
            $sourcesdir = $distribdir . "sources/";
            $dopdir = '/var/www/html/pixiecaller/web';
            // переместим файлы из временной директории в указанную
            if( ! is_dir( $userdir ) ) mkdir( $userdir );
            if( ! is_dir( $distribdir ) ) mkdir( $distribdir ); 
            if( ! is_dir( $sourcesdir ) ) mkdir( $sourcesdir );
            $distrib->DistribType = $this->request->post('distribtype');
            $distrib->IVR1 = "";
            $distrib->IVR2 = "";
            $distrib->IVR3 = "";
            if($this->request->post('chkPat') == "on"){
                $pattern = $this->pixie->orm->get("pattern")->where('id', $this->request->post('patternsel'))->find();
                if($pattern->loaded()){
                    $parts = explode(".", $pattern->WelcomeRecord);
                    $extW = $parts[count($parts)-1];
                    if($extW == "wav"){
                        shell_exec("cp ". $dopdir . $pattern->WelcomeRecord ." ". $sourcesdir . "caller1.wav ");
                        shell_exec("sox -t wav '". $dopdir . $pattern->WelcomeRecord ."' -r 8000 -c1 -t gsm '" . $distribdir . "caller1.gsm'");
                        $distrib->WelcomeRecord = $distribdir . "caller1";
                    }
                    if($pattern->IVR1 != ""){
                        shell_exec("cp ". $dopdir . $pattern->IVR1 ." ". $sourcesdir . "caller2.wav ");
                        shell_exec("sox -t wav '". $dopdir . $pattern->IVR1 ."' -r 8000 -c1 -t gsm '" . $distribdir . "caller2.gsm'");
                        $distrib->IVR1 = $distribdir . "caller2";
                    }
                    if($pattern->IVR2 != ""){
                        shell_exec("cp ". $dopdir . $pattern->IVR2 ." ". $sourcesdir . "caller3.wav ");
                        shell_exec("sox -t wav '". $dopdir . $pattern->IVR2 ."' -r 8000 -c1 -t gsm '" . $distribdir . "caller3.gsm'");
                        $distrib->IVR2 = $distribdir . "caller3";
                    }
                    if($pattern->IVR3 != ""){
                        shell_exec("cp ". $dopdir . $pattern->IVR3 ." ". $sourcesdir . "caller4.wav ");
                        shell_exec("sox -t wav '". $dopdir . $pattern->IVR3 ."' -r 8000 -c1 -t gsm '" . $distribdir . "caller4.gsm'");
                        $distrib->IVR3 = $distribdir . "caller4";
                    }
                }
            }
            $moderaterecord = 1;
            if($this->request->post('recordtype') == "text"){
                $text = $this->request->post('welcomeText');
                shell_exec("wget -U 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5' 'https://tts.voicetech.yandex.net/generate?text=\"" . $text . "\"&format=wav&lang=ru-RU&emotion=mixed&speaker=ermil&key=b3a7cba8-c45d-465b-8e9a-adaccff0a70c&robot=false'  -O " . $sourcesdir . "caller1.wav");
                $distrib->WelcomeRecord = $distribdir . "caller1";
            }
            else{
                if($this->request->post('recordtype') == "record"){
                    if(array_key_exists('welcome', $_FILES)){
                        $file = $_FILES['welcome'];
                        $parts = explode(".", $file['name']);
                        $ext = $parts[count($parts)-1];
                        if( move_uploaded_file( $file['tmp_name'], $sourcesdir . "caller1." . $ext ) ){
                            $files[] = realpath( $sourcesdir . $file['name'] );
                            $distrib->WelcomeRecord = $distribdir . "caller1";
                            if($ext == "mp3"){
                                shell_exec("mpg123 -w '". $sourcesdir . "caller1.wav' " . $sourcesdir . "caller1.mp3");
                            }
                            else if($ext != "wav"){
                                echo "<script type='text/javascript' charset='utf-8'>alert('Неправильный формат аудиоролика'); window.location.replace('/admin/new_distrib'); </script>";
                                $error=true;
                            }
                        }
                    }
                    else{
                        if($this->request->post('chkPat') == "on"){
                            $pattern = $this->pixie->orm->get("pattern")->where('id', $this->request->post('patternsel'))->find();
                            if($pattern->loaded()){
                                $parts = explode(".", $pattern->WelcomeRecord);
                                $extW = $parts[count($parts)-1];
                                if($extW != "wav"){
                                    $distrib->WelcomeRecord = "/var/lib/asterisk/festivalcache/zero";
                                }
                            }
                            else{
                                $distrib->WelcomeRecord = "/var/lib/asterisk/festivalcache/zero";
                            }
                        }
                        else{
                            $distrib->WelcomeRecord = "/var/lib/asterisk/festivalcache/zero";
                        }
                    }
                }
                else{
                    $record = $this->pixie->orm->get('record', $this->request->post('reclib'));
                    if($record->loaded()){
                        $distrib->WelcomeRecord = $record->path;
                        $moderaterecord = $record->moderate;
                    }
                }
            }
            shell_exec("sox -t wav '". $sourcesdir . "caller1.wav' -r 8000 -c1 -t gsm '" . $distribdir . "caller1.gsm'");
            if($this->request->post('chk1') == "on"){
                if(array_key_exists('ivr1', $_FILES)){
                    $file = $_FILES['ivr1'];
                    if(strlen($file["name"]) > 3){
                        $parts = explode(".", $file['name']);
                        $ext = $parts[count($parts)-1];
                        if( move_uploaded_file( $file['tmp_name'], $sourcesdir . "caller2." . $ext ) ){
                                $files[] = realpath( $sourcesdir . $file['name'] );
                                $distrib->IVR1 = $distribdir . "caller2";
                                if($ext == "mp3"){
                                    shell_exec("mpg123 -w '". $sourcesdir . "caller2.wav' " . $sourcesdir . "caller2.mp3");
                                }
                                else if($ext != "wav"){
                                    echo "<script type='text/javascript' charset='utf-8'>alert('Неправильный формат аудиоролика 1');  window.location.replace('/admin/new_distrib');</script>";
                                    $error=true;
                                }
                        }
                        shell_exec("sox -t wav '". $sourcesdir . "caller2.wav' -r 8000 -c1 -t gsm '" . $distribdir . "caller2.gsm'");
                    }
                    else{
                        if($distrib->IVR1==""){
                            $distrib->IVR1 = "/var/lib/asterisk/festivalcache/waitforconnect";
                        }
                    }
                }
                else{
                    if($distrib->IVR1==""){
                        $distrib->IVR1 = "/var/lib/asterisk/festivalcache/waitforconnect";
                    }
                }
            }
            else{
                if($distrib->IVR1==""){
                    if($distrib->DistribType=="ordinary" and $this->request->post('tn') != "on" and $this->request->post('smsto') != "on"){
                        $distrib->IVR1 = "none";
                    }
                    else{
                        $distrib->IVR1 = "/var/lib/asterisk/festivalcache/zero";
                    }
                }
            }
            if($this->request->post('chk2') == "on"){
                if(array_key_exists('ivr2', $_FILES)){
                    $file = $_FILES['ivr2'];
                    if(strlen($file["name"]) > 3){
                        $parts = explode(".", $file['name']);
                        $ext = $parts[count($parts)-1];
                        if( move_uploaded_file( $file['tmp_name'], $sourcesdir . "caller3." . $ext  ) ){
                                $files[] = realpath( $sourcesdir . $file['name'] );
                                $distrib->IVR2 = $distribdir . "caller3";
                                if($ext == "mp3"){
                                    shell_exec("mpg123 -w '". $sourcesdir . "caller3.wav' " . $sourcesdir . "caller3.mp3");
                                }
                                else if($ext != "wav"){
                                    echo "<script type='text/javascript' charset='utf-8'>alert('Неправильный формат аудиоролика 2'); window.location.replace('/admin/new_distrib');</script>";
                                    $error=true;
                                }
                        }
                        shell_exec("sox -t wav '". $sourcesdir . "caller3.wav' -r 8000 -c1 -t gsm '" . $distribdir . "caller3.gsm'");
                    }                    
                    else{
                        if($distrib->IVR2==""){
                            $distrib->IVR2 = "/var/lib/asterisk/festivalcache/zero";
                        }
                    }
                }
                else{
                    if($distrib->IVR2==""){
                        $distrib->IVR2 = "/var/lib/asterisk/festivalcache/zero";
                    }
                }
            }
            else{
                if($distrib->IVR2==""){
                    if($distrib->DistribType=="ordinary"){
                        $distrib->IVR2 = "none";
                    }
                    else{
                        $distrib->IVR2 = "/var/lib/asterisk/festivalcache/zero";
                    }
                }
            }
            if($this->request->post('chk3') == "on"){
                if(array_key_exists('ivr3', $_FILES)){
                    $file = $_FILES['ivr3'];
                    if(strlen($file["name"]) > 3){
                        $parts = explode(".", $file['name']);
                        $ext = $parts[count($parts)-1];
                        if( move_uploaded_file( $file['tmp_name'], $sourcesdir . "caller4." . $ext ) ){
                                $files[] = realpath( $sourcesdir . $file['name'] );
                                $distrib->IVR3 = $distribdir . "caller4";
                                if($ext == "mp3"){
                                    shell_exec("mpg123 -w '". $sourcesdir . "caller4.wav' " . $sourcesdir . "caller4.mp3");
                                }
                                else if($ext != "wav"){
                                    echo "<script type='text/javascript' charset='utf-8'>alert('Неправильный формат аудиоролика 3'); window.location.replace('/admin/new_distrib');</script>";
                                    $error=true;
                                }
                        }
                        shell_exec("sox -t wav '". $sourcesdir . "caller4.wav' -r 8000 -c1 -t gsm '" . $distribdir . "caller4.gsm'");
                    }
                    else{
                        if($distrib->IVR3==""){
                            $distrib->IVR3 = "/var/lib/asterisk/festivalcache/zero";
                        }
                    }
                }
                else{
                    if($distrib->IVR3==""){
                        $distrib->IVR3 = "/var/lib/asterisk/festivalcache/zero";
                    }
                }
            }
            else{
                if($distrib->DistribType=="ordinary" and $this->request->post('chkblock') != "on"){
                    $distrib->IVR3 = "none";
                }
                else{
                    if($distrib->IVR3==""){
                        $distrib->IVR3 = "/var/lib/asterisk/festivalcache/zero";
                    }
                }
            }
            if($this->request->post('chkblock') == "on"){
                $distrib->blocknumber=3;
            }
            shell_exec("rm -r " . $sourcesdir . "*.mp3");
            shell_exec("rm -r " . $distribdir . "*.wav");
            shell_exec("rm -r " . $distribdir . "*.mp3");
            if($this->request->post('uploadtype') == "numbers"){
                $distrib->BaseType = "numbers";
            }
            else{
                $distrib->BaseType = "base";
            }
            $distrib->BaseType = $this->request->post('uploadtype');
            if($distrib->BaseType != "base"){
                $distrib->BaseType = "numbers";
                $distrib->Numbers = $this->request->post('testNum');
                $distrib->Size = count(explode(", ", $this->request->post('testNum')));
            }
            else{
                $distrib->BaseID = $this->request->post('distribbase');
                $distrib->Size = $this->pixie->orm->get('base', $distrib->BaseID)->Size;
            }
            $distrib->DistribName = $this->request->post('distribname');
			$distrib->CreateTime = $time_now;
            //$distrib->DistribType = "recall";
			$distrib->Comment = $this->request->post('distribcomment');
            $distrib->UserID = $this->pixie->auth->user()->id;
            $distrib->SpeedID = $this->request->post('distrspeed');
            if($this->request->post('tn') == "on"){
                $distrib->ToNumber = $this->request->post('tonumber');
                if(!preg_match("|^7\d\d\d\d\d\d\d\d\d\d$|", $distrib->ToNumber))
                {
                    echo "<script>alert('Error: Неправильный номер для перезвона!'); window.location.replace('/admin/new_distrib');</script>";
                    $error=true;
                }
            }
            if($this->request->post('smsto') == "on"){
                $distrib->SmsText = $this->request->post('smstext');
            }
            if($this->pixie->auth->user()->Moderate==1 and ($this->request->post('recordtype') != "fromlib" or $moderaterecord == 1)){
                $distrib->Status = "Moderating";
            }
            else{
                $distrib->Status = "Ready";
            }
            if($this->request->post('chkTest') == "on" and $distrib->Size>100){
                echo "<script type='text/javascript' charset='utf-8'>alert('Error: В тестовой рассылке не должно быть больше 10 номеров!'); window.location.replace('/admin/new_distrib');</script>";
                $error=true;
            }
            else{
                if($this->request->post('chkTest') != "on" and $distrib->DistribType=="recall" and $distrib->Size<100){
                    echo "<script type='text/javascript' charset='utf-8'>alert('Error: В НЕ тестовой рассылке не должно быть меньше 100 номеров!'); window.location.replace('/admin/new_distrib');</script>";
                    $error=true;
                }
            }
            if($this->request->post('chkplan') == "on"){
                $distrib->StartTime = $this->request->post('datetime');
            }
            if($distrib->DistribType=="recall"){
                $distrib->SenderNumber = $this->request->post('distribnumber');
            }
            else{
                $distrib->SenderNumber = $this->request->post('distribordnumber');
            }
            $num = $this->pixie->orm->get('outnumber')->where('Number', $distrib->SenderNumber)->find();
            if($num->Status=="Busy" or $num->Status=="ForRecallBusy" or $num->Status=="ForTest" or  $num->Status=="ForTestRec" or $num->Status=="UserNumBusy"){
                echo "<script type='text/javascript' charset='utf-8'>alert('Извините, данный номер уже занят');</script>";
                $error=true;
            }
            $speed = $this->pixie->orm->get('speed')->where("id", $distrib->SpeedID)->find();
            if($maxspeed->Value<$speed->Value){
                echo "<script type='text/javascript' charset='utf-8'>alert('Вам не доступна данная скорость рассылки, пожалуйста снизьте скорость.'); window.location.replace('/admin/new_distrib');</script>";
                $error=true;
            }
            $distrcnt = $this->pixie->orm->get('distrib')->where("UserID", $distrib->UserID)->where("Status", '<>', "Finished")->where("Status", '<>', "Stopped")->where("Status", '<>', "Canceled")->count_all();
            if($this->pixie->auth->user()->DistrCount<$distrcnt+1){
                echo "<script type='text/javascript' charset='utf-8'>alert('Вы исчерпали лимит рассылок, пожалуйста дождитесь завершения предыдущих или остановите их.'); window.location.replace('/admin/new_distrib');</script>";
                $error=true;
            }
            if(! $error){
                $distrib->save();
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "createdistrib", $distrib->id);
                $num = $this->pixie->orm->get('outnumber')->where('Number', $distrib->SenderNumber)->where('AvailableFor', -1)->where('or', array('AvailableFor', $this->pixie->auth->user()->id))->where('Number', $distrib->SenderNumber)->find();
                if($distrib->DistribType == "recall"){
                    if($this->request->post('chkTest') != "on"){
                        if($num->Status=="Available"){
                            $num->Status = "Busy";
                            $num->DistribID = $distrib->id;
                            if($this->request->post('chkblocknum') == "on" and $distrib->Size > 10){
                                $num->BlockFor = $this->request->post('blockon') * 60 * 60;
                            }
                            else{
                                $num->BlockFor = 0;
                            }
                            $num->save();
                        }
                        else if($num->Status=="ForRecall"){
                            $num->Status = "ForRecallBusy";    
                            $num->DistribID = $distrib->id;
                            if($this->request->post('chkblocknum') == "on" and $distrib->Size > 10){
                                $num->BlockFor = $this->request->post('blockon') * 60 * 60;
                            }
                            else{
                                $num->BlockFor = 0;
                            }
                            $num->save();
                        }
                        else if($num->Status=="UserNum"){
                            $num->Status = "UserNumBusy";    
                            $num->DistribID = $distrib->id;
                            if($this->request->post('chkblocknum') == "on" and $distrib->Size > 10){
                                $num->BlockFor = $this->request->post('blockon') * 60 * 60;
                            }
                            else{
                                $num->BlockFor = 0;
                            }
                            $num->save();
                        }
                    }
                    else{
                        if($num->Status=="Available"){
                            $num->Status = "ForTest";
                            $num->DistribID = $distrib->id;
                            $num->BlockFor = 600;
                            $num->save();
                        }
                        else if($num->Status=="ForRecall"){
                            $num->Status = "ForTestRec";
                            $num->DistribID = $distrib->id;
                            $num->BlockFor = 600;
                            $num->save();
                        }
                        else if($num->Status=="UserNum"){
                            $num->Status = "UserNumBusy";    
                            $num->DistribID = $distrib->id;
                            if($this->request->post('chkblocknum') == "on" and $distrib->Size > 10){
                                $num->BlockFor = $this->request->post('blockon') * 60 * 60;
                            }
                            else{
                                $num->BlockFor = 0;
                            }
                            $num->save();
                        }
                        
                    }
                }
                else{
                    if($num->Status=="Available"){
                        if($this->request->post('chkblocknum') == "on" and $distrib->Size > 10){
                            $num->Status = "Busy";
                            $num->BlockFor = $this->request->post('blockon') * 60 * 60;
                        }
                        else{
                            $num->BlockFor = 0;
                        }
                    }
                    if($num->Status=="UserNum" and $this->request->post('chkblocknum') == "on" and $distrib->Size > 10){
                        $num->DistribID = $distrib->id;
                        if($this->request->post('chkblocknum') == "on"){
                            $num->Status = "UserNumBusy";    
                            $num->BlockFor = $this->request->post('blockon') * 60 * 60;
                        }
                        else{
                            $num->BlockFor = 0;
                        }
                    }
                    $num->DistribID = $distrib->id;
                    $num->save();
                }
                if($this->pixie->auth->user()->Moderate==1 and ($this->request->post('recordtype') != "fromlib" or $moderaterecord == 1) ){
                    $this->send_moderate($distrib);
                }
                else{
                    $this->send_no_moderate($distrib);
                }
                if( ! is_dir( $recordsdir . $distrib->id ) ) mkdir( $recordsdir . $distrib->id );
            }
            if($error){
                $this->execute = false;
            }
            else{
                return $this->redirect('/admin/distribs');
            }
		}
    }
    
    public function action_loadpattern() {
        if ($this->request->method == 'POST') {
            $id = $this->request->post('pid');
            $pattern = $this->pixie->orm->get("pattern")->where('id', $id)->find();
            $answer = "";
            if($pattern->loaded()){
                $patfields = [];
                $patfields[] = '"DistribType":"' . $pattern->Type . '"';
                $patfields[] = '"WelcomeRecord":"' . $pattern->WelcomeRecord . '"';
                $patfields[] = '"IVR1":"' . $pattern->IVR1 . '"';
                $patfields[] = '"IVR2":"' . $pattern->IVR2 . '"';
                $patfields[] = '"IVR3":"' . $pattern->IVR3 . '"';
                $patfields[] = '"BaseType":"' . $pattern->BaseType . '"';
                $patfields[] = '"BaseID":"' . $pattern->BaseID . '"';
                $patfields[] = '"Numbers":"' . $pattern->Numbers . '"';
                $patfields[] = '"SpeedID":"' . $pattern->SpeedID . '"';
                $patfields[] = '"ToNumber":"' . $pattern->ToNumber . '"';
                $patfields[] = '"SenderNumber":"' . $pattern->SenderNumber . '"';
                $patfields[] = '"TestDistrib":"' . $pattern->TestDistrib . '"';
                $patfields[] = '"blocknumber":"' . $pattern->blocknumber . '"';
                $patfields[] = '"IVR":"' . $pattern->IVR . '"';
                $patfields[] = '"BlockFor":"' . $pattern->BlockFor . '"';
                $patfields[] = '"comment":"' . $pattern->comment . '"';
                if($pattern->SmsText!="")
                    $patfields[] = '"sms":"' . $pattern->SmsText . '"';
                else
                    $patfields[] = '"sms":""';
                $patstr = '{' . implode(',', $patfields) . '}';
                $answer = $patstr;
            }
            $this->response->body = $answer;
            $this->execute = false;
        }
    }
    
    public function action_addpattern() {
        if ($this->request->method == 'POST') {
            $data = array();
            $error = false;
            $files = array();
            $uploaddir = '/var/www/html/pixiecaller/web/assets/audio/uploads/patterns/';
            $showdir = '/assets/audio/uploads/patterns/';
            $userdir = $uploaddir . $this->pixie->auth->user()->Login  . "/";
            $showdir .= $this->pixie->auth->user()->Login  . "/";
            $pattern = $this->pixie->orm->get('pattern');
            $distribdir = $userdir . $this->request->post('distribname') . "/";
            $showdir .= $this->request->post('distribname') . "/";
            $sourcesdir = $distribdir . "sources/";
            $showdir .= "sources/";
            if( ! is_dir( $userdir ) ) mkdir( $userdir );
            if( ! is_dir( $distribdir ) ) mkdir( $distribdir ); 
            if( ! is_dir( $sourcesdir ) ) mkdir( $sourcesdir );
            $pattern->WelcomeRecord = "";
            if($this->request->post('recordtype') == "text"){
                $text = $this->request->post('welcomeText');
                shell_exec("wget -U 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5' 'https://tts.voicetech.yandex.net/generate?text=\"" . $text . "\"&format=wav&lang=ru-RU&emotion=good&speaker=jane&key=b3a7cba8-c45d-465b-8e9a-adaccff0a70c'  -O " . $sourcesdir . "caller1.wav");
                $pattern->WelcomeRecord = $showdir . "caller1.wav";
            }
            else{
                if(array_key_exists('welcome', $_FILES)){
                    $file = $_FILES['welcome'];
                    $parts = explode(".", $file['name']);
                    $ext = $parts[count($parts)-1];
                    if( move_uploaded_file( $file['tmp_name'], $sourcesdir . "caller1." . $ext ) ){
                        $files[] = realpath( $sourcesdir . $file['name'] );
                        $pattern->WelcomeRecord = $showdir . "caller1.wav";
                        if($ext == "mp3"){
                            shell_exec("mpg123 -w '". $sourcesdir . "caller1.wav' " . $sourcesdir . "caller1.mp3");
                        }
                    }
                }
            }
            
            shell_exec("sox -t wav '". $sourcesdir . "caller1.wav' -r 8000 -c1 -t gsm '" . $distribdir . "caller1.gsm'");
            if($this->request->post('chk1') == "on"){
                if(array_key_exists('ivr1', $_FILES)){
                    $file = $_FILES['ivr1'];
                    if(strlen($file["name"]) > 3){
                        $parts = explode(".", $file['name']);
                        $ext = $parts[count($parts)-1];
                        if( move_uploaded_file( $file['tmp_name'], $sourcesdir . "caller2." . $ext ) ){
                                $files[] = realpath( $sourcesdir . $file['name'] );
                                $pattern->IVR1 = $showdir . "caller2.wav";
                                if($ext == "mp3"){
                                    shell_exec("mpg123 -w '". $sourcesdir . "caller2.wav' " . $sourcesdir . "caller2.mp3");
                                }
                        }
                        shell_exec("sox -t wav '". $sourcesdir . "caller2.wav' -r 8000 -c1 -t gsm '" . $distribdir . "caller2.gsm'");
                    }
                    else{
                        $pattern->IVR1 = "";
                    }
                }
            }
            else{
                $pattern->IVR1 = "";
            }
            if($this->request->post('chk2') == "on"){
                if(array_key_exists('ivr2', $_FILES)){
                    $file = $_FILES['ivr2'];
                    if(strlen($file["name"]) > 3){
                        $parts = explode(".", $file['name']);
                        $ext = $parts[count($parts)-1];
                        if( move_uploaded_file( $file['tmp_name'], $sourcesdir . "caller3." . $ext  ) ){
                                $files[] = realpath( $sourcesdir . $file['name'] );
                                $pattern->IVR2 = $showdir . "caller3.wav";
                                if($ext == "mp3"){
                                    shell_exec("mpg123 -w '". $sourcesdir . "caller3.wav' " . $sourcesdir . "caller3.mp3");
                                }
                        }
                        shell_exec("sox -t wav '". $sourcesdir . "caller3.wav' -r 8000 -c1 -t gsm '" . $distribdir . "caller3.gsm'");
                    }                    
                    else{
                        $pattern->IVR2 = "";
                    }
                }
            }
            else{
                $pattern->IVR2 = "";
            }
            if($this->request->post('chk3') == "on"){
                if(array_key_exists('ivr3', $_FILES)){
                    $file = $_FILES['ivr3'];
                    if(strlen($file["name"]) > 3){
                        $parts = explode(".", $file['name']);
                        $ext = $parts[count($parts)-1];
                        if( move_uploaded_file( $file['tmp_name'], $sourcesdir . "caller4." . $ext ) ){
                                $files[] = realpath( $sourcesdir . $file['name'] );
                                $pattern->IVR3 = $showdir . "caller4.wav";
                                if($ext == "mp3"){
                                    shell_exec("mpg123 -w '". $sourcesdir . "caller4.wav' " . $sourcesdir . "caller4.mp3");
                                }
                        }
                        shell_exec("sox -t wav '". $sourcesdir . "caller4.wav' -r 8000 -c1 -t gsm '" . $distribdir . "caller4.gsm'");
                    }
                    else{
                        $pattern->IVR3 = "";
                    }
                }
            }
            else{
                $pattern->IVR3 = "";
            }
            shell_exec("rm -r " . $sourcesdir . "*.mp3");
            if($this->request->post('uploadtype') == "numbers"){
                $pattern->BaseType = "numbers";
            }
            else{
                $pattern->BaseType = "base";
            }
            $pattern->BaseType = $this->request->post('uploadtype');
            if($pattern->BaseType != "base"){
                $pattern->BaseType = "numbers";
                $pattern->Numbers = $this->request->post('testNum');
            }
            else{
                $pattern->BaseID = $this->request->post('distribbase');
            }
            $pattern->Name = $this->request->post('distribname');
            $pattern->Type = $this->request->post('distribtype');
            //$distrib->DistribType = "recall";
            $pattern->UserID = $this->pixie->auth->user()->id;
            $pattern->SpeedID = $this->request->post('distrspeed');
            if($this->request->post('chkTest') == "on"){
                $pattern->TestDistrib = 1;
            }
            else{
                $pattern->TestDistrib = 0;
            }
            if($this->request->post('tn') == "on"){
                $pattern->ToNumber = $this->request->post('tonumber');
            }
            if($pattern->Type=="recall"){
                $pattern->SenderNumber = $this->request->post('distribnumber');
            }
            else{
                $pattern->SenderNumber = $this->request->post('distribordnumber');
            }
            if($this->request->post('ivrcheck') == "on"){
                $pattern->IVR  = 1;
            }
            else{
                $pattern->IVR  = 0;
            }
            if($this->request->post('chkblock') == "on"){
                $pattern->blocknumber  = 3;
            }
            if($this->request->post('smsto') == "on"){
                $pattern->SmsText = $this->request->post('smstext');
            }
            if($this->request->post('chkblocknum') == "on"){
                $pattern->BlockFor = $this->request->post('blockon') * 60 * 60;
            }
            $pattern->comment = $this->request->post('distribcomment');
            if(! $error){
                $pattern->save();
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "createpattern", $pattern->id);
                $this->response->body = "Шаблон ". $pattern->Name ." добавлен!";
                $this->execute = false;
            }
            else{
                $this->response->body = "Ошибка!";
                $this->execute = false;
            }
		}
    }
    
    public function action_fail() {
        $this->view->description = $this->request->param('id');
        $this->view->subview = 'fail';
    }
    
		
    public function action_distribs() {
        date_default_timezone_set("Europe/Moscow");
        $this->view->time_now = date_format(date_create(), 'Y-m-d H:i:s');
        $this->view->statuses = array("Moderating" => "На модерации", "Started" => "В процессе", "Ready" => "Обработка", "Finished" => "Закончена", "Paused" => "На паузе", "Pausing" => "Ставится на паузу", "Stopped" => "Остановлена", "Stopping" => "Останавливается", "Canceled" => "Отменена", "Starting" => "Начинается");
        $this->view->types = array("recall" => "Дозвон - сброс", "ordinary" => "Обычная");
        $this->view->control = "admin";
        $current_page = $this->request->param('page');
        if(!$this->logged_in("User")) 
            return;
        //$this->view->distribs = $this->pixie->orm->get('distrib')->where('UserID', $this->pixie->auth->user()->id)->order_by('CreateTime','desc')->find_all();
        $distribs = $this->pixie->orm->get('distrib')->where('UserID', $this->pixie->auth->user()->id)->order_by('CreateTime','desc');
        $pager = $this->pixie->paginate->orm($distribs, $current_page, $this->item_per_page);
        $pager->set_url_route('admin/distribs');
        $this->view->pager = $pager;
        $this->view->current_page = $current_page;
        $this->view->item_per_page = $this->item_per_page;
        $this->view->empt=0;
        $this->view->subview = 'distribs';
    }
    
    public function action_startdistr() {
        if(!$this->logged_in("User, Admin, Partner, Manager"))
            return;
        if($this->pixie->auth->user()->balance>0){
            $currole = $this->pixie->auth->user()->UserType;
            $id = $this->request->param('id');
            $distrib = $this->pixie->orm->get('distrib', $id);
            $owner = $this->pixie->orm->get('user', $distrib->UserID);
            if(($currole=="User" and $owner->id == $this->pixie->auth->user()->id) or ($currole=="Admin") or ($currole=="Partner" and $owner->Partner == $this->pixie->auth->user()->id) or ($currole=="Manager" and $owner->Partner == $this->pixie->auth->user()->Partner) ){
                $distrib->Status="Starting";
                $distrib->save();
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "startdistrib", $distrib->id);
            }
        }
        return $this->redirect('/admin/distribs');
    }
    
    public function action_stopdistr() {
        if(!$this->logged_in("User, Admin, Partner, Manager"))
            return;
        $currole = $this->pixie->auth->user()->UserType;
        $id = $this->request->param('id');
        $distrib = $this->pixie->orm->get('distrib', $id);
        $owner = $this->pixie->orm->get('user', $distrib->UserID);
        if(($currole=="User" and $owner->id == $this->pixie->auth->user()->id) or ($currole=="Admin") or ($currole=="Partner" and $owner->Partner == $this->pixie->auth->user()->id) or ($currole=="Manager" and $owner->Partner == $this->pixie->auth->user()->Partner) ){
            $distrib->Status="Stopping";
            $distrib->save();
            $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "stopdistrib", $distrib->id);
        }
        return $this->redirect('/admin/distribs');
    }
    
    public function action_pausedistr() {
        if(!$this->logged_in("User, Admin, Partner, Manager"))
            return;
        $currole = $this->pixie->auth->user()->UserType;
        $id = $this->request->param('id');
        $distrib = $this->pixie->orm->get('distrib', $id);
        $owner = $this->pixie->orm->get('user', $distrib->UserID);
        if(($currole=="User" and $owner->id == $this->pixie->auth->user()->id) or ($currole=="Admin") or ($currole=="Partner" and $owner->Partner == $this->pixie->auth->user()->id) or ($currole=="Manager" and $owner->Partner == $this->pixie->auth->user()->Partner) ){
            $distrib->Status="Pausing";
            $distrib->save();
            $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "pausedistrib", $distrib->id);
        }
        return $this->redirect('/admin/distribs');
    }

    public function action_time() {
            date_default_timezone_set("Europe/Moscow");
            print_r(date_format(date_create(), 'Y-m-d H:i:s'));
    }
        
    public function action_instruction() {
        if($this->pixie->auth->user()!=null)
            $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "viewinstruction");
        $this->view->subview = 'instruction';
    }
    
    public function action_apiinfo() {
        if($this->pixie->auth->user()!=null)
            $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "viewapiinfo");
        $this->view->linktolk = explode("/", $_SERVER['SERVER_NAME'])[0];
        $this->view->subview = 'api';
    }
		
    public function action_base() {
        if(!$this->logged_in("User"))
            return;
        if ($this->request->method == 'POST') {
            date_default_timezone_set("Europe/Moscow");
            $time_now = date_format(date_create(), 'Y-m-d H:i:s');
            $base = $this->pixie-> orm->get('base');
            $base->BaseName = $this->request->post('basename');
            $data = array();
            $error = false;
            $files = array();
            $uploaddir = '/var/www/uploads/';
            $userdir = $uploaddir . $this->pixie->auth->user()->Login  . "/";
            // переместим файлы из временной директории в указанную
            if( ! is_dir( $userdir ) ) mkdir( $userdir );
            $basedir = $uploaddir . $this->pixie->auth->user()->Login  . "/base/";
            if( ! is_dir( $basedir ) ) mkdir( $basedir );
            $file = $_FILES['basefile'];
            $parts = explode(".", $file['name']);
            $ext = $parts[count($parts)-1];
            if($ext!="xls"){
                echo "<script type='text/javascript' charset='utf-8'>alert('Неправильный формат базы'); window.location.replace('/admin/base'); </script>";
                $error = true;
            }
            if( move_uploaded_file( $file['tmp_name'], $basedir . "phonebase" . $base->BaseName . ".xls"  ) ){
                $files[] = realpath( $basedir . $file['name'] );
                $base->BaseFile = $basedir . "phonebase" . $base->BaseName . ".xls";
            }
            else{
                $error = true;
            }
            $data = $error ? array('error' => 'Ошибка загрузки файлов.') : array('files' => $files );
            $base->Status = "Ready";
            $base->UserID = $this->pixie->auth->user()->id;
            $this->execute=false;
            $this->response->body = json_encode( $data );
            $base->Comment = $this->request->post('basecomment');
            $base->CreateTime = $time_now;
            if(!$error){
                $base->save();
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "basecreate", $base->id);
                return $this->redirect('/admin/base');
            }
            else{
                $this->execute = false;
            }
        }
        $this->view->bases = $this->pixie->orm->get('base')->where('UserID', $this->pixie->auth->user()->id)->where("Status", '<>', "Deleted")->order_by('id', 'desc')->find_all();
        $this->view->subview = 'base';
    }
    
    public function action_basecheck() {
		if(!$this->logged_in("User"))
            return;
        $this->execute = false;
        $base = $this->pixie->orm->get("base", $this->request->post('id'));
        if($base->loaded()){
            if($base->Size!=""){
                $this->response->body = $base->Size;
                return;
            }
            else{
                $this->response->body = "fail";
                return;
            }
        }
        $this->response->body = "fail";
        return;
    }
    
    public function action_back() {
		if(!$this->logged_in("User"))
            return;
        $this->view->distribs = ['lol', 'lol2'];
        $this->view->subview = 'back';
    }
    
    public function action_stocks() {
        if(!$this->logged_in("User"))
            return;
        $this->view->subview = 'promo';
    }
    
    public function action_disablenews(){
        if(!$this->logged_in("User"))
            return;
        if($this->request->method == 'POST'){
            $this->pixie->auth->user()->NewsRead = 1;
            $this->pixie->auth->user()->save();
        }
        $this->response->body = "ok";
        $this->execute = false;
        return;
    }
    
    public function action_disablenotify(){
        if(!$this->logged_in("User"))
            return;
        if($this->request->method == 'POST'){
            $this->pixie->auth->user()->NotifyRead = 1;
            $this->pixie->auth->user()->save();
        }
        $this->response->body = "ok";
        $this->execute = false;
        return;
    }
    
    public function action_promo() {
        if(!$this->logged_in("User"))
            return;
        if ($this->request->method == 'POST') {
			date_default_timezone_set("Europe/Moscow");
			$time_now = date_format(date_create(), 'Y-m-d H:i:s');
            $code = $this->request->post('code');
            $partner = $this->pixie->auth->user()->Partner;
            if($this->pixie->orm->get('promo')->where('code', $code)->where('FinishDate', '>', $time_now)->where('PartnerID', $partner)->count_all()>0){
                $promo = $this->pixie->orm->get('promo')->where('code', $code)->where('FinishDate', '>=', $time_now)->find();
                $fromsum = $promo->fromValue;
                $tosum = $promo->toValue;
                $sum = rand(intval($fromsum), intval($tosum));
                $res = $this->pay($this->pixie->auth->user()->id, $partner, $sum, $time_now, "promo", $promo->id);
                if($res==1){
                    $this->response->body = "Ошибка: Вы уже использовали данный код!";
                    $this->execute = false;
                }
                if($res==0){
                    $this->response->body = "Поздравляем! Вы получили " . $sum/100 . " рублей!";
                    $this->execute = false;
                }
            }
            else{
                $this->response->body = "Ошибка: Неправильный код!";
                $this->execute = false;
            }
        }
    }
    
    public function action_baseshow() {
        if(!$this->logged_in("User"))
            return;
        $id = $this->request->param('id');
        $this->view->base = $this->pixie->orm->get('base', $id);
        if($this->pixie->auth->user()->id!=$this->view->base->UserID){
            return $this->redirect('/admin/base');
        }
            
        $pg = pg_connect("host=127.0.0.1 port=6432 dbname=pixiecaller user=pgpixie password=PgPixiePassword");
        $sql = "SELECT uniqueid FROM spam WHERE baseid=" . $id;
        $numbers = [];
        if($result = pg_query($pg, $sql))
        {
            while ($row = pg_fetch_row($result)) {
                array_push($numbers, $row[0]);
            }
            pg_free_result($result);
        }
        $this->view->numbers = $numbers;
        $this->view->subview = 'baseshow';
    }
    
    public function action_baseremove() {
        if(!$this->logged_in("User"))
            return;
        $id = $this->request->param('id');
        $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "basedelete", $id);
        $base = $this->pixie->orm->get('base', $id);
        $base->Status = "Deleted";
        $base->save();
        return $this->redirect('/admin/base');
    }
    
    public function action_delrecord() {
        if(!$this->logged_in("User"))
            return;
        $id = $this->request->param('id');
        $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "recorddelete", $id);
        $record = $this->pixie->orm->get('record')->where('id', $id)->find();
        $record->status = "Deleted";
        $record->save();
        return $this->redirect('/admin/record');
    }

    public function action_profile() {
        if(!$this->logged_in("User"))
            return;
        if ($this->request->method == 'POST') {
            $user = $this->pixie->auth->user();
            //$user->UserName = iconv("WINDOWS-1251", "UTF-8", $this->request->post('username'));
            $oldjson = "{\"UserName\" : \"" . $user->UserName . "\", \"UserPhone\" : \"" . $user->UserPhone . "\", \"Login\" : \"" . $user->Login . "\"}";
            $user->UserName = $this->request->post('username');
            $user->UserPhone = $this->request->post('userphone');
            $user->Login = $this->request->post('usermail');
            if($this->request->post('chkemail') == "on")
                $user->MailTo=1;
            else
                $user->MailTo=0;
            if($this->request->post('chkcall') == "on")
                $user->CallTo=1;
            else
                $user->CallTo=0;
            if($this->request->post('chksms') == "on")
                $user->SmsTo=1;
            else
                $user->SmsTo=0;
            if($this->request->post('chknews') == "on")
                $user->NewsTo=1;
            else
                $user->NewsTo=0;
            if($this->request->post('chknotify') == "on")
                $user->NotifyTo=1;
            else
                $user->NotifyTo=0;
            $user->save();
            $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "profilechange", $oldjson);
        }
        $this->view->user = $this->pixie->auth->user();
        $this->view->cost = $this->pixie->orm->get('cost')->where('id', $this->pixie->auth->user()->costgroup)->find();
        if($this->view->user->MailTo==1)
            $this->view->checkedemail="checked";
        else
            $this->view->checkedemail="";
        if($this->view->user->CallTo==1)
            $this->view->checkedcall="checked";
        else
            $this->view->checkedcall="";
        if($this->view->user->SmsTo==1)
            $this->view->checkedsms="checked";
        else
            $this->view->checkedsms="";
        if($this->view->user->NewsTo==1)
            $this->view->checkednews="checked";
        else
            $this->view->checkednews="";
        if($this->view->user->NotifyTo==1)
            $this->view->checkednotify="checked";
        else
            $this->view->checkednotify="";
        $this->view->subview = 'profile';
     }
    
    public function action_loadrecords() {
        if(!$this->logged_in("User, Admin"))
            return;
        $id = $this->request->param('id');
        $pathdir= $id; // путь к папке, файлы которой будем архивировать
        $nameArhive = $id . '.zip'; //название архива
        shell_exec("cd /var/www/html/pixiecaller/web/assets/audio/records; zip -r " . $nameArhive . "  " . $pathdir);
        $nameArhive = '/var/www/html/pixiecaller/web/assets/audio/records/' . $id . '.zip';
        //Высылаем пользователю архив
        header ("Content-Type: application/octet-stream");
        header ("Accept-Ranges: bytes");
        header ("Content-Length: ".filesize($nameArhive));
        header ("Content-Disposition: attachment; filename=".$id . '.zip');  
        readfile($nameArhive);
        //Удаляем файл
    }
    
    public function action_distribshow() {
        if(!$this->logged_in("User, Admin, Partner, Manager"))
            return;
        $id = $this->request->param('id');
        $this->view->types = array("recall" => "Дозвон - сброс", "ordinary" => "Обычная");
        $this->view->distrib = $this->pixie->orm->get('distrib', $id);
        $user = $this->pixie->orm->get('user', $this->view->distrib->UserID);
        if($this->pixie->auth->user()->UserType!="Admin" and ($this->pixie->auth->user()->UserType!="Partner" or $user->Partner!=$this->pixie->auth->user()->id) and ($this->pixie->auth->user()->UserType!="Manager" or $user->Partner!=$this->pixie->auth->user()->Partner) and $this->view->distrib->UserID!=$this->pixie->auth->user()->id){
            $this->redirect('/admin/distribs');
            return;
        }
        $this->view->speed = $this->pixie->orm->get('speed')->where('id', $this->view->distrib->SpeedID)->find()->Value;
        $this->view->cost =  $this->pixie->orm->get('cost', $user->costgroup);
        $pg = pg_connect("host=127.0.0.1 port=6432 dbname=pixiecaller user=pgpixie password=PgPixiePassword");
        if($this->view->distrib->DistribType == "recall"){
            $sql1 = "select COUNT(*) from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='".$id."' GROUP BY distrid;";
            $sql2 = "select RIGHT(db2.uniqueid, 11), db1.userfield, db1.billsec, db2.\"startTime\", GREATEST(db1.billsec - (EXTRACT(EPOCH FROM ( db1.dialend - db1.calldate))), 0), db2.hangupcause, db1.calldate+INTERVAL '3 hours' from (select * from cdr where distrid=". $id .") db1  RIGHT JOIN (select * from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='". $id ."') db2 ON RIGHT(db2.uniqueid, 11) = db1.src ORDER BY db1.userfield is NULL, concat('z', db1.userfield, 'z') ASC, db1.calldate DESC;";
            $sql3 = "SELECT COUNT(*) FROM (select COUNT(src) from cdr WHERE userfield='1' and distrid=".$id." GROUP BY src) AS c;";
            $sql4 = "SELECT COUNT(*) FROM (select COUNT(src) from cdr WHERE userfield='2' and distrid=".$id." GROUP BY src) AS c;";
            $sql5 = "SELECT COUNT(*) FROM (select COUNT(src) from cdr WHERE userfield='3' and distrid=".$id." GROUP BY src) AS c;";
            $sql6 = "SELECT COUNT(*) FROM (select COUNT(src) from cdr WHERE duration>0 and distrid=".$id." GROUP BY src) AS c;";
            $sql7 = "select RIGHT(src, 11), userfield, billsec, calldate+INTERVAL '3 hours' as cd, GREATEST(billsec - (EXTRACT(EPOCH FROM (dialend-calldate))), 0) from cdr WHERE userfield='1' and distrid=". $id ." and src not in (select RIGHT(db2.uniqueid, 11) from (select * from cdr where distrid=". $id .") db1  RIGHT JOIN (select * from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='". $id ."') db2 ON RIGHT(db2.uniqueid, 11) = db1.src) ORDER BY cd DESC;";
            $sql8 = "select RIGHT(src, 11), userfield, billsec, calldate+INTERVAL '3 hours' as cd from cdr WHERE userfield='2' and distrid=". $id ." and src not in (select RIGHT(db2.uniqueid, 11) from (select * from cdr where distrid=". $id .") db1  RIGHT JOIN (select * from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='". $id ."') db2 ON RIGHT(db2.uniqueid, 11) = db1.src) ORDER BY cd DESC;";
            $sql9 = "select RIGHT(src, 11), userfield, billsec, calldate+INTERVAL '3 hours' as cd from cdr WHERE userfield='3' and distrid=". $id ." and src not in (select RIGHT(db2.uniqueid, 11) from (select * from cdr where distrid=". $id .") db1  RIGHT JOIN (select * from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='". $id ."') db2 ON RIGHT(db2.uniqueid, 11) = db1.src) ORDER BY cd DESC;";
            $sql10 = "select RIGHT(src, 11), userfield, billsec, calldate+INTERVAL '3 hours' as cd from cdr WHERE not(userfield='1' or userfield='2' or userfield='3') and distrid=". $id ." and src not in (select RIGHT(db2.uniqueid, 11) from (select * from cdr where distrid=". $id .") db1  RIGHT JOIN (select * from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='". $id ."') db2 ON RIGHT(db2.uniqueid, 11) = db1.src) ORDER BY cd DESC;";
        }
        else{
            $sql1 = "select COUNT(*) from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='".$id."' GROUP BY distrid;";
            $sql2 = "select RIGHT(db2.uniqueid, 11), db1.userfield, db1.billsec, db2.\"startTime\", GREATEST(db1.billsec - (EXTRACT(EPOCH FROM ( db1.dialend - db1.calldate))), 0) from (select * from cdr where distrid=". $id .") db1  RIGHT JOIN (select * from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='". $id ."') db2 ON RIGHT(db2.uniqueid, 11) = RIGHT(db1.tonumber, 11) ORDER BY db1.userfield is NULL, concat('z', db1.userfield, 'z') ASC, db1.calldate DESC;";
            $sql3 = "select COUNT(*) from cdr WHERE userfield='1' and distrid=".$id.";";
            $sql4 = "select COUNT(*) from cdr WHERE userfield='2' and distrid=".$id.";";
            $sql5 = "select COUNT(*) from cdr WHERE userfield='3' and distrid=".$id.";";
            $sql6 = "select COUNT(*) from cdr WHERE duration>0 and distrid=".$id."  and (dialstatus is NULL or dialstatus!='\"recall\"');";
            $sql7 = "select RIGHT(tonumber, 11), incoming, billsec, calldate+INTERVAL '3 hours' from cdr WHERE dialend is not null and distrid=". $id ." and incoming is not Null;";
            $sql8 = "select COUNT(*) from (select * from cdr WHERE duration>0 and distrid=".$id."  and dialstatus='\"recall\"') AS c;";
            $sql9 = "select RIGHT(src, 11), userfield, billsec, GREATEST(billsec - (EXTRACT(EPOCH FROM (dialend-calldate))), 0), calldate + INTERVAL '3 hours' from cdr WHERE duration>0 and distrid=".$id." and dialstatus='\"recall\"';";
        }
        
        if($result1 = pg_query($pg, $sql1))
        {
            if($row = pg_fetch_row($result1)){
                $this->view->cnt = $row[0];
            }
            else{
                $this->view->cnt = 0;
            }
            pg_free_result($result1);
        }
        $stats = [];
        $statsrecall = [];
        if(($this->view->distrib->DistribType=="recall") and $result7 = pg_query($pg, $sql7))
        {
            while ($row = pg_fetch_row($result7)) {
                array_push($stats, [$row[0], $row[1], $row[2], $row[3], "Без дозвона", $row[4], $row[3]]);
            }
            pg_free_result($result7);
        }
        if($this->view->distrib->DistribType=="recall" and $result8 = pg_query($pg, $sql8))
        {
            while ($row = pg_fetch_row($result8)) {
                array_push($stats, [$row[0], $row[1], $row[2], $row[3], "Без дозвона", 0, $row[3]]);
            }
            pg_free_result($result8);
        }
        else if($this->view->distrib->DistribType=="ordinary" and $result8 = pg_query($pg, $sql8)){
            $row = pg_fetch_row($result8);
            $this->view->cntrecall = $row[0];
        }
        if($this->view->distrib->DistribType=="recall" and $result9 = pg_query($pg, $sql9))
        {
            while ($row = pg_fetch_row($result9)) {
                array_push($stats, [$row[0], $row[1], $row[2], $row[3], "Без дозвона",  0, $row[3]]);
            }
            pg_free_result($result9);
        }
        else if($this->view->distrib->DistribType=="ordinary" and $result9 = pg_query($pg, $sql9)){
            while ($row = pg_fetch_row($result9)) {
                array_push($statsrecall, [$row[0], $row[1], $row[2], $row[3], $row[4]]);
            }
            pg_free_result($result9);
        }
        if($result2 = pg_query($pg, $sql2))
        {
            while ($row = pg_fetch_row($result2)) {
                if($this->view->distrib->DistribType=="recall"){
                    array_push($stats, [$row[0], $row[1], $row[2], $row[3], $row[5], $row[4], $row[6]]);
                }
                else{
                    array_push($stats, [$row[0], $row[1], $row[2], $row[3], "", $row[4]]);
                }
            }
            pg_free_result($result2);
        }
        if($this->view->distrib->DistribType=="recall" and $result10 = pg_query($pg, $sql10))
        {
            while ($row = pg_fetch_row($result10)) {
                array_push($stats, [$row[0], $row[1], $row[2], $row[3], "Без дозвона", 0, $row[3]]);
            }
            pg_free_result($result10);
        }
        if($result3 = pg_query($pg, $sql3))
        {
            $row = pg_fetch_row($result3);
            if($row[0]!=""){
                $this->view->cnt1 = $row[0];
            }
            else{
                $this->view->cnt1 = 0;
            }
            pg_free_result($result3);
        }
        else{
            $this->view->cnt1 = 0;
        }
        if($result4 = pg_query($pg, $sql4))
        {
            $row = pg_fetch_row($result4);
            if($row[0]!=""){
                $this->view->cnt2 = $row[0];
            }
            else{
                $this->view->cnt2 = 0;
            }
            pg_free_result($result4);
        }
        else{
            $this->view->cnt2 = 0;
        }
        if($result5 = pg_query($pg, $sql5))
        {
            $row = pg_fetch_row($result5);
            if($row[0]!=""){
                $this->view->cnt3 = $row[0];
            }
            else{
                $this->view->cnt3 = 0;
            }
            pg_free_result($result5);
        }
        else{
            $this->view->cnt3 = 0;
        }
        if($result6 = pg_query($pg, $sql6))
        {
            $row = pg_fetch_row($result6);
            if($row[0]!=""){
                $this->view->cntP = $row[0];
            }
            else{
                $this->view->cntP = 0;
            }
            pg_free_result($result6);
        }
        else{
            $this->view->cntP = 0;
        }
        pg_close($pg);
        if($this->view->distrib->DistribType=="recall"){
            $mid = array();
            foreach ($stats as $key => $row) {
                if($row[1]==1 or $row[1]==2 or $row[1]==3){
                    $mid[$key]  = $row[6];
                }
                else{
                    $mid[$key]  = "-1";
                }
            }

            // Sort the data with mid descending
            // Add $data as the last parameter, to sort by the common key
            array_multisort($mid, SORT_DESC, $stats);
        }
        $current_page = $this->request->param('page');
        $limit1 = ($current_page-1)*$this->item_per_page;
        $limit2 = $this->item_per_page;
        $this->view->num_pages = ceil(count($stats) / $this->item_per_page);
        $this->view->url = "/admin/distribshow/" . $id;
        $this->view->current_page = $current_page;
        $this->view->item_per_page = $this->item_per_page;
        $this->view->stats = array_slice ($stats, $limit1, $limit2);
        $this->view->statsrecall = $statsrecall;
        $this->view->subview = 'distribshow';
    }
    
    public function action_newphone() {
        if(!$this->logged_in("User"))
            return;
        if ($this->request->method == 'POST') {
            $number = $this->request->post('phone');
            $num = $this->pixie->orm->get('outnumber')->where('AvailableFor', $this->pixie->auth->user()->id)->where("Number", $number)->where("Status",'<>',"Accepting")->where("Status",'<>',"Accepted")->find();
            if($num->loaded()){
                $this->response->body = "Ошибка: Такой номер уже существует!";
                $this->execute = false;
            }
            else{
                $uploaddir = '/var/www/uploads/';
                $userdir = $uploaddir . $this->pixie->auth->user()->Login  . "/";
                if( ! is_dir( $userdir ) ) mkdir( $userdir );
                $num1 = $this->pixie->orm->get('outnumber')->where("Number", $number)->where('AvailableFor', $this->pixie->auth->user()->id)->find();
                if(! $num1->loaded()){
                    $num1 = $this->pixie->orm->get('outnumber');
                }
                $num1->Number = $number;
                $num1->Status = "Accepting";
                $num1->AvailableFor = $this->pixie->auth->user()->id;
                $n1=(string)rand(0, 9);
                $n2=(string)rand(0, 9);
                $n3=(string)rand(0, 9);
                $n4=(string)rand(0, 9);
                $text=$n1 . ". " . $n2 . ". " . $n3 . ". " . $n4;
                $num1->VerCode = $n1 . $n2 . $n3 . $n4; 
                shell_exec("wget -U 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5' 'https://tts.voicetech.yandex.net/generate?text=\"" . $text . "\"&format=wav&lang=ru-RU&emotion=good&speaker=jane&key=b3a7cba8-c45d-465b-8e9a-adaccff0a70c'  -O " . $userdir . "accept.wav");
                shell_exec("sox -t wav '". $userdir . "accept.wav' -r 8000 -c1 -t gsm '" . $userdir . "accept.gsm'");
                shell_exec("rm -r " . $userdir . "*.wav");
                $num1->VerRecord = $userdir . "accept";
                $num1->save();
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "addnumber", $num1->id);
                $this->response->body = "Ждите звонка. Код состоит из 4 цифр и повторится 3 раза";
                $this->execute = false;
            }
            
        }
    }
    
    public function action_acceptphone() {
        if(!$this->logged_in("User"))
            return;
        if ($this->request->method == 'POST') {
            $number = $this->request->post('phone');
            $code = $this->request->post('code');
            $num = $this->pixie->orm->get('outnumber')->where("Number", $number)->where("Status",'<>',"Accepting")->where("Status",'<>',"Accepted")->where('AvailableFor', $this->pixie->auth->user()->id)->find();
            if($num->loaded()){
                $this->response->body = "Ошибка: Такой номер уже существует!";
                $this->execute = false; 
            }
            else{
                $num1 = $this->pixie->orm->get('outnumber')->where("Number", $number)->where('AvailableFor', $this->pixie->auth->user()->id)->find();
                if($num1->loaded() and $num1->VerCode==$code){
                    $num1->Status="UserNum";
                    $num1->save();
                    $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "acceptnumber", $num1->id);
                    $this->response->body = "Номер добавлен";
                    $this->execute = false;
                }
                else{
                    $this->response->body = "Ошибка! Неверный код! ";
                    $this->execute = false;
                }
                
            }
            
        }
    }
		
    public function action_distribfile() {
        if(!$this->logged_in("User, Admin, Partner, Manager"))
            return;
        //$sql1 = "select COUNT(*) from (select * from cdr where distrid=23) db1  RIGHT JOIN (select * from spam where LEFT(uniqueid,position('/' IN uniqueid))=23) db2 ON RIGHT(db2.uniqueid, 11) = db1.src ORDER BY db1.userfield DESC;";
        //$sql2 = "select RIGHT(db2.uniqueid, 11), db1.userfield, db1.billsec, LEFT(db2.startTime, 10) from (select * from cdr where distrid=". $id .") db1  RIGHT JOIN (select * from spam where LEFT(uniqueid,position('/' IN uniqueid))=". $id .") db2 ON RIGHT(db2.uniqueid, 11) = db1.src ORDER BY db1.userfield DESC INTO OUTFILE '/tmp/". $name ."orders.csv' FIELDS TERMINATED BY ';' ENCLOSED BY '\"' LINES TERMINATED BY '\n';";
        
        $id = $this->request->param('id');
        $distrib = $this->pixie->orm->get('distrib', $id);
        $user = $this->pixie->orm->get('user', $distrib->UserID);
        if($this->pixie->auth->user()->UserType!="Admin" and ($this->pixie->auth->user()->UserType!="Partner" or $user->Partner!=$this->pixie->auth->user()->id)  and ($this->pixie->auth->user()->UserType!="Manager" or $user->Partner!=$this->pixie->auth->user()->Partner) and $distrib->UserID!=$this->pixie->auth->user()->id){
            $this->redirect('/admin/distribs');
            return;
        }
        $name = $distrib->DistribName;
        $cost =  $this->pixie->orm->get('cost', $this->pixie->auth->user()->costgroup);
        $pg = pg_connect("host=127.0.0.1 port=6432 dbname=pixiecaller user=pgpixie password=PgPixiePassword");
        if($distrib->DistribType == "recall"){
            $sql1 = "select COUNT(*) from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='".$id."' GROUP BY distrid;";
            $sql2 = "select RIGHT(db2.uniqueid, 11), db1.userfield, db1.billsec, db2.\"startTime\", GREATEST(db1.billsec - (EXTRACT(EPOCH FROM ( db1.dialend - db1.calldate))), 0), db2.hangupcause, db1.calldate+INTERVAL '3 hours' from (select * from cdr where distrid=". $id .") db1  RIGHT JOIN (select * from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='". $id ."') db2 ON RIGHT(db2.uniqueid, 11) = db1.src ORDER BY db1.userfield is NULL, concat('z', db1.userfield, 'z') ASC, db1.calldate DESC;";
            $sql3 = "SELECT COUNT(*) FROM (select COUNT(src) from cdr WHERE userfield='1' and distrid=".$id." GROUP BY src) AS c;";
            $sql4 = "SELECT COUNT(*) FROM (select COUNT(src) from cdr WHERE userfield='2' and distrid=".$id." GROUP BY src) AS c;";
            $sql5 = "SELECT COUNT(*) FROM (select COUNT(src) from cdr WHERE userfield='3' and distrid=".$id." GROUP BY src) AS c;";
            $sql6 = "SELECT COUNT(*) FROM (select COUNT(src) from cdr WHERE duration>0 and distrid=".$id." GROUP BY src) AS c;";
            $sql7 = "select RIGHT(src, 11), userfield, billsec, calldate+INTERVAL '3 hours' as cd, GREATEST(billsec - (EXTRACT(EPOCH FROM (dialend-calldate))), 0) from cdr WHERE userfield='1' and distrid=". $id ." and src not in (select RIGHT(db2.uniqueid, 11) from (select * from cdr where distrid=". $id .") db1  RIGHT JOIN (select * from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='". $id ."') db2 ON RIGHT(db2.uniqueid, 11) = db1.src) ORDER BY cd DESC;";
            $sql8 = "select RIGHT(src, 11), userfield, billsec, calldate+INTERVAL '3 hours' as cd from cdr WHERE userfield='2' and distrid=". $id ." and src not in (select RIGHT(db2.uniqueid, 11) from (select * from cdr where distrid=". $id .") db1  RIGHT JOIN (select * from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='". $id ."') db2 ON RIGHT(db2.uniqueid, 11) = db1.src) ORDER BY cd DESC;";
            $sql9 = "select RIGHT(src, 11), userfield, billsec, calldate+INTERVAL '3 hours' as cd from cdr WHERE userfield='3' and distrid=". $id ." and src not in (select RIGHT(db2.uniqueid, 11) from (select * from cdr where distrid=". $id .") db1  RIGHT JOIN (select * from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='". $id ."') db2 ON RIGHT(db2.uniqueid, 11) = db1.src) ORDER BY cd DESC;";
            $sql10 = "select RIGHT(src, 11), userfield, billsec, calldate+INTERVAL '3 hours' as cd from cdr WHERE not(userfield='1' or userfield='2' or userfield='3') and distrid=". $id ." and src not in (select RIGHT(db2.uniqueid, 11) from (select * from cdr where distrid=". $id .") db1  RIGHT JOIN (select * from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='". $id ."') db2 ON RIGHT(db2.uniqueid, 11) = db1.src) GROUP BY src ORDER BY cd DESC;";
        }
        else{
            $sql1 = "select COUNT(*) from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='".$id."' GROUP BY distrid;";
            $sql2 = "select RIGHT(db2.uniqueid, 11), db1.userfield, db1.billsec, db2.\"startTime\", GREATEST(db1.billsec - (EXTRACT(EPOCH FROM ( db1.dialend - db1.calldate))), 0) from (select * from cdr where distrid=". $id .") db1  RIGHT JOIN (select * from spam where LEFT(uniqueid,position('/' IN uniqueid)-1)='". $id ."') db2 ON RIGHT(db2.uniqueid, 11) = RIGHT(db1.tonumber, 11) ORDER BY db1.userfield is NULL, concat('z', db1.userfield, 'z') ASC, db1.calldate DESC;";
            $sql3 = "select COUNT(*) from (select * from cdr WHERE userfield='1' and distrid=".$id.") AS c;";
            $sql4 = "select COUNT(*) from (select * from cdr WHERE userfield='2' and distrid=".$id.") AS c;";
            $sql5 = "select COUNT(*) from (select * from cdr WHERE userfield='3' and distrid=".$id.") AS c;";
            $sql6 = "select COUNT(*) from (select * from cdr WHERE duration>0 and distrid=".$id."  and (dialstatus is NULL or dialstatus!='\"recall\"')) AS c;";
            $sql7 = "select RIGHT(tonumber, 11), incoming, billsec, calldate+INTERVAL '3 hours' from cdr WHERE dialend is not null and distrid=". $id ." and incoming is not Null;";
            $sql8 = "select COUNT(*) from (select * from cdr WHERE duration>0 and distrid=".$id."  and dialstatus='\"recall\"') AS c;";
            $sql9 = "select RIGHT(src, 11), userfield, billsec, GREATEST(billsec - (EXTRACT(EPOCH FROM (dialend-calldate))), 0), calldate + INTERVAL '3 hours' from cdr WHERE duration>0 and distrid=".$id." and dialstatus='\"recall\"';";
        }
        if($result1 = pg_query($pg, $sql1))
        {
            if($row = pg_fetch_row($result1)){
                $cnt = $row[0];
            }
            pg_free_result($result1);
        }
        $stats = [];
        if($distrib->DistribType=="recall" and $result7 = pg_query($pg, $sql7))
        {
            while ($row = pg_fetch_row($result7)) {
                array_push($stats, [$row[0], $row[1], $row[2], $row[3], "Без дозвона", $row[4]]);
            }
            pg_free_result($result7);
        }
        if($distrib->DistribType=="recall" and $result8 = pg_query($pg, $sql8))
        {
            while ($row = pg_fetch_row($result8)) {
                array_push($stats, [$row[0], $row[1], $row[2], $row[3], "Без дозвона", 0]);
            }
            pg_free_result($result8);
        }
        if($distrib->DistribType=="recall" and $result9 = pg_query($pg, $sql9))
        {
            while ($row = pg_fetch_row($result9)) {
                array_push($stats, [$row[0], $row[1], $row[2], $row[3], "Без дозвона", 0]);
            }
            pg_free_result($result9);
        }
        if($result2 = pg_query($pg, $sql2))
        {
            while ($row = pg_fetch_row($result2)) {
                if($distrib->DistribType=="recall"){
                    array_push($stats, [$row[0], $row[1], $row[2], $row[3], $row[5], $row[4]]);
                }
                else{
                    array_push($stats, [$row[0], $row[1], $row[2], $row[3], "", $row[4]]);
                }
            }
            pg_free_result($result2);
        }
        if($distrib->DistribType=="recall" and $result10 = pg_query($pg, $sql10))
        {
            while ($row = pg_fetch_row($result10)) {
                array_push($stats, [$row[0], $row[1], $row[2], $row[3], "Без дозвона", 0]);
            }
            pg_free_result($result10);
        }
        if($result3 = pg_query($pg, $sql3))
        {
            if($row = pg_fetch_row($result3)){
                $cnt1 = $row[0];
            }
            pg_free_result($result3);
        }
        else{
            $cnt1 = 0;
        }
        if($result4 = pg_query($pg, $sql4))
        {
            if($row = pg_fetch_row($result4)){
                $cnt2 = $row[0];
            }
            pg_free_result($result4);
        }
        else{
            $cnt2 = 0;
        }
        if($result5 = pg_query($pg, $sql5))
        {
            if($row = pg_fetch_row($result5)){
                $cnt3 = $row[0];
            }
            pg_free_result($result5);
        }
        else{
            $cnt3 = 0;
        }
        if($result6 = pg_query($pg, $sql6))
        {
            if($row = pg_fetch_row($result6)){
                $cntP = $row[0];
            }
            pg_free_result($result6);
        }
        else{
            $cntP = 0;
        }
        pg_close($pg);
        $stats = $stats;
        if (ob_get_level()) {
                ob_end_clean();
        }
        //$file1="/tmp/". $name ."orders.csv";
        if(isset($_SERVER['HTTP_USER_AGENT']) and strpos($_SERVER['HTTP_USER_AGENT'],'MSIE'))
            Header('Content-Type: application/force-download');
        else{
            Header('Content-Type: application/octet-stream');
            Header('Accept-Ranges: bytes');
            Header('Content-Length: '.(100000));
            Header('Content-disposition: attachment; filename=' . $name . ".csv");
            print(iconv("UTF-8", "WINDOWS-1251", 'Рассылка;' . $distrib->DistribName . "\xA"));
            print(iconv("UTF-8", "WINDOWS-1251", 'Сделано звонков:;' . $cnt . "\xA"));
            if($distrib->DistribType == "recall")
            {
                print(iconv("UTF-8", "WINDOWS-1251", 'Людей перезвонило:;' . $cntP . "\xA"));
            }
            print(iconv("UTF-8", "WINDOWS-1251", 'Нажали 1:;' . $cnt1 . "\xA"));
            print(iconv("UTF-8", "WINDOWS-1251", 'Нажали 2:;' . $cnt2 . "\xA"));
            print(iconv("UTF-8", "WINDOWS-1251", 'Нажали 3:;' . $cnt3 . "\xA"));
            print(iconv("UTF-8", "WINDOWS-1251", 'Номер обзвона:;' . $distrib->SenderNumber . "\xA"));
            print(iconv("UTF-8", "WINDOWS-1251", 'Номер;'));
            if($distrib->DistribType == "recall"){
                print(iconv("UTF-8", "WINDOWS-1251", 'Цифра;'));
            }
            print(iconv("UTF-8", "WINDOWS-1251", 'Длительность;'));
            if($distrib->DistribType == "recall"){
                print(iconv("UTF-8", "WINDOWS-1251", 'Разговор;'));
            }
            print(iconv("UTF-8", "WINDOWS-1251", 'Дата;'));
            print(iconv("UTF-8", "WINDOWS-1251", 'Стоимость;' . "\xA"));
            foreach($stats as $stat){
                print($stat[0] . '; ');
                if($distrib->DistribType == "recall"){
                    print($stat[1] . '; ');
                }
                print($stat[2] . '; ');
                if($distrib->DistribType == "recall"){
                    print($stat[5] . '; ');
                }
                print($stat[3] . '; ');
                if($distrib->DistribType == "recall"){
                    if(trim(strval($stat[1])) == "1"){
                        print($cost->recall*$stat[2]/60/100);
                    }
                    else{
                        if(trim(strval($stat[1])) != "2" and trim(strval($stat[1])) != "3"){
                            if($stat[4] != "17"){
                                print("Не дозвон");
                            }
                            else{
                                print($cost->dial/100);
                            }
                        }
                        else{
                            print("0");
                        }
                    } 
                }
                else{
                    print($cost->ordinary*$stat[2]/60/100);
                }
                print(iconv("UTF-8", "WINDOWS-1251", "\xA"));
            }       
        }
        return $this->redirect('/admin/distribs');
    }
    
    public function action_distribinfo(){
        if(!$this->logged_in("User, Admin, Partner, Manager"))
            return;
        $id = $this->request->param('id');
        $distrib = $this->pixie->orm->get('distrib', $id);
        $user = $this->pixie->orm->get('user', $distrib->UserID);
        $name = $distrib->DistribName;
        $pg = pg_connect("host=127.0.0.1 port=6432 dbname=pixiecaller user=pgpixie password=PgPixiePassword");

        if($distrib->DistribType == "recall"){
            $sql = "SELECT number FROM spam WHERE number not in (SELECT src FROM cdr WHERE distrid = ". $id .") AND LEFT(uniqueid,position('/' IN uniqueid)-1)='". $id ."';";
        }
        else{
            $sql = "SELECT db1.number FROM (SELECT * FROM cdr RIGHT JOIN spam on cdr.tonumber = spam.uniqueid WHERE LEFT(uniqueid,position('/' IN uniqueid)-1) = '". $id ."') as db1 WHERE billsec IS NULL AND db1.number in (SELECT number FROM spam WHERE number not in (SELECT src FROM cdr WHERE distrid = ". $id .") AND LEFT(uniqueid,position('/' IN uniqueid)-1)='". $id ."');";
        }

        $stats = [];
        if($result = pg_query($pg, $sql))
        {
            while($row = pg_fetch_row($result)){
                if ($row != "")
                    array_push($stats, $row[0]);
            }
            pg_free_result($result);
        }

        if (count($stats) == 0){
            echo "<script type='text/javascript' charset='utf-8'>alert('No numbers'); window.location.replace('/admin/distribs');</script>";
            $this->execute = false;
            die();
        }

        if (ob_get_level()) {
            ob_end_clean();
        }

        if(isset($_SERVER['HTTP_USER_AGENT']) and strpos($_SERVER['HTTP_USER_AGENT'],'MSIE'))
            header('Content-Type: application/force-download');
        else{
            header("Content-Type: application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=" . $name . ".xls");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            foreach($stats as $stat){
                echo $stat . "\r\n";
            }
        }
        return $this->redirect('/admin/distribs');        
    }

    public function action_phone(){
        if(!$this->logged_in("User"))
            return;
        $this->view->subview = 'phone';
    }
    
    public function action_refferal() {
        if(!$this->logged_in("User"))
            return;
        $partner = $this->pixie->orm->get('user')->where("id", $this->pixie->auth->user()->Partner)->find();
        $this->view->partnercontact = $this->pixie->orm->get('contact')->where("id", $partner->Contacts)->find();
        $domain = $this->pixie->orm->get('domain')->where('UserID', $partner->id)->find()->name;
        $this->view->ref = "http://" . $domain . "/admin/register?ref_id=" . $this->pixie->auth->user()->id;
        // $this->view->refuser = "";
        // $this->view->refuser2 = "";
        // if($this->pixie->auth->user()->RefID != NULL){
            // $refu = $this->pixie->orm->get('user')->where("id", $this->pixie->auth->user()->RefID)->find();
            // if($refu->loaded()){
                // $this->view->refuser=$refu->Login;
                // if($refu->RefID != NULL){
                    // $refu2 = $this->pixie->orm->get('user')->where("id", $refu->RefID)->find();
                    // if($refu2->loaded()){
                        // $this->view->refuser2=$refu2->Login;
                    // }
                // }
            // }
        // }
        $this->view->refusers = $this->pixie->orm->get('user')->where("RefID", $this->pixie->auth->user()->id)->find_all()->as_array();
        $users = array_map(function ($o) {
                return $o->id;
            }, $this->view->refusers);
        $usersStr = '(' . implode(',', $users) . ')';
        
        $this->view->refusers2 = [];
        if(count($users)>0){
            $this->view->refusers2 = $this->pixie->orm->get('user')->where('RefID', 'IN', $this->pixie->db->expr($usersStr))->find_all()->as_array();
        }
        $payments1 = $this->pixie->orm->get('payment')->where('data', 'like', '%1refuser=' . $this->pixie->auth->user()->id . ',%')->find_all()->as_array();
        $this->view->pays1 = array_map(function ($o) {
                return [$o->paydate, $this->pixie->orm->get('user')->where('id', $o->userID)->find()->Login, $o->value/100, explode('=', explode(', ', $o->data)[1])[1]/100];
            }, $payments1);
        $payments2 = $this->pixie->orm->get('payment')->where('data', 'like', '%2refuser=' . $this->pixie->auth->user()->id . ',%')->find_all()->as_array();
        $this->view->pays2 = array_map(function ($o) {
                return [$o->paydate, $this->pixie->orm->get('user')->where('id', $o->userID)->find()->Login, $o->value/100, explode('=', explode(', ', $o->data)[3])[1]/100];
            }, $payments2);
        $this->view->subview = 'refferal';
    }
    
    public function action_hlrbase(){
        if(!$this->logged_in("User"))
            return;
        $id = $this->request->param('id');
        $this->view->hlr = $this->pixie->orm->get('hlrbase', $id);
        $statuss = [];
        if($this->pixie->auth->user()->id!=$this->view->hlr->UserID){
            return $this->redirect('/admin/litehlr');
        }
        $basetype = $this->request->get('show');
        $this->view->type = $basetype;
        if($basetype=="all"){
           $this->view->hlrcalls =  $this->pixie->orm->get('hlrcall')->where("baseid", $id)->find_all()->as_array();
        }
        else if($basetype=="reached"){
            $this->view->hlrcalls =  $this->pixie->orm->get('hlrcall')->where("status", 16)->where("baseid", $id)->where('or', array("status", 17))->where("baseid", $id)->find_all()->as_array();
        }
        else if($basetype=="notreached"){
            $this->view->hlrcalls =  $this->pixie->orm->get('hlrcall')->where("status", '!=', 16)->where("status", '!=', 17)->where("baseid", $id)->find_all()->as_array();
        }
        foreach($this->view->hlrcalls as $call){
            if($call->status == 16 or $call->status == 17){
                $statuss[$call->id] = "Доступен";
            }
            else if(!preg_match("|^\d+$|", $call->status)){
                $statuss[$call->id] = "Не проверен";
            }
            else{
                $statuss[$call->id] = "Недоступен";
            }
        }
        $this->view->statuses = $statuss;
        $this->view->subview = 'hlrbase';
        
    }
    
    public function action_hlrbaseload(){
        if(!$this->logged_in("User"))
            return;
        $id = $this->request->param('id');
        $this->view->hlr = $this->pixie->orm->get('hlrbase', $id);
        if($this->pixie->auth->user()->id!=$this->view->hlr->UserID){
            return $this->redirect('/admin/litehlr');
        }
        $basetype = $this->request->get('show');
        $this->view->type = $basetype;
        if($basetype=="all"){
           $this->view->hlrcalls =  $this->pixie->orm->get('hlrcall')->where("baseid", $id)->find_all()->as_array();
        }
        else if($basetype=="reached"){
            $this->view->hlrcalls =  $this->pixie->orm->get('hlrcall')->where("status", 16)->where("baseid", $id)->where('or', array("status", 17))->where("baseid", $id)->find_all()->as_array();
        }
        else if($basetype=="notreached"){
            $this->view->hlrcalls =  $this->pixie->orm->get('hlrcall')->where("status", '!=', 16)->where("status", '!=', 17)->where("baseid", $id)->find_all()->as_array();
        }
        if (ob_get_level()) {
                ob_end_clean();
        }
        //$file1="/tmp/". $name ."orders.csv";
        if(isset($_SERVER['HTTP_USER_AGENT']) and strpos($_SERVER['HTTP_USER_AGENT'],'MSIE'))
            Header('Content-Type: application/force-download');
        else{
            Header('Content-Type: application/octet-stream');
            Header('Accept-Ranges: bytes');
            Header('Content-Length: '.(100000));
            Header('Content-disposition: attachment; filename=' . $this->view->hlr->Name . ".csv");
            print(iconv("UTF-8", "WINDOWS-1251", 'Рассылка;' . $this->view->hlr->Name . "\xA"));
            print(iconv("UTF-8", "WINDOWS-1251", 'Номер;'));
            print(iconv("UTF-8", "WINDOWS-1251", 'Статус;' . "\xA"));
            foreach($this->view->hlrcalls as $call){
                print(iconv("UTF-8", "WINDOWS-1251", $call->number . ';'));
                
                if($call->status == 16 or $call->status == 17){
                    print(iconv("UTF-8", "WINDOWS-1251", "Доступен" . ';' . "\xA"));
                }
                else if(!preg_match("|^\d+$|", $call->status)){
                    print(iconv("UTF-8", "WINDOWS-1251", "Не проверен" . ';' . "\xA"));
                }
                else{
                    print(iconv("UTF-8", "WINDOWS-1251", "Недоступен" . ';' . "\xA"));
                }
            }
        }
        return $this->redirect('/admin/litehlr');
    }
    
    public function action_litehlr() {
        if(!$this->logged_in("User"))
            return;
        date_default_timezone_set("Europe/Moscow");
        $this->view->statuses = array("Ready" => "Обработка", "Adding" => "Загрузка", "Added" => "Добавлена", "Finished" => "Закончена", "Paused" => "На паузе", "Pausing" => "Ставится на паузу", "Stopped" => "Остановлена", "Stopping" => "Останавливается", "Canceled" => "Отменена", "Deleted" => "Не загружена", "Started" => "В процессе");
        $this->view->hlrname = date_format(date_create(), 'Y-m-d_H-i-s');
        $this->view->hlrbases = $this->pixie->orm->get('hlrbase')->where('UserID', $this->pixie->auth->user()->id)->order_by('id', 'desc')->find_all()->as_array();
        $this->view->hlrcost = $this->pixie->orm->get('cost')->where('id', $this->pixie->auth->user()->costgroup)->find()->hlr;
        if($this->request->method == 'POST'){
            if($this->pixie->orm->get("contact")->where('id', $this->pixie->orm->get("user")->where("id", $this->pixie->auth->user()->Partner)->find()->Contacts)->find()->hlron==1){
                date_default_timezone_set("Europe/Moscow");
                $time_now = date_format(date_create(), 'Y-m-d H:i:s');
                $day_now =  date_format(date_create(), 'Y-m-d 00:00:00');
                $speed = $this->pixie->orm->get("speed")->where('value', "2000")->find();
                $hlrcnt = $this->pixie->orm->get('hlrbase')->where('UserID', $this->pixie->auth->user()->id)->where('CreateTime', '>', $day_now)->count_all();
                if($hlrcnt<10){
                    $hlr = $this->pixie->orm->get('hlrbase');
                    $hlr->Name = $this->request->post('hlrname');
                    $hlr->SpeedID = $speed->id;
                    $data = array();
                    $error = false;
                    $files = array();
                    $uploaddir = '/var/www/uploads/';
                    $userdir = $uploaddir . $this->pixie->auth->user()->Login  . "/";
                    // переместим файлы из временной директории в указанную
                    if( ! is_dir( $userdir ) ) mkdir( $userdir );
                    $hlrdir = $uploaddir . $this->pixie->auth->user()->Login  . "/hlrbase/";
                    if( ! is_dir( $hlrdir ) ) mkdir( $hlrdir );
                    $file = $_FILES['hlrfile'];
                    if( move_uploaded_file( $file['tmp_name'], $hlrdir . "hlrbase" . $hlr->Name . ".xls"  ) ){
                        $files[] = realpath( $hlrdir . $file['name'] );
                        $hlr->basefile = $hlrdir . "hlrbase" . $hlr->Name . ".xls";
                    }
                    else{
                            $error = true;
                    }
                    $hlr->Status = "Ready";
                    $hlr->UserID = $this->pixie->auth->user()->id;
                    $hlr->CreateTime = $time_now;
                    $hlr->save();
                    $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "hlrcreate", $hlr->id);
                    return $this->redirect('/admin/litehlr');
                }
                else{
                    echo "<script type='text/javascript' charset='utf-8'>alert('Извините, у вас уже было более 3 проверок в день! Чтобы узнать подробности обратитесь в техподдержку!');</script>";
                    return $this->redirect('/admin/litehlr');
                }
            }
            else{
                echo "<script type='text/javascript' charset='utf-8'>alert('Извините, вам не доступен данный функционал');</script>";
                return $this->redirect('/admin/distribs');
            }
        }
        $this->view->subview = 'litehlr';
    }
    
    public function action_w1pay(){
        if(!$this->logged_in("User"))
            return;
        if($this->request->method == 'POST'){
            $dom = $this->pixie->orm->get('domain')->where("UserID", $this->pixie->auth->user()->Partner)->find();
            $key = $this->pixie->config->get('w1.payment.w1.key');
            $mid = $this->pixie->config->get('w1.payment.w1.mid');
            $cid = $this->pixie->config->get('w1.payment.w1.cid');
            
            date_default_timezone_set("Europe/Moscow");
            $time_now = date_format(date_create(), 'Y-m-d H:i:s');
            
            $payment = $this->pixie->orm->get('payment');
            $payment->type="w1processed";
            $sum=0;
            if($this->request->post("amount")>0 and $this->request->post("amount")<1000){
                $sum = $this->request->post("amount")*1.07;
            }
            else if($this->request->post("amount")>=1000){
                $sum = $this->request->post("amount")*1.02;
            }
            else{
                exit();
            }
            $payment->value=$this->request->post("amount")*100;
            $payment->userID = $this->pixie->auth->user()->id;
            $payment->partnerID = $this->pixie->auth->user()->Partner;
            $payment->paydate = $time_now;
            $payment->save();
            $id = $payment->id;
            $fields = array();
            // Добавление полей формы в ассоциативный массив
            $fields["WMI_MERCHANT_ID"]    = $mid;
            $fields["WMI_PAYMENT_AMOUNT"] = number_format( $sum , 2 , "." , "" );
            $fields["WMI_CURRENCY_ID"]    = $cid;
            $fields["WMI_PAYMENT_NO"]     = $id;
            $fields["WMI_DESCRIPTION"]    = "BASE64:".base64_encode($dom->name . " | " . $this->pixie->auth->user()->Login . " | " . $this->request->post("amount") . " | WalletOne");
            $fields["WMI_SUCCESS_URL"]    = "http://" . $dom->name . "/admin/successpay";
            $fields["WMI_FAIL_URL"]       = "http://" . $dom->name . "/admin/failpay";
         
            //Сортировка значений внутри полей
            foreach($fields as $name => $val) 
            {
                if(is_array($val))
                {
                    usort($val, "strcasecmp");
                    $fields[$name] = $val;
                }
            }
         
            // Формирование сообщения, путем объединения значений формы, 
            // отсортированных по именам ключей в порядке возрастания.
            uksort($fields, "strcasecmp");
            $fieldValues = "";
         
            foreach($fields as $value) 
            {
                if(is_array($value))
                    foreach($value as $v)
                    {
                        //Конвертация из текущей кодировки (UTF-8)
                        //необходима только если кодировка магазина отлична от Windows-1251
                        $v = iconv("utf-8", "windows-1251", $v);
                        $fieldValues .= $v;
                    }
                else
                {
                    //Конвертация из текущей кодировки (UTF-8)
                    //необходима только если кодировка магазина отлична от Windows-1251
                    $value = iconv("utf-8", "windows-1251", $value);
                    $fieldValues .= $value;
                }
            }
         
            // Формирование значения параметра WMI_SIGNATURE, путем 
            // вычисления отпечатка, сформированного выше сообщения, 
            // по алгоритму MD5 и представление его в Base64
         
            $signature = base64_encode(pack("H*", md5($fieldValues . $key)));
         
            //Добавление параметра WMI_SIGNATURE в словарь параметров формы
         
            $fields["WMI_SIGNATURE"] = $signature;
         
            // Формирование HTML-кода платежной формы
         
            print "<form action='https://wl.walletone.com/checkout/checkout/Index' method='POST'>";
         
            foreach($fields as $key => $val)
            {
                if(is_array($val))
                    foreach($val as $value)
                    {
                        print "$key: <input type='text' name='$key' value='$value'/>";
                    }
                else     
                    print "$key: <input type='text' name='$key' value='$val'/>";
            }
         
            print "<input type='submit'/></form>";
            // $myCurl = curl_init();
            // curl_setopt_array($myCurl, array(
                // CURLOPT_URL => 'https://wl.walletone.com/checkout/checkout/Index',
                // CURLOPT_RETURNTRANSFER => true,
                // CURLOPT_POST => true,
                // CURLOPT_POSTFIELDS => http_build_query($fields)
            // ));
            // $response = curl_exec($myCurl);
            // curl_close($myCurl);
            //die ($response);
            exit();
        }
    }
    
    public function action_successpay(){
        $this->view->subview = 'successpay';
    }
    
    public function action_failpay(){
        $this->view->subview = 'failpay';
    }
    
    public function action_paycomplete(){
        if(isset($_POST["WMI_SIGNATURE"]) and isset($_POST["WMI_ORDER_STATE"]) and isset($_POST["WMI_PAYMENT_NO"])){
            $skey = $this->pixie->config->get('w1.payment.w1.key');
            // Извлечение всех параметров POST-запроса, кроме WMI_SIGNATURE
            foreach($_POST as $name => $value){
                if ($name !== "WMI_SIGNATURE") $params[$name] = $value;
            }
            // Сортировка массива по именам ключей в порядке возрастания
            // и формирование сообщения, путем объединения значений формы
            uksort($params, "strcasecmp"); $values = "";
            foreach($params as $name => $value){
                //Конвертация из текущей кодировки (UTF-8)
                //необходима только если кодировка магазина отлична от Windows-1251
                $value = iconv("utf-8", "windows-1251", $value);
                $values .= $value;
            }
            // Формирование подписи для сравнения ее с параметром WMI_SIGNATURE
            $signature = base64_encode(pack("H*", md5($values . $skey)));

            //Сравнение полученной подписи с подписью W1

            if ($signature == $_POST["WMI_SIGNATURE"])
            {
                if (strtoupper($_POST["WMI_ORDER_STATE"]) == "ACCEPTED"){
                    // TODO: Пометить заказ, как «Оплаченный» в системе учета магазина
                    date_default_timezone_set("Europe/Moscow");
                    $time_now = date_format(date_create(), 'Y-m-d H:i:s');
                    $payid = $params["WMI_PAYMENT_NO"];
                    $pay = $this->pixie->orm->get('payment')->where('id', $payid)->find();
                    if($pay->loaded())
                        $this->payfromw1($pay->userID, $pay->partnerID, $pay->value, $time_now, 'w1', $pay->id);
                }
            }
        }
        print "WMI_RESULT=" . strtoupper('OK') . "&";
        exit();
    }

}


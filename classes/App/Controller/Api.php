<?php
namespace App\Controller;

class Api extends \App\Page {
    
    public function action_call() {
        $uid = $this->check_key();
        if(!$uid)
        {
            $this->response->body = "{'error' : 'wrong api key'}";
            $this->execute = false;
            return;
        }
		date_default_timezone_set("Europe/Moscow");
		$time_now = date_format(date_create(), 'Y-m-d H:i:s');
        $user = $this->pixie->orm->get('user')->where('id', $uid->UserID)->find(); 
        if($user->loaded()){
            $call = $this->pixie->orm->get('apicall');
            $call->type = "call";
            $call->userid = $user->id;
            $recid = $this->request->post('record');
            $record = $this->pixie->orm->get('record')->where('id', $recid)->where('userid', $user->Partner)->where('or', array('userid', $user->id))->where('id', $recid)->find();
            if($record->loaded() and $record->status=="Ready"){
                $call->recordpath = $record->path;
            }
            else{
                $this->response->body = "{'error' : 'welcome record is not exists or not available'}";
                $this->execute = false;
                return;
            }
            $outnum = $this->request->post('outnumber');
            $outnumber = $this->pixie->orm->get('outnumber')->where('Number', $outnum)->where('Status', "UserNum")->where('AvailableFor', $user->id)->find();
            if($outnumber->loaded()){
                $call->outnumber = $outnumber->Number;
            }
            else{
                $this->response->body = "{'error' : 'wrong outbound number'}";
                $this->execute = false;
                return;
            }
            $num = $this->request->post('number');
            if(preg_match("|^7\d\d\d\d\d\d\d\d\d\d$|", $num)){
                $call->number = $num;
                $interval = date_interval_create_from_date_string($uid->period . " seconds");
                $time_limit = date_format(date_sub(date_create_from_format('Y-m-d H:i:s', $time_now), $interval), 'Y-m-d H:i:s');
                $callsc = $this->pixie->orm->get('apicall')->where('number', $num)->where('userid', $user->id)->where('createtime', '>', $time_limit)->order_by('endtime', 'desc')->count_all();
                if($uid->callscnt <= $callsc){
                    $this->response->body = "{'error' : 'call limit reached for that number'}";
                    $this->execute = false;
                    return;
                }
            }
            else{
                $this->response->body = "{'error' : 'wrong number to call'}";
                $this->execute = false;
                return;
            }
            
            if(isset($_POST['ivr']) and $this->request->post('ivr')==1){
                for($i=1; $i<4; $i++){
                    $ivrn = 'ivr' . $i;
                    if(isset($_POST[$ivrn])){
                        $recid1 = $this->request->post($ivrn);
                        $record1 = $this->pixie->orm->get('record')->where('id', $recid1)->where('userid', $user->Partner)->where('or', array('userid', $user->id))->where('id', $recid1)->find();
                        if($record1->loaded() and $record1->status=="Ready"){
                            if($i==1)
                                $call->ivr1path = $record1->path;
                            if($i==2)
                                $call->ivr2path = $record1->path;
                            if($i==3)
                                $call->ivr3path = $record1->path;
                        }
                        else{
                            $this->response->body = "{'error' : '" . $ivrn . " record is not exists or not available'}";
                            $this->execute = false;
                            return;
                        }
                    }
                    else{
                        if($i==1)
                            $call->ivr1path = "/var/lib/asterisk/festivalcache/zero";
                        if($i==2)
                            $call->ivr2path = "/var/lib/asterisk/festivalcache/zero";
                        if($i==3)
                            $call->ivr3path = "/var/lib/asterisk/festivalcache/zero";
                    }
                }
                if(isset($_POST["tonumber"])){
                    $num = $this->request->post('tonumber');
                    if(preg_match("|^7\d\d\d\d\d\d\d\d\d\d$|", $num)){
                        $call->tonumber = $num;
                    }
                    else{
                        $this->response->body = "{'error' : 'wrong manager number to call'}";
                        $this->execute = false;
                        return;
                    }
                }
                if(isset($_POST["blst"])){
                    $bl = $this->request->post('blst');
                    if($bl==1 or $bl==2){
                        $call->blocknumber=3;
                    }
                }
            }
            else{
                $call->ivr1path="none";
                $call->ivr2path="none";
                $call->ivr3path="none";
            }
            
            $call->createtime = $time_now;
            $call->status = "Ready";
            $call->cost = 0;
            $call->save();
            $this->event($user->id, $user->id, "createapicall", $call->id);
            $this->response->body = "{'id' : '" . $call->id . "', 'status' : 'accepted'}";
            $this->execute = false;
		}
        else{
            $this->response->body = "{'error' : 'user is not exists'}";
            $this->execute = false;
            return;
        }
    }
    
    public function action_balance() {
        $uid = $this->check_key();
        if(!$uid){
            $this->response->body = "{'error' : 'wrong api key'}";
            $this->execute = false;
            return;
        }
		date_default_timezone_set("Europe/Moscow");
		$time_now = date_format(date_create(), 'Y-m-d H:i:s');
        $userid = $uid->UserID;
        $user = $this->pixie->orm->get('user')->where('id', $userid)->find(); 
        if($user->loaded()){
            $this->response->body = "{'amount' : " . $user->balance/100 . ", 'cur':'RUB'}";
            $this->execute = false;
            return;
        }
    }

    public function action_status() {
        $uid = $this->check_key();
        if(!$uid){
            $this->response->body = "{'error' : 'wrong api key'}";
            $this->execute = false;
            return;
        }
		date_default_timezone_set("Europe/Moscow");
		$time_now = date_format(date_create(), 'Y-m-d H:i:s');
        $userid = $uid->UserID;
        $user = $this->pixie->orm->get('user')->where('id', $userid)->find(); 
        if($user->loaded()){
            $call = $this->pixie->orm->get('apicall')->where('id', $this->request->post('call'))->where('userid', $userid)->find();
            if($call->loaded())
            {
                if($call->status=="Ready"){
                    $this->response->body = "{'id' : '" . $call->id . "' , 'status' : 'Processing'}";
                    $this->execute = false;
                }
                else{
                    if($call->endtime=='-infinity'){
                        $endtime = '0000-00-00 00:00:00';
                    }
                    else{
                        $endtime = $call->endtime;
                    }
                    if($call->starttime=='-infinity'){
                        $starttime = '0000-00-00 00:00:00';
                    }
                    else{
                        $starttime = $call->starttime;
                    }
                    $duration = date_timestamp_get(date_create_from_format('Y-m-d H:i:s', $endtime))-date_timestamp_get(date_create_from_format('Y-m-d H:i:s', $starttime));
                    $pg = pg_connect("host=127.0.0.1 port=6432 dbname=pixiecaller user=pgpixie password=PgPixiePassword");
                    $sql = "SELECT userfield FROM cdr WHERE distrid = '-" . $call->id . "'";
                    $ivrnum = "";
                    if($result1 = pg_query($pg, $sql))
                    {
                        $row = pg_fetch_row($result1);
                        $ivrnum = $row[0];
                        pg_free_result($result1);
                    }
                    $fields = ["'id' : '" . $call->id . "'" , "'status' : '" . $call->status . "'" , "'duration' : " . $duration , "'cost' : " . $call->cost, "'starttime' : '" . $starttime . "'"];
                    if($ivrnum!=""){
                        $fields[] = "'ivr' : " . $ivrnum;
                    }
                    if($call->manstatus!=""){
                        $fields[] = "'manstatus' : '" . $call->manstatus . "'";
                    }
                    if($call->manstarttime!="-infinity"){
                        $fields[] = "'manstarttime' : '" . $call->manstarttime . "'";
                        if($call->manendtime!="-infinity"){
                            $durationman = date_timestamp_get(date_create_from_format('Y-m-d H:i:s', $call->manendtime))-date_timestamp_get(date_create_from_format('Y-m-d H:i:s', $call->manstarttime));
                            $fields[] = "'durationman' : " . $durationman;
                        }
                    }
                    $this->response->body = "{" . implode(', ', $fields) . "}";
                    $this->execute = false;
                }
            }
            else{
                $this->response->body = "{'error' : 'call is not exists'}";
                $this->execute = false;
            }
		}
        else{
            $this->response->body = "{'error' : 'user is not exists'}";
            $this->execute = false;
        }
    }
    
    public function action_vercall(){
        $uid = $this->check_key();
        if(!$uid){
            $this->response->body = "{'error' : 'wrong api key'}";
            $this->execute = false;
            return;
        }
        date_default_timezone_set("Europe/Moscow");
        $time_now = date_format(date_create(), 'Y-m-d H:i:s');
        $user = $this->pixie->orm->get('user')->where('id', $uid->UserID)->find(); 
        if($user->loaded()){
            $call = $this->pixie->orm->get('apicall');
            $call->userid = $user->id;
            $num = $this->request->post('number');
            if(preg_match("|^7\d\d\d\d\d\d\d\d\d\d$|", $num)){
                $call->number = $num;
                $interval = date_interval_create_from_date_string($uid->period . " seconds");
                $time_limit = date_format(date_sub(date_create_from_format('Y-m-d H:i:s', $time_now), $interval), 'Y-m-d H:i:s');
                $callsc = $this->pixie->orm->get('apicall')->where('number', $num)->where('userid', $user->id)->where('createtime', '>', $time_limit)->order_by('endtime', 'desc')->count_all();
                if($uid->callscnt <= $callsc){
                    $this->response->body = "{'error' : 'call limit reached for that number'}";
                    $this->execute = false;
                    return;
                }
            }
            else{
                $this->response->body = "{'error' : 'wrong number to call'}";
                $this->execute = false;
                return;
            }
            $type = $this->request->post('type');
            $call->type = $type;
            if($type=="codenumber"){
                $code = $this->request->post('code');
                if(preg_match("|^\d\d\d\d$|", $code)){
                    $call->data = "{ 'code' : " . $code . " }";
                }
            }
            $call->createtime = $time_now;
            $call->status = "Ready";
            $call->cost = 0;
            $call->save();
            $this->event($user->id, $user->id, "createapicall", $call->id);
            $this->response->body = "{'id' : '" . $call->id . "', 'status' : 'accepted'}";
            $this->execute = false;
        }
        else{
            $this->response->body = "{'error' : 'user is not exists'}";
            $this->execute = false;
            return;
        }
    }

    public function action_callsinfo(){
        $uid = $this->check_key();
        if(!$uid){
            $this->response->body = "{'error' : 'wrong api key'}";
            $this->execute = false;
            return;
        }
        date_default_timezone_set("Europe/Moscow");
        $time_now = date_format(date_create(), 'Y-m-d H:i:s');
        $user = $this->pixie->orm->get('user')->where('id', $userid)->find(); 
        if($user->loaded()){
            $call = $this->pixie->orm->get('apicall');
            $date_from = $this->request->post('date_from');
            $date_to = $this->request->post('date_to');
            $query = "SELECT createtime, number, tonumber, records.id";
            if($result = pg_query($pg, $query))
            {
                while ($row = $result->fetch_row()) {
                }
                pg_free_result($result);
            }

        }
        else{
            $this->response->body = "{'error' : 'user is not exists'}";
            $this->execute = false;
            return;
        }        
    }
}


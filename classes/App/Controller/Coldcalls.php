<?php
namespace App\Controller;

class Coldcalls extends \App\Page {
    
    public function action_statistics() {
        if(!$this->logged_in("User"))
            return;
        $this->view->robots = $this->pixie->orm->get("robot")->where('UserID', $this->pixie->auth->user()->id)->find_all();
        $this->view->subview = 'statistics';
    }
    
    public function action_robotinfo() {
        if(!$this->logged_in("User"))
            return;
        $id = $this->request->param('id');
        $files = scandir ("/var/www/html/pixiecaller/web/assets/audio/uploads/");
        $files1 = [];
        foreach($files as $file){
            if(explode('_', $file)[0]==$id){
                $str = file_get_contents ("/tmp/logs" . explode('.', $file)[0]);
                array_push($files1, ["/assets/audio/uploads/" . $file, $str]);
            }
        }
        $this->view->files = $files1;
        $this->view->subview = 'robotinfo';
    }
    
    public function action_newrobot() {
        if(!$this->logged_in("User"))
            return;
		date_default_timezone_set("Europe/Moscow");
		$time_now = date_format(date_create(), 'Y-m-d H:i:s');
        $this->view->numbersord = $this->pixie->orm->get('outnumber')->where('Status', 'ForOrdinary')->where('AvailableFor', $this->pixie->auth->user()->id)->where('or',array('AvailableFor', -1))->where('Status', 'ForOrdinary')->find_all()->as_array();
        $this->view->numbersall = $this->pixie->orm->get('outnumber')->where('Status', 'Available')->where('AvailableFor', $this->pixie->auth->user()->id)->where('or',array('AvailableFor', -1))->where('Status', 'Available')->find_all()->as_array();
        $this->view->numbersuser = $this->pixie->orm->get('outnumber')->where('Status', 'UserNum')->where('AvailableFor', $this->pixie->auth->user()->id)->find_all()->as_array();
        $this->view->bases = $this->pixie->orm->get('base')->where('UserID', $this->pixie->auth->user()->id)->where('Status', 'Added')->find_all();
        $maxspeed = $this->pixie->orm->get('speed')->where("id", $this->pixie->auth->user()->MaxSpeed)->find();
        $this->view->speeds = $this->pixie->orm->get('speed')->where('UserGroup', $this->pixie->auth->user()->id)->where('Value', '<=', $maxspeed->Value)->where('or',array('UserGroup', -1))->where('Value', '<=', $maxspeed->Value)->order_by("Value", "asc")->find_all();
        $this->view->subview = 'newrobot';
        if ($this->request->method == 'POST') {
            $data = array();
            $error = false;
            $files = array();
            $uploaddir = '/var/www/html/pixiecaller/web/assets/audio/uploads/';
            $userdir = $uploaddir . $this->pixie->auth->user()->Login  . "/";
            $robot = $this->pixie->orm->get('robot');
            $distribdir = $userdir . $this->request->post('robotname') . "/";
            $sourcesdir = $distribdir . "sources/";
            $dopdir = '/var/www/html/pixiecaller/web';
            if( ! is_dir( $userdir ) ) mkdir( $userdir );
            if( ! is_dir( $distribdir ) ) mkdir( $distribdir ); 
            if( ! is_dir( $sourcesdir ) ) mkdir( $sourcesdir );
            for($i = 1; $i < 9; $i++){
                if(array_key_exists('audio' . $i, $_FILES)){
                    $file = $_FILES['audio' . $i];
                    $parts = explode(".", $file['name']);
                    $ext = $parts[count($parts)-1];
                    if( move_uploaded_file( $file['tmp_name'], $sourcesdir . "audio" . $i . "." . $ext ) ){
                        $files[] = realpath( $sourcesdir . $file['name'] );
                        if($ext == "mp3"){
                            shell_exec("mpg123 -w '". $sourcesdir . "audio" . $i . ".wav' " . $sourcesdir . "audio" . $i . ".mp3");
                        }
                    }
                }
                shell_exec("sox -t wav '". $sourcesdir . "audio" . $i . ".wav' -r 8000 -c1 -t gsm '" . $distribdir . "audio" . $i . ".gsm'");
            }
            $robot->audio1 = $distribdir . "audio1";
            $robot->audio2 = $distribdir . "audio2";
            $robot->audio3 = $distribdir . "audio3";
            $robot->audio4 = $distribdir . "audio4";
            $robot->audio5 = $distribdir . "audio5";
            $robot->audio6 = $distribdir . "audio6";
            $robot->audio7 = $distribdir . "audio7";
            $robot->audio8 = $distribdir . "audio8";
            $robot->options3 = $this->request->post('options3');
            $robot->options4 = $this->request->post('options4');
            $robot->options5 = $this->request->post('options5');
            $robot->options6 = $this->request->post('options6');
            $robot->options7 = $this->request->post('options7');
            $robot->options8 = $this->request->post('options8');
            shell_exec("rm -r " . $sourcesdir . "*.mp3");
            shell_exec("rm -r " . $distribdir . "*.wav");
            shell_exec("rm -r " . $distribdir . "*.mp3");
            if($this->request->post('basetype') == "numbers"){
                $robot->BaseType = "numbers";
            }
            else{
                $robot->BaseType = "base";
            }
            $robot->BaseType = $this->request->post('basetype');
            if($robot->BaseType != "base"){
                $robot->BaseType = "numbers";
                $robot->Numbers = $this->request->post('numberscall');
                $robot->Size = count(explode(", ", $this->request->post('numberscall')));
            }
            else{
                $robot->BaseID = $this->request->post('robotbase');
                $robot->Size = $this->pixie->orm->get('base', $robot->BaseID)->Size;
            }
            $robot->RobotName = $this->request->post('robotname');
			$robot->CreateTime = $time_now;
			$robot->Comment = $this->request->post('robotcomment');
            $robot->UserID = $this->pixie->auth->user()->id;
            $robot->SpeedID = $this->request->post('robotspeed');
            $robot->Status = "Ready";
            $robot->SenderNumber = $this->request->post('robotordnumber');
            $num = $this->pixie->orm->get('outnumber')->where('Number', $robot->SenderNumber)->find();
            if($num->Status=="Busy" or $num->Status=="ForRecallBusy" or $num->Status=="ForTest" or  $num->Status=="ForTestRec"){
                echo "<script type='text/javascript' charset='utf-8'>alert('Извините, данный номер уже занят');</script>";
                $error=true;
            }
            $speed = $this->pixie->orm->get('speed')->where("id", $robot->SpeedID)->find();
            if($maxspeed->Value<$speed->Value){
                echo "<script type='text/javascript' charset='utf-8'>alert('Вам не доступна данная скорость рассылки, пожалуйста снизьте скорость.');</script>";
                $error=true;
            }
            if(! $error){
                $robot->save();
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "createrobot", $robot->id);
            }
            if($error){
                $this->execute = false;
            }
            else{
                return $this->redirect('/coldcalls/newrobot');
            }
		}
    }
    
}
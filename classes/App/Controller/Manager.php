<?php

namespace App\Controller;

class Manager extends \App\Page {

	public function action_index() {
        if(!$this->logged_in("Manager"))
            return;
    }
    
    public function action_profile() {
        if(!$this->logged_in("Manager"))
            return;
        if ($this->request->method == 'POST') {
            $user = $this->pixie->auth->user();
            $oldjson = "{\"UserName\" : \"" . $user->UserName . "\", \"UserPhone\" : \"" . $user->UserPhone . "\", \"Login\" : \"" . $user->Login . "\"}";
            $user->UserPhone = $this->request->post('userphone');
            $us = $this->pixie->orm->get('user')->where('Login', $this->request->post('usermail'))->find();
            if(!$us->loaded()){
                $user->Login = $this->request->post('usermail');
            }
            $user->save();
            $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "profilechange", $oldjson);
        }
        $this->view->user = $this->pixie->auth->user();
        $this->view->subview = 'profilem';
    }

}

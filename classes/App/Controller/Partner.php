<?php
namespace App\Controller;

class Partner extends \App\Page {

    public function action_index() {
        if(!$this->logged_in("Partner"))
            return;
    }
    
    public function action_managers() {
        if(!$this->logged_in("Partner"))
            return;
        $this->view->managers = $this->pixie->orm->get('user')->where('UserType', 'Manager')->where('Partner', $this->pixie->auth->user()->id)->find_all()->as_array();
        $uscnt = [];
        foreach($this->view->managers as $man){
            $cnt = $this->pixie->orm->get('user')->where('Manager', $man->id)->count_all();
            $uscnt[$man->id]=$cnt;
        }
        $this->view->uscnt = $uscnt;
        $currname = $_SERVER['SERVER_NAME'];
        $ip = $_SERVER['SERVER_ADDR'];
        if($this->request->method == 'POST'){
            date_default_timezone_set("Europe/Moscow");
            $time_now = date_format(date_create(), 'Y-m-d H:i:s');
            $login = $this->request->post('Login');
            $phonenumber = $this->request->post('Phone');
            $password = $this->request->post('Password') . $this->salt;
            $hash = $this->pixie->auth->provider('password')->hash_password($password);
            $user = $this->pixie->orm->get('user');
            $user->Partner = $this->pixie->auth->user()->id;
            $dom = $this->pixie->orm->get('domain')->where('UserID', $this->pixie->auth->user()->id)->find();
            $defaults = $this->pixie->orm->get('defaultvalue')->where('domainID', $dom->id)->find();
            $user->Login = $login;
            $user->Password = $hash;
            $user->UserPhone = $phonenumber;
            $user->UserType = "Manager";
            $user->balance = 0;
            $user->RefID = $this->refid;
            $user->costgroup = $defaults->costgroup;
            
            if($this->pixie->orm->get('user')->where('Login', $login)->count_all()==0){
                $user->save();
                $this->event($user->id, $this->pixie->auth->user()->id, "newmanager", $user->costgroup);
                if(count($this->view->managers)==0){
                    $users1 = $this->pixie->orm->get('user')->where('UserType', 'User')->where('Partner', $this->pixie->auth->user()->id)->find_all();
                    foreach($users1 as $user1){
                        $user1->Manager = $user->id;
                        $user1->save();
                    }
                }
            }
            else{
                return;
            }
        }
        $this->view->subview = 'managers';
    }
    
    public function action_events() {
        if(!$this->logged_in("Partner, Manager"))
            return;
        $current_page = $this->request->param('page');
        $limit1 = ($current_page-1)*$this->item_per_page;
        $limit2 = $this->item_per_page;
        $type = "";
        $subject = "";
        if(array_key_exists("eventtype",$_COOKIE))
            $type = $_COOKIE["eventtype"];
        if(array_key_exists("eventsubj",$_COOKIE))
            $subject = $_COOKIE["eventsubj"];
        if($this->request->method == 'POST'){
            $type = $this->request->post('eventtype');
            $subject = $this->request->post('eventsubj');
        }
        $this->view->eventtype = $type;
        $this->view->eventsubj = $subject;
        $currole = $this->pixie->auth->user()->UserType;
        $usrs = $this->pixie->orm->get('user')->where($currole, $this->pixie->auth->user()->id)->find_all()->as_array();
        $users = array_map(function ($o) {
                return $o->id;
            }, $usrs);
        $usersStr = '(' . implode(',', $users) . ')';
        if(count($usrs)>0){
            if($this->request->method == 'POST'){
                $type = $this->request->post('eventtype');
                $subject = $this->request->post('eventsubj');
                setcookie ("eventtype", $type);
                setcookie ("eventsubj", $subject);
            }
            $this->view->eventtype = $type;
            $this->view->eventsubj = $subject;
            $s = $this->pixie->orm->get('user')->where($currole, $this->pixie->auth->user()->id)->where('Login', $subject)->find();
            
            if($s->loaded()){
                $evs = $this->pixie->orm->get('event')->where("Type", 'like', '%' . $type . '%')->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->where('Subject', $s->id)->where("or", array("Type", 'like', '%' . $type . '%'))->where('Object', 'IN', $this->pixie->db->expr($usersStr))->where('Subject', $s->id)->order_by('id', 'desc')->order_by('id', 'desc')->limit($limit2)->offset($limit1)->find_all()->as_array();
                $this->view->num_pages = ceil($this->pixie->orm->get('event')->where("Type", 'like', '%' . $type . '%')->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->where('Subject', $s->id)->where("or", array("Type", 'like', '%' . $type . '%'))->where('Object', 'IN', $this->pixie->db->expr($usersStr))->where('Subject', $s->id)->count_all() / $this->item_per_page);
            }
            else{
                $evs = $this->pixie->orm->get('event')->where("Type", 'like', '%' . $type . '%')->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->where("or", array("Type", 'like', '%' . $type . '%'))->where('Object', 'IN', $this->pixie->db->expr($usersStr))->order_by('id', 'desc')->limit($limit2)->offset($limit1)->find_all()->as_array();
                $this->view->num_pages = ceil($this->pixie->orm->get('event')->where("Type", 'like', '%' . $type . '%')->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->where("or", array("Type", 'like', '%' . $type . '%'))->where('Object', 'IN', $this->pixie->db->expr($usersStr))->count_all() / $this->item_per_page);
            }
            $events = array_map(function ($o) {
                $subj = $this->pixie->orm->get('user')->where('id', $o->Subject)->find();
                $obj = $this->pixie->orm->get('user')->where('id', $o->Object)->find();
                if($subj->loaded() and $obj->loaded())
                return [$o->EventTime, $o->Type, $subj->Login, $obj->Login, $o->Sum . "; " . $o->Data . "; " . $o->RefID . "; " . $o->EnterPage . "; " . $o->FromPage . "; " . $o->UtmSource . "; " . $o->UtmCampaign . "; " . $o->UtmTerm, "/partner/clientinfo/" . $o->Subject, "/partner/clientinfo/" . $o->Object];
            }, $evs);
            // else{
                // $evs = $this->pixie->orm->get('event')->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->where('or', array('Object', 'IN', $this->pixie->db->expr($usersStr)))->order_by('id', 'desc')->limit($limit2)->offset($limit1)->find_all()->as_array();
                // $events = array_map(function ($o) {
                    // $subj = $this->pixie->orm->get('user')->where('id', $o->Subject)->find();
                    // $obj = $this->pixie->orm->get('user')->where('id', $o->Object)->find();
                    // if($subj->loaded() and $obj->loaded())
                    // return [$o->EventTime, $o->Type, $subj->Login, $obj->Login, $o->Sum . "; " . $o->Data . "; " . $o->RefID . "; " . $o->EnterPage . "; " . $o->FromPage . "; " . $o->UtmSource . "; " . $o->UtmCampaign . "; " . $o->UtmTerm, "/partner/clientinfo/" . $o->Subject, "/partner/clientinfo/" . $o->Object];
                // }, $evs);
                // $this->view->num_pages = ceil($this->pixie->orm->get('event')->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->where('or', array('Object', 'IN', $this->pixie->db->expr($usersStr)))->count_all() / $this->item_per_page);
            // }
            $this->view->url = "/partner/events";
            $this->view->events = $events;
            $this->view->current_page = $current_page;
            $this->view->item_per_page = $this->item_per_page;
        }
        $this->view->subview = 'events';
    }
    
    public function action_analytics() {
        if(!$this->logged_in("Partner"))
            return;
        date_default_timezone_set("Europe/Moscow");
        $period = "month";
        $percnt = 3;
        if(isset($_POST['period']))
            $period = $this->request->post('period');
        $this->view->periodname = $period;
        if(isset($_POST['percnt']))
            $percnt = $this->request->post('percnt');
        $periods = [];
        for($i = 0; $i < $percnt; $i++){
            if($period=="month")
                $periods[$i]=date_format(date_sub(date_create(), date_interval_create_from_date_string($i.' month')), 'Y-m-01 00:00:00');
            if($period=="day")
                $periods[$i]=date_format(date_sub(date_create(), date_interval_create_from_date_string($i.' day')), 'Y-m-d 00:00:00');
            if($period=="week"){
                $weekdaynow = date_format(date_create(), 'N');
                $periods[$i]=date_format(date_sub(date_create(), date_interval_create_from_date_string((($i*7)+intval($weekdaynow)-1).' day')), 'Y-m-d 00:00:00');
            }
        }
        $usrs = $this->pixie->orm->get('user')->where('Partner', $this->pixie->auth->user()->id)->find_all()->as_array();
        $users = array_map(function ($o) {
                return $o->id;
            }, $usrs);
        $usersStr = '(' . implode(',', $users) . ')';
        $evs=[];
        $evs[0] = $this->pixie->orm->get('event')->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->where('Type', 'like', "%register%")->where('EventTime', '>', $periods[0])->order_by('id', 'desc')->find_all()->as_array();
        for($i = 1; $i < $percnt; $i++){
            $evs[$i] = $this->pixie->orm->get('event')->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->where('Type', 'like', "%register%")->where('EventTime', '>', $periods[$i])->where('EventTime', '<', $periods[$i-1])->order_by('id', 'desc')->find_all()->as_array();
        }
        
        // $evs1 = $this->pixie->orm->get('event')->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->where('Type', 'like', "%register%")->where('EventTime', 'like', $time_plus1 . "%")->order_by('id', 'desc')->find_all()->as_array();
        // $evs2 = $this->pixie->orm->get('event')->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->where('Type', 'like', "%register%")->where('EventTime', 'like', $time_plus2 . "%")->order_by('id', 'desc')->find_all()->as_array();
        $evsall = $this->pixie->orm->get('event')->where('Type', 'like', "%register%")->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->find_all()->as_array();
        
        
        
        $sources1=array('foo' => array(0));
        $sources=[];
        for($i = 0; $i < $percnt; $i++){
            $regs[$i] = array('foo' => 'bar');
            $paycnt[$i] = array('foo' => 'bar');
            $paysum[$i] = array('foo' => 'bar');
            $distribcnt[$i] = array('foo' => 'bar');
        }
        foreach($evsall as $ev){
            if($ev->UtmSource!=""){
                $src = $ev->UtmSource;
            }
            else{
                $src = "-";
            }
            if(!in_array($src, $sources)){
                $sources[] = $src;
                for($i = 0; $i < $percnt; $i++){
                    $regs[$i][$src] = 0;
                    $paysum[$i][$src] = 0;
                    $distribcnt[$i][$src] = 0;
                    $paycnt[$i][$src] = 0;
                }
            }
            // Сбор все пользователей сгруппированных по источникам
            $sources1[$src][] = $ev->Subject;
        }
        // Количество и сумма оплат, кол-во рассылок по источникам
        foreach($sources1 as $key => $src){
            $paycnt[0][$key] = $this->pixie->orm->get('event')->where('Type', 'like', "%balancechange%")->where('Data', 'not like', '%marketing%')->where('Data', 'not like', '%reffer%')->where('Data', 'not like', '%start%')->where('Data', 'not like', '%w1processed%')->where('Data', 'not like', '%promo%')->where('EventTime', '>', $periods[0])->where('Object', 'IN', $this->pixie->db->expr('(' . implode(',', $src) . ')'))->count_all();
            $distribcnt[0][$key] = $this->pixie->orm->get('distrib')->where('Size', '>', 10)->where('Status', '!=', "Canceled")->where('Status', '!=', "Moderating")->where('StartTime', '>', $periods[0])->where('UserID', 'IN', $this->pixie->db->expr('(' . implode(',', $src) . ')'))->count_all();
            $pays = $this->pixie->orm->get('event')->where('Type', 'like', "%balancechange%")->where('Data', 'not like', '%marketing%')->where('Data', 'not like', '%reffer%')->where('Data', 'not like', '%start%')->where('Data', 'not like', '%w1processed%')->where('Data', 'not like', '%promo%')->where('EventTime', '>', $periods[0])->where('Object', 'IN', $this->pixie->db->expr('(' . implode(',', $src) . ')'))->find_all()->as_array();
            foreach($pays as $pay){
                $paysum[0][$key] += $pay->Sum;
            }
            for($i = 1; $i < $percnt; $i++){
                $paycnt[$i][$key] = $this->pixie->orm->get('event')->where('Type', 'like', "%balancechange%")->where('Data', 'not like', '%marketing%')->where('Data', 'not like', '%reffer%')->where('Data', 'not like', '%start%')->where('Data', 'not like', '%w1processed%')->where('Data', 'not like', '%promo%')->where('EventTime', '>', $periods[$i])->where('EventTime', '<', $periods[$i-1])->where('Object', 'IN', $this->pixie->db->expr('(' . implode(',', $src) . ')'))->count_all();
                $distribcnt[$i][$key] = $this->pixie->orm->get('distrib')->where('Size', '>', 10)->where('Status', '!=', "Canceled")->where('Status', '!=', "Moderating")->where('StartTime', '>', $periods[$i])->where('StartTime', '<', $periods[$i-1])->where('UserID', 'IN', $this->pixie->db->expr('(' . implode(',', $src) . ')'))->count_all();
                $pays = $this->pixie->orm->get('event')->where('Type', 'like', "%balancechange%")->where('Data', 'not like', '%marketing%')->where('Data', 'not like', '%reffer%')->where('Data', 'not like', '%start%')->where('Data', 'not like', '%w1processed%')->where('Data', 'not like', '%promo%')->where('EventTime', '>', $periods[$i])->where('EventTime', '<', $periods[$i-1])->where('Object', 'IN', $this->pixie->db->expr('(' . implode(',', $src) . ')'))->find_all()->as_array();
                foreach($pays as $pay){
                    $paysum[$i][$key] += $pay->Sum;
                }
            }
        }
        // Инициализация массива регистраций
        for($i = 0; $i < $percnt; $i++){
            foreach($evs[$i] as $ev){
                if($ev->UtmSource!=""){
                    $src = $ev->UtmSource;
                }
                else{
                    $src = "-";
                }
                $regs[$i][$src] += 1;
            }
        }
        $this->view->regs = $regs;
        $this->view->sources = $sources;
        $this->view->period = $periods;
        $this->view->paycnt = $paycnt;
        $this->view->percnt = $percnt;
        $this->view->paysum = $paysum;
        $this->view->distribcnt = $distribcnt;
        $this->view->subview = 'analytics';
    }
    
    public function action_gethistory() {
        if(!$this->logged_in("Partner, Manager"))
            return;
        if($this->request->method == 'POST'){
            $uid = $this->request->post('userid');
            $pays = $this->pixie->orm->get('payment')->where('userID', $uid)->order_by("paydate", "desc")->limit(4)->find_all()->as_array();
            $paysStr = "";
            if($this->pixie->orm->get('payment')->where('userID', $uid)->count_all()>0){
                $payments = array_map(function ($o) {
                    return $o->paydate . "," . $this->pixie->orm->get('paytype')->where('name', $o->type)->find()->description . "," . $o->value;
                }, $pays);
                $paysStr = implode(';', $payments);
            }
            $this->response->body = $paysStr;
            $this->execute = false;
        }
    }
    
    public function action_changebalance() {
        if(!$this->logged_in("Partner"))
            return;
        if($this->request->method == 'POST'){
            date_default_timezone_set("Europe/Moscow");
			$time_now = date_format(date_create(), 'Y-m-d H:i:s');
            $uid = $this->request->post('userid');
            $sum = $this->request->post('sum') * 100;
            $type = $this->request->post('type');
            $user = $this->pixie->orm->get('user')->where('id', $uid)->find();
            $partner = $this->pixie->orm->get('user')->where('id', $user->Partner)->find();
            $res = $this->pay($uid, $partner->id, $sum, $time_now, $type);
            $this->response->body = "Платеж совершен.";
            $this->execute = false;
        }
    }
    
    public function action_setdefault() {
        if(!$this->logged_in("Partner"))
            return;
        if($this->request->method == 'POST'){
            $usrs = $this->pixie->orm->get('domain')->where("UserID", $this->pixie->auth->user()->id)->find_all()->as_array();
            $domains = array_map(function ($o) {
                return $o->id;
            }, $usrs);
            $domainsStr = '(' . implode(',', $domains) . ')';
            $defaults = $this->pixie->orm->get('defaultvalue')->where('domainID', 'IN', $this->pixie->db->expr($domainsStr))->find();
            $uid = $this->request->post('userid');
            $user = $this->pixie->orm->get('user')->where('id', $uid)->find();
            $user->costgroup=$defaults->costgroup;
            $user->save();
            $this->event($user->id, $this->pixie->auth->user()->id, "changetarif", $user->costgroup);
            $this->response->body = "Тариф назначен";
            $this->execute = false;
        }
    }
    
    public function action_changetariff() {
        if(!$this->logged_in("Partner"))
            return;
        if($this->request->method == 'POST'){
            $recall = $this->request->post('recall');
            $dial = $this->request->post('dial');
            $ordinary = $this->request->post('ordinary');
            $uid = $this->request->post('userid');
            $user = $this->pixie->orm->get('user')->where('id', $uid)->find();
            $costs1 = $this->pixie->orm->get('cost')->where('recall', $recall)->where('dial', $dial)->where('ordinary', $ordinary)->find();
            if($costs1->loaded()){
                $user->costgroup=$costs1->id;
                $user->save();
                $this->event($user->id, $this->pixie->auth->user()->id, "changetarif", $user->costgroup);
                $this->response->body = "Тариф назначен";
                $this->execute = false;
            }
            else{
                $costs2 = $this->pixie->orm->get('cost');
                $costs2->recall = $recall;
                $costs2->dial = $dial;
                $costs2->ordinary = $ordinary;
                $costs2->save();
                $user->costgroup=$costs2->id;
                $user->save();
                $this->event($user->id, $this->pixie->auth->user()->id, "changetarif", $user->costgroup);
                $this->response->body = "Новый тариф назначен";
                $this->execute = false;
            }
        }
    }
    
    public function action_addcomment() {
        if(!$this->logged_in("Partner, Manager"))
            return;
        $id = $this->request->post('userid');
        $text = $this->request->post('text');
        $this->event($id, $this->pixie->auth->user()->id, "addcomment", $text);
        $this->response->body = "Комментарий оставлен";
        $this->execute = false;
    }
    
    public function action_loadinfo() {
        if(!$this->logged_in("Partner, Manager"))
            return;
        if (ob_get_level()) {
                ob_end_clean();
        }
        if(isset($_SERVER['HTTP_USER_AGENT']) and strpos($_SERVER['HTTP_USER_AGENT'],'MSIE'))
            Header('Content-Type: application/force-download');
        else{
            Header('Content-Type: application/octet-stream');
            Header('Accept-Ranges: bytes');
            Header('Content-Length: '.(100000));
            Header('Content-disposition: attachment; filename=clients.csv');
            print(iconv("UTF-8", "Windows-1251","Почта;Телефон;Баланс;Перезвон;Обычная;Дозвон;Кол-во рассылок" . "\r\n"));
            $currole = $this->pixie->auth->user()->UserType;
            $users = $this->pixie->orm->get("user")->where($currole, $this->pixie->auth->user()->id)->find_all();
            foreach($users as $user){
                $cost = $this->pixie->orm->get('cost')->where('id', $user->costgroup)->find();
                $distrs=$this->pixie->orm->get('distrib')->where('UserID', $user->id)->count_all();
                print($user->Login . ";" . $user->UserPhone . ";" . $user->balance/100 . ";" . (string)$cost->recall/100 . ";" . (string)$cost->ordinary/100 . ";" . (string)$cost->dial/100 . ";" . $distrs . "\r\n");
            }       
        }
        return $this->redirect('/su/users');
    }
    
    public function action_loadphones() {
        if(!$this->logged_in("Partner, Manager"))
            return;
        if (ob_get_level()) {
                ob_end_clean();
        }
        if(isset($_SERVER['HTTP_USER_AGENT']) and strpos($_SERVER['HTTP_USER_AGENT'],'MSIE'))
            Header('Content-Type: application/force-download');
        else{
            Header('Content-Type: application/octet-stream');
            Header('Accept-Ranges: bytes');
            Header('Content-Length: '.(100000));
            Header('Content-disposition: attachment; filename=phones.txt');
            $currole = $this->pixie->auth->user()->UserType;
            $users = $this->pixie->orm->get("user")->where($currole, $this->pixie->auth->user()->id)->where('CallTo', 1)->find_all();
            foreach($users as $user){
                print($user->UserPhone . "\r\n");
            }       
        }
        return $this->redirect('/su/users');
    }
    
    public function action_editinfo() {
        if(!$this->logged_in("Partner, Manager"))
            return;
        $currole = $this->pixie->auth->user()->UserType;
        $id = $this->request->post('userid');
        $user=$this->pixie->orm->get('user')->where("id", $id)->find();
        $utmsource = $this->request->post('utmsource');
        $utmcampaign = $this->request->post('utmcampaign');
        $utmterm = $this->request->post('utmterm');
        $userinfo = $this->request->post('userinfo');
        if($currole=="Partner"){
            $regevent = $this->pixie->orm->get('event')->where("Type", 'like', '%register%')->where("Subject", $id)->find();
            if($regevent->loaded()){
                $regevent->UtmSource = $utmsource;
                $regevent->UtmCampaign = $utmcampaign;
                $regevent->UtmTerm = $utmterm;
                $regevent->save();
            }
        }
        $uinfo = $this->pixie->orm->get('userinfo')->where("UserID", $id)->find();
        if($uinfo->loaded()){
            $uinfo->Info = $userinfo;
            $uinfo->save();
        }
        else{
            $uinfo = $this->pixie->orm->get('userinfo');
            $uinfo->Info = $userinfo;
            $uinfo->UserID = $id;
            $uinfo->save();
        }
        if($this->request->post('chkemail')=="true"){
            $user->MailTo=1;
        }
        else{
            $user->MailTo=0;
        }
        if($this->request->post('chkcall')=="true"){
            $user->CallTo=1;
        }
        else{
            $user->CallTo=0;
        }
        $this->response->body = "Обновлено";
        $newuser=$this->pixie->orm->get('user')->where("Login", $this->request->post('username'))->find();
        if($currole!='Manager'){
            if(!$newuser->loaded()){
                $user->Login = $this->request->post('username');
            }
            else{
                if($this->request->post('username')!=$user->Login){
                    $this->response->body = "Email не обновлен, такой пользователь уже существует!";
                }
            }
        }
        $user->save();
        $this->event($id, $this->pixie->auth->user()->id, "editinfo", $id);
        $this->execute = false;
    }
    
    public function action_transferto(){
        if(!$this->logged_in("Partner, Admin"))
            return;
        $userid = $this->request->post('User');
        $partnerlogin = $this->request->post('Partner');
        $user = $this->pixie->orm->get('user')->where('UserType', 'User')->where('id', $userid)->find();
        if($user->loaded()){
            $partner = $this->pixie->orm->get('user')->where('Login', $partnerlogin)->where('UserType', 'Partner')->find();
            if($partner->loaded()){
                $user->Partner = $partner->id;
                $user->Manager = NULL;
                $user->save();
                $this->response->body = "Пользователь передан!";
                $this->event($userid, $this->pixie->auth->user()->id, "transfer", $partner->id);
            }
            else{
               $this->response->body = "Извините, такого партнера не существует!"; 
            }
        }
        else{
            $this->response->body = "Извините, такого пользователя не существует!";
        }
        $this->execute = false;
    }
    
    public function action_transfertomanager(){
        if(!$this->logged_in("Partner, Admin, Manager"))
            return;
        $userid = $this->request->post('User');
        $managerid = $this->request->post('Manager');
        $user = $this->pixie->orm->get('user')->where('UserType', 'User')->where('id', $userid)->find();
        if($user->loaded()){
            $exec = true;
            if($this->pixie->auth->user()->UserType == "Manager"){
                if($user->Manager != $this->pixie->auth->user()->id){
                    $exec = false;
                }
            }
            if($exec){
                $manager = $this->pixie->orm->get('user')->where('id', $managerid)->where('Partner', $user->Partner)->where('UserType', 'Manager')->find();
                if($manager->loaded()){
                    $user->Manager = $managerid;
                    $user->save();
                    $this->response->body = "Пользователь передан!";
                    $this->event($userid, $this->pixie->auth->user()->id, "transfertoman", $managerid);
                }
                else{
                   $this->response->body = "Извините, такого менеджера не существует!"; 
                }
            }
            else{
                $this->response->body = "Вы не можете передавать чужого пользователя!";
            }
        }
        else{
            $this->response->body = "Извините, такого пользователя не существует!";
        }
        $this->execute = false;
    }
    
    public function action_clientinfo() {
        if(!$this->logged_in("Partner, Manager"))
            return;
        $currole = $this->pixie->auth->user()->UserType;
        $this->view->currole = $currole;
        if($currole=="Manager"){
            $partner = $this->pixie->orm->get('user', $this->pixie->auth->user()->Partner)->find();
        }
        else{
            $partner = $this->pixie->auth->user();
        }
        $id = $this->request->param('id');
        $this->view->ty = "partner";
        $this->view->user = $this->pixie->orm->get('user')->where('id', $id)->find();
        $this->view->refmail="";
        $this->view->refmail2="";
        if($this->view->user->RefID!=-1){
            $this->view->refmail=$this->pixie->orm->get('user')->where('id', $this->view->user->RefID)->find()->Login;
        }
        $users2 = $this->pixie->orm->get('user')->where('RefID', $id)->find_all()->as_array();
        if(count($users2)!=0){
            $this->view->refmail2="b";
        }
        if($this->view->user->UserType!="User" or $this->view->user->Partner!=$partner->id){
            return $this->redirect('/su/users');
        }
        $this->view->partner = $this->pixie->orm->get('user')->where('id', $this->view->user->Partner)->find();
        $this->view->partners = $this->pixie->orm->get('user')->where('UserType', "Partner")->where('id', "!=", $this->view->user->Partner)->find_all()->as_array();
        $this->view->managers = [];
        if(strval($this->view->user->Manager)!=""){
            $this->view->managers = $this->pixie->orm->get('user')->where('UserType', "Manager")->where('Partner', $this->view->partner->id)->where('id', "!=", $this->view->user->Manager)->find_all()->as_array();
        }
        $this->view->distrs = $this->pixie->orm->get('distrib')->where('UserID', $id)->count_all();
        $this->view->cost = $this->pixie->orm->get('cost')->where('id', $this->view->user->costgroup)->find();
        $this->view->userinfo = "";
        $this->view->regdate = "";
        $this->view->usource = "";
        $this->view->ucampaign = "";
        $this->view->uterm = "";
        if($this->view->user->MailTo==1)
            $this->view->checkedemail="checked";
        else
            $this->view->checkedemail="";
        if($this->view->user->CallTo==1)
            $this->view->checkedcall="checked";
        else
            $this->view->checkedcall="";
        $uinfo = $this->pixie->orm->get('userinfo')->where("UserID", $id)->find();
        $regevent = $this->pixie->orm->get('event')->where("Type", 'like', '%register%')->where("Subject", $id)->find();
        if($uinfo->loaded()){
            $this->view->userinfo = $uinfo->Info;
        }
        if($regevent->loaded()){
            $this->view->regdate = $regevent->EventTime;
            $this->view->usource = $regevent->UtmSource;
            $this->view->ucampaign = $regevent->UtmCampaign;
            $this->view->uterm = $regevent->UtmTerm;
        }
        if($this->request->method == 'POST'){
            $type = $this->request->post('eventtype');
            $evs = $this->pixie->orm->get('event')->where("Type", 'like', '%' . $type . '%')->where('Subject', $id)->where('or', array('Object', $id))->where("Type", 'like', '%' . $type . '%')->order_by('id', 'desc')->limit(1000)->find_all()->as_array();
            $this->view->events = array_map(function ($o) {
                $subj = $this->pixie->orm->get('user')->where('id', $o->Subject)->find();
                $obj = $this->pixie->orm->get('user')->where('id', $o->Object)->find();
                if($subj->loaded() and $obj->loaded())
                return [$o->EventTime, $o->Type, $subj->Login, $obj->Login, $o->Sum . "; " . $o->Data . "; " . $o->RefID . "; " . $o->EnterPage . "; " . $o->FromPage . "; " . $o->UtmSource . "; " . $o->UtmCampaign . "; " . $o->UtmTerm];
            }, $evs);
        }
        else{
            $evs = $this->pixie->orm->get('event')->where('Subject', $id)->where('or', array('Object', $id))->order_by('id', 'desc')->limit(1000)->find_all()->as_array();
            $this->view->events = array_map(function ($o) {
                $subj = $this->pixie->orm->get('user')->where('id', $o->Subject)->find();
                $obj = $this->pixie->orm->get('user')->where('id', $o->Object)->find();
                if($subj->loaded() and $obj->loaded())
                return [$o->EventTime, $o->Type, $subj->Login, $obj->Login, $o->Sum . "; " . $o->Data . "; " . $o->RefID . "; " . $o->EnterPage . "; " . $o->FromPage . "; " . $o->UtmSource . "; " . $o->UtmCampaign . "; " . $o->UtmTerm];
            }, $evs);
        }
        $this->view->subview = 'clientinfo';
    }
    
    public function action_clients() {
        if(!$this->logged_in("Partner, Manager"))
            return;
        $currole = $this->pixie->auth->user()->UserType;
        $this->view->currole = $currole;
        $managerfilter = -1;
        if($currole=="Manager"){
            $managerfilter = $this->pixie->auth->user()->id;
        }
        if(array_key_exists("managerfilter",$_COOKIE))
            $managerfilter = $_COOKIE["managerfilter"];
        $this->view->managerf=$managerfilter;
        if($currole=="Manager"){
            $partner = $this->pixie->orm->get('user')->where('id', $this->pixie->auth->user()->Partner)->find();
            $usrs = $this->pixie->orm->get('domain')->where("UserID", $partner->id)->find_all()->as_array();
        }
        else{
            $partner = $this->pixie->auth->user();
            $usrs = $this->pixie->orm->get('domain')->where("UserID", $this->pixie->auth->user()->id)->find_all()->as_array();
        }
        $this->view->managers = $this->pixie->orm->get('user')->where('Partner', $partner->id)->where('UserType', 'Manager')->find_all()->as_array();
        $domains = array_map(function ($o) {
            return $o->id;
        }, $usrs);
        $domainsStr = '(' . implode(',', $domains) . ')';
        $this->view->defaults = $this->pixie->orm->get('defaultvalue')->where('domainID', 'IN', $this->pixie->db->expr($domainsStr))->find();
        $this->view->defaultcosts = $this->pixie->orm->get('cost')->where('id', $this->view->defaults->costgroup)->find();
        $type = -1;
        if(array_key_exists("clienttype",$_COOKIE))
            $type = $_COOKIE["clienttype"];
        $this->view->cltype=$type;
        if(isset($_GET['src'])){
            if($_GET['src']=="-"){
                $src="";
                $evs1 = $this->pixie->orm->get('event')->where('Type', 'like', "%register%")->where('UtmSource', '!=', "")->find_all()->as_array();
                $ev = array_map(function ($o) {
                    return $o->id;
                }, $evs1);
                $evStr = '(' . implode(',', $ev) . ')';
                $evs = $this->pixie->orm->get('event')->where('id', 'NOT IN', $this->pixie->db->expr($evStr))->where('Type', 'like', "%register%")->find_all()->as_array();
            }
            else{
                $src=$_GET['src'];
                $evs = $this->pixie->orm->get('event')->where('Type', 'like', "%register%")->where('UtmSource', 'like', $src)->find_all()->as_array();
            }
            $usrs = array_map(function ($o) {
                return $o->Subject;
            }, $evs);
            $usrsStr = '(' . implode(',', $usrs) . ')';
            if($type!=-1)
                if($managerfilter==-1){
                    $this->view->users = $this->pixie->orm->get('user')->where($currole, $this->pixie->auth->user()->id)->where('UserType', 'User')->where('id', 'IN', $this->pixie->db->expr($usrsStr))->where("ClientStatus", $type)->order_by('id', 'desc')->find_all()->as_array();
                }
                else{
                    $this->view->users = $this->pixie->orm->get('user')->where("Manager", $managerfilter)->where($currole, $this->pixie->auth->user()->id)->where('UserType', 'User')->where('id', 'IN', $this->pixie->db->expr($usrsStr))->where("ClientStatus", $type)->order_by('id', 'desc')->find_all()->as_array();
                }
            else
                if($managerfilter==-1){
                    $this->view->users = $this->pixie->orm->get('user')->where($currole, $this->pixie->auth->user()->id)->where('UserType', 'User')->where('id', 'IN', $this->pixie->db->expr($usrsStr))->order_by('id', 'desc')->find_all()->as_array();
                }
                else{
                    $this->view->users = $this->pixie->orm->get('user')->where("Manager", $managerfilter)->where($currole, $this->pixie->auth->user()->id)->where('UserType', 'User')->where('id', 'IN', $this->pixie->db->expr($usrsStr))->order_by('id', 'desc')->find_all()->as_array();
                }
        }
        else{
            if($type!=-1)
                if($managerfilter==-1){
                    $this->view->users = $this->pixie->orm->get('user')->where('Partner', $partner->id)->where('UserType', 'User')->where("ClientStatus", $type)->order_by('id', 'desc')->find_all()->as_array();
                }
                else{
                    $this->view->users = $this->pixie->orm->get('user')->where("Manager", $managerfilter)->where('Partner', $partner->id)->where('UserType', 'User')->where("ClientStatus", $type)->order_by('id', 'desc')->find_all()->as_array();
                }
            else
                if($managerfilter==-1){
                    $this->view->users = $this->pixie->orm->get('user')->where('Partner', $partner->id)->where('UserType', 'User')->order_by('id', 'desc')->find_all()->as_array();
                }
                else{
                    $this->view->users = $this->pixie->orm->get('user')->where("Manager", $managerfilter)->where('Partner', $partner->id)->where('UserType', 'User')->order_by('id', 'desc')->find_all()->as_array();
                }
        }
        $costs1 = array(); 
        $distrs1 = array(); 
        foreach($this->view->users as $user1){
            $cost = $this->pixie->orm->get('cost')->where('id', $user1->costgroup)->find();
            $costs1[$user1->id]=(string)$cost->recall/100 . "::" . (string)$cost->ordinary/100 . "::" . (string)$cost->dial/100;
            $distrs1[$user1->id]=$this->pixie->orm->get('distrib')->where('UserID', $user1->id)->count_all();
        }
        $this->view->costs = $costs1;
        $this->view->distrs = $distrs1;
        $this->view->types = $this->pixie->orm->get('paytype')->where('auto', 0)->find_all()->as_array();
        if($this->request->method == 'POST'){
            if($currole=="Manager"){
                $this->response->body = "Недостаточно прав для выполнения данной операции!";
                $this->execute = false;
            }
            else{
                $recall = $this->request->post('recall');
                $dial = $this->request->post('dial');
                $ordinary = $this->request->post('ordinary');
                $costs1 = $this->pixie->orm->get('cost')->where('recall', $recall)->where('dial', $dial)->where('ordinary', $ordinary)->find();
                if($costs1->loaded()){
                    $this->view->defaults->costgroup=$costs1->id;
                    $this->view->defaults->save();
                    $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "changedefaulttarif", $costs1->id);
                    $this->response->body = "Назначен тариф по умолчанию";
                    $this->execute = false;
                }
                else{
                    $costs2 = $this->pixie->orm->get('cost');
                    $costs2->recall = $recall;
                    $costs2->dial = $dial;
                    $costs2->ordinary = $ordinary;
                    $costs2->save();
                    $this->view->defaults->costgroup=$costs2->id;
                    $this->view->defaults->save();
                    $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "changedefaulttarif", $costs2->id);
                    $this->response->body = "Назначен новый тариф по умолчанию";
                    $this->execute = false;
                }
            }
        }
        $this->view->subview = 'clients';
    }
    
    public function action_start() {
        if(!$this->logged_in("Partner"))
            return;
        $this->view->user = $this->pixie->auth->user();
        if($this->request->method == 'POST'){
            return $this->redirect('/admin/new_distrib');
        }
        $this->view->subview = 'start';
    }
    
    public function action_fail() {
        $this->view->description = $this->request->param('id');
        $this->view->subview = 'fail';
    }
    
    // public function action_tariffs() {
        // if(!$this->logged_in("Partner"))
            // return;
        // $users = $this->pixie->orm->get('user')->where("Partner", $this->pixie->auth->user()->id)->find_all()->as_array();
        // $usrsStr = '(' . implode(',', $uss) . ')';
        // $this->view->distribs = $this->pixie->orm->get('distrib')->where('UserID', 'IN', $this->pixie->db->expr($usrsStr))->order_by('CreateTime','desc')->find_all();
        // $this->view->subview = 'distribsp';
    // }
    
    public function action_distribs() {
        date_default_timezone_set("Europe/Moscow");
        $this->view->time_now = date_format(date_create(), 'Y-m-d H:i:s');
        $this->view->statuses = array("Moderating" => "На модерации", "Started" => "В процессе", "Ready" => "Обработка", "Finished" => "Закончена", "Paused" => "На паузе", "Pausing" => "Ставится на паузу", "Stopped" => "Остановлена", "Stopping" => "Останавливается", "Canceled" => "Отменена", "Starting" => "Начинается");
        $this->view->types = array("recall" => "Дозвон - сброс", "ordinary" => "Обычная");
        $this->view->control = "partner";
        $current_page = $this->request->param('page');
        $currole = $this->pixie->auth->user()->UserType;
        if(!$this->logged_in("Partner, Manager"))
            return;
        $managerfilter = -1;
        if($currole=="Manager"){
            $managerfilter = $this->pixie->auth->user()->id;
        }
        if(array_key_exists("managerfilter",$_COOKIE))
            $managerfilter = $_COOKIE["managerfilter"];
        $this->view->managerf=$managerfilter;
        if($currole=="Manager"){
            $partner = $this->pixie->orm->get('user')->where('id', $this->pixie->auth->user()->Partner)->find();
        }
        else{
            $partner = $this->pixie->auth->user();
        }
        $this->view->managers = $this->pixie->orm->get('user')->where('Partner', $partner->id)->where('UserType', 'Manager')->find_all()->as_array();
        if($managerfilter==-1){
            $usrs = $this->pixie->orm->get('user')->where('Partner', $partner->id)->find_all()->as_array();
        }
        else{
            $usrs = $this->pixie->orm->get('user')->where('Partner', $partner->id)->where('Manager', $managerfilter)->find_all()->as_array();
        }
        $uss = array_map(function ($o) {
            return $o->id;
        }, $usrs);
        $usrsStr = '(' . implode(',', $uss) . ')';
        if(count($uss)>0){
            $distribs = $this->pixie->orm->get('distrib')->where('UserID', 'IN', $this->pixie->db->expr($usrsStr))->order_by('CreateTime','desc');
            if($distribs->count_all()>0){
                $pager = $this->pixie->paginate->orm($distribs, $current_page, $this->item_per_page);
                $pager->set_url_route('partner/distribs');
                $this->view->item_per_page = $this->item_per_page;
                $this->view->pager = $pager;
                $this->view->current_page = $current_page;
                $this->view->empt=0;
            }
            else{
                $this->view->empt=1;
            }
        }
        else{
            $this->view->empt=1;
        }
        $this->view->subview = 'distribs';
    }
    
    public function action_profile() {
        if(!$this->logged_in("Partner"))
            return;
        $this->view->contact = "None";
        $admin = $this->pixie->orm->get("user")->where("UserType", "Admin")->find();
        $dopdir = '/var/www/html/pixiecaller/web';
        $this->view->cost = $this->pixie->orm->get('cost')->where('id', $this->pixie->auth->user()->costgroup)->find();
        if($this->pixie->auth->user()->Contacts!=NULL){
            $contact1 = $this->pixie->orm->get("contact")->where("id", $this->pixie->auth->user()->Contacts)->find();
        }
        if($admin->Contacts!=NULL){
            $contact2 = $this->pixie->orm->get("contact")->where("id", $admin->Contacts)->find();
        }
        $this->view->wAudio = "";
        $this->view->tAudio = "";
        $this->view->check=false;
        if($this->pixie->auth->user()->Contacts!=NULL and $contact1->loaded()){
            $this->view->contact = $contact1;
            if($contact1->WelcomeAudio==""){
                if($admin->Contacts!=NULL and $contact2->loaded()){
                    if($contact1->WelcomeAudio=="None"){
                        $this->view->check=true;
                    }
                    else{
                        $this->view->wAudio = str_replace ($dopdir, "", $contact2->WelcomeAudio . "s.wav");
                    }
                }
            }
            else{
                if($contact1->WelcomeAudio=="None"){
                    $this->view->check=true;
                }
                else{
                    $this->view->wAudio = str_replace ($dopdir, "", $contact1->WelcomeAudio . "s.wav");
                }
            }
            if($contact1->TestAudio==""){
                if($admin->Contacts!=NULL and $contact2->loaded()){
                    $this->view->tAudio = str_replace ($dopdir, "", $contact2->TestAudio . "s.wav");
                }
            }
            else{
                $this->view->tAudio = str_replace ($dopdir, "", $contact1->TestAudio . "s.wav");
            }
        }
        else{
            if($admin->Contacts!=NULL and $contact2->loaded()){
                if($contact2->WelcomeAudio=="None"){
                    $this->view->check=true;
                }
                else{
                    $this->view->wAudio = str_replace ($dopdir, "", $contact2->WelcomeAudio . "s.wav");
                }
                $this->view->tAudio = str_replace ($dopdir, "", $contact2->TestAudio . "s.wav");
            }
        }
        if ($this->request->method == 'POST') {
            $user = $this->pixie->auth->user();
            $oldjson = "{\"UserName\" : \"" . $user->UserName . "\", \"UserPhone\" : \"" . $user->UserPhone . "\", \"Login\" : \"" . $user->Login . "\"}";
            $user->UserPhone = $this->request->post('userphone');
            $us = $this->pixie->orm->get('user')->where('Login', $this->request->post('usermail'))->find();
            if(!$us->loaded()){
                $user->Login = $this->request->post('usermail');
            }
            $user->save();
            $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "profilechange", $oldjson);
        }
        $this->view->user = $this->pixie->auth->user();
        $this->view->subview = 'profilep';
    }
    
    public function action_editcontacts() {
        if(!$this->logged_in("Partner"))
            return;
        $cont = "None";
        if($this->pixie->auth->user()->Contacts!=NULL){
            $contact = $this->pixie->orm->get("contact")->where("id", $this->pixie->auth->user()->Contacts)->find();
        }
        if($this->pixie->auth->user()->Contacts!=NULL and $contact->loaded()){
            $cont = $contact;
        }
        if ($this->request->method == 'POST') {
            $user = $this->pixie->auth->user();
            $uploaddir = '/var/www/html/pixiecaller/web/assets/audio/uploads/';
            $userdir = $uploaddir . $this->pixie->auth->user()->Login  . "/";
            $welcome = true;
            if($this->request->post('welcomecall') == "on"){
                $welcome = false;
            }
            if( ! is_dir( $userdir ) ) mkdir( $userdir );
            if($cont!="None"){
                $contact->UserPhone = $this->request->post('phone');
                $contact->UserEmail = $this->request->post('email');
                $contact->maincolor = $this->request->post('maincolor');
                if($welcome){
                    if(array_key_exists('welcome1', $_FILES)){
                        $file = $_FILES['welcome1'];
                        $parts = explode(".", $file['name']);
                        $ext = $parts[count($parts)-1];
                        if( move_uploaded_file( $file['tmp_name'], $userdir . "welcome1s." . $ext ) ){
                            $contact->WelcomeAudio = $userdir . "welcome1";
                            if($ext == "mp3"){
                                shell_exec("mpg123 -w '". $userdir . "welcome1s.wav' " . $userdir . "welcome1s.mp3");
                            }
                            shell_exec("sox -t wav '". $userdir . "welcome1s.wav' -r 8000 -c1 -t gsm '" . $userdir . "welcome1.gsm'");
                            shell_exec("rm -r " . $userdir . "welcome1s.mp3");
                        }
                    }
                }
                else{
                    $contact->WelcomeAudio = "None";
                }
                if(array_key_exists('test', $_FILES)){
                    $file = $_FILES['test'];
                    $parts = explode(".", $file['name']);
                    $ext = $parts[count($parts)-1];
                    if( move_uploaded_file( $file['tmp_name'], $userdir . "test1s." . $ext ) ){
                        $contact->TestAudio = $userdir . "test1";
                        if($ext == "mp3"){
                            shell_exec("mpg123 -w '". $userdir . "test1s.wav' " . $userdir . "test1s.mp3");
                        }
                        shell_exec("sox -t wav '". $userdir . "test1s.wav' -r 8000 -c1 -t gsm '" . $userdir . "test1.gsm'");
                        shell_exec("rm -r " . $userdir . "test1s.mp3");
                    }
                }
                if(array_key_exists('picback', $_FILES)){
                    $file = $_FILES['picback'];
                    $parts = explode(".", $file['name']);
                    if( move_uploaded_file( $file['tmp_name'], "/var/www/html/pixiecaller/web/assets/img/partnerback" . $this->pixie->auth->user()->id ) ){
                    }
                }
                if(array_key_exists('piclogo', $_FILES)){
                    $file = $_FILES['piclogo'];
                    $parts = explode(".", $file['name']);
                    if( move_uploaded_file( $file['tmp_name'], "/var/www/html/pixiecaller/web/assets/img/partnerlogo" . $this->pixie->auth->user()->id ) ){
                    }
                }
                if($this->request->post('ref')=="on"){
                    $contact->refon=1;
                }
                else{
                    $contact->refon=0;
                }
                if($this->request->post('hlr')=="on"){
                    $contact->hlron=1;
                }
                else{
                    $contact->hlron=0;
                }
                if($this->request->post('promomail')=="on"){
                    $contact->promomailon=1;
                }
                else{
                    $contact->promomailon=0;
                }
                if($this->request->post('newclientsms')=="on"){
                    $contact->newclientsmson=1;
                }
                else{
                    $contact->newclientsmson=0;
                }
                if($this->request->post('casemail')=="on"){
                    $contact->casemailon=1;
                }
                else{
                    $contact->casemailon=0;
                }
                if($this->request->post('newson')=="on"){
                    $contact->newson=1;
                }
                else{
                    $contact->newson=0;
                }
                if($this->request->post('notifyon')=="on"){
                    $contact->notifyon=1;
                }
                else{
                    $contact->notifyon=0;
                }
                $contact->percent1 = $this->request->post('percent1');
                $contact->percent2 = $this->request->post('percent2');
                $tagsToReplace = array('&amp;'=>'&', '&lt;'=>'<', '&gt;'=>'>', "\r\n" =>'<br>' );
                $contact->requisites = $this->request->post('requisites');
                foreach($tagsToReplace as $key => $value){
                    $contact->requisites = str_replace($key, $value, $contact->requisites);
                }
                $contact->save();
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "contactchange");
            }
            else{
                $contact = $this->pixie->orm->get("contact");
                $contact->UserPhone = $this->request->post('phone');
                $contact->UserEmail = $this->request->post('email');
                $contact->maincolor = $this->request->post('maincolor');
                if($welcome){
                    if(array_key_exists('welcome1', $_FILES)){
                        $file = $_FILES['welcome1'];
                        $parts = explode(".", $file['name']);
                        $ext = $parts[count($parts)-1];
                        if( move_uploaded_file( $file['tmp_name'], $userdir . "welcome1s." . $ext ) ){
                            $contact->WelcomeAudio = $userdir . "welcome1";
                            if($ext == "mp3"){
                                shell_exec("mpg123 -w '". $userdir . "welcome1s.wav' " . $userdir . "welcome1s.mp3");
                            }
                            shell_exec("sox -t wav '". $userdir . "welcome1s.wav' -r 8000 -c1 -t gsm '" . $userdir . "welcome1.gsm'");
                            shell_exec("rm -r " . $userdir . "welcome1s.mp3");
                        }
                    }
                }
                else{
                    $contact->WelcomeAudio = "None";
                }
                if(array_key_exists('test', $_FILES)){
                    $file = $_FILES['test'];
                    $parts = explode(".", $file['name']);
                    $ext = $parts[count($parts)-1];
                    if( move_uploaded_file( $file['tmp_name'], $userdir . "test1s." . $ext ) ){
                        $contact->TestAudio = $userdir . "test1";
                        if($ext == "mp3"){
                            shell_exec("mpg123 -w '". $userdir . "test1s.wav' " . $userdir . "test1s.mp3");
                        }
                        shell_exec("sox -t wav '". $userdir . "test1s.wav' -r 8000 -c1 -t gsm '" . $userdir . "test1.gsm'");
                        shell_exec("rm -r " . $userdir . "test1s.mp3");
                    }
                }
                if(array_key_exists('picback', $_FILES)){
                    $file = $_FILES['picback'];
                    $parts = explode(".", $file['name']);
                    if( move_uploaded_file( $file['tmp_name'], "/var/www/html/pixiecaller/web/assets/img/partnerback" . $this->pixie->auth->user()->id ) ){
                    }
                }
                if(array_key_exists('piclogo', $_FILES)){
                    $file = $_FILES['piclogo'];
                    $parts = explode(".", $file['name']);
                    if( move_uploaded_file( $file['tmp_name'], "/var/www/html/pixiecaller/web/assets/img/partnerlogo" . $this->pixie->auth->user()->id ) ){
                    }
                }
                $contact->save();
                $oldjson = "{\"Contacts\" : \"" . $user->Contacts . "\"}";
                $user->Contacts = $contact->id;
                if($this->request->post('ref')=="on"){
                    $contact->refon=1;
                }
                else{
                    $contact->refon=0;
                }
                if($this->request->post('hlr')=="on"){
                    $contact->hlron=1;
                }
                else{
                    $contact->hlron=0;
                }
                if($this->request->post('promomail')=="on"){
                    $contact->promomailon=1;
                }
                else{
                    $contact->promomailon=0;
                }
                if($this->request->post('newclientsms')=="on"){
                    $contact->newclientsmson=1;
                }
                else{
                    $contact->newclientsmson=0;
                }
                if($this->request->post('casemail')=="on"){
                    $contact->casemailon=1;
                }
                else{
                    $contact->casemailon=0;
                }
                $contact->percent1 = $this->request->post('percent1');
                $contact->percent2 = $this->request->post('percent2');
                $tagsToReplace = array('&amp;'=>'&', '&lt;'=>'<', '&gt;'=>'>', "\r\n" =>'<br>' );
                $contact->requisites = $this->request->post('requisites');
                foreach($tagsToReplace as $key => $value){
                    $contact->requisites = str_replace($key, $value, $contact->requisites);
                }
                $user->save();
                $contact->save();
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "contactchange", $oldjson);
            }
            $this->response->body = "Изменения сохранены";
            $this->execute = false;
        }
    }
    
    // public function action_newphone() {
        // if(!$this->logged_in("Partner"))
            // return;
        // if ($this->request->method == 'POST') {
            // $number = $this->request->post('phone');
            // $num = $this->pixie->orm->get('outnumber')->where("Number", $number)->where("Status",'<>',"Accepting")->find();
            // if($num->loaded()){
                // $this->response->body = "Ошибка: Такой номер уже существует!";
                // $this->execute = false;
            // }
            // else{
                // $uploaddir = '/var/www/uploads/';
                // $userdir = $uploaddir . $this->pixie->auth->user()->Login  . "/";
                // $num1 = $this->pixie->orm->get('outnumber');
                // $num1->Number = $number;
                // $num1->Status = "Accepting";
                // $num1->AvailableFor = $this->pixie->auth->user()->id;
                // $n1=(string)rand(0, 9);
                // $n2=(string)rand(0, 9);
                // $n3=(string)rand(0, 9);
                // $n4=(string)rand(0, 9);
                // $text=$n1 . ". " . $n2 . ". " . $n3 . ". " . $n4;
                // $num1->VerCode = $n1 . $n2 . $n3 . $n4; 
                // shell_exec("wget -U 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5' 'https://tts.voicetech.yandex.net/generate?text=\"" . $text . "\"&format=wav&lang=ru-RU&emotion=good&speaker=jane&key=b3a7cba8-c45d-465b-8e9a-adaccff0a70c'  -O " . $userdir . "accept.wav");
                // shell_exec("sox -t wav '". $userdir . "accept.wav' -r 8000 -c1 -t gsm '" . $userdir . "accept.gsm'");
                // shell_exec("rm -r " . $userdir . "*.wav");
                // $num1->VerRecord = $userdir . "accept";
                // $num1->save();
                // $this->response->body = "Ждите звонка. Код состоит из 4 цифр и повторится 3 раза";
                // $this->execute = false;
            // }
            
        // }
    // }
    
    // public function action_acceptphone() {
        // if(!$this->logged_in("Partner"))
            // return;
        // if ($this->request->method == 'POST') {
            // $number = $this->request->post('phone');
            // $code = $this->request->post('code');
            // $num = $this->pixie->orm->get('outnumber')->where("Number", $number)->where("Status",'<>',"Accepting")->find();
            // if($num->loaded()){
                // $this->response->body = "Ошибка: Такой номер уже существует!";
                // $this->execute = false; 
            // }
            // else{
                // $num1 = $this->pixie->orm->get('outnumber')->where("Number", $number)->find();
                // if($num1->loaded() and $num1->VerCode==$code){
                    // $num1->Status="UserNum";
                    // $num1->save();
                    // $this->response->body = "Номер добавлен";
                    // $this->execute = false;
                // }
                // else{
                    // $this->response->body = "Ошибка! Неверный код! ";
                    // $this->execute = false;
                // }
                
            // }
            
        // }
    // }

}
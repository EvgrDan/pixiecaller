<?php
namespace App\Controller;

class Sms extends \App\Page {
    
    public function action_changestatus() {
		$jsonstr = file_get_contents('php://input');
        $json = json_decode($jsonstr);
        if($json->error_code==200){
            foreach($json->result as $res){
                $sms = $this->pixie->orm->get('sm')->where('id', $res->user_id)->find();
                if($sms->loaded()){
                    $sms->status = $res->status;
                    $sms->save();
                }
            }
        }
    }
    
}
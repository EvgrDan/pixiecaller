<?php
namespace App\Controller;

class Su extends \App\Page {
    
    public function action_addcomment() {
        if(!$this->logged_in("Admin"))
            return;
        $id = $this->request->post('userid');
        $text = $this->request->post('text');
        $this->event($id, $this->pixie->auth->user()->id, "addcomment", $text);
        $this->response->body = "Комментарий оставлен";
        $this->execute = false;
    }
    
    public function action_news() {
        if(!$this->logged_in("Admin"))
            return;
        if($this->request->method == 'POST'){
            $name = $this->request->post('newsname');
            $text = $this->request->post('newstext');
            $news = $this->pixie->orm->get("news");
            $news->Name = $name;
            $news->Text = $text;
            $news->save();
            $users = $this->pixie->orm->get("user")->find_all();
            foreach($users as $user){
                $user->NewsRead=0;
                $user->save();
            }
        }
        $this->view->news = $this->pixie->orm->get("news")->find_all();
        $this->view->subview = 'news';
    }
    
    public function action_analytics() {
        if(!$this->logged_in("Admin"))
            return;
        date_default_timezone_set("Europe/Moscow");
        $period = "month";
        $percnt = 3;
        if(isset($_POST['period']))
            $period = $this->request->post('period');
        $this->view->periodname = $period;
        if(isset($_POST['percnt']))
            $percnt = $this->request->post('percnt');
        $periods = [];
        for($i = 0; $i < $percnt; $i++){
            if($period=="month")
                $periods[$i]=date_format(date_sub(date_create(), date_interval_create_from_date_string($i.' month')), 'Y-m-01 00:00:00');
            if($period=="day")
                $periods[$i]=date_format(date_sub(date_create(), date_interval_create_from_date_string($i.' day')), 'Y-m-d 00:00:00');
            if($period=="week"){
                $weekdaynow = date_format(date_create(), 'N');
                $periods[$i]=date_format(date_sub(date_create(), date_interval_create_from_date_string((($i*7)+intval($weekdaynow)-1).' day')), 'Y-m-d 00:00:00');
            }
        }
        $usrs = $this->pixie->orm->get('user')->where('UserType', 'Partner')->find_all()->as_array();
        $users = array_map(function ($o) {
                return $o->id;
            }, $usrs);
        $usersStr = '(' . implode(',', $users) . ')';
        $evs=[];
        $evs[0] = $this->pixie->orm->get('event')->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->where('Type', 'like', "%register%")->where('EventTime', '>', $periods[0])->order_by('id', 'desc')->find_all()->as_array();
        for($i = 1; $i < $percnt; $i++){
            $evs[$i] = $this->pixie->orm->get('event')->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->where('Type', 'like', "%register%")->where('EventTime', '>', $periods[$i])->where('EventTime', '<', $periods[$i-1])->order_by('id', 'desc')->find_all()->as_array();
        }
        
        // $evs1 = $this->pixie->orm->get('event')->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->where('Type', 'like', "%register%")->where('EventTime', 'like', $time_plus1 . "%")->order_by('id', 'desc')->find_all()->as_array();
        // $evs2 = $this->pixie->orm->get('event')->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->where('Type', 'like', "%register%")->where('EventTime', 'like', $time_plus2 . "%")->order_by('id', 'desc')->find_all()->as_array();
        $evsall = $this->pixie->orm->get('event')->where('Type', 'like', "%register%")->where('Subject', 'IN', $this->pixie->db->expr($usersStr))->find_all()->as_array();
        
        
        
        $sources1=array('foo' => array(0));
        $sources=[];
        for($i = 0; $i < $percnt; $i++){
            $regs[$i] = array('foo' => 'bar');
            $paycnt[$i] = array('foo' => 'bar');
            $paysum[$i] = array('foo' => 'bar');
            $distribcnt[$i] = array('foo' => 'bar');
        }
        foreach($evsall as $ev){
            if($ev->UtmSource!=""){
                $src = $ev->UtmSource;
            }
            else{
                $src = "-";
            }
            if(!in_array($src, $sources)){
                $sources[] = $src;
                for($i = 0; $i < $percnt; $i++){
                    $regs[$i][$src] = 0;
                    $paysum[$i][$src] = 0;
                    $distribcnt[$i][$src] = 0;
                    $paycnt[$i][$src] = 0;
                }
            }
            // Сбор все пользователей сгруппированных по источникам
            $sources1[$src][] = $ev->Subject;
        }
        // Количество и сумма оплат, кол-во рассылок по источникам
        foreach($sources1 as $key => $src){
            $paycnt[0][$key] = $this->pixie->orm->get('event')->where('Type', 'like', "%balancechange%")->where('Data', 'not like', '%marketing%')->where('Data', 'not like', '%reffer%')->where('Data', 'not like', '%start%')->where('Data', 'not like', '%w1processed%')->where('Data', 'not like', '%promo%')->where('EventTime', '>', $periods[0])->where('Object', 'IN', $this->pixie->db->expr('(' . implode(',', $src) . ')'))->count_all();
            $distribcnt[0][$key] = $this->pixie->orm->get('distrib')->where('Size', '>', 10)->where('Status', '!=', "Canceled")->where('Status', '!=', "Moderating")->where('StartTime', '>', $periods[0])->where('UserID', 'IN', $this->pixie->db->expr('(' . implode(',', $src) . ')'))->count_all();
            $pays = $this->pixie->orm->get('event')->where('Type', 'like', "%balancechange%")->where('Data', 'not like', '%marketing%')->where('Data', 'not like', '%reffer%')->where('Data', 'not like', '%start%')->where('Data', 'not like', '%w1processed%')->where('Data', 'not like', '%promo%')->where('EventTime', '>', $periods[0])->where('Object', 'IN', $this->pixie->db->expr('(' . implode(',', $src) . ')'))->find_all()->as_array();
            foreach($pays as $pay){
                $paysum[0][$key] += $pay->Sum;
            }
            for($i = 1; $i < $percnt; $i++){
                $paycnt[$i][$key] = $this->pixie->orm->get('event')->where('Type', 'like', "%balancechange%")->where('Data', 'not like', '%marketing%')->where('Data', 'not like', '%reffer%')->where('Data', 'not like', '%start%')->where('Data', 'not like', '%w1processed%')->where('Data', 'not like', '%promo%')->where('EventTime', '>', $periods[$i])->where('EventTime', '<', $periods[$i-1])->where('Object', 'IN', $this->pixie->db->expr('(' . implode(',', $src) . ')'))->count_all();
                $distribcnt[$i][$key] = $this->pixie->orm->get('distrib')->where('Size', '>', 10)->where('Status', '!=', "Canceled")->where('Status', '!=', "Moderating")->where('StartTime', '>', $periods[$i])->where('StartTime', '<', $periods[$i-1])->where('UserID', 'IN', $this->pixie->db->expr('(' . implode(',', $src) . ')'))->count_all();
                $pays = $this->pixie->orm->get('event')->where('Type', 'like', "%balancechange%")->where('Data', 'not like', '%marketing%')->where('Data', 'not like', '%reffer%')->where('Data', 'not like', '%start%')->where('Data', 'not like', '%w1processed%')->where('Data', 'not like', '%promo%')->where('EventTime', '>', $periods[$i])->where('EventTime', '<', $periods[$i-1])->where('Object', 'IN', $this->pixie->db->expr('(' . implode(',', $src) . ')'))->find_all()->as_array();
                foreach($pays as $pay){
                    $paysum[$i][$key] += $pay->Sum;
                }
            }
        }
        // Инициализация массива регистраций
        for($i = 0; $i < $percnt; $i++){
            foreach($evs[$i] as $ev){
                if($ev->UtmSource!=""){
                    $src = $ev->UtmSource;
                }
                else{
                    $src = "-";
                }
                $regs[$i][$src] += 1;
            }
        }
        $this->view->regs = $regs;
        $this->view->sources = $sources;
        $this->view->period = $periods;
        $this->view->paycnt = $paycnt;
        $this->view->percnt = $percnt;
        $this->view->paysum = $paysum;
        $this->view->distribcnt = $distribcnt;
        $this->view->subview = 'analytics';
    }
    
    public function action_smsreports() {
        if(!$this->logged_in("Admin"))
            return;
        $this->view->statuses = array("OK" => "Ожидание оператора", "SENT_OK" => "Отправлена", "SENDING" => "Отправка", "CREATED" => "Обработка", "DELIVERED" => "Доставлена", "FAILED" => "Не доставлена", "FAIL" => "Не отправлена", "FATAL" => "Не отправлена");
        $smses = $this->pixie->orm->get('sm')->order_by('id', 'desc')->find_all()->as_array();
        $this->view->smses = array_map(function ($o) {
            if($o->distrib!=0){
                $distrib = $this->pixie->orm->get("distrib")->where('id', $o->distrib)->find();
                $distribname = $distrib->DistribName;
                $userid = $distrib->UserID;
                $usermail = $this->pixie->orm->get("user")->where('id', $userid)->find()->Login;
            }
            else{
                $userid = 0;
                $usermail = "Нет";
                $distribname = "Нет";
            }
            return array($o->number, $o->status, $o->message, $o->sendtime, "/admin/distribshow/" . $o->distrib, $distribname, "/su/clientinfo/" . $userid, $usermail);
        }, $smses);
        $this->view->subview = 'smsreports';
    }
    
    public function action_editinfo() {
        if(!$this->logged_in("Admin"))
            return;
        $id = $this->request->post('userid');
        $user=$this->pixie->orm->get('user')->where("id", $id)->find();
        $utmsource = $this->request->post('utmsource');
        $utmcampaign = $this->request->post('utmcampaign');
        $utmterm = $this->request->post('utmterm');
        $userinfo = $this->request->post('userinfo');
        $regevent = $this->pixie->orm->get('event')->where("Type", 'like', '%register%')->where("Subject", $id)->find();
        if($regevent->loaded()){
            $regevent->UtmSource = $utmsource;
            $regevent->UtmCampaign = $utmcampaign;
            $regevent->UtmTerm = $utmterm;
            $regevent->save();
        }
        else{
            $eventdescriptor = [];
            $eventdescriptor["EventTime"] = "2015-10-01 00:00:00";
            $eventdescriptor["Type"] = "register";
            $eventdescriptor["Subject"] = $id;
            $eventdescriptor["Object"] = $id;
            $eventdescriptor["UtmSource"] = $utmsource;
            $eventdescriptor["UtmCampaign"] = $utmcampaign;
            $eventdescriptor["UtmTerm"] = $utmterm;
            $this->writetobase($eventdescriptor);
        }
        $uinfo = $this->pixie->orm->get('userinfo')->where("UserID", $id)->find();
        if($uinfo->loaded()){
            $uinfo->Info = $userinfo;
            $uinfo->save();
        }
        else{
            $uinfo = $this->pixie->orm->get('userinfo');
            $uinfo->Info = $userinfo;
            $uinfo->UserID = $id;
            $uinfo->save();
        }
        
        if($this->request->post('chkemail')=="true"){
            $user->MailTo="1";
        }
        else{
            $user->MailTo="0";
        }
        if($this->request->post('chkcall')=="true"){
            $user->CallTo="1";
        }
        else{
            $user->CallTo="0";
        }
        $newuser=$this->pixie->orm->get('user')->where("Login", $this->request->post('username'))->find();
        if(!$newuser->loaded()){
            $user->Login = $this->request->post('username');
        }
        $user->MaxSpeed = $this->request->post('maxspeed');
        $user->DistrCount = $this->request->post('distribcount');
        $user->save();
        $this->event($id, $this->pixie->auth->user()->id, "editinfo", $id);
        $this->response->body = "Обновлено";
        $this->execute = false;
    }
    
    public function action_loademails() {
        if(!$this->logged_in("Admin"))
            return;
        if (ob_get_level()) {
                ob_end_clean();
        }
        if(isset($_SERVER['HTTP_USER_AGENT']) and strpos($_SERVER['HTTP_USER_AGENT'],'MSIE'))
            Header('Content-Type: application/force-download');
        else{
            Header('Content-Type: application/octet-stream');
            Header('Accept-Ranges: bytes');
            Header('Content-Length: '.(100000));
            Header('Content-disposition: attachment; filename=emails.txt');
            $users = $this->pixie->orm->get("user")->where('MailTo', 1)->find_all();
            foreach($users as $user){
                print($user->Login . "\r\n");
            }       
        }
        return $this->redirect('/su/users');
    }
    
    public function action_loadphones() {
        if(!$this->logged_in("Admin"))
            return;
        if (ob_get_level()) {
                ob_end_clean();
        }
        if(isset($_SERVER['HTTP_USER_AGENT']) and strpos($_SERVER['HTTP_USER_AGENT'],'MSIE'))
            Header('Content-Type: application/force-download');
        else{
            Header('Content-Type: application/octet-stream');
            Header('Accept-Ranges: bytes');
            Header('Content-Length: '.(100000));
            Header('Content-disposition: attachment; filename=phones.txt');
            $users = $this->pixie->orm->get("user")->where('CallTo', 1)->find_all();
            foreach($users as $user){
                print($user->UserPhone . "\r\n");
            }       
        }
        return $this->redirect('/su/users');
    }
    
    public function action_editkey() {
        if(!$this->logged_in("Admin"))
            return;
        $id = $this->request->post('userid');
        $apikey = $this->pixie->orm->get("uniqueid")->where('UserID', $id)->where('action', 'api')->find();
        if(!$apikey->loaded()){
            $apikey = $this->pixie->orm->get("uniqueid");
            $apikey->action = "api";
            $apikey->UserID = $id;
        }
        $uuid=$this->generateRandomSelection(0,30,64);
        $uuid=implode($uuid);
        $uuid=substr($uuid,1,32);
        $apikey->uuid = $uuid;
        $apikey->save();
        $this->event($id, $this->pixie->auth->user()->id, "newapikey", $id);
        $this->response->body = $apikey->uuid;
        $this->execute = false;
    }
    
    public function action_clientinfo() {
        if(!$this->logged_in("Admin"))
            return;
        $currole = $this->pixie->auth->user()->UserType;
        $this->view->currole = $currole;
        $this->view->speeds = $this->pixie->orm->get('speed')->find_all();
        $id = $this->request->param('id');
        $this->view->ty = "su";
        $this->view->user = $this->pixie->orm->get('user')->where('id', $id)->find();
        $this->view->cost = $this->pixie->orm->get('cost')->where('id', $this->view->user->costgroup)->find();
        $this->view->refmail="";
        if($this->view->user->RefID!=NULL){
            $refuser=$this->pixie->orm->get('user')->where('id', $this->view->user->RefID)->find();
            if($refuser->loaded()){
               $this->view->refmail = $refuser->Login;
            }
        }
        if($this->view->user->UserType=="User"){
            $this->view->distrs = $this->pixie->orm->get('distrib')->where('UserID', $id)->count_all();
        }
        else if($this->view->user->UserType=="Partner"){
            $dopdir = '/var/www/html/pixiecaller/web';
            $usrs = $this->pixie->orm->get('user')->where('Partner', $this->view->user->id)->find_all()->as_array();
            $uscnt = $this->pixie->orm->get('user')->where('Partner', $this->view->user->id)->count_all();
            if($uscnt>0){
                $uss = array_map(function ($o) {
                    return $o->id;
                }, $usrs);
                $usrsStr = '(' . implode(',', $uss) . ')';
                $this->view->distrs=$this->pixie->orm->get('distrib')->where('UserID',  'IN', $this->pixie->db->expr($usrsStr))->count_all();
            }
            else{
                $this->view->distrs=0;
            }
            if($this->view->user->Contacts!=NULL){
                $contact = $this->pixie->orm->get("contact")->where("id", $this->view->user->Contacts)->find();
            }
            $this->view->wAudio = "";
            $this->view->tAudio = "";
            $this->view->check=false;
            $admin = $this->pixie->auth->user();
            $contact2 = $this->pixie->orm->get("contact")->where("id", $admin->Contacts)->find();
            if($this->view->user->Contacts!=NULL and $contact->loaded()){
                $this->view->contact = $contact;
                if($contact->WelcomeAudio==""){
                    if($admin->Contacts!=NULL and $contact2->loaded()){
                        if($contact->WelcomeAudio=="None"){
                            $this->view->check=true;
                        }
                        else{
                            $this->view->wAudio = str_replace ($dopdir, "", $contact2->WelcomeAudio . "s.wav");
                        }
                    }
                }
                else{
                    if($contact->WelcomeAudio=="None"){
                        $this->view->check=true;
                    }
                    else{
                        $this->view->wAudio = str_replace ($dopdir, "", $contact->WelcomeAudio . "s.wav");
                    }
                }
                if($contact->TestAudio==""){
                    if($admin->Contacts!=NULL and $contact2->loaded()){
                        $this->view->tAudio = str_replace ($dopdir, "", $contact2->TestAudio . "s.wav");
                    }
                }
                else{
                    $this->view->tAudio = str_replace ($dopdir, "", $contact->TestAudio . "s.wav");
                }
            }
        }
        $this->view->partner = $this->pixie->orm->get('user')->where('id', $this->view->user->Partner)->find();
        $this->view->partners = $this->pixie->orm->get('user')->where('UserType', 'Partner')->where('id', "!=", $this->view->user->Partner)->find_all()->as_array();
        $manager = 0;
        if(is_null ($this->view->user->Manager)){
            $manager=0;
        }
        else{
            $manager=$this->view->user->Manager;
        }
        $this->view->managers = $this->pixie->orm->get('user')->where('UserType', 'Manager')->where('Partner', $this->view->partner->id)->where('id', "!=",  $manager)->find_all()->as_array();
        $this->view->userinfo = "";
        $this->view->regdate = "";
        $this->view->usource = "";
        $this->view->ucampaign = "";
        $this->view->uterm = "";
        $this->view->apikey = "";
        if($this->view->user->MailTo==1)
            $this->view->checkedemail="checked";
        else
            $this->view->checkedemail="";
        if($this->view->user->CallTo==1)
            $this->view->checkedcall="checked";
        else
            $this->view->checkedcall="";
        $uinfo = $this->pixie->orm->get('userinfo')->where("UserID", $id)->find();
        $regevent = $this->pixie->orm->get('event')->where("Type", 'like', '%register%')->where("Subject", $id)->find();
        $api = $this->pixie->orm->get("uniqueid")->where('UserID', $id)->where('action', 'api')->find();
        if($api->loaded()){
            $this->view->apikey = $api->uuid;
        }
        if($uinfo->loaded()){
            $this->view->userinfo = $uinfo->Info;
        }
        if($regevent->loaded()){
            $this->view->regdate = $regevent->EventTime;
            $this->view->usource = $regevent->UtmSource;
            $this->view->ucampaign = $regevent->UtmCampaign;
            $this->view->uterm = $regevent->UtmTerm;
        }
        if($this->request->method == 'POST'){
            $type = $this->request->post('eventtype');
            $evs = $this->pixie->orm->get('event')->where("Type", 'like', '%' . $type . '%')->where('Subject', $id)->where('or', array('Object', $id))->where("Type", 'like', '%' . $type . '%')->order_by('id', 'desc')->limit(1000)->find_all()->as_array();
            $this->view->events = array_map(function ($o) {
                $subj = $this->pixie->orm->get('user')->where('id', $o->Subject)->find();
                $obj = $this->pixie->orm->get('user')->where('id', $o->Object)->find();
                if($subj->loaded() and $obj->loaded())
                return [$o->EventTime, $o->Type, $subj->Login, $obj->Login, $o->Sum . "; " . $o->Data . "; " . $o->RefID . "; " . $o->EnterPage . "; " . $o->FromPage . "; " . $o->UtmSource . "; " . $o->UtmCampaign . "; " . $o->UtmTerm];
            }, $evs);
        }
        else{
            $evs = $this->pixie->orm->get('event')->where('Subject', $id)->where('or', array('Object', $id))->order_by('id', 'desc')->limit(1000)->find_all()->as_array();
            $this->view->events = array_map(function ($o) {
                $subj = $this->pixie->orm->get('user')->where('id', $o->Subject)->find();
                $obj = $this->pixie->orm->get('user')->where('id', $o->Object)->find();
                if($subj->loaded() and $obj->loaded())
                return [$o->EventTime, $o->Type, $subj->Login, $obj->Login, $o->Sum . "; " . $o->Data . "; " . $o->RefID . "; " . $o->EnterPage . "; " . $o->FromPage . "; " . $o->UtmSource . "; " . $o->UtmCampaign . "; " . $o->UtmTerm];
            }, $evs);
        }
        $this->view->subview = 'clientinfo';
    }
    
    public function action_moderate() {
        if(!$this->logged_in("Admin"))
            return;

        $id = $this->request->param('id');
        $user = $this->pixie->orm->get('user')->where('id', $id)->find();
        if ($user->Moderate == 1){
            $user->Moderate = 0;
        }
        else{
            $user->Moderate = 1;
        }
        $user->save();
        return $this->redirect('/su/users');
    }
    
    public function action_moderaterecord() {
        if(!$this->logged_in("Admin"))
            return;

        $id = $this->request->param('id');
        $record = $this->pixie->orm->get('record')->where('id', $id)->find();
        if ($record->moderate == 1){
            $record->moderate = 0;
        }
        else{
            $record->moderate = 1;
        }
        $record->save();
        return $this->redirect('/su/record');
    }


    public function action_index() {
        if(!$this->logged_in("Admin"))
            return;
    }
    
    public function action_record() {
        if(!$this->logged_in('Admin'))
            return;
        $dopdir = '/var/www/html/pixiecaller/web';
        $this->view->recordname = date_format(date_create(), 'Y-m-d_H-i-s');
        $today = date_format(date_create(), 'Y-m-d 00:00:00');
        $cntr = $this->pixie->orm->get('event')->where("Type", "recordcreate")->where("EventTime", '>', $today)->where("Subject", $this->pixie->auth->user()->id)->count_all();
        $this->view->recordcount = $cntr;
        $uid = $this->request->param('id');
        if ($this->request->method == 'POST') {
            
            $record = $this->pixie->orm->get('record');
            $record->save();
            $uploaddir = '/var/www/html/pixiecaller/web/assets/audio/uploads/';
            $userdir = $uploaddir . $this->pixie->auth->user()->Login  . "/";
            if( ! is_dir( $userdir ) ) mkdir( $userdir );
            $recordsdir = $userdir  . "records/";
            if( ! is_dir( $recordsdir ) ) mkdir( $recordsdir );
            $sourcesdir = $recordsdir  . "sources/";
            if( ! is_dir( $sourcesdir ) ) mkdir( $sourcesdir );
            
            $record->name = $this->request->post('recordname');
            if($this->request->post('recordtype1') == "text"){
                $text = $this->request->post('recordText');
                $record->textsource = $text;
                shell_exec("wget -U 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5' 'https://tts.voicetech.yandex.net/generate?text=\"" . $text . "\"&format=wav&lang=ru-RU&emotion=good&speaker=jane&key=b3a7cba8-c45d-465b-8e9a-adaccff0a70c'  -O " . $sourcesdir . "record" . $record->id . ".wav");
                $record->path = $recordsdir . "record" . $record->id;
            }
            else{
                if(array_key_exists('recordFile', $_FILES)){
                    $file = $_FILES['recordFile'];
                    $parts = explode(".", $file['name']);
                    $ext = $parts[count($parts)-1];
                    if( move_uploaded_file( $file['tmp_name'], $sourcesdir . "record" . $record->id . "." . $ext ) ){
                        $files[] = realpath( $sourcesdir . $file['name'] );
                        $record->path = $recordsdir . "record" . $record->id;
                        if($ext == "mp3"){
                            shell_exec("mpg123 -w '". $sourcesdir . "record" . $record->id . ".wav' " . $sourcesdir . "record" . $record->id . ".mp3");
                        }
                    }
                }
            }
            shell_exec("sox -t wav '". $sourcesdir . "record" . $record->id . ".wav' -r 8000 -c1 -t gsm '" . $sourcesdir . "record" . $record->id . ".gsm'");
            $record->status = "Moderating";
            $record->userid = $this->pixie->auth->user()->id;
            $record->save();
            $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "recordcreate", $record->id);
            return $this->redirect('/su/record');
        }
        $this->view->statuses = array("Moderating" => "На модерации", "Ready" => "Доступен", "Refused" => "Отклонен");
        if ($this->request->param('id') != ""){
            $this->view->records = $this->pixie->orm->get('record')->where('status', '!=', 'Deleted')->where('userid', $uid)->order_by('id', 'desc')->find_all()->as_array();
        }
        else{
            $this->view->records = $this->pixie->orm->get('record')->where('status', '!=', 'Deleted')->order_by('id', 'desc')->find_all()->as_array();
            $users = array();
            foreach($this->view->records as $rec){
                $usr = $this->pixie->orm->get('user')->where('id', $rec->userid)->find();
                if($usr->loaded()){
                    $users[$rec->userid] = $usr->Login;
                }
            }
            $this->view->users = $users;
        }

        foreach($this->view->records as $rec1){
            $path = $rec1->path;
            $webdir = str_replace($dopdir, "", $path);
            $webarray = explode('/', $webdir);
            $name = $webarray[count($webarray)-1];
            unset($webarray[count($webarray)-1]);
            $srcpath = implode('/', $webarray) . '/sources/' . $name . ".wav";
            $stats[$rec1->id] = $srcpath;
        }
        if (!empty($stats))
            $this->view->paths = $stats;
        $this->view->subview = 'record';
    }
    
    public function action_delrecord() {
        $id = $this->request->param('id');
        $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "recorddelete", $id);
        $record = $this->pixie->orm->get('record')->where('id', $id)->find();
        $record->status = "Deleted";
        $record->save();
        return $this->redirect('/su/record');
    }

    public function action_commentrecord(){
        if(!$this->logged_in("Admin"))
            return;
        if($this->request->method == 'POST'){
            $rid = $this->request->post('recordid');
            $text = $this->request->post('text');
            $record = $this->pixie->orm->get('record')->where('id', $rid)->find();
            if($record->loaded()){
                $record->admincomment = $text;
                $record->save();
            }
            $this->execute = false;
        }
    }
    
    public function action_acceptrecord() {
        if($this->pixie->auth->user() != null and $this->pixie->auth->user()->UserType=="Admin"){
            if(!$this->logged_in("Admin"))
                return;
            $id = $this->request->param('id');
            $record = $this->pixie->orm->get('record', $id);
            if($record->status=="Moderating"){
                $record->status="Ready";
                $record->save();
                $this->event($record->userid, $this->pixie->auth->user()->id, "moderaterecordok", $id);
            }
            return $this->redirect('/su/record');
        }
        else{
            $uuid = $this->request->get("uid");
            $uid = $this->pixie->orm->get("uniqueid")->where('uuid', $uuid)->where('action', 'moderate')->find();
            if($uid->loaded()){
                $id = $this->request->param('id');
                $record = $this->pixie->orm->get('record', $id);
                if($record->status=="Moderating"){
                    $record->status="Ready";
                    $record->save();
                    $this->event($record->userid, $this->pixie->orm->get('user')->where('UserType', 'Admin')->find()->id, "moderaterecordok", $id);
                    echo "<script>alert('Модерация записи № " . $id . " прошла успешно!');</script>";
                }
                else{
                    echo "<script>alert('Запись № " . $id . " уже была отмодерирована!');</script>";
                }
            }
            else{
                echo "<script>alert('Неверный запрос!');</script>";
            }
        }
    }
    
    public function action_declinerecord() {
        if($this->pixie->auth->user() != null and $this->pixie->auth->user()->UserType=="Admin"){
            if(!$this->logged_in("Admin"))
                return;
            $id = $this->request->param('id');
            $record = $this->pixie->orm->get('record', $id);
            if($record->status=="Moderating"){
                $record->status="Canceled";
                $record->save();
                $this->event($record->userid, $this->pixie->auth->user()->id, "moderaterecordfail", $id);
            }
            return $this->redirect('/su/record');
        }
        else{
            $uuid = $this->request->get("uid");
            $uid = $this->pixie->orm->get("uniqueid")->where('uuid', $uuid)->where('action', 'moderate')->find();
            if($uid->loaded()){
                $id = $this->request->param('id');
                $record = $this->pixie->orm->get('record', $id);
                if($record->status=="Moderating"){
                    $record->status="Canceled";
                    $record->save();
                    $this->event($record->userid, $this->pixie->orm->get('user')->where('UserType', 'Admin')->find()->id, "moderaterecordfail", $id);
                    echo "<script>alert('Модерация записи № " . $id . " прошла успешно!');</script>";
                }
                else{
                    echo "<script>alert('Запись № " . $id . " уже была отмодерирована!');</script>";
                }
            }
            else{
                echo "<script>alert('Неверный запрос!');</script>";
            }
        }
    }
    
    public function action_sendmessage() {
        $fromid = 18;
        $toid = 19;
        $message = "Hello!";
        $type = "email";
        $this->trigger($toid, $fromid, $message, $type);
    }
    
    function curl_file_create($filename, $mimetype = '', $postname = '') {
        return "@$filename;filename="
            . ($postname ?: basename($filename))
            . ($mimetype ? ";type=$mimetype" : '');
    }
        
    function generateRandomSelection($min, $max, $count)
    {
        $result=array();
        if($min>$max) return $result;
        $count=min(max($count,0),$max-$min+1);
        while(count($result)<$count) {
            $value=rand($min,$max-count($result));
            foreach($result as $used) if($used<=$value) $value++; else break;
            $result[]=dechex($value);
            sort($result);
        }
        shuffle($result);
        return $result;
    }
    
    function recognizer($file, $key) {
        $uuid=$this->generateRandomSelection(0,30,64);
        $uuid=implode($uuid);    $uuid=substr($uuid,1,32);
        $curl = curl_init();
        $url = 'https://asr.yandex.net/asr_xml?'.http_build_query(array(
            'key'=>$key,
            'uuid' => $uuid,
            'topic' => 'notes',
            'lang'=>'ru-RU'
        ));
        curl_setopt($curl, CURLOPT_URL, $url);
        $data=file_get_contents(realpath($file));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: audio/x-wav'));
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        $response = curl_exec($curl);
        $err = curl_errno($curl);
        curl_close($curl);
        if ($err)
            throw new exception("curl err $err");
        echo $response;
    }
    
    public function action_recognize(){
        if(!$this->logged_in("Admin"))
            return;
        if($this->request->method == 'POST'){
            $file = $_FILES['speech'];
            $parts = explode(".", $file['name']);
            $ext = $parts[count($parts)-1];
            move_uploaded_file( $file['tmp_name'], "/var/www/uploads/" . $file['name'] );
            // $parts = explode(".", $file['name']);
            // $ext = $parts[count($parts)-1];
            // if( move_uploaded_file( $file['tmp_name'], $distribdir . "caller1." . $ext ) ){
                // $files[] = realpath( $distribdir . $file['name'] );
                // $distrib->WelcomeRecord = $distribdir . "caller1";
                // if($ext == "mp3"){
                    // shell_exec("mpg123 -w '". $distribdir . "caller1.wav' " . $distribdir . "caller1.mp3");
                // }
            // }
            $key = "b3a7cba8-c45d-465b-8e9a-adaccff0a70c";
            $this->recognizer("/var/www/uploads/" . $file['name'], $key);
            print("done\n");
        }
        $this->view->subview = 'recognize';
    }
    
    public function action_users() {
        if(!$this->logged_in("Admin"))
            return;
        $this->view->colors = array(0 => "#C0C0C0", 1 => "#008000", 2 => "#FF0000", 3 => "#000000", 4 => "#FF8C00");
        $type = -1;
        if(array_key_exists("clienttype",$_COOKIE))
            $type = $_COOKIE["clienttype"];
        $this->view->cltype=$type;
        if(isset($_GET['src'])){
            if($_GET['src']=="-"){
                $src="";
                $evs1 = $this->pixie->orm->get('event')->where('Type', 'like', "%register%")->where('UtmSource', '!=', "")->find_all()->as_array();
                $ev = array_map(function ($o) {
                    return $o->id;
                }, $evs1);
                $evStr = '(' . implode(',', $ev) . ')';
                $evs = $this->pixie->orm->get('event')->where('id', 'NOT IN', $this->pixie->db->expr($evStr))->where('Type', 'like', "%register%")->find_all()->as_array();
            }
            else{
                $src=$_GET['src'];
                $evs = $this->pixie->orm->get('event')->where('Type', 'like', "%register%")->where('UtmSource', 'like', $src)->find_all()->as_array();
            }
            $usrs = array_map(function ($o) {
                return $o->Subject;
            }, $evs);
            $usrsStr = '(' . implode(',', $usrs) . ')';
            if($type!=-1)
                $this->view->users = $this->pixie->orm->get('user')->where("UserType", "User")->where('id', 'IN', $this->pixie->db->expr($usrsStr))->where("ClientStatus", $type)->order_by('id', 'desc')->find_all()->as_array();
            else
                $this->view->users = $this->pixie->orm->get('user')->where("UserType", "User")->where('id', 'IN', $this->pixie->db->expr($usrsStr))->order_by('id', 'desc')->find_all()->as_array();
        }
        else{
            if($type!=-1)
                $this->view->users = $this->pixie->orm->get('user')->where("UserType", "User")->where("ClientStatus", $type)->order_by("id", 'desc')->find_all()->as_array();
            else
                $this->view->users = $this->pixie->orm->get('user')->where("UserType", "User")->order_by("id", 'desc')->find_all()->as_array(); 
        }
        $costs1 = array(); 
        $distrs1 = array(); 
        $this->view->types = $this->pixie->orm->get('paytype')->where('auto', 0)->find_all()->as_array();
        foreach($this->view->users as $user1){
            $cost = $this->pixie->orm->get('cost')->where('id', $user1->costgroup)->find();
            $costs1[$user1->id]=(string)($cost->recall / 100) . "/" . (string)($cost->ordinary / 100) . "/" . (string)($cost->dial / 100);
            $distrs1[$user1->id]=$this->pixie->orm->get('distrib')->where('UserID', $user1->id)->count_all();
        }
        $this->view->costs = $costs1;
        $this->view->distrs = $distrs1;
        $this->view->subview = 'users';
    }
    
    public function action_createpartner() {
        if(!$this->logged_in("Admin"))
            return;
        if($this->request->method == 'POST'){
            $uid = $this->request->post('userid');
            $user = $this->pixie->orm->get('user')->where('id', $uid)->find();
            if($user->UserType == "User"){
                $this->event($user->id, $this->pixie->auth->user()->id, "rolechange", $user->UserType);
                $this->event($user->id, $this->pixie->auth->user()->id, "parentchange", $user->Partner);
                $user->UserType="Partner";
                $user->Partner=$this->pixie->auth->user()->id;
                $contact = $this->pixie->orm->get("contact");
                $contact->UserPhone = $user->UserPhone;
                $contact->UserEmail = $user->Login;
                $contact->save();
                $user->Contacts = $contact->id;
                $user->save();
                $dom = $this->pixie->orm->get("domain");
                $dom->name = $this->request->post('domainname');
                $dom->landos = $this->request->post('landos');
                $dom->UserID = $user->id;
                $dom->save();
                $defval = $this->pixie->orm->get("defaultvalue");
                $defval->costgroup = $user->costgroup;
                $defval->startbalance = 1000;
                $defval->domainID = $dom->id;
                $defval->save();
                $this->response->body = "Партнер создан";
                $this->execute = false;
            }
            else{
                $this->response->body = "Тип пользователя не позволяет преобразовать его в партнера";
                $this->execute = false;
            }
        }
    }
    
    public function action_gethistory() {
        if(!$this->logged_in("Admin"))
            return;
        if($this->request->method == 'POST'){
            $uid = $this->request->post('userid');
            $pays = $this->pixie->orm->get('payment')->where('userID', $uid)->order_by("paydate", "desc")->limit(4)->find_all()->as_array();
            $paysStr = "";
            if($this->pixie->orm->get('payment')->where('userID', $uid)->count_all()>0){
                $payments = array_map(function ($o) {
                    return $o->paydate . "," . $this->pixie->orm->get('paytype')->where('name', $o->type)->find()->description . "," . $o->value;
                }, $pays);
                $paysStr = implode(';', $payments);
            }
            $this->response->body = $paysStr;
            $this->execute = false;
        }
    }
    
    public function action_changebalance() {
        if(!$this->logged_in("Admin"))
            return;
        if($this->request->method == 'POST'){
            date_default_timezone_set("Europe/Moscow");
			$time_now = date_format(date_create(), 'Y-m-d H:i:s');
            $uid = $this->request->post('userid');
            $sum = $this->request->post('sum') * 100;
            $type = $this->request->post('type');
            $user = $this->pixie->orm->get('user')->where('id', $uid)->find();
            $partner = $this->pixie->orm->get('user')->where('id', $user->Partner)->find();
            $res = $this->pay($user->id, $partner->id, $sum, $time_now, $type);
            $this->response->body = "Платеж совершен.";
            $this->execute = false;
        }
    }
    
    public function action_changetariff() {
        if(!$this->logged_in("Admin"))
            return;
        if($this->request->method == 'POST'){
            $recall = $this->request->post('recall');
            $dial = $this->request->post('dial');
            $ordinary = $this->request->post('ordinary');
            $uid = $this->request->post('userid');
            $user = $this->pixie->orm->get('user')->where('id', $uid)->find();
            $costs1 = $this->pixie->orm->get('cost')->where('recall', $recall)->where('dial', $dial)->where('ordinary', $ordinary)->find();
            if($costs1->loaded()){
                $user->costgroup=$costs1->id;
                $user->save();
                $this->event($user->id, $this->pixie->auth->user()->id, "changetarif", $user->costgroup);
                $this->response->body = "Тариф назначен";
                $this->execute = false;
            }
            else{
                $costs2 = $this->pixie->orm->get('cost');
                $costs2->recall = $recall;
                $costs2->dial = $dial;
                $costs2->ordinary = $ordinary;
                $costs2->save();
                $user->costgroup=$costs2->id;
                $user->save();
                $this->event($user->id, $this->pixie->auth->user()->id, "changetarif", $user->costgroup);
                $this->response->body = "Новый тариф назначен";
                $this->execute = false;
            }
        }
    }
    
    public function action_partners() {
        if(!$this->logged_in("Admin"))
            return;
        $current_page = $this->request->param('page');
        //$current_page=1;
        $this->view->users = $this->pixie->orm->get('user')->where('UserType', "Partner")->find_all()->as_array();
        $users = $this->pixie->orm->get('user')->where('UserType', "Partner");
        $pager = $this->pixie->paginate->orm($users, $current_page, $this->item_per_page);
        $pager->set_url_route('su/partners');
        $this->view->current_page = $current_page;
        $this->view->item_per_page = $this->item_per_page;
        $this->view->pager = $pager;
        $costs1 = array(); 
        $distrs1 = array();
        $this->view->types = $this->pixie->orm->get('paytype')->where('auto', 0)->find_all()->as_array();
        foreach($this->view->users as $user1){
            $cost = $this->pixie->orm->get('cost')->where('id', $user1->costgroup)->find();
            $usrs = $this->pixie->orm->get('user')->where('Partner', $user1->id)->find_all()->as_array();
            $uscnt = $this->pixie->orm->get('user')->where('Partner', $user1->id)->count_all();
            if($uscnt>0){
                $uss = array_map(function ($o) {
                    return $o->id;
                }, $usrs);
                $usrsStr = '(' . implode(',', $uss) . ')';
                $distrs1[$user1->id]=$this->pixie->orm->get('distrib')->where('UserID',  'IN', $this->pixie->db->expr($usrsStr))->count_all();
            }
            else{
                $distrs1[$user1->id]=0;
            }
            $costs1[$user1->id]=(string)$cost->recall . "/" . (string)$cost->ordinary . "/" . (string)$cost->dial;
            
        }
        $this->view->costs = $costs1;
        $this->view->distrs = $distrs1;
        $this->view->subview = 'partners';
    }
    
    public function action_lines() {
        if(!$this->logged_in("Admin"))
            return;
        //$connline = explode(':', $this->pixie->config->get('db.default.connection'))[1];
        //$connarr = explode(';', $connline);
        //$mysqli = mysqli_connect(explode("=", $connarr[0])[1], $this->pixie->config->get('db.default.user'), $this->pixie->config->get('db.default.password'), explode("=", $connarr[1])[1]);
        //$pgcon = pg_connect("host=127.0.0.1 port=6432 dbname=pixiecaller user=pgpixie password=PgPixiePassword");
        //$this->view->connections = 0;
        //$sql1 = 'show status like "Threads_connected";';
        //if($result1 = pg_query($mysqli, $sql1)){
            //$row = pg_fetch_row($result1);
            //$this->view->connections = (int)$row[1];
            //pg_free_result($result1);
        //}
        //pg_close($pgcon);
        $this->view->lines = $this->pixie->orm->get('outline')->order_by("callorder", "asc")->find_all()->as_array();
        $this->view->countsfree = $this->pixie->orm->get('outline')->where("isenabled", 1)->where("isbusy", 0)->count_all();
        $this->view->counts = $this->pixie->orm->get('outline')->where("isenabled", 1)->count_all();
        $this->view->numbers = $this->pixie->orm->get('outnumber')->where('Status', '!=', 'Disabled')->order_by("Status", "desc")->find_all()->as_array();
        $this->view->subview = 'lines';
    }
    
    public function action_freeline() {
        if(!$this->logged_in("Admin"))
            return;
        $id = $this->request->param('id');
        $line = $this->pixie->orm->get('outline')->where('id', $id)->find();
        if($line->isbusy==1){
            $line->isbusy=0;
            $line->save();
            $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "freeline", $id);
        }
        return $this->redirect('/su/lines');
    }
    
    public function action_freenumber() {
        if(!$this->logged_in("Admin"))
            return;
        $id = $this->request->param('id');
        $num = $this->pixie->orm->get('outnumber')->where('id', $id)->find();
        if($num->Status=="Busy" or $num->Status=="ForTest"){
            $num->Status="Available";
            $num->save();
            $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "freenumber", $id);
        }
        return $this->redirect('/su/lines');
    }
    
    public function action_events() {
        if(!$this->logged_in("Admin"))
            return;
        $current_page = $this->request->param('page');
        $limit1 = ($current_page-1)*$this->item_per_page;
        $limit2 = $this->item_per_page;
        $type = "";
        $subject = "";
        if(array_key_exists("eventtype",$_COOKIE))
            $type = $_COOKIE["eventtype"];
        if(array_key_exists("eventsubj",$_COOKIE))
            $subject = $_COOKIE["eventsubj"];
        if($this->request->method == 'POST'){
            $type = $this->request->post('eventtype');
            $subject = $this->request->post('eventsubj');
            setcookie ("eventtype", $type);
            setcookie ("eventsubj", $subject);
        }
        $this->view->eventtype = $type;
        $this->view->eventsubj = $subject;
        $s = $this->pixie->orm->get('user')->where('Login', $subject)->find();
        if($s->loaded()){
            $evs = $this->pixie->orm->get('event')->where("Type", 'like', '%' . $type . '%')->where('Subject', $s->id)->order_by('id', 'desc')->limit($limit2)->offset($limit1)->find_all()->as_array();
            $this->view->num_pages = ceil($this->pixie->orm->get('event')->where("Type", 'like', '%' . $type . '%')->where('Subject', $s->id)->count_all() / $this->item_per_page);
        }
        else{
            $evs = $this->pixie->orm->get('event')->where("Type", 'like', '%' . $type . '%')->order_by('id', 'desc')->limit($limit2)->offset($limit1)->find_all()->as_array();
            $this->view->num_pages = ceil($this->pixie->orm->get('event')->where("Type", 'like', '%' . $type . '%')->count_all() / $this->item_per_page);
        }
        $events = array_map(function ($o) {
            $subj = $this->pixie->orm->get('user')->where('id', $o->Subject)->find();
            $obj = $this->pixie->orm->get('user')->where('id', $o->Object)->find();
            if($subj->loaded() and $obj->loaded())
            return [$o->EventTime, $o->Type, $subj->Login, $obj->Login, $o->Sum . "; " . $o->Data . "; " . $o->RefID . "; " . $o->EnterPage . "; " . $o->FromPage . "; " . $o->UtmSource . "; " . $o->UtmCampaign . "; " . $o->UtmTerm, "/su/clientinfo/" . $o->Subject, "/su/clientinfo/" . $o->Object];
        }, $evs);
        // }
        // else{
            // $evs = $this->pixie->orm->get('event')->order_by('id', 'desc')->limit($limit2)->offset($limit1)->find_all()->as_array();
            // $events = array_map(function ($o) {
                // $subj = $this->pixie->orm->get('user')->where('id', $o->Subject)->find();
                // $obj = $this->pixie->orm->get('user')->where('id', $o->Object)->find();
                // if($subj->loaded() and $obj->loaded())
                // return [$o->EventTime, $o->Type, $subj->Login, $obj->Login, $o->Sum . "; " . $o->Data . "; " . $o->RefID . "; " . $o->EnterPage . "; " . $o->FromPage . "; " . $o->UtmSource . "; " . $o->UtmCampaign . "; " . $o->UtmTerm, "/su/clientinfo/" . $o->Subject, "/su/clientinfo/" . $o->Object];
            // }, $evs);
            // $this->view->num_pages = ceil($this->pixie->orm->get('event')->count_all() / $this->item_per_page);
        // }
        $this->view->url = "/su/events";
        $this->view->events = $events;
        $this->view->current_page = $current_page;
        $this->view->item_per_page = $this->item_per_page;
        $this->view->subview = 'events';
    }
    
    public function action_distribs() {
        date_default_timezone_set("Europe/Moscow");
        $this->view->time_now = date_format(date_create(), 'Y-m-d H:i:s');
        $this->view->statuses = array("Moderating" => "На модерации", "Started" => "В процессе", "Ready" => "Обработка", "Finished" => "Закончена", "Paused" => "На паузе", "Pausing" => "Ставится на паузу", "Stopped" => "Остановлена", "Stopping" => "Останавливается", "Canceled" => "Отменена", "Starting" => "Начинается");
        $this->view->types = array("recall" => "Дозвон - сброс", "ordinary" => "Обычная");
        $this->view->control = "su";
        $current_page = $this->request->param('page');
        if(!$this->logged_in("Admin"))
            return;
        $usrs = $this->pixie->orm->get('user')->find_all()->as_array();
        $uss = array_map(function ($o) {
            return $o->id;
        }, $usrs);
        $usrsStr = '(' . implode(',', $uss) . ')';
        $speeds = [];
        $speedall = $this->pixie->orm->get('speed')->find_all();
        foreach($speedall as $speed){
            $speeds[$speed->id] = $speed->Value;
        }
        $this->view->speeds = $speeds;
        //$this->view->distribs = $this->pixie->orm->get('distrib')->where('UserID', 'IN', $this->pixie->db->expr($usrsStr))->order_by('CreateTime','desc')->limit(500)->find_all();
        $distribs = $this->pixie->orm->get('distrib')->where('UserID', 'IN', $this->pixie->db->expr($usrsStr))->order_by('CreateTime','desc');
        $connline = explode(':', $this->pixie->config->get('db.default.connection'))[1];
        $connarr = explode(';', $connline);
        //$mysqli = mysqli_connect(explode("=", $connarr[0])[1], $this->pixie->config->get('db.default.user'), $this->pixie->config->get('db.default.password'), explode("=", $connarr[1])[1]);
        $pgcon = pg_connect("host=127.0.0.1 port=6432 dbname=pixiecaller user=pgpixie password=PgPixiePassword");
        $this->view->speedsum = 0;
        $sql1 = 'SELECT SUM("Value") FROM speeds AS db1 JOIN (SELECT "SpeedID" FROM distribs WHERE "Status"= \'Started\') AS db2 ON db1.id=db2."SpeedID";';
        if($result1 = pg_query($pgcon, $sql1)){
            $row = pg_fetch_row($result1);
            $this->view->speedsum = (int)$row[0];
            pg_free_result($result1);
        }
        pg_close($pgcon);
        $pager = $this->pixie->paginate->orm($distribs, $current_page, $this->item_per_page);
        $pager->set_url_route('su/distribs');
        $this->view->pager = $pager;
        $this->view->item_per_page = $this->item_per_page;
        $this->view->current_page = $current_page;
        $this->view->empt=0;
        $this->view->subview = 'distribs';
    }
    
    public function action_submit_distrib() {
        if(!$this->logged_in("Admin"))
            return;
        if($this->request->method == 'POST'){
        }
    }
    
    public function action_commentdistrib() {
        if(!$this->logged_in("Admin"))
            return;
        if($this->request->method == 'POST'){
            $id = $this->request->post('distrid');
            $text = $this->request->post('text');
            $distrib = $this->pixie->orm->get('distrib', $id);
            if($distrib->loaded()){
                $distrib->admincomment = $text;
                $distrib->save();
            }
            $this->execute = false;
        }
    }

    public function action_acceptdistr() {
        if($this->pixie->auth->user() != null and $this->pixie->auth->user()->UserType=="Admin"){
            if(!$this->logged_in("Admin"))
                return;
            $id = $this->request->param('id');
            $distrib = $this->pixie->orm->get('distrib', $id);
            if($distrib->Status=="Moderating"){
                $distrib->Status="Ready";
                $distrib->save();
                $this->event($distrib->UserID, $this->pixie->auth->user()->id, "moderateok", $id);
            }
            return $this->redirect('/su/distribs');
        }
        else{
            $uuid = $this->request->get("uid");
            $uid = $this->pixie->orm->get("uniqueid")->where('uuid', $uuid)->where('action', 'moderate')->find();
            if($uid->loaded()){
                $id = $this->request->param('id');
                $distrib = $this->pixie->orm->get('distrib', $id);
                if($distrib->Status=="Moderating"){
                    $distrib->Status="Ready";
                    $distrib->save();
                    $this->event($distrib->UserID, $this->pixie->orm->get('user')->where('UserType', 'Admin')->find()->id, "moderateok", $id);
                    echo "<script>alert('Модерация рассылки № " . $id . " прошла успешно!');</script>";
                }
                else{
                    echo "<script>alert('Рассылка № " . $id . " уже была отмодерирована!');</script>";
                }
            }
            else{
                echo "<script>alert('Неверный запрос!');</script>";
            }
        }
    }
    
    public function action_declinedistr() {
        if($this->pixie->auth->user() != null and $this->pixie->auth->user()->UserType=="Admin"){
            if(!$this->logged_in("Admin"))
                return;
            $id = $this->request->param('id');
            $distrib = $this->pixie->orm->get('distrib', $id);
            if($distrib->Status=="Moderating"){
                $distrib->Status="Canceled";
                $distrib->save();
                $num = $this->pixie->orm->get('outnumber')->where("DistribID", $distrib->id)->find();
                if($num->loaded()){
                    if($num->Status == "ForRecallBusy"){
                        $num->Status = "ForRecall";
                        $num->save();
                    }
                    if($num->Status == "Busy"){
                        $num->Status = "Available";
                        $num->save();
                    }
                    if($num->Status == "ForTest"){
                        $num->Status = "Available";
                        $num->save();
                    }
                    if($num->Status == "ForTestRec"){
                        $num->Status = "ForRecall";
                        $num->save();
                    }
                }
                $this->event($distrib->UserID, $this->pixie->auth->user()->id, "moderatefail", $id);
            }
            return $this->redirect('/su/distribs');
        }
        else{
            $uuid = $this->request->get("uid");
            $uid = $this->pixie->orm->get("uniqueid")->where('uuid', $uuid)->where('action', 'moderate')->find();
            if($uid->loaded()){
                $id = $this->request->param('id');
                $distrib = $this->pixie->orm->get('distrib', $id);
                if($distrib->Status=="Moderating"){
                    $distrib->Status="Canceled";
                    $distrib->save();
                    $num = $this->pixie->orm->get('outnumber')->where("DistribID", $distrib->id)->find();
                    if($num->loaded()){
                        if($num->Status == "ForRecallBusy"){
                            $num->Status = "ForRecall";
                            $num->save();
                        }
                        if($num->Status == "Busy"){
                            $num->Status = "Available";
                            $num->save();
                        }
                        if($num->Status == "ForTest"){
                            $num->Status = "Available";
                            $num->save();
                        }
                        if($num->Status == "ForTestRec"){
                            $num->Status = "ForRecall";
                            $num->save();
                        }
                    }
                    $this->event($distrib->UserID, $this->pixie->orm->get('user')->where('UserType', 'Admin')->find()->id, "moderatefail", $id);
                    echo "<script>alert('Модерация рассылки № " . $id . " прошла успешно!');</script>";
                }
                else{
                    echo "<script>alert('Рассылка № " . $id . " уже была отмодерирована!');</script>";
                }
            }
            else{
                echo "<script>alert('Неверный запрос!');</script>";
            }
        }
    }
    
    private function loadfile($file){
        if(!$this->logged_in("Admin"))
            return;
        if (file_exists($file)) {
            // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
            // если этого не сделать файл будет читаться в память полностью!
            if (ob_get_level()) {
                ob_end_clean();
            }
            // заставляем браузер показать окно сохранения файла
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            // читаем файл и отправляем его пользователю
            readfile($file);
            exit;
        }
    }
    
    public function action_getsounds() {
        $id = $this->request->post('did');
        $distrib = $this->pixie->orm->get('distrib')->where('id', $id)->find();
        $answer = "";
        $search = "/var/www/html/pixiecaller/web";
        if($distrib->loaded()){
            $fields = [];
            if($distrib->WelcomeRecord != "/var/lib/asterisk/festivalcache/zero"  and $distrib->WelcomeRecord != "none"){
                $r = $distrib->WelcomeRecord;
                $arr = explode('/', str_replace ( $search, "", $r ));
                $file = array_pop($arr);
                $str = implode('/', $arr) . '/sources/' . $file . '.wav';
                $fields[] = '"WelcomeRecord":"' . $str . '"';
            }
            else{
                $fields[] = '"WelcomeRecord":""';
            }
            if($distrib->IVR1 != "/var/lib/asterisk/festivalcache/zero"  and $distrib->IVR1 != "none"){
                $r = $distrib->IVR1;
                $arr = explode('/', str_replace ( $search, "", $r ));
                $file = array_pop($arr);
                $str = implode('/', $arr) . '/sources/' . $file . '.wav';
                $fields[] = '"IVR1":"' . $str . '"';
            }
            else{
                $fields[] = '"IVR1":""';
            }
            if($distrib->IVR2 != "/var/lib/asterisk/festivalcache/zero" and $distrib->IVR2 != "none"){
                $r = $distrib->IVR2;
                $arr = explode('/', str_replace ( $search, "", $r ));
                $file = array_pop($arr);
                $str = implode('/', $arr) . '/sources/' . $file . '.wav';
                $fields[] = '"IVR2":"' . $str  . '"';
            }
            else{
                $fields[] = '"IVR2":""';
            }
            if($distrib->IVR3 != "/var/lib/asterisk/festivalcache/zero"  and $distrib->IVR3 != "none"){
                $r = $distrib->IVR3;
                $arr = explode('/', str_replace ( $search, "", $r ));
                $file = array_pop($arr);
                $str = implode('/', $arr) . '/sources/' . $file . '.wav';
                $fields[] = '"IVR3":"' . $str  . '"';
            }
            else{
                $fields[] = '"IVR3":""';
            }
            if(trim($distrib->SmsText) != ""){
                $r = preg_replace('/[\x00-\x1F\x7F]/', '', $distrib->SmsText);
                $r = str_replace("\"", "'", $r);
                $fields[] = '"SmsText":"' . $r  . '"';
            }
            else{
                $fields[] = '"SmsText":""';
            }
            $str = '{' . implode(',', $fields) . '}';
            $answer = $str;
        }
        $this->response->body = $answer;
        $this->execute = false;
    }
    
    public function action_profile() {
        if(!$this->logged_in("Admin"))
            return;
        $this->view->contact = "None";
        $admin = $this->pixie->orm->get("user")->where("UserType", "Admin")->find();
        $dopdir = '/var/www/html/pixiecaller/web';
        if($this->pixie->auth->user()->Contacts!=NULL){
            $contact1 = $this->pixie->orm->get("contact")->where("id", $this->pixie->auth->user()->Contacts)->find();
        }
        if($admin->Contacts!=NULL){
            $contact2 = $this->pixie->orm->get("contact")->where("id", $admin->Contacts)->find();
        }
        $this->view->wAudio = "";
        $this->view->tAudio = "";
        if($this->pixie->auth->user()->Contacts!=NULL and $contact1->loaded()){
            $this->view->contact = $contact1;
            if($contact1->WelcomeAudio==""){
                if($admin->Contacts!=NULL and $contact2->loaded()){
                    $this->view->wAudio = str_replace ($dopdir, "", $contact2->WelcomeAudio . "s.wav");
                }
            }
            else{
                $this->view->wAudio = str_replace ($dopdir, "", $contact1->WelcomeAudio . "s.wav");
            }
            if($contact1->TestAudio==""){
                if($admin->Contacts!=NULL and $contact2->loaded()){
                    $this->view->tAudio = str_replace ($dopdir, "", $contact2->TestAudio . "s.wav");
                }
            }
            else{
                $this->view->tAudio = str_replace ($dopdir, "", $contact1->TestAudio . "s.wav");
            }
        }
        else{
            if($admin->Contacts!=NULL and $contact2->loaded()){
                $this->view->wAudio = str_replace ($dopdir, "", $contact2->WelcomeAudio . "s.wav");
                $this->view->tAudio = str_replace ($dopdir, "", $contact2->TestAudio . "s.wav");
            }
        }
        
        if ($this->request->method == 'POST') {
            $user = $this->pixie->auth->user();
            //$user->UserName = iconv("WINDOWS-1251", "UTF-8", $this->request->post('username'));
            //$user->UserName = $this->request->post('username');
            $oldjson = "{\"UserName\" : \"" . $user->UserName . "\", \"UserPhone\" : \"" . $user->UserPhone . "\", \"Login\" : \"" . $user->Login . "\"}";
            $user->UserPhone = $this->request->post('userphone');
            $user->Login = $this->request->post('usermail');
            $user->save();
            $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "profilechange", $oldjson);
        }
        $this->view->user = $this->pixie->auth->user();
        $this->view->subview = 'profilea';
    }
    
    public function action_editcontacts() {
        if(!$this->logged_in("Admin"))
            return;
        $cont = "None";
        if($this->pixie->auth->user()->Contacts!=NULL){
            $contact = $this->pixie->orm->get("contact")->where("id", $this->pixie->auth->user()->Contacts)->find();
        }
        if($this->pixie->auth->user()->Contacts!=NULL and $contact->loaded()){
            $cont = $contact;
        }
        if ($this->request->method == 'POST') {
            $user = $this->pixie->auth->user();
            $uploaddir = '/var/www/html/pixiecaller/web/assets/audio/uploads/';
            $userdir = $uploaddir . $this->pixie->auth->user()->Login  . "/";
            if( ! is_dir( $userdir ) ) mkdir( $userdir );
            if($cont!="None"){
                $contact->UserPhone = $this->request->post('phone');
                $contact->UserEmail = $this->request->post('email');
                // $contact->UserName = $this->request->post('name');
                if(array_key_exists('welcome1', $_FILES)){
                    $file = $_FILES['welcome1'];
                    $parts = explode(".", $file['name']);
                    $ext = $parts[count($parts)-1];
                    if( move_uploaded_file( $file['tmp_name'], $userdir . "welcome1s." . $ext ) ){
                        $contact->WelcomeAudio = $userdir . "welcome1";
                        if($ext == "mp3"){
                            shell_exec("mpg123 -w '". $userdir . "welcome1s.wav' " . $userdir . "welcome1s.mp3");
                        }
                        shell_exec("sox -t wav '". $userdir . "welcome1s.wav' -r 8000 -c1 -t gsm '" . $userdir . "welcome1.gsm'");
                        shell_exec("rm -r " . $userdir . "welcome1s.mp3");
                    }
                }
                if(array_key_exists('test', $_FILES)){
                    $file = $_FILES['test'];
                    $parts = explode(".", $file['name']);
                    $ext = $parts[count($parts)-1];
                    if( move_uploaded_file( $file['tmp_name'], $userdir . "test1s." . $ext ) ){
                        $contact->TestAudio = $userdir . "test1";
                        if($ext == "mp3"){
                            shell_exec("mpg123 -w '". $userdir . "test1s.wav' " . $userdir . "test1s.mp3");
                        }
                        shell_exec("sox -t wav '". $userdir . "test1s.wav' -r 8000 -c1 -t gsm '" . $userdir . "test1.gsm'");
                        shell_exec("rm -r " . $userdir . "test1s.mp3");
                    }
                }
                $tagsToReplace = array('&amp;'=>'&', '&lt;'=>'<', '&gt;'=>'>', "\r\n" =>'<br>' );
                $contact->requisites = $this->request->post('requisites');
                foreach($tagsToReplace as $key => $value){
                    $contact->requisites = str_replace($key, $value, $contact->requisites);
                }
                $contact->save();
                $oldjson = "{\"ContactsUserPhone\" : \"" . $contact->UserPhone . "\", \"ContactsUserMail\" : \"" . $contact->UserEmail . "\", \"ContactsUserName\" : \"" . $contact->UserName . "\"}";
                
                
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "profilechange");
            }
            else{
                $contact = $this->pixie->orm->get("contact");
                $contact->UserPhone = $this->request->post('phone');
                $contact->UserEmail = $this->request->post('email');
                // $contact->UserName = $this->request->post('name');
                if(array_key_exists('welcome1', $_FILES)){
                    $file = $_FILES['welcome1'];
                    $parts = explode(".", $file['name']);
                    $ext = $parts[count($parts)-1];
                    if( move_uploaded_file( $file['tmp_name'], $userdir . "welcome1s." . $ext ) ){
                        $contact->WelcomeAudio = $userdir . "welcome1";
                        if($ext == "mp3"){
                            shell_exec("mpg123 -w '". $userdir . "welcome1s.wav' " . $userdir . "welcome1s.mp3");
                        }
                        shell_exec("sox -t wav '". $userdir . "welcome1s.wav' -r 8000 -c1 -t gsm '" . $userdir . "welcome1.gsm'");
                        shell_exec("rm -r " . $userdir . "welcome1s.mp3");
                    }
                }
                if(array_key_exists('test', $_FILES)){
                    $file = $_FILES['test'];
                    $parts = explode(".", $file['name']);
                    $ext = $parts[count($parts)-1];
                    if( move_uploaded_file( $file['tmp_name'], $userdir . "test1s." . $ext ) ){
                        $contact->TestAudio = $userdir . "test1";
                        if($ext == "mp3"){
                            shell_exec("mpg123 -w '". $userdir . "test1s.wav' " . $userdir . "test1s.mp3");
                        }
                        shell_exec("sox -t wav '". $userdir . "test1s.wav' -r 8000 -c1 -t gsm '" . $userdir . "test1.gsm'");
                        shell_exec("rm -r " . $userdir . "test1s.mp3");
                    }
                }
                $contact->save();
                $oldjson = "{\"Contacts\" : \"" . $user->Contacts . "\"}";
                $user->Contacts = $contact->id;
                $user->save();
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "profilechange", $oldjson);
            }
            $this->response->body = "Изменения сохранены";
            $this->execute = false;
        }
    }
    
}
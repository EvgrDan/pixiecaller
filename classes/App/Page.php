<?php

namespace App;
use Mailgun\Mailgun;
// use Sendgrid\Sendgrid;
/**
 * Base controller
 *
 * @property-read \App\Pixie $pixie Pixie dependency container
 */
class Page extends \PHPixie\Controller {
  
	protected $auth;

	protected $view;
    
    protected $salt;
    
    protected $mainv=true;
    protected $custom;

	public function before() {
        $this->mainv=true;
        $this->view = $this->pixie->view('main');
        $this->salt="golos";
        $this->utmsource = "";
        $this->utmcampaign = "";
        $this->utmterm = "";
        $this->refid = "";
        $this->starturl = "";
        $this->refersite = "";
        $this->medium = "";
        $this->content = "";
        $this->item_per_page = 100;
        if($this->request->method == 'GET'){
            if(array_key_exists("items",$_GET)){
                $this->item_per_page = $this->request->get("items");
                setcookie("items",$this->request->get("items"));
            }
            $this->utmsource = $this->request->get("utm_source");
            $this->utmcampaign = $this->request->get("utm_campaign");
            $this->utmterm = $this->request->get("utm_term");
            if($this->request->get("utm_ref_id")!="" and $this->request->get("utm_ref_id")!="undefined"){
                $this->refid = $this->request->get("utm_ref_id");
            }
            else if($this->request->get("ref_id")!="" and $this->request->get("ref_id")!="undefined" ){
                $this->refid = $this->request->get("ref_id");
            }
            $this->starturl = $this->request->get("utm_start_url");
            $this->refersite = $this->request->get("utm_refer_site");
            $this->medium = $this->request->get("utm_medium");
            $this->content = $this->request->get("utm_content");
        }
        else{
            if(array_key_exists("utm_source",$_COOKIE))
                $this->utmsource = $_COOKIE["utm_source"];
            if(array_key_exists("utm_campaign",$_COOKIE))
                $this->utmcampaign = $_COOKIE["utm_campaign"];
            if(array_key_exists("utm_term",$_COOKIE))
                $this->utmterm = $_COOKIE["utm_term"];
            if(array_key_exists("utm_ref_id",$_COOKIE) and $_COOKIE["utm_ref_id"]!="undefined")
                $this->refid = $_COOKIE["utm_ref_id"];
            if(array_key_exists("ref_id",$_COOKIE) and $_COOKIE["ref_id"]!="undefined")
                $this->refid = $_COOKIE["ref_id"];
            if(array_key_exists("utm_start_url",$_COOKIE))
                $this->starturl = $_COOKIE["utm_start_url"];
            if(array_key_exists("utm_refer_site",$_COOKIE))
                $this->refersite = $_COOKIE["utm_refer_site"];
            if(array_key_exists("utm_medium",$_COOKIE))
                $this->medium = $_COOKIE["utm_medium"];
            if(array_key_exists("utm_content",$_COOKIE))
                $this->content = $_COOKIE["utm_content"];
        }
        if(!array_key_exists("items",$_GET) and array_key_exists("items",$_COOKIE))
            $this->item_per_page = $_COOKIE["items"];
	}

	public function after() {
        if($this->pixie->auth->user() != null and $this->pixie->auth->user()->UserType=="User"){
            date_default_timezone_set("Europe/Moscow");
            $time_now = date_format(date_create(), 'Y-m-d H:i:s');
            $lastenter = $this->pixie->orm->get("event")->where("Type", "adminenter")->where("Subject", $this->pixie->auth->user()->id)->order_by("EventTime", "desc")->find();
            if($lastenter->loaded()){
                if(date_timestamp_get(date_create_from_format('Y-m-d H:i:s', $time_now))-date_timestamp_get(date_create_from_format('Y-m-d H:i:s', $lastenter->EventTime)) > 10800){
                    $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "adminenter", $_SERVER['SERVER_NAME']);
                }
            }
            else{
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "adminenter", $_SERVER['SERVER_NAME']);
            }
            $lastpay = $this->pixie->orm->get('payment')->where("userID", $this->pixie->auth->user()->id)->where("type", 'IN', $this->pixie->db->expr("('start', 'manual', 'w1')"))->order_by('paydate', 'desc')->find();
            $lastnotify = $this->pixie->orm->get('event')->where('Type', 'lowbalance')->where('Subject', $this->pixie->auth->user()->id)->order_by('EventTime', 'desc')->find();
            if($lastpay->loaded() and ($this->pixie->auth->user()->balance < $lastpay->value*0.2) and ((!$lastnotify->loaded() or $lastpay->paydate > $lastnotify->EventTime))){
                $this->trigger($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, 'У вас заканчиваются деньги, не забудьте <a onclick="showpay(\'block\')" href="#">пополнить баланс</a>.', "notify", "Баланс на исходе!");
                $this->event($this->pixie->auth->user()->id, $this->pixie->auth->user()->id, "lowbalance");
            }
        }
        if($this->mainv){
            $this->response->body = $this->view->render();
        }
        else{
            $this->response->body = $this->pixie->view($this->custom)->render();
            $this->mainv=true;
        }
	}
    
    protected function mandrillsend($tomail, $frommail, $message, $theme, $namefrom="", $nameto=""){
        try {
            $mandrill = new \Mandrill($this->pixie->config->get('mandrill.email.mandrill.key'));
            $message = array(
                'html' => $message,
                'text' => 'Example text content',
                'subject' => $theme,
                'from_email' => $frommail,
                'from_name' => $namefrom,
                'to' => array(
                    array(
                        'email' => $tomail,
                        'name' => $nameto,
                        'type' => 'to'
                    )
                ),
                'important' => false,
                'tags' => array('f1golos')
            );
            $async = false;
            $result = $mandrill->messages->send($message, $async);
        } catch(Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
            throw $e;
        }
    }
    
    protected function mailgunsend($tomail, $frommail, $message, $theme, $namefrom="", $nameto=""){
        try {
            $mailgun = new Mailgun($this->pixie->config->get('mailgun.email.mailgun.key'));
            $fromdomain = explode ('@', $frommail)[1];
            $message = array(
                'html' => $message,
                'subject' => $theme,
                'from' => $namefrom . " <" . $frommail . ">",
                'to' => $tomail
            );
            $result = $mailgun->sendMessage($fromdomain, $message);
        } catch(Exception $e) {
            // errors are thrown as exceptions
            echo 'A mailgun error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            throw $e;
        }
    }
    
    protected function sendgridsend($tomail, $frommail, $message, $theme, $namefrom="", $nameto=""){
        try {
            $sendgrid = new \SendGrid($this->pixie->config->get('sendgrid.email.sendgrid.key'), array("turn_off_ssl_verification" => true));
            $email = new \SendGrid\Email();
            $email
                ->addTo($tomail)
                ->setFrom($frommail)
                ->setSubject($theme)
                ->setHtml($message);
            $res = $sendgrid->send($email);
        } catch(Exception $e) {
            // errors are thrown as exceptions
            echo 'A sendgrid error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            throw $e;
        }
    }
	
    protected function trigger($touserid, $fromuserid, $message, $type, $theme="Информационное сообщение"){
        if($type=="email"){
            $touser = $this->pixie->orm->get('user')->where('id', $touserid)->find();
            $dom = $this->pixie->orm->get('domain')->where('UserID', $fromuserid)->find();
            $fromuser = 'noreply@' . $dom->landos;
            // сделать не логин, а UserEmail?
            if($touser->MailTo==1){
                $this->sendgridsend($touser->Login, $fromuser, $message, $theme);
            }
        }
        if($type=="notify"){
            $touser = $this->pixie->orm->get('user')->where('id', $touserid)->find();
            if($touser->NotifyRead and $this->pixie->orm->get("contact")->where('id', $this->pixie->orm->get("user")->where("id", $touser->Partner)->find()->Contacts)->find()->notifyon==1){
                $touser->NotifyRead = 0;
                $touser->save();
                $notify = $this->pixie->orm->get('notification');
                $notify->Name = $theme;
                $notify->Text = $message;
                $notify->AvailableFor = $touserid;
                $notify->save();
                $this->event($touserid, $touserid, "notify", $notify->id);
            }
        }
    }
    
    protected function writetobase($eventdescriptor){
        $fields = "\"EventTime\", \"Type\", \"Subject\", \"Object\", \"Sum\", \"Data\", \"RefID\", \"EnterPage\", \"FromPage\", \"UtmSource\", \"UtmCampaign\", \"UtmTerm\", \"UtmContent\", \"UtmMedium\", \"ToCall\", \"CallFile\"";
        $farray = explode(', ', $fields);
        $resarr = array();
        $valarr = array();
        foreach($farray as $f){
            if(array_key_exists($f, $eventdescriptor)){
                $resarr[]=$f;
                $valarr[]="'" . $eventdescriptor[$f] . "'";
            }
        }
        $resfields = implode(', ', $resarr);
        $valfields = implode(', ', $valarr);
        $query = "INSERT INTO events(" . $resfields . ") VALUES(" . $valfields . ");";
        // $mysqli = mysqli_connect("localhost", "root", "Nhe,jxrfPdjyb", "pixiecaller");
        $pgcon = pg_connect("host=127.0.0.1 port=6432 dbname=pixiecaller user=pgpixie password=PgPixiePassword");
        // mysqli_query($mysqli, "SET NAMES utf8;");
        if(!($result1 = pg_query($pgcon, $query)))
        {
            die();
        }
        pg_close($pgcon);
    }
    
    protected function amplitudeSend($values){
        $curl = curl_init();
        $url = 'https://api.amplitude.com/httpapi?api_key=5c32ab9701221b516372e3fe9dcb977b&event=[' . $values . ']';
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 3);
        $response = curl_exec($curl);
        $err = curl_errno($curl);
        curl_close($curl);
    }
    
    protected function event($touserid, $fromuserid, $type, $message="", $sum=0, $callfile=""){
        $json = json_decode("{}");
        $customjson = json_decode("{}");
        date_default_timezone_set("Europe/Moscow");
        $time_now = date_format(date_create(), 'Y-m-d H:i:s');
        $eventdescriptor = [];
        if($time_now!=""){
            $eventdescriptor["\"EventTime\""] = $time_now;
            $json->time = time();
        }
        if($type!=""){
            $eventdescriptor["\"Type\""] = $type;
            if($type == "balancechange"){
                $jdata = json_decode(str_replace('\"', '"', $message));
                if($jdata->type=="w1" or $jdata->type=="manual"){
                    $json->event_type = 'pay';
                    $json->price = $sum / 100;
                }
                else{
                    $json->event_type = $jdata->type . "_pay";
                }
            }
            else{
                $json->event_type = $type;
            }
        }
        if($fromuserid!=""){
            $eventdescriptor["\"Subject\""] = $fromuserid;
            $customjson->author_id = $fromuserid;
        }
        if($callfile!=""){
            $eventdescriptor["\"CallFile\""] = $callfile;
            $eventdescriptor["\"ToCall\""] = 1;
        }   
        if($touserid!=""){
            $eventdescriptor["\"Object\""] = $touserid;
            $json->user_id = $touserid;
        }
        if($this->refid!=""){
            $eventdescriptor["\"RefID\""] = $this->refid;
            $customjson->ref_id = $this->refid;
        }
        if($this->starturl!=""){
            $eventdescriptor["\"EnterPage\""] = $this->starturl;
            $customjson->start_url = $this->starturl;
        }
        if($this->refersite!=""){
            $eventdescriptor["\"FromPage\""] = $this->refersite;
            $customjson->refer_site = $this->refersite;
        }
        if($this->utmsource!=""){
            $eventdescriptor["\"UtmSource\""] = $this->utmsource;
            $customjson->utm_source = $this->utmsource;
        }
        if($this->utmcampaign!=""){
            $eventdescriptor["\"UtmCampaign\""] = $this->utmcampaign;
            $customjson->utm_campaign = $this->utmcampaign;
        }
        if($this->utmterm!=""){
            $eventdescriptor["\"UtmTerm\""] = $this->utmterm;
            $customjson->utm_term = $this->utmterm;
        }
        if($this->medium!="")
            $eventdescriptor["\"UtmMedium\""] = $this->medium;
        if($this->content!="")
            $eventdescriptor["\"UtmContent\""] = $this->content;
        if($message != ""){
            $eventdescriptor["\"Data\""] = $message;
            $customjson->data = json_decode(str_replace('\"', '"', $message));
        }
        $eventdescriptor["\"Sum\""] = $sum;
        $customjson->sum = $sum;
        $this->writetobase($eventdescriptor);
        $user = $this->pixie->orm->get('user')->where('id', $touserid)->find();
        $partner = $this->pixie->orm->get('user')->where('id', $user->Partner)->find();
        $json->productId = $partner->id;
        $customjson->partner_email = $partner->Login;
        $json->app_version = $this->pixie->config->get('version.version.app_version.value');
        $json->event_properties = $customjson;
        $values = json_encode($json);
        $this->amplitudeSend($values);
    }
    
    protected function pay($user1, $partner1, $sum1, $time_now, $type, $promoid=0){
        $payment = $this->pixie->orm->get('payment');
        if($promoid!=0){
            if($this->pixie->orm->get('payment')->where('userID', $user1)->where('promoID', $promoid)->where('partnerID', $partner1)->count_all()==0){
                $user = $this->pixie->orm->get('user')->where('id', $user1)->find();
                $user->balance = $user->balance + $sum1;
                $user->save();
                $payment->promoID = $promoid;
                $payment->userID = $user1;
                $payment->partnerID = $partner1;
                $payment->value = $sum1;
                $payment->paydate = $time_now;
                $payment->type = $type;
                $payment->save();
                if($type!="start"){
                    $this->event($user1, $partner1, "balancechange", "{ \"id\": " . $payment->id . ", \"type\" : \"" . $type . "\"}", $sum1);
                    if($user->UserType=="User"){
                        $partner = $this->pixie->orm->get('user')->where('id', $partner1)->find();
                        $dom = $this->pixie->orm->get('domain')->where('UserID', $partner1)->find();
                        $emailView = $this->pixie->view('templates/email/balanceUpdated');
                        $emailView->linktolk = $dom->name . "/admin/login";
                        $emailView->linktolandos = $dom->landos;
                        $emailView->projectname = $dom->landos;
                        $emailView->phone = $this->pixie->orm->get('contact')->where('id', $partner->Contacts)->find()->UserPhone;
                        $emailView->balance = $user->balance/100;
                        $message = $emailView->render();
                        $theme = "Пополнен баланс на " . $dom->name;
                        $this->trigger($user->id, $user->Partner, $message, "email", $theme);
                    }
                }
                return 0;
            }
            else{
                return 1;
            }
        }
        else{
            $user = $this->pixie->orm->get('user')->where('id', $user1)->find();
            $user->balance = $user->balance + $sum1;
            $user->save();
            $payment->userID = $user1;
            $payment->partnerID = $partner1;
            $payment->value = $sum1;
            $payment->paydate = $time_now;
            $payment->type = $type;
            $payment->save();
            if($type!="start"){
                $this->event($user1, $partner1, "balancechange", "{ \"id\": " . $payment->id . ", \"type\" : \"" . $type . "\"}", $sum1);
                if($user->UserType=="User" and $type!="reffer"){
                    $partner = $this->pixie->orm->get('user')->where('id', $partner1)->find();
                    $contact = $this->pixie->orm->get('contact')->where('id', $partner->Contacts)->find();
                    if($user->RefID != NULL){
                        $refuser = $this->pixie->orm->get('user')->where('id', $user->RefID)->find();
                        if($refuser->loaded() and $contact->refon==1 and $type=="manual"){
                            $this->pay($refuser->id, $partner1, $contact->percent1*$sum1/100, $time_now, "reffer");
                            $payment->data = "1refuser=" . $refuser->id . ", percent1=" . $contact->percent1*$sum1/100;
                            $payment->save();
                            if($refuser->RefID != NULL){
                                $refrefuser = $this->pixie->orm->get('user')->where('id', $refuser->RefID)->find();
                                if($refrefuser->loaded()){
                                    $this->pay($refrefuser->id, $partner1, $contact->percent2*$sum1/100, $time_now, "reffer");
                                    $payment->data = $payment->data . ", 2refuser=" . $refrefuser->id . ", percent2=" . $contact->percent2*$sum1/100;
                                    $payment->save();
                                }
                            }
                        }
                    }
                    $dom = $this->pixie->orm->get('domain')->where('UserID', $partner1)->find();
                    $emailView = $this->pixie->view('templates/email/balanceUpdated');
                    $emailView->linktolk = $dom->name . "/admin/login";
                    $emailView->linktolandos = $dom->landos;
                    $emailView->projectname = $dom->landos;
                    $emailView->phone = $this->pixie->orm->get('contact')->where('id', $partner->Contacts)->find()->UserPhone;
                    $emailView->balance = $user->balance/100;
                    $message = $emailView->render();
                    $theme = "Пополнен баланс на " . $dom->name;
                    $this->trigger($user->id, $user->Partner, $message, "email", $theme);
                }
            }
            return 0;
        }
        
    }
    
    protected function payfromw1($user1, $partner1, $sum1, $time_now, $type, $payid){
        $payment = $this->pixie->orm->get('payment')->where('id', $payid)->find();
        $user = $this->pixie->orm->get('user')->where('id', $user1)->find();
        $user->balance = $user->balance + $sum1;
        $user->save();
        $payment->userID = $user1;
        $payment->partnerID = $partner1;
        $payment->value = $sum1;
        $payment->paydate = $time_now;
        $payment->type = $type;
        $payment->save();
        if($type!="start"){
            $this->event($user1, $partner1, "balancechange", "{ \"id\": " . $payment->id . ", \"type\" : \"" . $type . "\"}", $sum1);
            if($user->UserType=="User" and $type!="reffer"){
                $partner = $this->pixie->orm->get('user')->where('id', $partner1)->find();
                $contact = $this->pixie->orm->get('contact')->where('id', $partner->Contacts)->find();
                if($user->RefID != NULL){
                    $refuser = $this->pixie->orm->get('user')->where('id', $user->RefID)->find();
                    if($refuser->loaded() and $contact->refon==1 and $type=="manual"){
                        $this->pay($refuser->id, $partner1, $contact->percent1*$sum1/100, $time_now, "reffer");
                        $payment->data = "1refuser=" . $refuser->id . ", percent1=" . $contact->percent1*$sum1/100;
                        $payment->save();
                        if($refuser->RefID != NULL){
                            $refrefuser = $this->pixie->orm->get('user')->where('id', $refuser->RefID)->find();
                            if($refrefuser->loaded()){
                                $this->pay($refrefuser->id, $partner1, $contact->percent2*$sum1/100, $time_now, "reffer");
                                $payment->data = $payment->data . ", 2refuser=" . $refrefuser->id . ", percent2=" . $contact->percent2*$sum1/100;
                                $payment->save();
                            }
                        }
                    }
                }
                $dom = $this->pixie->orm->get('domain')->where('UserID', $partner1)->find();
                $emailView = $this->pixie->view('templates/email/balanceUpdated');
                $emailView->linktolk = $dom->name . "/admin/login";
                $emailView->linktolandos = $dom->landos;
                $emailView->projectname = $dom->landos;
                $emailView->phone = $this->pixie->orm->get('contact')->where('id', $partner->Contacts)->find()->UserPhone;
                $emailView->balance = $user->balance/100;
                $message = $emailView->render();
                $theme = "Пополнен баланс на " . $dom->name;
                $this->trigger($user->id, $user->Partner, $message, "email", $theme);
            }
        }
        return 0;
    }
    
	protected function logged_in($role = null){
        if($this->pixie->auth->user() == null){
            $this->redirect('/admin/login');
            return false;
        }
        if($role)
            $roles = explode(', ', $role);
        if($role && !in_array($this->pixie->auth->user()->UserType, $roles)){
            if($this->pixie->auth->user()->UserType=="Admin"){
                $this->redirect('/su/distribs');
                return false;
            }
            else{
                if($this->pixie->auth->user()->UserType=="User"){
                    $this->redirect('/admin/distribs');
                    return false;
                }
                else{
                    if($this->pixie->auth->user()->UserType=="Partner"){
                        $this->redirect('/partner/clients');
                        return false;
                    }
                    else{
                        if($this->pixie->auth->user()->UserType=="Manager"){
                            $this->redirect('/partner/clients');
                            return false;
                        }
                        else{
                            $this->redirect('/admin/login');
                            return false;
                        }
                    }
                }
            }
            // $this->response->body = "You don't have the permissions to access this page";
            // $this->execute=false;
            // return false;
        }
        return true;
    }
    
    protected function check_key($role = null){
        $uuid = $this->request->post("uid");
        $uid = $this->pixie->orm->get("uniqueid")->where('uuid', $uuid)->where('action', 'api')->find();
        if($uid->loaded()){
            return $uid;
        }
        return false;
    }
    
    protected function generateRandomSelection($min, $max, $count)
    {
        $result=array();
        if($min>$max) return $result;
        $count=min(max($count,0),$max-$min+1);
        while(count($result)<$count) {
            $value=rand($min,$max-count($result));
            foreach($result as $used) if($used<=$value) $value++; else break;
            $result[]=dechex($value);
            sort($result);
        }
        shuffle($result);
        return $result;
    }


}

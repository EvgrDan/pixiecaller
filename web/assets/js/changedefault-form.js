$().ready(function() {
    $("#changedefault").validate({
        rules: {
            recall: {
                required: true,
                digits: true
            },
            ordinary: {
                required: true,
                digits: true
            },
            dial: {
                required: true,
                digits: true
            }
        },
        messages: {
        }
    });
    var tn = $("div[name='subdefault']")
    tn.click(function() {
        $.ajax({
          type: "POST",
          url: "/partner/clients",
          data: "recall="+$('input[name="recall"]').val()+"&ordinary="+$('input[name="ordinary"]').val()+"&dial="+$('input[name="dial"]').val(),
          success: function(data){
            swal( data );
          }
        });
    });
    var tn1 = $("div[name='clfilterbtn']")
    tn1.click(function() {
        var filter = $('select[name="clienttype"]').val();
        cookiePath = "/";
        var cookieName = "clienttype";
        var cookieValue = filter;
        var cookieHours = 3; // defaultCookieLifetime
        var d = new Date();
        d.setTime(d.getTime() + (cookieHours * 3600 * 1000));
        var expires = "expires=" + d.toUTCString() + ";";
        var path = "path=" + cookiePath + ";";
        document.cookie = cookieName + "=" + cookieValue + "; " + expires + path;
        
        var filterman = $('select[name="managerfilter"]').val();
        cookiePath = "/";
        var cookieName = "managerfilter";
        var cookieValue = filterman;
        var cookieHours = 3; // defaultCookieLifetime
        var d = new Date();
        d.setTime(d.getTime() + (cookieHours * 3600 * 1000));
        var expires = "expires=" + d.toUTCString() + ";";
        var path = "path=" + cookiePath + ";";
        document.cookie = cookieName + "=" + cookieValue + "; " + expires + path;
        window.location.replace("/partner/clients");
    });
})
$().ready(function() {
    $("#changeuser").validate({
        rules: {
            usernum: {
                required: true,
                russianPhone: true
            },
            usermail: {
                required: true,
                email: true
            }
        },
        messages: {
        }
    });
    $("#contactform").validate({
        rules: {
            percent1: {
                percentPartner: true
            },
            percent2: {
                percentPartner: true
            }
        },
        messages: {
        }
    });
    var tn = $("div[name='subcontactbtn']");
    tn.click(function() {
        var formData = new FormData($("#contactform")[0]);
        var txtreq = safe_tags_replace($("#reqtext")[0].value);
        formData.append("requisites",txtreq);
        $.ajax({
          type: "POST",
          url: "/partner/editcontacts",
          data: formData,
          async: false,
          success: function(data){
            swal( data );
          },
          cache: false,
          contentType: false,
          processData: false
        });
    });
    
    var tn = $("div[name='subcontactbtnsu']");
    tn.click(function() {
        var formData = new FormData($("#contactform")[0]);
        var txtreq = safe_tags_replace($("#reqtext")[0].value);
        formData.append("requisites",txtreq);
        $.ajax({
          type: "POST",
          url: "/su/editcontacts",
          data: formData,
          async: false,
          success: function(data){
            swal( data );
          },
          cache: false,
          contentType: false,
          processData: false
        });
    });
})

var tagsToReplace = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;'
};

function replaceTag(tag) {
    return tagsToReplace[tag] || tag;
}

function safe_tags_replace(str) {
    return str.replace(/[&<>]/g, replaceTag);
}

jQuery.validator.addMethod("russianPhone", function(value, element) {
    return this.optional(element) || /^7\d{10}$/.test(value);
}, "Номер должен начинаться с '7' и содержать 11 цифр");

jQuery.validator.addMethod("percentPartner", function(value, element) {
    return this.optional(element) || /^\d(\d)?$/.test(value);
}, "Процент должен быть числом и не может быть больше 100");
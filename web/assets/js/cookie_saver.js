function setCookie(cookieName, cookieValue, cookieExpiresDays, cookiePath) {
    cookiePath = cookiePath || "/";
    var cookieHours = 3; // defaultCookieLifetime
    if (cookieExpiresDays) {
        cookieHours = cookieExpiresDays;
    }
    var d = new Date();
    d.setTime(d.getTime() + (cookieHours * 3600 * 1000));
    var expires = "expires=" + d.toUTCString() + ";";
    var path = "path=" + cookiePath + ";";
    document.cookie = cookieName + "=" + cookieValue + "; " + expires + path;
}

function getCookie(cookieName) {
    var name = cookieName + "=";
    var allCookies = document.cookie.split(';');
    for (var i = 0; i < allCookies.length; i++) {
        var cookie = allCookies[i];
        while (cookie.charAt(0) == ' ')
            cookie = cookie.substring(1);
        if (cookie.indexOf(name) != -1) {
            return cookie.substring(name.length, cookie.length);
        }
    }
    return undefined;
}

function readGetParamByName(name) {
    if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(location.search)) {
        return decodeURIComponent(name[1]);
    }
}

function checkParam(param) {
    return (document.location.search.indexOf(param) > -1);
}

function checkCookie(param) {
    return document.cookie.indexOf(param) > -1;
}

function getStringParams() {
    var result = [];
    if (checkCookie('utm_')) {
        result.push('utm_term=' + getCookie('utm_term') + '&utm_source=' + getCookie('utm_source') + '&utm_campaign=' + getCookie('utm_campaign') + '&utm_medium=' + getCookie('utm_medium') + '&utm_content=' 
+ getCookie('utm_content') + '&utm_ref_id=' + getCookie('utm_ref_id') + '&utm_refer_site=' + getCookie('utm_refer_site') + '&utm_start_url=' + getCookie('utm_start_url'));
    }
    if (checkCookie('ref_id')) {
        result.push('ref_id=' + getCookie('ref_id'));
    }
    result = result.join('&');
    return result;
}

function getDictParams() {
    var result = {};
    if (checkCookie('utm_')) {
        result.utm_term = getCookie('utm_term');
        result.utm_source = getCookie('utm_source');
        result.utm_campaign = getCookie('utm_campaign');
        result.utm_ref_id = getCookie('utm_ref_id');
        result.utm_medium = getCookie('utm_medium');
        result.utm_content = getCookie('utm_content');
        result.utm_refer_site = getCookie('utm_refer_site');
        result.utm_start_url = getCookie('utm_start_url');
    }
    if (checkCookie('ref_id')) {
        result.ref_id = getCookie('ref_id');
    }
    return result;
}

function paramsIntoCookies(type) {
    if (checkParam('utm')) {
        setCookie('utm_term', readGetParamByName('utm_term'));
        setCookie('utm_source', readGetParamByName('utm_source'));
        setCookie('utm_campaign', readGetParamByName('utm_campaign'));
        setCookie('utm_ref_id', readGetParamByName('utm_ref_id'));
        setCookie('utm_medium', readGetParamByName('utm_medium'));
        setCookie('utm_content', readGetParamByName('utm_content'));
        setCookie('utm_refer_site', readGetParamByName('utm_refer_site'));
        setCookie('utm_start_url', readGetParamByName('utm_start_url'));
    }
    if (checkParam('ref_id')) {
        setCookie('ref_id', readGetParamByName('ref_id'));
    }
    if (type == 'string'){
        return getStringParams();
    }
    else if (type == 'dict'){
        return getDictParams();
    }
    else if(type == 'link'){
        jQuery('.link-get-params').attr('href',jQuery('.link-get-params').attr('href') + '?' + getStringParams())
    }
}


// getCookie(name) - возвращает печенюху по имени
// setCookie(name, value) - пишет печенюху по заданому имени с заданой начинкой
// readGetParamByName(name) - читает GET параметр по имени
// checkParam(name) - проверяет наличие GET параметра
// checkCookie(name) - проверяет наличие печенюхи
// paramsIntoCookies(type) - пишет все нужные параметры при их наличии в печенюхи, type принимает значения string или dict возвращает результат в заданом типе
// getStringParams() - возвращает параметры в виде URI строки
// getDictParams() - возвращает параметры в виде словаря

// Пример использования:
// если нет необходимости возвращать данные, то просто вызываем при загрузке страницы (или нажатии кнопки)
paramsIntoCookies('link');
// ngclick="paramsIntoCookies();
// Если есть необходимость получить данные в каком-либо формате то
// paramsIntoCookies('string');
// paramsIntoCookies('dict');
// paramsIntoCookies('link');
$().ready(function() {
    var tn1 = $("div[name='distrfilterbtn']")
    tn1.click(function() {
        
        var filterman = $('select[name="managerfilter"]').val();
        cookiePath = "/";
        var cookieName = "managerfilter";
        var cookieValue = filterman;
        var cookieHours = 3; // defaultCookieLifetime
        var d = new Date();
        d.setTime(d.getTime() + (cookieHours * 3600 * 1000));
        var expires = "expires=" + d.toUTCString() + ";";
        var path = "path=" + cookiePath + ";";
        document.cookie = cookieName + "=" + cookieValue + "; " + expires + path;
        window.location.replace("/partner/distribs");
    });
})
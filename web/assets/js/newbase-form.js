$().ready(function() {
    $("#newbase").validate({
        rules: {
            basename: {
                required: true,
                bName: true
            }
        },
        messages: {
        }
    });
});

jQuery.validator.addMethod("bName", function(value, element) {
    return this.optional(element) || /^([A-z]|\d)+$/.test(value);
}, "Имя не должно содержать ничего кроме латинских букв и цифр!");
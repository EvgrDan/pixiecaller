//Оставь надежду всяк сюда входящий, испокон веков тут царит тьма и ужас, многие поколения были бессильны побороть эту тьму
//Пророчество говорит, что однажды явится бравый воин с руками из плеч, который сможет навести порядок в этом мире
//День 1: Я вошел в это ужасное место, все было завалено одинаковыми зомбаками, половину из них вырезал
//День 2: Мне начинает казаться, что я схожу с ума, убрал большую часть ненужного кода, но строки все еще превосходят меня количеством, мне кажется этот день нам не пережить... 

function setblock(){
    if(!$("input[name='chkblocknum']").is(":checked"))
        $("input[name='chkblocknum']").trigger("click");
    $("select[name='blockon']").val(2);
}

function unblock(){
    if($("input[name='chkblocknum']").is(":checked"))
        $("input[name='chkblocknum']").trigger("click");
}

function radioClick(lbl1, lbl2, rad1, rad2, lbl3, rad3){
    if(arguments.length==4){
        rad1.prop('checked', true);
        rad2.prop('checked', false);
        lbl1.removeClass("btn-grey");
        lbl1.addClass("btn-red");
        lbl2.addClass("btn-grey");
        lbl2.removeClass("btn-red");
    }
    else{
        rad1.prop('checked', true);
        rad2.prop('checked', false);
        rad3.prop('checked', false);
        lbl1.removeClass("btn-grey");
        lbl1.addClass("btn-red");
        lbl2.addClass("btn-grey");
        lbl2.removeClass("btn-red");
        lbl3.addClass("btn-grey");
        lbl3.removeClass("btn-red");
    }
}

function ivrInit(ivrChk, ivr){
    var init1 = ivrChk.is(":checked");
    if(init1){
        ivr.removeClass("hide");
    }
    else{
        ivr.addClass("hide");
    }
    var ivrMusicInput = ivr.find("input").attr("disabled", !init1);
    ivrChk.click(function() {
        if(this.checked){
            ivr.removeClass("hide");
        }
        else{
            ivr.addClass("hide");
        }
        ivrMusicInput.attr("disabled", !this.checked);
    });
}

$(document).ready(function() {
    $("#newdistr").validate({
        rules: {
            tonumber: {
                required: true,
                russianPhone: true
            },
            distribname: {
                required: true,
                distrName: true
            },
            testNum: {
                required: true,
                russianPhones: true
            }
        },
        messages: {
        }
    });

    var tst1 = $("label[name='baseclick']");
    var tst2 = $("label[name='numbersclick']");
    var initT1 = tst1.hasClass("active");
    if(initT1){
        $("#distrbasedev").removeClass("hide");
        $("select[name='distribbase']").removeClass("hide");
        $("textarea[name='testNum']").addClass("hide");
        radioClick(tst1, tst2, $("#q158"), $("#q159"));
    }
    tst1.click(function() {
        $("#distrbasedev").removeClass("hide");
        $("select[name='distribbase']").removeClass("hide");
        $("textarea[name='testNum']").addClass("hide");
        radioClick(tst1, tst2, $("#q158"), $("#q159"));
    });
    
    var initT2 = tst2.hasClass("active");
    if(initT2){
        $("#distrbasedev").addClass("hide");
        $("select[name='distribbase']").addClass("hide");
        $("textarea[name='testNum']").removeClass("hide");
        radioClick(tst2, tst1, $("#q159"), $("#q158"));
    }
    tst2.click(function() {
        $("#distrbasedev").addClass("hide");
        $("select[name='distribbase']").addClass("hide");
        $("textarea[name='testNum']").removeClass("hide");
        radioClick(tst2, tst1, $("#q159"), $("#q158"));
    });
    
    var tst3 = $("label[name='fileclick']");
    var tst4 = $("label[name='textclick']");
    var tst41 = $("label[name='libclick']");
    var initT3 = tst3.hasClass("active");
    if(initT3){
        $('input[name="welcome"]').removeClass("hide");
        $("textarea[name='welcomeText']").addClass("hide");
        $("#seldevrec").addClass("hide");
        $("#selrec").addClass("hide");
        radioClick(tst3, tst4, $("#q157"), $("#q156"), tst41, $("#q155"));
    }
    tst3.click(function() {
        $('input[name="welcome"]').removeClass("hide");
        $("textarea[name='welcomeText']").addClass("hide");
        $("#seldevrec").addClass("hide");
        $("#selrec").addClass("hide");
        radioClick(tst3, tst4, $("#q157"), $("#q156"), tst41, $("#q155"));
    });
    
    var initT4 = tst4.hasClass("active");
    if(initT4){
        $('input[name="welcome"]').addClass("hide");
        $("textarea[name='welcomeText']").removeClass("hide");
        $("#seldevrec").addClass("hide");
        $("#selrec").addClass("hide");
        radioClick(tst4, tst3, $("#q156"), $("#q157"), tst41, $("#q155"));
    }
    tst4.click(function() {
        $('input[name="welcome"]').addClass("hide");
        $("textarea[name='welcomeText']").removeClass("hide");
        $("#seldevrec").addClass("hide");
        $("#selrec").addClass("hide");
        radioClick(tst4, tst3, $("#q156"), $("#q157"), tst41, $("#q155"));
    });
    
    var initT41 = tst41.hasClass("active");
    if(initT41){
        $('input[name="welcome"]').addClass("hide");
        $("textarea[name='welcomeText']").addClass("hide");
        $("#seldevrec").removeClass("hide");
        $("#selrec").removeClass("hide");
        radioClick(tst41, tst3, $("#q155"), $("#q157"), tst4, $("#q156"));
    }
    tst41.click(function() {
        $('input[name="welcome"]').addClass("hide");
        $("textarea[name='welcomeText']").addClass("hide");
        $("#seldevrec").removeClass("hide");
        $("#selrec").removeClass("hide");
        radioClick(tst41, tst3, $("#q155"), $("#q157"), tst4, $("#q156"));
    });
    
    var ivr1 = $("input[name='chk1']");
    var ivr11 = $("input[name='ivr1']");
    ivrInit(ivr1, ivr11);
    
    var ivr2Chk = $("input[name='chk2']");
    var ivr2 = $("input[name='ivr2']");
    ivrInit(ivr2Chk, ivr2);
    
    var ivr3Chk = $("input[name='chk3']");
    var ivr3 = $("input[name='ivr3']");
    ivrInit(ivr3Chk, ivr3);
    
    var tnChk = $("input[name='tn']");
    var tn = $("input[name='tonumber']");
    ivrInit(tnChk, tn);
    
    
    var planned = $("input[name='chkplan']");
    var init5 = planned.is(":checked");
    if(!init5){
        $("#datetimepicker1").addClass("hide");
        $('label[name="lblbegin"]').addClass("hide");
    }
    else{
        $("#datetimepicker1").removeClass("hide");
        $('label[name="lblbegin"]').removeClass("hide");
    }
    planned.click(function() {
        if(!this.checked){
            $("#datetimepicker1").addClass("hide");
            $('label[name="lblbegin"]').addClass("hide");
        }
        else{
            $("#datetimepicker1").removeClass("hide");
            $('label[name="lblbegin"]').removeClass("hide");
        }
        tonum.attr("disabled", !this.checked);
    });
    
    var ivrchecked = $("input[name='ivrcheck']");
    var init7 = ivrchecked.is(":checked");
    if(!init7){
        $("#ivrmenu").addClass("hide");
    }
    else{
        $("#ivrmenu").removeClass("hide");
        var ivr1 = $("input[name='chk1']");
        var init1 = ivr1.is(":checked");
        if(!init1){
            $("input[name='ivr1']").addClass("hide");
        }
        else{
            $("input[name='ivr1']").removeClass("hide");
        }
        var ivr2 = $("input[name='chk2']");
        var init2 = ivr2.is(":checked");
        if(!init2){
            $("input[name='ivr2']").addClass("hide");
        }
        else{
            $("input[name='ivr2']").removeClass("hide");
        }
        var ivr3 = $("input[name='chk3']");
        var init3 = ivr3.is(":checked");
        if(!init3){
            $("input[name='ivr3']").addClass("hide");
        }
        else{
            $("input[name='ivr3']").removeClass("hide");
        }
        var tn = $("input[name='tn']");
        var init4 = tn.is(":checked");
        if(!init4){
            $("input[name='tonumber']").addClass("hide");
        }
        else{
            $("input[name='tonumber']").removeClass("hide");
        }
    }
    ivrchecked.click( function() {
        
        if(!this.checked){
            $("#ivrmenu").addClass("hide");
        }
        else{
            $("#ivrmenu").removeClass("hide");
            var ivr1 = $("input[name='chk1']");
            var init1 = ivr1.is(":checked");
            if(!init1){
                $("input[name='ivr1']").addClass("hide");
            }
            else{
                $("input[name='ivr1']").removeClass("hide");
            }
            var ivr2 = $("input[name='chk2']");
            var init2 = ivr2.is(":checked");
            if(!init2){
                $("input[name='ivr2']").addClass("hide");
            }
            else{
                $("input[name='ivr2']").removeClass("hide");
            }
            var ivr3 = $("input[name='chk3']");
            var init3 = ivr3.is(":checked");
            if(!init3){
                $("input[name='ivr3']").addClass("hide");
            }
            else{
                $("input[name='ivr3']").removeClass("hide");
            }
            var tn = $("input[name='tn']");
            var init4 = tn.is(":checked");
            if(!init4){
                $("input[name='tonumber']").addClass("hide");
            }
            else{
                $("input[name='tonumber']").removeClass("hide");
            }
        }
    });
    var ivr1MusicInput = $("input[name='ivr1']").find("input").attr("disabled", !init1);
    ivr1.click(function() {
        if(!this.checked){
            $("input[name='ivr1']").addClass("hide");
        }
        else{
            $("input[name='ivr1']").removeClass("hide");
        }
        ivr1MusicInput.attr("disabled", !this.checked);
    });
    
    
    var pat = $("input[name='chkPat']");
    var init6 = pat.is(":checked");
    if(!init6){
        $('select[name="patternsel"]').addClass("hide");
        $('#sel5div').addClass("hide");
    }
    else{
        $('select[name="patternsel"]').removeClass("hide");
        $('#sel5div').removeClass("hide");
    }
    pat.click(function() {
        if(!this.checked){
            $('select[name="patternsel"]').addClass("hide");
            $('#sel5div').addClass("hide");
        }
        else{
            $('select[name="patternsel"]').removeClass("hide");
            $('#sel5div').removeClass("hide");
        }
    });
    var patbtn = $('label[name="patternbtn"]');
    patbtn.click(function() {
        var fd = new FormData();
        var other_data = $('#newdistr').serializeArray();
        $.each(other_data,function(key,input){
            fd.append(input.name,input.value);
        });
        var tst3 = $("label[name='fileclick']");
        var initT3 = tst3.hasClass("active");
        if(initT3){
            var file_data = $('input[name="welcome"]').prop('files')[0];
            fd.append('welcome', file_data);
        }
        var ivr1 = $("input[name='chk1']");
        var init1 = ivr1.is(":checked");
        if(init1){
            var file_data = $("input[name='ivr1']").prop('files')[0];
            fd.append('ivr1', file_data);
        }
        var ivr2 = $("input[name='chk2']");
        var init2 = ivr2.is(":checked");
        if(init2){
            var file_data = $("input[name='ivr2']").prop('files')[0];
            fd.append('ivr2', file_data);
        }
        var ivr3 = $("input[name='chk3']");
        var init3 = ivr3.is(":checked");
        if(init3){
            var file_data = $("input[name='ivr3']").prop('files')[0];
            fd.append('ivr3', file_data);
        }
        $.ajax({
          type: "POST",
          url: "/admin/addpattern",
          contentType: false,
          processData: false,
          data: fd,
          success: function(data){
            swal( data );
          }
        });
    });
    var smschk = $("input[name='smsto']");
    var initsms = smschk.is(":checked");
    if(!initsms){
        $("label[name='smstextlbl']").addClass("hide");
        $("i[name='smstoquest']").addClass("hide");
        $("textarea[name='smstext']").addClass("hide");
    }
    else{
        $("label[name='smstextlbl']").removeClass("hide");
        $("i[name='smstoquest']").removeClass("hide");
        $("textarea[name='smstext']").removeClass("hide");
    }
    smschk.click(function() {
        if(!this.checked){
            $("label[name='smstextlbl']").addClass("hide");
            $("i[name='smstoquest']").addClass("hide");
            $("textarea[name='smstext']").addClass("hide");
        }
        else{
            $("label[name='smstextlbl']").removeClass("hide");
            $("i[name='smstoquest']").removeClass("hide");
            $("textarea[name='smstext']").removeClass("hide")
        }
    });
    var selpat = $("select[name='patternsel']");
    selpat.change(function() {
        if(this.value=="0"){
            $('select[name="patternsel"]').removeClass("hide");
        }
        else{
            $.ajax({
              type: "POST",
              url: "/admin/loadpattern",
              data: "pid="+$('select[name="patternsel"]').val(),
              success: function(data){
                if(data!=""){
                    $('a[name="showfields"]').click();
                    var obj = jQuery.parseJSON( data );
                    if(obj.DistribType=="ordinary"){
                        $("#q156").trigger("click");
                        $('label[name="ordclick"]').trigger("click");
                    }
                    if(obj.DistribType=="recall"){
                        $("#q157").trigger("click");
                        $('label[name="recallclick"]').trigger("click");
                    }
                    if(obj.BaseType=="base"){
                        $("#q158").trigger("click");
                        $('label[name="baseclick"]').trigger("click");
                        $("select[name='distribbase']").val(obj.BaseID);
                    }
                    if(obj.BaseType=="numbers"){
                        $("#q159").trigger("click");
                        $('label[name="numbersclick"]').trigger("click");
                        $('textarea[name="testNum"]').val(obj.Numbers);
                    }
                    if(obj.SpeedID!=""){
                        $("select[name='distrspeed']").val(obj.SpeedID);
                    }
                    if(obj.ToNumber!=""){
                        $("input[name='tn']").trigger("click");
                        $("#tonumber").val(obj.ToNumber);
                    }
                    if(obj.SenderNumber!=""){
                        if(obj.DistribType=="recall"){
                            $("select[name='distribnumber']").val(obj.SenderNumber);
                        }
                        if(obj.DistribType=="ordinary"){
                            $("select[name='distribordnumber']").val(obj.SenderNumber);
                        }
                    }
                    if(obj.TestDistrib=="1"){
                        $("input[name='chkTest']").trigger("click");
                    }
                    if(obj.WelcomeRecord!=""){
                        $("#welcomeaudioplayer").removeClass('hide');
                        $('label[name="fileclick"]').trigger("click");
                        $("#welcomeaudio").attr("src", obj.WelcomeRecord).detach().appendTo("#welcomeaudioplayer");
                    }
                    else{
                        $("#welcomeaudioplayer").addClass('hide');
                    }
                    if(obj.IVR1!=""){
                        $("input[name='chk1']").trigger("click");
                        $("#ivr1audioplayer").removeClass('hide');
                        $("#ivr1audio").attr("src", obj.IVR1).detach().appendTo("#ivr1audioplayer");
                    }
                    else{
                        $("#ivr1audioplayer").addClass('hide');
                    }
                    if(obj.IVR2!=""){
                        $("input[name='chk2']").trigger("click");
                        $("#ivr2audioplayer").removeClass('hide');
                        $("#ivr2audio").attr("src", obj.IVR2).detach().appendTo("#ivr2audioplayer");
                    }
                    else{
                        $("#ivr2audioplayer").addClass('hide');
                    }
                    if(obj.IVR3!=""){
                        $("input[name='chk3']").trigger("click");
                        $("#ivr3audioplayer").removeClass('hide');
                        $("#ivr3audio").attr("src", obj.IVR3).detach().appendTo("#ivr3audioplayer");
                    }
                    else{
                        $("#ivr3audioplayer").addClass('hide');
                    }
                    if(obj.blocknumber=="3"){
                        $("input[name='chkblock']").trigger("click");
                    }
                    if(obj.IVR==1){
                        $("input[name='ivrcheck']").trigger("click");
                    }
                    if(obj.BlockFor!=0){
                        $("input[name='chkblocknum']").trigger("click");
                        $("select[name='blockon']").val(obj.BlockFor/60/60);
                    }
                    if(obj.sms!=""){
                        $("input[name='smsto']").trigger("click");
                        $('textarea[name="smstext"]').val(obj.sms);
                    }
                    if(obj.comment!=""){
                        $('textarea[name="distribcomment"]').val(obj.comment);
                    }
                }
              }
            });
        }
    });
    $("#q160").prop('checked', true);
    var rad1 = $('input[name=distribtype]:checked').val()
    if(rad1=="ordinary"){
        $('select[name="distribnumber"]').addClass("hide");
        $('select[name="distribordnumber"]').removeClass("hide");
        $('label[name="tstLbl"]').addClass("hide");
        $('input[name="chkTest"]').addClass("hide");
        var planned = $("input[name='chkplan']");
        planned.removeClass("hide");
        $("label[name='lblplan']").removeClass("hide");
        var init5 = planned.is(":checked");
        if(!init5){
            $("#datetimepicker1").addClass("hide");
            $('label[name="lblbegin"]').addClass("hide");
        }
        else{
            $("#datetimepicker1").removeClass("hide");
            $('label[name="lblbegin"]').removeClass("hide");
        }
        var ivrchecked = $("input[name='ivrcheck']");
        var init7 = ivrchecked.is(":checked");
        if(!init7){
            $("#ivrmenu").addClass("hide");
        }
        else{
            $("#ivrmenu").removeClass("hide");
            var ivr1 = $("input[name='chk1']");
            var init1 = ivr1.is(":checked");
            if(!init1){
                $("input[name='ivr1']").addClass("hide");
            }
            else{
                $("input[name='ivr1']").removeClass("hide");
            }
            var ivr2 = $("input[name='chk2']");
            var init2 = ivr2.is(":checked");
            if(!init2){
                $("input[name='ivr2']").addClass("hide");
            }
            else{
                $("input[name='ivr2']").removeClass("hide");
            }
            var ivr3 = $("input[name='chk3']");
            var init3 = ivr3.is(":checked");
            if(!init3){
                $("input[name='ivr3']").addClass("hide");
            }
            else{
                $("input[name='ivr3']").removeClass("hide");
            }
            var tn = $("input[name='tn']");
            var init4 = tn.is(":checked");
            if(!init4){
                $("input[name='tonumber']").addClass("hide");
            }
            else{
                $("input[name='tonumber']").removeClass("hide");
            }
        }
    }
    else if(rad1=="recall"){
        $('label[name="tstLbl"]').removeClass("hide");
        $('input[name="chkTest"]').removeClass("hide");
        $('select[name="distribnumber"]').removeClass("hide");
        $('select[name="distribordnumber"]').addClass("hide");
        var planned = $("input[name='chkplan']");
        planned.removeClass("hide");
        var init5 = planned.is(":checked");
        if(!init5){
            $("#datetimepicker1").addClass("hide");
            $('label[name="lblbegin"]').addClass("hide");
        }
        else{
            $("#datetimepicker1").removeClass("hide");
            $('label[name="lblbegin"]').removeClass("hide");
        }
        var ivrchecked = $("input[name='ivrcheck']");
        var init7 = ivrchecked.is(":checked");
        if(!init7){
            $("#ivrmenu").addClass("hide");
        }
        else{
            $("#ivrmenu").removeClass("hide");
            var ivr1 = $("input[name='chk1']");
            var init1 = ivr1.is(":checked");
            if(!init1){
                $("input[name='ivr1']").addClass("hide");
            }
            else{
                $("input[name='ivr1']").removeClass("hide");
            }
            var ivr2 = $("input[name='chk2']");
            var init2 = ivr2.is(":checked");
            if(!init2){
                $("input[name='ivr2']").addClass("hide");
            }
            else{
                $("input[name='ivr2']").removeClass("hide");
            }
            var ivr3 = $("input[name='chk3']");
            var init3 = ivr3.is(":checked");
            if(!init3){
                $("input[name='ivr3']").addClass("hide");
            }
            else{
                $("input[name='ivr3']").removeClass("hide");
            }
            var tn = $("input[name='tn']");
            var init4 = tn.is(":checked");
            if(!init4){
                $("input[name='tonumber']").addClass("hide");
            }
            else{
                $("input[name='tonumber']").removeClass("hide");
            }
        }
    }
    else{
        $('input[name="chkTest"]').addClass("hide");
        $('select[name="distribnumber"]').addClass("hide");
        $('select[name="distribordnumber"]').addClass("hide");
    }
    $('label[name="ordclick"]').click(function() {
        $("#q160").prop('checked', true);
        $("#q161").prop('checked', false);
        $('label[name="ordclick"]').addClass("active");
        $('label[name="recallclick"]').removeClass("active");
        $('label[name="ordclick"]').addClass("btn-red");
        $('label[name="ordclick"]').removeClass("btn-grey");
        $('label[name="recallclick"]').removeClass("btn-red");
        $('label[name="recallclick"]').addClass("btn-grey");
        $('select[name="distribnumber"]').addClass("hide");
        $('select[name="distribordnumber"]').removeClass("hide");
        $('label[name="tstLbl"]').addClass("hide");
        $('input[name="chkTest"]').addClass("hide");
    });
    $('label[name="recallclick"]').click(function() {
        $("#q160").prop('checked', false);
        $("#q161").prop('checked', true);
        $('label[name="ordclick"]').removeClass("active");
        $('label[name="recallclick"]').addClass("active");
        $('label[name="ordclick"]').removeClass("btn-red");
        $('label[name="ordclick"]').addClass("btn-grey");
        $('label[name="recallclick"]').addClass("btn-red");
        $('label[name="recallclick"]').removeClass("btn-grey");
        $('select[name="distribnumber"]').removeClass("hide");
        $('select[name="distribordnumber"]').addClass("hide");
        $('label[name="tstLbl"]').removeClass("hide");
        $('input[name="chkTest"]').removeClass("hide");
        
    });
    
    $('a[name="showfields"]').click(function() {
        $('label[name="typelabel"]').removeClass("hide");
        $('label[name="recallclick"]').removeClass("hide");
        $('label[name="ordclick"]').removeClass("hide");
        $('i[name="distribtypequest"]').removeClass("hide");
        $('i[name="distribtypequest"]').removeClass("hide");
        var planned = $("input[name='chkplan']");
        planned.removeClass("hide");
        $("label[name='lblplan']").removeClass("hide");
        $('label[name="commentlbl"]').removeClass("hide");
        $('textarea[name="distribcomment"]').removeClass("hide");
        $('label[name="patternbtn"]').removeClass("hide");
    });
    $("#q156").trigger("click");
    $('label[name="ordclick"]').trigger("click");
    $('label[name="typelabel"]').addClass("hide");
    $('label[name="recallclick"]').addClass("hide");
    $('label[name="ordclick"]').addClass("hide");
    $('i[name="distribtypequest"]').addClass("hide");
    $('i[name="distribtypequest"]').addClass("hide");
    var planned = $("input[name='chkplan']");
    planned.addClass("hide");
    $("label[name='lblplan']").addClass("hide");
    $('label[name="commentlbl"]').addClass("hide");
    $('textarea[name="distribcomment"]').addClass("hide");
    $('label[name="patternbtn"]').addClass("hide");
    $('#sel1').change(function() {
        if(this.options[this.selectedIndex].id=='href')
            window.location.href=this.options[this.selectedIndex].value;
        else 
            if(this.options[this.selectedIndex].id=='setblock')
                setblock();
            else
                unblock();
    });
    
    $('#sel3').change(function() {
        if(this.options[this.selectedIndex].id=='href')
            window.location.href=this.options[this.selectedIndex].value;
        else 
            if(this.options[this.selectedIndex].id=='setblock')
                setblock();
            else
                unblock();
    });
})

// $().onload(function() {

// $("#q156").trigger("click");

// })

jQuery.validator.addMethod("russianPhones", function(value, element) {
    return this.optional(element) || /^7\d{10}(, 7\d{10}){0,9}$/.test(value);
}, "Номера должены идти через запятую и начинаться с '7' и содержать 11 цифр и их не должно быть больше 10");

jQuery.validator.addMethod("russianPhone", function(value, element) {
    return this.optional(element) || /^7\d{10}$/.test(value);
}, "Номер должен начинаться с '7' и содержать 11 цифр");

jQuery.validator.addMethod("distrName", function(value, element) {
    return this.optional(element) || /^([A-z]|\d|\-|\_)+$/.test(value);
}, "Имя не должно содержать ничего кроме латинских букв и цифр!");

$(function () {
    $('#datetimepicker1').datetimepicker({
        locale: 'ru',
        format: 'YYYY-MM-DD HH:mm:ss'
    });
});
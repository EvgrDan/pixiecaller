$().ready(function() {
    $("#newphone").validate({
        rules: {
            num: {
                required: true,
                russianPhone: true
            }
        },
        messages: {
        }
    });
    
    $('div[name="codelbl"]').addClass("hide");
    $('input[name="phonecode"]').addClass("hide");
    $('div[name="submitphonebtn"]').addClass("hide");
    
    var tn = $("div[name='subphonebtn']")
    tn.click(function() {
        $('div[name="codelbl"]').removeClass("hide");
        $('input[name="phonecode"]').removeClass("hide");
        $('div[name="submitphonebtn"]').removeClass("hide");
        $.ajax({
          type: "POST",
          url: "/admin/newphone",
          data: "phone="+$('input[name="num"]').val(),
          success: function(data){
            swal( data );
          }
        });
    });
    var tn1 = $("div[name='submitphonebtn']") 
    tn1.click(function() {
        $('div[name="codelbl"]').removeClass("hide");
        $('input[name="phonecode"]').removeClass("hide");
        $('button[name="submitphonebtn"]').removeClass("hide");
        $.ajax({
          type: "POST",
          url: "/admin/acceptphone",
          data: "phone="+$('input[name="num"]').val()+"&code="+$('input[name="phonecode"]').val(),
          success: function(data){
            swal( data );
          }
        });
    });
    
})

jQuery.validator.addMethod("russianPhone", function(value, element) {
    return this.optional(element) || /^7\d{10}$/.test(value);
}, "Номер должен начинаться с '7' и содержать 11 цифр");
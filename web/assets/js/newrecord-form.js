$(document).ready(function() {

    $("#newrecord").validate({
        rules: {
            callNumber: {
                required: true,
                russianPhone: true
            }
        },
        messages: {
        }            
    });

    var tst3 = $("label[name='fileclick1']");
    var tst4 = $("label[name='textclick1']");
    var tst5 = $("label[name='dictclick1']");

    var initT3 = tst3.hasClass("active");
    if(initT3){
        $('input[name="recordFile"]').removeClass("hide");
        $('textarea[name="recordText"]').addClass("hide");
        $('input[name="callNumber"]').addClass("hide");
        $('button[name="createbtn"]').removeClass("hide");
        $('button[name="callbtn"]').addClass("hide");
        tst3.removeClass("btn-grey");
        tst3.addClass("btn-red");
        tst4.addClass("btn-grey");
        tst4.removeClass("btn-red");
        tst5.addClass("btn-grey");
        tst5.removeClass("btn-red");
        $("#q158").prop('checked', false);
        $("#q157").prop('checked', true);
        $("#q156").prop('checked', false);
    }
    tst3.click(function() {
        $('input[name="recordFile"]').removeClass("hide");
        $('textarea[name="recordText"]').addClass("hide");
        $('input[name="callNumber"]').addClass("hide");
        $('button[name="createbtn"]').removeClass("hide");
        $('button[name="callbtn"]').addClass("hide");
        tst3.removeClass("btn-grey");
        tst3.addClass("btn-red");
        tst4.addClass("btn-grey");
        tst4.removeClass("btn-red");
        tst5.addClass("btn-grey");
        tst5.removeClass("btn-red");
        $("#q158").prop('checked', false);
        $("#q157").prop('checked', true);
        $("#q156").prop('checked', false);
    });
    
    var initT4 = tst4.hasClass("active");
    if(initT4){
        $('input[name="recordFile"]').addClass("hide");
        $('textarea[name="recordText"]').removeClass("hide");
        $('input[name="callNumber"]').addClass("hide");
        $('button[name="createbtn"]').removeClass("hide");
        $('button[name="callbtn"]').addClass("hide");
        tst5.addClass("btn-grey");
        tst5.removeClass("btn-red");
        tst4.removeClass("btn-grey");
        tst4.addClass("btn-red");
        tst3.addClass("btn-grey");
        tst3.removeClass("btn-red");
        $("#q158").prop('checked', false);
        $("#q157").prop('checked', false);
        $("#q156").prop('checked', true);
    }
    tst4.click(function() {
        $('input[name="recordFile"]').addClass("hide");
        $('textarea[name="recordText"]').removeClass("hide");
        $('input[name="callNumber"]').addClass("hide");
        $('button[name="createbtn"]').removeClass("hide");
        $('button[name="callbtn"]').addClass("hide");        
        tst5.addClass("btn-grey");
        tst5.removeClass("btn-red");
        tst4.removeClass("btn-grey");
        tst4.addClass("btn-red");
        tst3.addClass("btn-grey");
        tst3.removeClass("btn-red");
        $("#q158").prop('checked', false);    
        $("#q157").prop('checked', false);
        $("#q156").prop('checked', true);
    });

    var initT5 = tst5.hasClass("active");
    if(initT5){
        $('textarea[name="recordText"]').addClass("hide");
        $('input[name="recordFile"]').addClass("hide");
        $('input[name="callNumber"]').removeClass("hide");
        $('button[name="createbtn"]').addClass("hide");
        $('button[name="callbtn"]').removeClass("hide");  
        tst5.removeClass("btn-grey");
        tst5.addClass("btn-red");
        tst4.addClass("btn-grey");
        tst4.removeClass("btn-red");
        tst3.addClass("btn-grey");
        tst3.removeClass("btn-red");
        $("#q158").prop('checked', true);
        $("#q157").prop('checked', false);
        $("#q156").prop('checked', false);
    }
    tst5.click(function() {
        $('textarea[name="recordText"]').addClass("hide");
        $('input[name="recordFile"]').addClass("hide");
        $('input[name="callNumber"]').removeClass("hide");
        $('button[name="createbtn"]').addClass("hide");
        $('button[name="callbtn"]').removeClass("hide");  
        tst5.removeClass("btn-grey");
        tst5.addClass("btn-red");
        tst4.addClass("btn-grey");
        tst4.removeClass("btn-red");
        tst3.addClass("btn-grey");
        tst3.removeClass("btn-red");
        $("#q158").prop('checked', true);
        $("#q157").prop('checked', false);
        $("#q156").prop('checked', false);
    });
});

jQuery.validator.addMethod("russianPhone", function(value, element) {
    return this.optional(element) || /^7\d{10}$/.test(value);
}, "Номер должен начинаться с '7' и содержать 11 цифр");
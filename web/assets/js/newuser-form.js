$().ready(function() {
    $("#newuser").validate({
        rules: {
            Login: {
                required: true,
                email: true
            },
            Phone: {
                required: true,
                russianPhone: true
            }
        },
        messages: {
        }
    });
    var tn1 = $("#fpass");
    tn1.click(function() {
        if($('#newuser').valid()){
            $.ajax({
                type: "POST",
                url: "/admin/forgetpassword",
                data: "Login="+$('input[name="Login"]').val(),
                success: function(data){
                    swal( data );
                }
            });
        }
    });
})

jQuery.validator.addMethod("russianPhone", function(value, element) {
    return this.optional(element) || /^7\d{10}$/.test(value);
}, "Номер должен начинаться с '7' и содержать 11 цифр");